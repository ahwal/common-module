<?php

use Illuminate\Support\Facades\Route;
use Modules\Common\Http\Livewire\Admin\ChoicesPage;
use Modules\Common\Http\Livewire\Admin\ModulesPage;
use Modules\Common\Http\Livewire\Admin\SettingsPage;

Route::middleware(['auth:sanctum', 'verified', 'module:setting'])
    ->prefix('admin/setting')
    ->as('admin.setting.')
    ->group(function () {

        Route::get('/', SettingsPage::class)
            ->name('index');

        Route::get('/modules', ModulesPage::class)
            ->name('modules');

        Route::get('/choices', ChoicesPage::class)
            ->name('choices');
    });
