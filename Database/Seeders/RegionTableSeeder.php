<?php

namespace Modules\Common\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->truncate();

        DB::table('regions')->insert(array (

            array (
                'code' => '11',
                'name' => 'ACEH',
            ),

            array (
                'code' => '11.01',
                'name' => 'KAB. ACEH SELATAN',
            ),

            array (
                'code' => '11.01.01',
                'name' => 'Bakongan',
            ),

            array (
                'code' => '11.01.02',
                'name' => 'Kluet Utara',
            ),

            array (
                'code' => '11.01.03',
                'name' => 'Kluet Selatan',
            ),

            array (
                'code' => '11.01.04',
                'name' => 'Labuhan Haji',
            ),

            array (
                'code' => '11.01.05',
                'name' => 'Meukek',
            ),

            array (
                'code' => '11.01.06',
                'name' => 'Samadua',
            ),

            array (
                'code' => '11.01.07',
                'name' => 'Sawang',
            ),

            array (
                'code' => '11.01.08',
                'name' => 'Tapaktuan',
            ),

            array (
                'code' => '11.01.09',
                'name' => 'Trumon',
            ),

            array (
                'code' => '11.01.10',
                'name' => 'Pasi Raja',
            ),

            array (
                'code' => '11.01.11',
                'name' => 'Labuhan Haji Timur',
            ),

            array (
                'code' => '11.01.12',
                'name' => 'Labuhan Haji Barat',
            ),

            array (
                'code' => '11.01.13',
                'name' => 'Kluet Tengah',
            ),

            array (
                'code' => '11.01.14',
                'name' => 'Kluet Timur',
            ),

            array (
                'code' => '11.01.15',
                'name' => 'Bakongan Timur',
            ),

            array (
                'code' => '11.01.16',
                'name' => 'Trumon Timur',
            ),

            array (
                'code' => '11.01.17',
                'name' => 'Kota Bahagia',
            ),

            array (
                'code' => '11.01.18',
                'name' => 'Trumon Tengah',
            ),

            array (
                'code' => '11.02',
                'name' => 'KAB. ACEH TENGGARA',
            ),

            array (
                'code' => '11.02.01',
                'name' => 'Lawe Alas',
            ),

            array (
                'code' => '11.02.02',
                'name' => 'Lawe Sigala-Gala',
            ),

            array (
                'code' => '11.02.03',
                'name' => 'Bambel',
            ),

            array (
                'code' => '11.02.04',
                'name' => 'Babussalam',
            ),

            array (
                'code' => '11.02.05',
                'name' => 'Badar',
            ),

            array (
                'code' => '11.02.06',
                'name' => 'Babul Makmur',
            ),

            array (
                'code' => '11.02.07',
                'name' => 'Darul Hasanah',
            ),

            array (
                'code' => '11.02.08',
                'name' => 'Lawe Bulan',
            ),

            array (
                'code' => '11.02.09',
                'name' => 'Bukit Tusam',
            ),

            array (
                'code' => '11.02.10',
                'name' => 'Semadam',
            ),

            array (
                'code' => '11.02.11',
                'name' => 'Babul Rahmah',
            ),

            array (
                'code' => '11.02.12',
                'name' => 'Ketambe',
            ),

            array (
                'code' => '11.02.13',
                'name' => 'Deleng Pokhkisen',
            ),

            array (
                'code' => '11.02.14',
                'name' => 'Lawe Sumur',
            ),

            array (
                'code' => '11.02.15',
                'name' => 'Tanoh Alas',
            ),

            array (
                'code' => '11.02.16',
                'name' => 'Leuser',
            ),

            array (
                'code' => '11.03',
                'name' => 'KAB. ACEH TIMUR',
            ),

            array (
                'code' => '11.03.01',
                'name' => 'Darul Aman',
            ),

            array (
                'code' => '11.03.02',
                'name' => 'Julok',
            ),

            array (
                'code' => '11.03.03',
                'name' => 'Idi Rayeuk',
            ),

            array (
                'code' => '11.03.04',
                'name' => 'Birem Bayeun',
            ),

            array (
                'code' => '11.03.05',
                'name' => 'Serbajadi',
            ),

            array (
                'code' => '11.03.06',
                'name' => 'Nurussalam',
            ),

            array (
                'code' => '11.03.07',
                'name' => 'Peureulak',
            ),

            array (
                'code' => '11.03.08',
                'name' => 'Rantau Selamat',
            ),

            array (
                'code' => '11.03.09',
                'name' => 'Simpang Ulim',
            ),

            array (
                'code' => '11.03.10',
                'name' => 'Rantau Peureulak',
            ),

            array (
                'code' => '11.03.11',
                'name' => 'Pante Bidari',
            ),

            array (
                'code' => '11.03.12',
                'name' => 'Madat',
            ),

            array (
                'code' => '11.03.13',
                'name' => 'Indra Makmu',
            ),

            array (
                'code' => '11.03.14',
                'name' => 'Idi Tunong',
            ),

            array (
                'code' => '11.03.15',
                'name' => 'Banda Alam',
            ),

            array (
                'code' => '11.03.16',
                'name' => 'Peudawa',
            ),

            array (
                'code' => '11.03.17',
                'name' => 'Peureulak Timur',
            ),

            array (
                'code' => '11.03.18',
                'name' => 'Peureulak Barat',
            ),

            array (
                'code' => '11.03.19',
                'name' => 'Sungai Raya',
            ),

            array (
                'code' => '11.03.20',
                'name' => 'Simpang Jernih',
            ),

            array (
                'code' => '11.03.21',
                'name' => 'Darul Ihsan',
            ),

            array (
                'code' => '11.03.22',
                'name' => 'Darul Falah',
            ),

            array (
                'code' => '11.03.23',
                'name' => 'Idi Timur',
            ),

            array (
                'code' => '11.03.24',
                'name' => 'Peunaron',
            ),

            array (
                'code' => '11.04',
                'name' => 'KAB. ACEH TENGAH',
            ),

            array (
                'code' => '11.04.01',
                'name' => 'Linge',
            ),

            array (
                'code' => '11.04.02',
                'name' => 'Silih Nara',
            ),

            array (
                'code' => '11.04.03',
                'name' => 'Bebesen',
            ),

            array (
                'code' => '11.04.07',
                'name' => 'Pegasing',
            ),

            array (
                'code' => '11.04.08',
                'name' => 'Bintang',
            ),

            array (
                'code' => '11.04.10',
                'name' => 'Ketol',
            ),

            array (
                'code' => '11.04.11',
                'name' => 'Kebayakan',
            ),

            array (
                'code' => '11.04.12',
                'name' => 'Kute Panang',
            ),

            array (
                'code' => '11.04.13',
                'name' => 'Celala',
            ),

            array (
                'code' => '11.04.17',
                'name' => 'Laut Tawar',
            ),

            array (
                'code' => '11.04.18',
                'name' => 'Atu Lintang',
            ),

            array (
                'code' => '11.04.19',
                'name' => 'Jagong Jeget',
            ),

            array (
                'code' => '11.04.20',
                'name' => 'Bies',
            ),

            array (
                'code' => '11.04.21',
                'name' => 'Rusip Antara',
            ),

            array (
                'code' => '11.05',
                'name' => 'KAB. ACEH BARAT',
            ),

            array (
                'code' => '11.05.01',
                'name' => 'Johan Pahwalan',
            ),

            array (
                'code' => '11.05.02',
                'name' => 'Kaway XVI',
            ),

            array (
                'code' => '11.05.03',
                'name' => 'Sungai Mas',
            ),

            array (
                'code' => '11.05.04',
                'name' => 'Woyla',
            ),

            array (
                'code' => '11.05.05',
                'name' => 'Samatiga',
            ),

            array (
                'code' => '11.05.06',
                'name' => 'Bubon',
            ),

            array (
                'code' => '11.05.07',
                'name' => 'Arongan Lambalek',
            ),

            array (
                'code' => '11.05.08',
                'name' => 'Pante Ceureumen',
            ),

            array (
                'code' => '11.05.09',
                'name' => 'Meureubo',
            ),

            array (
                'code' => '11.05.10',
                'name' => 'Woyla Barat',
            ),

            array (
                'code' => '11.05.11',
                'name' => 'Woyla Timur',
            ),

            array (
                'code' => '11.05.12',
                'name' => 'Panton Reu',
            ),

            array (
                'code' => '11.06',
                'name' => 'KAB. ACEH BESAR',
            ),

            array (
                'code' => '11.06.01',
                'name' => 'Lhoong',
            ),

            array (
                'code' => '11.06.02',
                'name' => 'Lhoknga',
            ),

            array (
                'code' => '11.06.03',
                'name' => 'Indrapuri',
            ),

            array (
                'code' => '11.06.04',
                'name' => 'Seulimeum',
            ),

            array (
                'code' => '11.06.05',
                'name' => 'Montasik',
            ),

            array (
                'code' => '11.06.06',
                'name' => 'Sukamakmur',
            ),

            array (
                'code' => '11.06.07',
                'name' => 'Darul Imarah',
            ),

            array (
                'code' => '11.06.08',
                'name' => 'Peukan Bada',
            ),

            array (
                'code' => '11.06.09',
                'name' => 'Mesjid Raya',
            ),

            array (
                'code' => '11.06.10',
                'name' => 'Ingin Jaya',
            ),

            array (
                'code' => '11.06.11',
                'name' => 'Kuta Baro',
            ),

            array (
                'code' => '11.06.12',
                'name' => 'Darussalam',
            ),

            array (
                'code' => '11.06.13',
                'name' => 'Pulo Aceh',
            ),

            array (
                'code' => '11.06.14',
                'name' => 'Lembah Seulawah',
            ),

            array (
                'code' => '11.06.15',
                'name' => 'Kota Jantho',
            ),

            array (
                'code' => '11.06.16',
                'name' => 'Kota Cot Glie',
            ),

            array (
                'code' => '11.06.17',
                'name' => 'Kuta Malaka',
            ),

            array (
                'code' => '11.06.18',
                'name' => 'Simpang Tiga',
            ),

            array (
                'code' => '11.06.19',
                'name' => 'Darul Kamal',
            ),

            array (
                'code' => '11.06.20',
                'name' => 'Baitussalam',
            ),

            array (
                'code' => '11.06.21',
                'name' => 'Krueng Barona Jaya',
            ),

            array (
                'code' => '11.06.22',
                'name' => 'Leupung',
            ),

            array (
                'code' => '11.06.23',
                'name' => 'Blang Bintang',
            ),

            array (
                'code' => '11.07',
                'name' => 'KAB. PIDIE',
            ),

            array (
                'code' => '11.07.03',
                'name' => 'Batee',
            ),

            array (
                'code' => '11.07.04',
                'name' => 'Delima',
            ),

            array (
                'code' => '11.07.05',
                'name' => 'Geumpang',
            ),

            array (
                'code' => '11.07.06',
                'name' => 'Geulumpang Tiga',
            ),

            array (
                'code' => '11.07.07',
                'name' => 'Indra Jaya',
            ),

            array (
                'code' => '11.07.08',
                'name' => 'Kembang Tanjong',
            ),

            array (
                'code' => '11.07.09',
                'name' => 'Kota Sigli',
            ),

            array (
                'code' => '11.07.11',
                'name' => 'Mila',
            ),

            array (
                'code' => '11.07.12',
                'name' => 'Muara Tiga',
            ),

            array (
                'code' => '11.07.13',
                'name' => 'Mutiara',
            ),

            array (
                'code' => '11.07.14',
                'name' => 'Padang Tiji',
            ),

            array (
                'code' => '11.07.15',
                'name' => 'Peukan Baro',
            ),

            array (
                'code' => '11.07.16',
                'name' => 'Pidie',
            ),

            array (
                'code' => '11.07.17',
                'name' => 'Sakti',
            ),

            array (
                'code' => '11.07.18',
                'name' => 'Simpang Tiga',
            ),

            array (
                'code' => '11.07.19',
                'name' => 'Tangse',
            ),

            array (
                'code' => '11.07.21',
                'name' => 'Tiro/Truseb',
            ),

            array (
                'code' => '11.07.22',
                'name' => 'Keumala',
            ),

            array (
                'code' => '11.07.24',
                'name' => 'Mutiara Timur',
            ),

            array (
                'code' => '11.07.25',
                'name' => 'Grong-grong',
            ),

            array (
                'code' => '11.07.27',
                'name' => 'Mane',
            ),

            array (
                'code' => '11.07.29',
                'name' => 'Glumpang Baro',
            ),

            array (
                'code' => '11.07.31',
                'name' => 'Titeue',
            ),

            array (
                'code' => '11.08',
                'name' => 'KAB. ACEH UTARA',
            ),

            array (
                'code' => '11.08.01',
                'name' => 'Baktiya',
            ),

            array (
                'code' => '11.08.02',
                'name' => 'Dewantara',
            ),

            array (
                'code' => '11.08.03',
                'name' => 'Kuta Makmur',
            ),

            array (
                'code' => '11.08.04',
                'name' => 'Lhoksukon',
            ),

            array (
                'code' => '11.08.05',
                'name' => 'Matangkuli',
            ),

            array (
                'code' => '11.08.06',
                'name' => 'Muara Batu',
            ),

            array (
                'code' => '11.08.07',
                'name' => 'Meurah Mulia',
            ),

            array (
                'code' => '11.08.08',
                'name' => 'Samudera',
            ),

            array (
                'code' => '11.08.09',
                'name' => 'Seunuddon',
            ),

            array (
                'code' => '11.08.10',
                'name' => 'Syamtalira Aron',
            ),

            array (
                'code' => '11.08.11',
                'name' => 'Syamtalira Bayu',
            ),

            array (
                'code' => '11.08.12',
                'name' => 'Tanah Luas',
            ),

            array (
                'code' => '11.08.13',
                'name' => 'Tanah Pasir',
            ),

            array (
                'code' => '11.08.14',
                'name' => 'T. Jambo Aye  47',
            ),

            array (
                'code' => '11.08.15',
                'name' => 'Sawang',
            ),

            array (
                'code' => '11.08.16',
                'name' => 'Nisam',
            ),

            array (
                'code' => '11.08.17',
                'name' => 'Cot Girek',
            ),

            array (
                'code' => '11.08.18',
                'name' => 'Langkahan',
            ),

            array (
                'code' => '11.08.19',
                'name' => 'Baktiya Barat',
            ),

            array (
                'code' => '11.08.20',
                'name' => 'Paya Bakong',
            ),

            array (
                'code' => '11.08.21',
                'name' => 'Nibong',
            ),

            array (
                'code' => '11.08.22',
                'name' => 'Simpang Kramat',
            ),

            array (
                'code' => '11.08.23',
                'name' => 'Lapang',
            ),

            array (
                'code' => '11.08.24',
                'name' => 'Pirak Timur',
            ),

            array (
                'code' => '11.08.25',
                'name' => 'Geuredong Pase',
            ),

            array (
                'code' => '11.08.26',
                'name' => 'Banda Baro',
            ),

            array (
                'code' => '11.08.27',
                'name' => 'Nisam Antara',
            ),

            array (
                'code' => '11.09',
                'name' => 'KAB. SIMEULUE',
            ),

            array (
                'code' => '11.09.01',
                'name' => 'Simeulue Tengah',
            ),

            array (
                'code' => '11.09.02',
                'name' => 'Salang',
            ),

            array (
                'code' => '11.09.03',
                'name' => 'Teupah Barat',
            ),

            array (
                'code' => '11.09.04',
                'name' => 'Simeulue Timur',
            ),

            array (
                'code' => '11.09.05',
                'name' => 'Teluk Dalam',
            ),

            array (
                'code' => '11.09.06',
                'name' => 'Simeulue Barat',
            ),

            array (
                'code' => '11.09.07',
                'name' => 'Teupah Selatan',
            ),

            array (
                'code' => '11.09.08',
                'name' => 'Alapan',
            ),

            array (
                'code' => '11.09.09',
                'name' => 'Teupah Tengah',
            ),

            array (
                'code' => '11.09.10',
                'name' => 'Simeulue Cut',
            ),

            array (
                'code' => '11.10',
                'name' => 'KAB. ACEH SINGKIL',
            ),

            array (
                'code' => '11.10.01',
                'name' => 'Pulau Banyak',
            ),

            array (
                'code' => '11.10.02',
                'name' => 'Simpang Kanan',
            ),

            array (
                'code' => '11.10.04',
                'name' => 'Singkil',
            ),

            array (
                'code' => '11.10.06',
                'name' => 'Gunung Meriah',
            ),

            array (
                'code' => '11.10.09',
                'name' => 'Kota Baharu',
            ),

            array (
                'code' => '11.10.10',
                'name' => 'Singkil Utara',
            ),

            array (
                'code' => '11.10.11',
                'name' => 'Danau Paris',
            ),

            array (
                'code' => '11.10.12',
                'name' => 'Suro Makmur',
            ),

            array (
                'code' => '11.10.13',
                'name' => 'Singkohor',
            ),

            array (
                'code' => '11.10.14',
                'name' => 'Kuala Baru',
            ),

            array (
                'code' => '11.10.16',
                'name' => 'Pulau Banyak Barat',
            ),

            array (
                'code' => '11.11',
                'name' => 'KAB. BIREUEN',
            ),

            array (
                'code' => '11.11.01',
                'name' => 'Samalanga',
            ),

            array (
                'code' => '11.11.02',
                'name' => 'Jeunieb',
            ),

            array (
                'code' => '11.11.03',
                'name' => 'Peudada',
            ),

            array (
                'code' => '11.11.04',
                'name' => 'Jeumpa',
            ),

            array (
                'code' => '11.11.05',
                'name' => 'Peusangan',
            ),

            array (
                'code' => '11.11.06',
                'name' => 'Makmur',
            ),

            array (
                'code' => '11.11.07',
                'name' => 'Gandapura',
            ),

            array (
                'code' => '11.11.08',
                'name' => 'Pandrah',
            ),

            array (
                'code' => '11.11.09',
                'name' => 'Juli',
            ),

            array (
                'code' => '11.11.10',
                'name' => 'Jangka',
            ),

            array (
                'code' => '11.11.11',
                'name' => 'Simpang Mamplam',
            ),

            array (
                'code' => '11.11.12',
                'name' => 'Peulimbang',
            ),

            array (
                'code' => '11.11.13',
                'name' => 'Kota Juang',
            ),

            array (
                'code' => '11.11.14',
                'name' => 'Kuala',
            ),

            array (
                'code' => '11.11.15',
                'name' => 'Peusangan Siblah Krueng',
            ),

            array (
                'code' => '11.11.16',
                'name' => 'Peusangan Selatan',
            ),

            array (
                'code' => '11.11.17',
                'name' => 'Kuta Blang',
            ),

            array (
                'code' => '11.12',
                'name' => 'KAB. ACEH BARAT DAYA',
            ),

            array (
                'code' => '11.12.01',
                'name' => 'Blang Pidie',
            ),

            array (
                'code' => '11.12.02',
                'name' => 'Tangan-Tangan',
            ),

            array (
                'code' => '11.12.03',
                'name' => 'Manggeng',
            ),

            array (
                'code' => '11.12.04',
                'name' => 'Susoh',
            ),

            array (
                'code' => '11.12.05',
                'name' => 'Kuala Batee',
            ),

            array (
                'code' => '11.12.06',
                'name' => 'Babah Rot',
            ),

            array (
                'code' => '11.12.07',
                'name' => 'Setia',
            ),

            array (
                'code' => '11.12.08',
                'name' => 'Jeumpa',
            ),

            array (
                'code' => '11.12.09',
                'name' => 'Lembah Sabil',
            ),

            array (
                'code' => '11.13',
                'name' => 'KAB. GAYO LUES',
            ),

            array (
                'code' => '11.13.01',
                'name' => 'Blangkejeren',
            ),

            array (
                'code' => '11.13.02',
                'name' => 'Kutapanjang',
            ),

            array (
                'code' => '11.13.03',
                'name' => 'Rikit Gaib',
            ),

            array (
                'code' => '11.13.04',
                'name' => 'Terangun',
            ),

            array (
                'code' => '11.13.05',
                'name' => 'Pining',
            ),

            array (
                'code' => '11.13.06',
                'name' => 'Blangpegayon',
            ),

            array (
                'code' => '11.13.07',
                'name' => 'Puteri Betung',
            ),

            array (
                'code' => '11.13.08',
                'name' => 'Dabun Gelang',
            ),

            array (
                'code' => '11.13.09',
                'name' => 'Blangjerango',
            ),

            array (
                'code' => '11.13.10',
                'name' => 'Teripe Jaya',
            ),

            array (
                'code' => '11.13.11',
                'name' => 'Pantan Cuaca',
            ),

            array (
                'code' => '11.14',
                'name' => 'KAB. ACEH JAYA',
            ),

            array (
                'code' => '11.14.01',
                'name' => 'Teunom',
            ),

            array (
                'code' => '11.14.02',
                'name' => 'Krueng Sabee',
            ),

            array (
                'code' => '11.14.03',
                'name' => 'Setia Bhakti',
            ),

            array (
                'code' => '11.14.04',
                'name' => 'Sampoiniet',
            ),

            array (
                'code' => '11.14.05',
                'name' => 'Jaya',
            ),

            array (
                'code' => '11.14.06',
                'name' => 'Panga',
            ),

            array (
                'code' => '11.14.07',
                'name' => 'Indra Jaya',
            ),

            array (
                'code' => '11.14.08',
                'name' => 'Darul Hikmah',
            ),

            array (
                'code' => '11.14.09',
                'name' => 'Pasie Raya',
            ),

            array (
                'code' => '11.15',
                'name' => 'KAB. NAGAN RAYA',
            ),

            array (
                'code' => '11.15.01',
                'name' => 'Kuala',
            ),

            array (
                'code' => '11.15.02',
                'name' => 'Seunagan',
            ),

            array (
                'code' => '11.15.03',
                'name' => 'Seunagan Timur',
            ),

            array (
                'code' => '11.15.04',
                'name' => 'Beutong',
            ),

            array (
                'code' => '11.15.05',
                'name' => 'Darul Makmur',
            ),

            array (
                'code' => '11.15.06',
                'name' => 'Suka Makmue',
            ),

            array (
                'code' => '11.15.07',
                'name' => 'Kuala Pesisir',
            ),

            array (
                'code' => '11.15.08',
                'name' => 'Tadu Raya',
            ),

            array (
                'code' => '11.15.09',
                'name' => 'Tripa Makmur',
            ),

            array (
                'code' => '11.15.10',
                'name' => 'Beutong Ateuh Banggalang',
            ),

            array (
                'code' => '11.16',
                'name' => 'KAB. ACEH TAMIANG',
            ),

            array (
                'code' => '11.16.01',
                'name' => 'Manyak Payed',
            ),

            array (
                'code' => '11.16.02',
                'name' => 'Bendahara',
            ),

            array (
                'code' => '11.16.03',
                'name' => 'Karang Baru',
            ),

            array (
                'code' => '11.16.04',
                'name' => 'Seruway',
            ),

            array (
                'code' => '11.16.05',
                'name' => 'Kota Kualasinpang',
            ),

            array (
                'code' => '11.16.06',
                'name' => 'Kejuruan Muda',
            ),

            array (
                'code' => '11.16.07',
                'name' => 'Tamiang Hulu',
            ),

            array (
                'code' => '11.16.08',
                'name' => 'Rantau',
            ),

            array (
                'code' => '11.16.09',
                'name' => 'Banda Mulia',
            ),

            array (
                'code' => '11.16.10',
                'name' => 'Bandar Pusaka',
            ),

            array (
                'code' => '11.16.11',
                'name' => 'Tenggulun',
            ),

            array (
                'code' => '11.16.12',
                'name' => 'Sekerak',
            ),

            array (
                'code' => '11.17',
                'name' => 'KAB. BENER MERIAH',
            ),

            array (
                'code' => '11.17.01',
                'name' => 'Pintu Rime Gayo',
            ),

            array (
                'code' => '11.17.02',
                'name' => 'Permata',
            ),

            array (
                'code' => '11.17.03',
                'name' => 'Syiah Utama',
            ),

            array (
                'code' => '11.17.04',
                'name' => 'Bandar',
            ),

            array (
                'code' => '11.17.05',
                'name' => 'Bukit',
            ),

            array (
                'code' => '11.17.06',
                'name' => 'Wih Pesam',
            ),

            array (
                'code' => '11.17.07',
                'name' => 'Timang gajah',
            ),

            array (
                'code' => '11.17.08',
                'name' => 'Bener Kelipah',
            ),

            array (
                'code' => '11.17.09',
                'name' => 'Mesidah',
            ),

            array (
                'code' => '11.17.10',
                'name' => 'Gajah Putih',
            ),

            array (
                'code' => '11.18',
                'name' => 'KAB. PIDIE JAYA',
            ),

            array (
                'code' => '11.18.01',
                'name' => 'Meureudu',
            ),

            array (
                'code' => '11.18.02',
                'name' => 'Ulim',
            ),

            array (
                'code' => '11.18.03',
                'name' => 'Jangka Buaya',
            ),

            array (
                'code' => '11.18.04',
                'name' => 'Bandar Dua',
            ),

            array (
                'code' => '11.18.05',
                'name' => 'Meurah Dua',
            ),

            array (
                'code' => '11.18.06',
                'name' => 'Bandar Baru',
            ),

            array (
                'code' => '11.18.07',
                'name' => 'Panteraja',
            ),

            array (
                'code' => '11.18.08',
                'name' => 'Trienggadeng',
            ),

            array (
                'code' => '11.71',
                'name' => 'KOTA BANDA ACEH',
            ),

            array (
                'code' => '11.71.01',
                'name' => 'Baiturrahman',
            ),

            array (
                'code' => '11.71.02',
                'name' => 'Kuta Alam',
            ),

            array (
                'code' => '11.71.03',
                'name' => 'Meuraxa',
            ),

            array (
                'code' => '11.71.04',
                'name' => 'Syiah Kuala',
            ),

            array (
                'code' => '11.71.05',
                'name' => 'Lueng Bata',
            ),

            array (
                'code' => '11.71.06',
                'name' => 'Kuta Raja',
            ),

            array (
                'code' => '11.71.07',
                'name' => 'Banda Raya',
            ),

            array (
                'code' => '11.71.08',
                'name' => 'Jaya Baru',
            ),

            array (
                'code' => '11.71.09',
                'name' => 'Ulee Kareng',
            ),

            array (
                'code' => '11.72',
                'name' => 'KOTA SABANG',
            ),

            array (
                'code' => '11.72.01',
                'name' => 'Sukakarya',
            ),

            array (
                'code' => '11.72.02',
                'name' => 'Sukajaya',
            ),

            array (
                'code' => '11.73',
                'name' => 'KOTA LHOKSEUMAWE',
            ),

            array (
                'code' => '11.73.01',
                'name' => 'Muara Dua',
            ),

            array (
                'code' => '11.73.02',
                'name' => 'Banda Sakti',
            ),

            array (
                'code' => '11.73.03',
                'name' => 'Blang Mangat',
            ),

            array (
                'code' => '11.73.04',
                'name' => 'Muara Satu',
            ),

            array (
                'code' => '11.74',
                'name' => 'KOTA LANGSA',
            ),

            array (
                'code' => '11.74.01',
                'name' => 'Langsa Timur',
            ),

            array (
                'code' => '11.74.02',
                'name' => 'Langsa Barat',
            ),

            array (
                'code' => '11.74.03',
                'name' => 'Langsa Kota',
            ),

            array (
                'code' => '11.74.04',
                'name' => 'Langsa Lama',
            ),

            array (
                'code' => '11.74.05',
                'name' => 'Langsa Baro',
            ),

            array (
                'code' => '11.75',
                'name' => 'KOTA SUBULUSSALAM',
            ),

            array (
                'code' => '11.75.01',
                'name' => 'Simpang Kiri',
            ),

            array (
                'code' => '11.75.02',
                'name' => 'Penanggalan',
            ),

            array (
                'code' => '11.75.03',
                'name' => 'Rundeng',
            ),

            array (
                'code' => '11.75.04',
                'name' => 'Sultan Daulat',
            ),

            array (
                'code' => '11.75.05',
                'name' => 'Longkib',
            ),

            array (
                'code' => '12',
                'name' => 'SUMATERA UTARA',
            ),

            array (
                'code' => '12.01',
                'name' => 'KAB. TAPANULI TENGAH',
            ),

            array (
                'code' => '12.01.01',
                'name' => 'Barus',
            ),

            array (
                'code' => '12.01.02',
                'name' => 'Sorkam',
            ),

            array (
                'code' => '12.01.03',
                'name' => 'Pandan',
            ),

            array (
                'code' => '12.01.04',
                'name' => 'Pinangsori',
            ),

            array (
                'code' => '12.01.05',
                'name' => 'Manduamas',
            ),

            array (
                'code' => '12.01.06',
                'name' => 'Kolang',
            ),

            array (
                'code' => '12.01.07',
                'name' => 'Tapian Nauli',
            ),

            array (
                'code' => '12.01.08',
                'name' => 'Sibabangun',
            ),

            array (
                'code' => '12.01.09',
                'name' => 'Sosor Gadong',
            ),

            array (
                'code' => '12.01.10',
                'name' => 'Sorkam Barat',
            ),

            array (
                'code' => '12.01.11',
                'name' => 'Sirandorung',
            ),

            array (
                'code' => '12.01.12',
                'name' => 'Andam Dewi',
            ),

            array (
                'code' => '12.01.13',
                'name' => 'Sitahuis',
            ),

            array (
                'code' => '12.01.14',
                'name' => 'Tukka',
            ),

            array (
                'code' => '12.01.15',
                'name' => 'Badiri',
            ),

            array (
                'code' => '12.01.16',
                'name' => 'Pasaribu Tobing',
            ),

            array (
                'code' => '12.01.17',
                'name' => 'Barus Utara',
            ),

            array (
                'code' => '12.01.18',
                'name' => 'Suka Bangun',
            ),

            array (
                'code' => '12.01.19',
                'name' => 'Lumut',
            ),

            array (
                'code' => '12.01.20',
                'name' => 'Sarudik',
            ),

            array (
                'code' => '12.02',
                'name' => 'KAB. TAPANULI UTARA',
            ),

            array (
                'code' => '12.02.01',
                'name' => 'Tarutung',
            ),

            array (
                'code' => '12.02.02',
                'name' => 'Siatas Barita',
            ),

            array (
                'code' => '12.02.03',
                'name' => 'Adian Koting',
            ),

            array (
                'code' => '12.02.04',
                'name' => 'Sipoholon',
            ),

            array (
                'code' => '12.02.05',
                'name' => 'Pahae Julu',
            ),

            array (
                'code' => '12.02.06',
                'name' => 'Pahae Jae',
            ),

            array (
                'code' => '12.02.07',
                'name' => 'Simangumban',
            ),

            array (
                'code' => '12.02.08',
                'name' => 'Purba Tua',
            ),

            array (
                'code' => '12.02.09',
                'name' => 'Siborong-Borong',
            ),

            array (
                'code' => '12.02.10',
                'name' => 'Pagaran',
            ),

            array (
                'code' => '12.02.11',
                'name' => 'Parmonangan',
            ),

            array (
                'code' => '12.02.12',
                'name' => 'Sipahutar',
            ),

            array (
                'code' => '12.02.13',
                'name' => 'Pangaribuan',
            ),

            array (
                'code' => '12.02.14',
                'name' => 'Garoga',
            ),

            array (
                'code' => '12.02.15',
                'name' => 'Muara',
            ),

            array (
                'code' => '12.03',
                'name' => 'KAB. TAPANULI SELATAN',
            ),

            array (
                'code' => '12.03.01',
                'name' => 'Angkola Barat',
            ),

            array (
                'code' => '12.03.02',
                'name' => 'Batang Toru',
            ),

            array (
                'code' => '12.03.03',
                'name' => 'Angkola Timur',
            ),

            array (
                'code' => '12.03.04',
                'name' => 'Sipirok',
            ),

            array (
                'code' => '12.03.05',
                'name' => 'Saipar Dolok Hole',
            ),

            array (
                'code' => '12.03.06',
                'name' => 'Angkola Selatan',
            ),

            array (
                'code' => '12.03.07',
                'name' => 'Batang Angkola',
            ),

            array (
                'code' => '12.03.14',
                'name' => 'Arse',
            ),

            array (
                'code' => '12.03.20',
                'name' => 'Marancar',
            ),

            array (
                'code' => '12.03.21',
                'name' => 'Sayur Matinggi',
            ),

            array (
                'code' => '12.03.22',
                'name' => 'Aek Bilah',
            ),

            array (
                'code' => '12.03.29',
                'name' => 'Muara Batang Toru',
            ),

            array (
                'code' => '12.03.30',
                'name' => 'Tano Tombangan Angkola',
            ),

            array (
                'code' => '12.03.31',
                'name' => 'Angkola Sangkunur',
            ),

            array (
                'code' => '12.04',
                'name' => 'KAB. NIAS',
            ),

            array (
                'code' => '12.04.05',
                'name' => 'Hiliduho',
            ),

            array (
                'code' => '12.04.06',
                'name' => 'Gido',
            ),

            array (
                'code' => '12.04.10',
                'name' => 'Idanogawo',
            ),

            array (
                'code' => '12.04.11',
                'name' => 'Bawolato',
            ),

            array (
                'code' => '12.04.20',
                'name' => 'Hiliserangkai',
            ),

            array (
                'code' => '12.04.21',
                'name' => 'Botomuzoi',
            ),

            array (
                'code' => '12.04.27',
                'name' => 'Ulugawo',
            ),

            array (
                'code' => '12.04.28',
                'name' => 'Ma\'u  11',
            ),

            array (
                'code' => '12.04.29',
                'name' => 'Somolo-molo',
            ),

            array (
                'code' => '12.04.35',
                'name' => 'Sogae\'adu  11',
            ),

            array (
                'code' => '12.05',
                'name' => 'KAB. LANGKAT',
            ),

            array (
                'code' => '12.05.01',
                'name' => 'Bahorok',
            ),

            array (
                'code' => '12.05.02',
                'name' => 'Salapian',
            ),

            array (
                'code' => '12.05.03',
                'name' => 'Kuala',
            ),

            array (
                'code' => '12.05.04',
                'name' => 'Sei Bingei',
            ),

            array (
                'code' => '12.05.05',
                'name' => 'Binjai',
            ),

            array (
                'code' => '12.05.06',
                'name' => 'Selesai',
            ),

            array (
                'code' => '12.05.07',
                'name' => 'Stabat',
            ),

            array (
                'code' => '12.05.08',
                'name' => 'Wampu',
            ),

            array (
                'code' => '12.05.09',
                'name' => 'Secanggang',
            ),

            array (
                'code' => '12.05.10',
                'name' => 'Hinai',
            ),

            array (
                'code' => '12.05.11',
                'name' => 'Tanjung Pura',
            ),

            array (
                'code' => '12.05.12',
                'name' => 'Padang Tualang',
            ),

            array (
                'code' => '12.05.13',
                'name' => 'Gebang',
            ),

            array (
                'code' => '12.05.14',
                'name' => 'Babalan',
            ),

            array (
                'code' => '12.05.15',
                'name' => 'Pangkalan Susu',
            ),

            array (
                'code' => '12.05.16',
                'name' => 'Besitang',
            ),

            array (
                'code' => '12.05.17',
                'name' => 'Sei Lepan',
            ),

            array (
                'code' => '12.05.18',
                'name' => 'Brandan Barat',
            ),

            array (
                'code' => '12.05.19',
                'name' => 'Batang Serangan',
            ),

            array (
                'code' => '12.05.20',
                'name' => 'Sawit Seberang',
            ),

            array (
                'code' => '12.05.21',
                'name' => 'Sirapit',
            ),

            array (
                'code' => '12.05.22',
                'name' => 'Kutambaru',
            ),

            array (
                'code' => '12.05.23',
                'name' => 'Pematang Jaya',
            ),

            array (
                'code' => '12.06',
                'name' => 'KAB. KARO',
            ),

            array (
                'code' => '12.06.01',
                'name' => 'Kabanjahe',
            ),

            array (
                'code' => '12.06.02',
                'name' => 'Berastagi',
            ),

            array (
                'code' => '12.06.03',
                'name' => 'Barusjahe',
            ),

            array (
                'code' => '12.06.04',
                'name' => 'Tigapanah',
            ),

            array (
                'code' => '12.06.05',
                'name' => 'Merek',
            ),

            array (
                'code' => '12.06.06',
                'name' => 'Munte',
            ),

            array (
                'code' => '12.06.07',
                'name' => 'Juhar',
            ),

            array (
                'code' => '12.06.08',
                'name' => 'Tigabinanga',
            ),

            array (
                'code' => '12.06.09',
                'name' => 'Laubaleng',
            ),

            array (
                'code' => '12.06.10',
                'name' => 'Mardingding',
            ),

            array (
                'code' => '12.06.11',
                'name' => 'Payung',
            ),

            array (
                'code' => '12.06.12',
                'name' => 'Simpang Empat',
            ),

            array (
                'code' => '12.06.13',
                'name' => 'Kutabuluh',
            ),

            array (
                'code' => '12.06.14',
                'name' => 'Dolat Rayat',
            ),

            array (
                'code' => '12.06.15',
                'name' => 'Merdeka',
            ),

            array (
                'code' => '12.06.16',
                'name' => 'Naman Teran',
            ),

            array (
                'code' => '12.06.17',
                'name' => 'Tiganderket',
            ),

            array (
                'code' => '12.07',
                'name' => 'KAB. DELI SERDANG',
            ),

            array (
                'code' => '12.07.01',
                'name' => 'Gunung Meriah',
            ),

            array (
                'code' => '12.07.02',
                'name' => 'Tanjung Morawa',
            ),

            array (
                'code' => '12.07.03',
                'name' => 'Sibolangit',
            ),

            array (
                'code' => '12.07.04',
                'name' => 'Kutalimbaru',
            ),

            array (
                'code' => '12.07.05',
                'name' => 'Pancur Batu',
            ),

            array (
                'code' => '12.07.06',
                'name' => 'Namorambe',
            ),

            array (
                'code' => '12.07.07',
                'name' => 'Sibiru-biru',
            ),

            array (
                'code' => '12.07.08',
                'name' => 'STM Hilir',
            ),

            array (
                'code' => '12.07.09',
                'name' => 'Bangun Purba',
            ),

            array (
                'code' => '12.07.19',
                'name' => 'Galang',
            ),

            array (
                'code' => '12.07.20',
                'name' => 'STM Hulu',
            ),

            array (
                'code' => '12.07.21',
                'name' => 'Patumbak',
            ),

            array (
                'code' => '12.07.22',
                'name' => 'Deli Tua',
            ),

            array (
                'code' => '12.07.23',
                'name' => 'Sunggal',
            ),

            array (
                'code' => '12.07.24',
                'name' => 'Hamparan Perak',
            ),

            array (
                'code' => '12.07.25',
                'name' => 'Labuhan Deli',
            ),

            array (
                'code' => '12.07.26',
                'name' => 'Percut Sei Tuan',
            ),

            array (
                'code' => '12.07.27',
                'name' => 'Batang Kuis',
            ),

            array (
                'code' => '12.07.28',
                'name' => 'Lubuk Pakam',
            ),

            array (
                'code' => '12.07.31',
                'name' => 'Pagar Merbau',
            ),

            array (
                'code' => '12.07.32',
                'name' => 'Pantai Labu',
            ),

            array (
                'code' => '12.07.33',
                'name' => 'Beringin',
            ),

            array (
                'code' => '12.08',
                'name' => 'KAB. SIMALUNGUN',
            ),

            array (
                'code' => '12.08.01',
                'name' => 'Siantar',
            ),

            array (
                'code' => '12.08.02',
                'name' => 'Gunung Malela',
            ),

            array (
                'code' => '12.08.03',
                'name' => 'Gunung Maligas',
            ),

            array (
                'code' => '12.08.04',
                'name' => 'Panei',
            ),

            array (
                'code' => '12.08.05',
                'name' => 'Panombeian Pane',
            ),

            array (
                'code' => '12.08.06',
                'name' => 'Jorlang Hataran',
            ),

            array (
                'code' => '12.08.07',
                'name' => 'Raya Kahean',
            ),

            array (
                'code' => '12.08.08',
                'name' => 'Bosar Maligas',
            ),

            array (
                'code' => '12.08.09',
                'name' => 'Sidamanik',
            ),

            array (
                'code' => '12.08.10',
                'name' => 'Pematang Sidamanik',
            ),

            array (
                'code' => '12.08.11',
                'name' => 'Tanah Jawa',
            ),

            array (
                'code' => '12.08.12',
                'name' => 'Hatonduhan',
            ),

            array (
                'code' => '12.08.13',
                'name' => 'Dolok Panribuan',
            ),

            array (
                'code' => '12.08.14',
                'name' => 'Purba',
            ),

            array (
                'code' => '12.08.15',
                'name' => 'Haranggaol Horison',
            ),

            array (
                'code' => '12.08.16',
                'name' => 'Girsang Sipangan Bolon',
            ),

            array (
                'code' => '12.08.17',
                'name' => 'Dolok Batu Nanggar',
            ),

            array (
                'code' => '12.08.18',
                'name' => 'Huta Bayu Raja',
            ),

            array (
                'code' => '12.08.19',
                'name' => 'Jawa Maraja Bah Jambi',
            ),

            array (
                'code' => '12.08.20',
                'name' => 'Dolok Pardamean',
            ),

            array (
                'code' => '12.08.21',
                'name' => 'Pematang Bandar',
            ),

            array (
                'code' => '12.08.22',
                'name' => 'Bandar Huluan',
            ),

            array (
                'code' => '12.08.23',
                'name' => 'Bandar',
            ),

            array (
                'code' => '12.08.24',
                'name' => 'Bandar Masilam',
            ),

            array (
                'code' => '12.08.25',
                'name' => 'Silimakuta',
            ),

            array (
                'code' => '12.08.26',
                'name' => 'Dolok Silau',
            ),

            array (
                'code' => '12.08.27',
                'name' => 'Silou Kahean',
            ),

            array (
                'code' => '12.08.28',
                'name' => 'Tapian Dolok',
            ),

            array (
                'code' => '12.08.29',
                'name' => 'Raya',
            ),

            array (
                'code' => '12.08.30',
                'name' => 'Ujung Padang',
            ),

            array (
                'code' => '12.08.31',
                'name' => 'Pamatang Silima Huta',
            ),

            array (
                'code' => '12.09',
                'name' => 'KAB. ASAHAN',
            ),

            array (
                'code' => '12.09.08',
                'name' => 'Meranti',
            ),

            array (
                'code' => '12.09.09',
                'name' => 'Air Joman',
            ),

            array (
                'code' => '12.09.10',
                'name' => 'Tanjung Balai',
            ),

            array (
                'code' => '12.09.11',
                'name' => 'Sei Kepayang',
            ),

            array (
                'code' => '12.09.12',
                'name' => 'Simpang Empat',
            ),

            array (
                'code' => '12.09.13',
                'name' => 'Air Batu',
            ),

            array (
                'code' => '12.09.14',
                'name' => 'Pulau Rakyat',
            ),

            array (
                'code' => '12.09.15',
                'name' => 'Bandar Pulau',
            ),

            array (
                'code' => '12.09.16',
                'name' => 'Buntu Pane',
            ),

            array (
                'code' => '12.09.17',
                'name' => 'Bandar Pasir Mandoge',
            ),

            array (
                'code' => '12.09.18',
                'name' => 'Aek Kuasan',
            ),

            array (
                'code' => '12.09.19',
                'name' => 'Kota Kisaran Barat',
            ),

            array (
                'code' => '12.09.20',
                'name' => 'Kota Kisaran Timur',
            ),

            array (
                'code' => '12.09.21',
                'name' => 'Aek Songsongan',
            ),

            array (
                'code' => '12.09.22',
                'name' => 'Rahunig',
            ),

            array (
                'code' => '12.09.23',
                'name' => 'Sei Dadap',
            ),

            array (
                'code' => '12.09.24',
                'name' => 'Sei Kepayang Barat',
            ),

            array (
                'code' => '12.09.25',
                'name' => 'Sei Kepayang Timur',
            ),

            array (
                'code' => '12.09.26',
                'name' => 'Tinggi Raja',
            ),

            array (
                'code' => '12.09.27',
                'name' => 'Setia Janji',
            ),

            array (
                'code' => '12.09.28',
                'name' => 'Silau Laut',
            ),

            array (
                'code' => '12.09.29',
                'name' => 'Rawang Panca Arga',
            ),

            array (
                'code' => '12.09.30',
                'name' => 'Pulo Bandring',
            ),

            array (
                'code' => '12.09.31',
                'name' => 'Teluk Dalam',
            ),

            array (
                'code' => '12.09.32',
                'name' => 'Aek Ledong',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '12.10',
                'name' => 'KAB. LABUHANBATU',
            ),

            array (
                'code' => '12.10.01',
                'name' => 'Rantau Utara',
            ),

            array (
                'code' => '12.10.02',
                'name' => 'Rantau Selatan',
            ),

            array (
                'code' => '12.10.07',
                'name' => 'Bilah Barat',
            ),

            array (
                'code' => '12.10.08',
                'name' => 'Bilah Hilir',
            ),

            array (
                'code' => '12.10.09',
                'name' => 'Bilah Hulu',
            ),

            array (
                'code' => '12.10.14',
                'name' => 'Pangkatan',
            ),

            array (
                'code' => '12.10.18',
                'name' => 'Panai Tengah',
            ),

            array (
                'code' => '12.10.19',
                'name' => 'Panai Hilir',
            ),

            array (
                'code' => '12.10.20',
                'name' => 'Panai Hulu',
            ),

            array (
                'code' => '12.11',
                'name' => 'KAB. DAIRI',
            ),

            array (
                'code' => '12.11.01',
                'name' => 'Sidikalang',
            ),

            array (
                'code' => '12.11.02',
                'name' => 'Sumbul',
            ),

            array (
                'code' => '12.11.03',
                'name' => 'Tigalingga',
            ),

            array (
                'code' => '12.11.04',
                'name' => 'Siempat Nempu',
            ),

            array (
                'code' => '12.11.05',
                'name' => 'Silima Pungga Pungga',
            ),

            array (
                'code' => '12.11.06',
                'name' => 'Tanah Pinem',
            ),

            array (
                'code' => '12.11.07',
                'name' => 'Siempat Nempu Hulu',
            ),

            array (
                'code' => '12.11.08',
                'name' => 'Siempat Nempu Hilir',
            ),

            array (
                'code' => '12.11.09',
                'name' => 'Pegagan Hilir',
            ),

            array (
                'code' => '12.11.10',
                'name' => 'Parbuluan',
            ),

            array (
                'code' => '12.11.11',
                'name' => 'Lae Parira',
            ),

            array (
                'code' => '12.11.12',
                'name' => 'Gunung Sitember',
            ),

            array (
                'code' => '12.11.13',
                'name' => 'Berampu',
            ),

            array (
                'code' => '12.11.14',
                'name' => 'Silahisabungan',
            ),

            array (
                'code' => '12.11.15',
                'name' => 'Sitinjo',
            ),

            array (
                'code' => '12.12',
                'name' => 'KAB. TOBA SAMOSIR',
            ),

            array (
                'code' => '12.12.01',
                'name' => 'Balige',
            ),

            array (
                'code' => '12.12.02',
                'name' => 'Laguboti',
            ),

            array (
                'code' => '12.12.03',
                'name' => 'Silaen',
            ),

            array (
                'code' => '12.12.04',
                'name' => 'Habinsaran',
            ),

            array (
                'code' => '12.12.05',
                'name' => 'Pintu Pohan Meranti',
            ),

            array (
                'code' => '12.12.06',
                'name' => 'Borbor',
            ),

            array (
                'code' => '12.12.07',
                'name' => 'Porsea',
            ),

            array (
                'code' => '12.12.08',
                'name' => 'Ajibata',
            ),

            array (
                'code' => '12.12.09',
                'name' => 'Lumban Julu',
            ),

            array (
                'code' => '12.12.10',
                'name' => 'Uluan',
            ),

            array (
                'code' => '12.12.19',
                'name' => 'Sigumpar',
            ),

            array (
                'code' => '12.12.20',
                'name' => 'Siantar Narumonda',
            ),

            array (
                'code' => '12.12.21',
                'name' => 'Nassau',
            ),

            array (
                'code' => '12.12.22',
                'name' => 'Tampahan',
            ),

            array (
                'code' => '12.12.23',
                'name' => 'Bonatua Lunasi',
            ),

            array (
                'code' => '12.12.24',
                'name' => 'Parmaksian',
            ),

            array (
                'code' => '12.13',
                'name' => 'KAB. MANDAILING NATAL',
            ),

            array (
                'code' => '12.13.01',
                'name' => 'Panyabungan',
            ),

            array (
                'code' => '12.13.02',
                'name' => 'Panyabungan Utara',
            ),

            array (
                'code' => '12.13.03',
                'name' => 'Panyabungan Timur',
            ),

            array (
                'code' => '12.13.04',
                'name' => 'Panyabungan Selatan',
            ),

            array (
                'code' => '12.13.05',
                'name' => 'Panyabungan Barat',
            ),

            array (
                'code' => '12.13.06',
                'name' => 'Siabu',
            ),

            array (
                'code' => '12.13.07',
                'name' => 'Bukit Malintang',
            ),

            array (
                'code' => '12.13.08',
                'name' => 'Kotanopan',
            ),

            array (
                'code' => '12.13.09',
                'name' => 'Lembah Sorik Marapi',
            ),

            array (
                'code' => '12.13.10',
                'name' => 'Tambangan',
            ),

            array (
                'code' => '12.13.11',
                'name' => 'Ulu Pungkut',
            ),

            array (
                'code' => '12.13.12',
                'name' => 'Muara Sipongi',
            ),

            array (
                'code' => '12.13.13',
                'name' => 'Batang Natal',
            ),

            array (
                'code' => '12.13.14',
                'name' => 'Lingga Bayu',
            ),

            array (
                'code' => '12.13.15',
                'name' => 'Batahan',
            ),

            array (
                'code' => '12.13.16',
                'name' => 'Natal',
            ),

            array (
                'code' => '12.13.17',
                'name' => 'Muara Batang Gadis',
            ),

            array (
                'code' => '12.13.18',
                'name' => 'Ranto Baek',
            ),

            array (
                'code' => '12.13.19',
                'name' => 'Huta Bargot',
            ),

            array (
                'code' => '12.13.20',
                'name' => 'Puncak Sorik Marapi',
            ),

            array (
                'code' => '12.13.21',
                'name' => 'Pakantan',
            ),

            array (
                'code' => '12.13.22',
                'name' => 'Sinunukan',
            ),

            array (
                'code' => '12.13.23',
                'name' => 'Naga Juang',
            ),

            array (
                'code' => '12.14',
                'name' => 'KAB. NIAS SELATAN',
            ),

            array (
                'code' => '12.14.01',
                'name' => 'Lolomatua',
            ),

            array (
                'code' => '12.14.02',
                'name' => 'Gomo',
            ),

            array (
                'code' => '12.14.03',
                'name' => 'Lahusa',
            ),

            array (
                'code' => '12.14.04',
                'name' => 'Hibala',
            ),

            array (
                'code' => '12.14.05',
                'name' => 'Pulau-Pulau Batu',
            ),

            array (
                'code' => '12.14.06',
                'name' => 'Teluk Dalam',
            ),

            array (
                'code' => '12.14.07',
                'name' => 'Amandraya',
            ),

            array (
                'code' => '12.14.08',
                'name' => 'Lalowau',
            ),

            array (
                'code' => '12.14.09',
                'name' => 'Susua',
            ),

            array (
                'code' => '12.14.10',
                'name' => 'Maniamolo',
            ),

            array (
                'code' => '12.14.11',
                'name' => 'Hilimegai',
            ),

            array (
                'code' => '12.14.12',
                'name' => 'Toma',
            ),

            array (
                'code' => '12.14.13',
                'name' => 'Mazino',
            ),

            array (
                'code' => '12.14.14',
                'name' => 'Umbunasi',
            ),

            array (
                'code' => '12.14.15',
                'name' => 'Aramo',
            ),

            array (
                'code' => '12.14.16',
                'name' => 'Pulau-Pulau Batu Timur',
            ),

            array (
                'code' => '12.14.17',
                'name' => 'Mazo',
            ),

            array (
                'code' => '12.14.18',
                'name' => 'Fanayama',
            ),

            array (
                'code' => '12.14.19',
                'name' => 'Ulunoyo',
            ),

            array (
                'code' => '12.14.20',
                'name' => 'Huruna',
            ),

            array (
                'code' => '12.14.21',
                'name' => 'O\'o\'u  11',
            ),

            array (
                'code' => '12.14.22',
                'name' => 'Onohazumba',
            ),

            array (
                'code' => '12.14.23',
                'name' => 'Hilisalawa\'ahe  11',
            ),

            array (
                'code' => '12.14.24',
                'name' => 'Ulususua',
            ),

            array (
                'code' => '12.14.25',
                'name' => 'Sidua\'ori  11',
            ),

            array (
                'code' => '12.14.26',
                'name' => 'Somambawa',
            ),

            array (
                'code' => '12.14.27',
                'name' => 'Boronadu',
            ),

            array (
                'code' => '12.14.28',
                'name' => 'Simuk',
            ),

            array (
                'code' => '12.14.29',
                'name' => 'Pulau-Pulau Batu Barat',
            ),

            array (
                'code' => '12.14.30',
                'name' => 'Pulau-Pulau Batu Utara',
            ),

            array (
                'code' => '12.14.31',
                'name' => 'Tanah Masa',
            ),

            array (
                'code' => '12.15',
                'name' => 'KAB. PAKPAK BHARAT',
            ),

            array (
                'code' => '12.15.01',
                'name' => 'Sitelu Tali Urang Jehe',
            ),

            array (
                'code' => '12.15.02',
                'name' => 'Kerajaan',
            ),

            array (
                'code' => '12.15.03',
                'name' => 'Salak',
            ),

            array (
                'code' => '12.15.04',
                'name' => 'Sitelu Tali Urang Julu',
            ),

            array (
                'code' => '12.15.05',
                'name' => 'Pergetteng Getteng Sengkut',
            ),

            array (
                'code' => '12.15.06',
                'name' => 'Pagindar',
            ),

            array (
                'code' => '12.15.07',
                'name' => 'Tinada',
            ),

            array (
                'code' => '12.15.08',
                'name' => 'Siempat Rube',
            ),

            array (
                'code' => '12.16',
                'name' => 'KAB. HUMBANG HASUNDUTAN',
            ),

            array (
                'code' => '12.16.01',
                'name' => 'Parlilitan',
            ),

            array (
                'code' => '12.16.02',
                'name' => 'Pollung',
            ),

            array (
                'code' => '12.16.03',
                'name' => 'Baktiraja',
            ),

            array (
                'code' => '12.16.04',
                'name' => 'Paranginan',
            ),

            array (
                'code' => '12.16.05',
                'name' => 'Lintong Nihuta',
            ),

            array (
                'code' => '12.16.06',
                'name' => 'Dolok Sanggul',
            ),

            array (
                'code' => '12.16.07',
                'name' => 'Sijamapolang',
            ),

            array (
                'code' => '12.16.08',
                'name' => 'Onan Ganjang',
            ),

            array (
                'code' => '12.16.09',
                'name' => 'Pakkat',
            ),

            array (
                'code' => '12.16.10',
                'name' => 'Tarabintang',
            ),

            array (
                'code' => '12.17',
                'name' => 'KAB. SAMOSIR',
            ),

            array (
                'code' => '12.17.01',
                'name' => 'Simanindo',
            ),

            array (
                'code' => '12.17.02',
                'name' => 'Onan Runggu',
            ),

            array (
                'code' => '12.17.03',
                'name' => 'Nainggolan',
            ),

            array (
                'code' => '12.17.04',
                'name' => 'Palipi',
            ),

            array (
                'code' => '12.17.05',
                'name' => 'Harian',
            ),

            array (
                'code' => '12.17.06',
                'name' => 'Sianjar Mula Mula',
            ),

            array (
                'code' => '12.17.07',
                'name' => 'Ronggur Nihuta',
            ),

            array (
                'code' => '12.17.08',
                'name' => 'Pangururan',
            ),

            array (
                'code' => '12.17.09',
                'name' => 'Sitio-tio',
            ),

            array (
                'code' => '12.18',
                'name' => 'KAB. SERDANG BEDAGAI',
            ),

            array (
                'code' => '12.18.01',
                'name' => 'Pantai Cermin',
            ),

            array (
                'code' => '12.18.02',
                'name' => 'Perbaungan',
            ),

            array (
                'code' => '12.18.03',
                'name' => 'Teluk Mengkudu',
            ),

            array (
                'code' => '12.18.04',
                'name' => 'Sei. Rampah  17',
            ),

            array (
                'code' => '12.18.05',
                'name' => 'Tanjung Beringin',
            ),

            array (
                'code' => '12.18.06',
                'name' => 'Bandar Khalifah',
            ),

            array (
                'code' => '12.18.07',
                'name' => 'Dolok Merawan',
            ),

            array (
                'code' => '12.18.08',
                'name' => 'Sipispis',
            ),

            array (
                'code' => '12.18.09',
                'name' => 'Dolok Masihul',
            ),

            array (
                'code' => '12.18.10',
                'name' => 'Kotarih',
            ),

            array (
                'code' => '12.18.11',
                'name' => 'Silinda',
            ),

            array (
                'code' => '12.18.12',
                'name' => 'Serba Jadi',
            ),

            array (
                'code' => '12.18.13',
                'name' => 'Tebing Tinggi',
            ),

            array (
                'code' => '12.18.14',
                'name' => 'Pegajahan',
            ),

            array (
                'code' => '12.18.15',
                'name' => 'Sei Bamban',
            ),

            array (
                'code' => '12.18.16',
                'name' => 'Tebing Syahbandar',
            ),

            array (
                'code' => '12.18.17',
                'name' => 'Bintang Bayu',
            ),

            array (
                'code' => '12.19',
                'name' => 'KAB. BATU BARA',
            ),

            array (
                'code' => '12.19.01',
                'name' => 'Medang Deras',
            ),

            array (
                'code' => '12.19.02',
                'name' => 'Sei Suka',
            ),

            array (
                'code' => '12.19.03',
                'name' => 'Air Putih',
            ),

            array (
                'code' => '12.19.04',
                'name' => 'Lima Puluh',
            ),

            array (
                'code' => '12.19.05',
                'name' => 'Talawi',
            ),

            array (
                'code' => '12.19.06',
                'name' => 'Tanjung Tiram',
            ),

            array (
                'code' => '12.19.07',
                'name' => 'Sei Balai',
            ),

            array (
                'code' => '12.20',
                'name' => 'KAB. PADANG LAWAS UTARA',
            ),

            array (
                'code' => '12.20.01',
                'name' => 'Dolok Sigompulon',
            ),

            array (
                'code' => '12.20.02',
                'name' => 'Dolok',
            ),

            array (
                'code' => '12.20.03',
                'name' => 'Halongonan',
            ),

            array (
                'code' => '12.20.04',
                'name' => 'Padang Bolak',
            ),

            array (
                'code' => '12.20.05',
                'name' => 'Padang Bolak Julu',
            ),

            array (
                'code' => '12.20.06',
                'name' => 'Portibi',
            ),

            array (
                'code' => '12.20.07',
                'name' => 'Batang Onang',
            ),

            array (
                'code' => '12.20.08',
                'name' => 'Simangambat',
            ),

            array (
                'code' => '12.20.09',
                'name' => 'Hulu Sihapas',
            ),

            array (
                'code' => '12.21',
                'name' => 'KAB. PADANG LAWAS',
            ),

            array (
                'code' => '12.21.01',
                'name' => 'Sosopan',
            ),

            array (
                'code' => '12.21.02',
                'name' => 'Barumun Tengah',
            ),

            array (
                'code' => '12.21.03',
                'name' => 'Huristak',
            ),

            array (
                'code' => '12.21.04',
                'name' => 'Lubuk Barumun',
            ),

            array (
                'code' => '12.21.05',
                'name' => 'Huta Raja Tinggi',
            ),

            array (
                'code' => '12.21.06',
                'name' => 'Ulu Barumun',
            ),

            array (
                'code' => '12.21.07',
                'name' => 'Barumun',
            ),

            array (
                'code' => '12.21.08',
                'name' => 'Sosa',
            ),

            array (
                'code' => '12.21.09',
                'name' => 'Batang Lubu Sutam',
            ),

            array (
                'code' => '12.21.10',
                'name' => 'Barumun Selatan',
            ),

            array (
                'code' => '12.21.11',
                'name' => 'Aek Nabara Barumun',
            ),

            array (
                'code' => '12.21.12',
                'name' => 'Sihapas Barumun',
            ),

            array (
                'code' => '12.22',
                'name' => 'KAB. LABUHANBATU SELATAN',
            ),

            array (
                'code' => '12.22.01',
                'name' => 'Kotapinang',
            ),

            array (
                'code' => '12.22.02',
                'name' => 'Kampung Rakyat',
            ),

            array (
                'code' => '12.22.03',
                'name' => 'Torgamba',
            ),

            array (
                'code' => '12.22.04',
                'name' => 'Sungai Kanan',
            ),

            array (
                'code' => '12.22.05',
                'name' => 'Silangkitang',
            ),

            array (
                'code' => '12.23',
                'name' => 'KAB. LABUHANBATU UTARA',
            ),

            array (
                'code' => '12.23.01',
                'name' => 'Kualuh Hulu',
            ),

            array (
                'code' => '12.23.02',
                'name' => 'Kualuh Leidong',
            ),

            array (
                'code' => '12.23.03',
                'name' => 'Kualuh Hilir',
            ),

            array (
                'code' => '12.23.04',
                'name' => 'Aek Kuo',
            ),

            array (
                'code' => '12.23.05',
                'name' => 'Marbau',
            ),

            array (
                'code' => '12.23.06',
                'name' => 'Na IX - X',
            ),

            array (
                'code' => '12.23.07',
                'name' => 'Aek Natas',
            ),

            array (
                'code' => '12.23.08',
                'name' => 'Kualuh Selatan',
            ),

            array (
                'code' => '12.24',
                'name' => 'KAB. NIAS UTARA',
            ),

            array (
                'code' => '12.24.01',
                'name' => 'Lotu',
            ),

            array (
                'code' => '12.24.02',
                'name' => 'Sawo',
            ),

            array (
                'code' => '12.24.03',
                'name' => 'Tuhemberua',
            ),

            array (
                'code' => '12.24.04',
                'name' => 'Sitolu Ori',
            ),

            array (
                'code' => '12.24.05',
                'name' => 'Namohalu Esiwa',
            ),

            array (
                'code' => '12.24.06',
                'name' => 'Alasa Talumuzoi',
            ),

            array (
                'code' => '12.24.07',
                'name' => 'Alasa',
            ),

            array (
                'code' => '12.24.08',
                'name' => 'Tugala Oyo',
            ),

            array (
                'code' => '12.24.09',
                'name' => 'Afulu',
            ),

            array (
                'code' => '12.24.10',
                'name' => 'Lahewa',
            ),

            array (
                'code' => '12.24.11',
                'name' => 'Lahewa Timur',
            ),

            array (
                'code' => '12.25',
                'name' => 'KAB. NIAS BARAT',
            ),

            array (
                'code' => '12.25.01',
                'name' => 'Lahomi',
            ),

            array (
                'code' => '12.25.02',
                'name' => 'Sirombu',
            ),

            array (
                'code' => '12.25.03',
                'name' => 'Mandrehe Barat',
            ),

            array (
                'code' => '12.25.04',
                'name' => 'Moro\'o  10',
            ),

            array (
                'code' => '12.25.05',
                'name' => 'Mandrehe',
            ),

            array (
                'code' => '12.25.06',
                'name' => 'Mandrehe Utara',
            ),

            array (
                'code' => '12.25.07',
                'name' => 'Lolofitu Moi',
            ),

            array (
                'code' => '12.25.08',
                'name' => 'Ulu Moro\'o  5',
            ),

            array (
                'code' => '12.71',
                'name' => 'KOTA MEDAN',
            ),

            array (
                'code' => '12.71.01',
                'name' => 'Medan Kota',
            ),

            array (
                'code' => '12.71.02',
                'name' => 'Medan Sunggal',
            ),

            array (
                'code' => '12.71.03',
                'name' => 'Medan Helvetia',
            ),

            array (
                'code' => '12.71.04',
                'name' => 'Medan Denai',
            ),

            array (
                'code' => '12.71.05',
                'name' => 'Medan Barat',
            ),

            array (
                'code' => '12.71.06',
                'name' => 'Medan Deli',
            ),

            array (
                'code' => '12.71.07',
                'name' => 'Medan Tuntungan',
            ),

            array (
                'code' => '12.71.08',
                'name' => 'Medan Belawan',
            ),

            array (
                'code' => '12.71.09',
                'name' => 'Medan Amplas',
            ),

            array (
                'code' => '12.71.10',
                'name' => 'Medan Area',
            ),

            array (
                'code' => '12.71.11',
                'name' => 'Medan Johor',
            ),

            array (
                'code' => '12.71.12',
                'name' => 'Medan Marelan',
            ),

            array (
                'code' => '12.71.13',
                'name' => 'Medan Labuhan',
            ),

            array (
                'code' => '12.71.14',
                'name' => 'Medan Tembung',
            ),

            array (
                'code' => '12.71.15',
                'name' => 'Medan Maimun',
            ),

            array (
                'code' => '12.71.16',
                'name' => 'Medan Polonia',
            ),

            array (
                'code' => '12.71.17',
                'name' => 'Medan Baru',
            ),

            array (
                'code' => '12.71.18',
                'name' => 'Medan Perjuangan',
            ),

            array (
                'code' => '12.71.19',
                'name' => 'Medan Petisah',
            ),

            array (
                'code' => '12.71.20',
                'name' => 'Medan Timur',
            ),

            array (
                'code' => '12.71.21',
                'name' => 'Medan Selayang',
            ),

            array (
                'code' => '12.72',
                'name' => 'KOTA PEMATANG SIANTAR',
            ),

            array (
                'code' => '12.72.01',
                'name' => 'Siantar Timur',
            ),

            array (
                'code' => '12.72.02',
                'name' => 'Siantar Barat',
            ),

            array (
                'code' => '12.72.03',
                'name' => 'Siantar Utara',
            ),

            array (
                'code' => '12.72.04',
                'name' => 'Siantar Selatan',
            ),

            array (
                'code' => '12.72.05',
                'name' => 'Siantar Marihat',
            ),

            array (
                'code' => '12.72.06',
                'name' => 'Siantar Martoba',
            ),

            array (
                'code' => '12.72.07',
                'name' => 'Siantar Sitalasari',
            ),

            array (
                'code' => '12.72.08',
                'name' => 'Siantar Marimbun',
            ),

            array (
                'code' => '12.73',
                'name' => 'KOTA SIBOLGA',
            ),

            array (
                'code' => '12.73.01',
                'name' => 'Sibolga Utara',
            ),

            array (
                'code' => '12.73.02',
                'name' => 'Sibolga Kota',
            ),

            array (
                'code' => '12.73.03',
                'name' => 'Sibolga Selatan',
            ),

            array (
                'code' => '12.73.04',
                'name' => 'Sibolga Sambas',
            ),

            array (
                'code' => '12.74',
                'name' => 'KOTA TANJUNG BALAI',
            ),

            array (
                'code' => '12.74.01',
                'name' => 'Tanjung Balai Selatan',
            ),

            array (
                'code' => '12.74.02',
                'name' => 'Tanjung Balai Utara',
            ),

            array (
                'code' => '12.74.03',
                'name' => 'Sei Tualang Raso',
            ),

            array (
                'code' => '12.74.04',
                'name' => 'Teluk Nibung',
            ),

            array (
                'code' => '12.74.05',
                'name' => 'Datuk Bandar',
            ),

            array (
                'code' => '12.74.06',
                'name' => 'Datuk Bandar Timur',
            ),

            array (
                'code' => '12.75',
                'name' => 'KOTA BINJAI',
            ),

            array (
                'code' => '12.75.01',
                'name' => 'Binjai Utara',
            ),

            array (
                'code' => '12.75.02',
                'name' => 'Binjai Kota',
            ),

            array (
                'code' => '12.75.03',
                'name' => 'Binjai Barat',
            ),

            array (
                'code' => '12.75.04',
                'name' => 'Binjai Timur',
            ),

            array (
                'code' => '12.75.05',
                'name' => 'Binjai Selatan',
            ),

            array (
                'code' => '12.76',
                'name' => 'KOTA TEBING TINGGI',
            ),

            array (
                'code' => '12.76.01',
                'name' => 'Padang Hulu',
            ),

            array (
                'code' => '12.76.02',
                'name' => 'Rambutan',
            ),

            array (
                'code' => '12.76.03',
                'name' => 'Padang Hilir',
            ),

            array (
                'code' => '12.76.04',
                'name' => 'Bajenis',
            ),

            array (
                'code' => '12.76.05',
                'name' => 'Tebing Tinggi Kota',
            ),

            array (
                'code' => '12.77',
                'name' => 'KOTA PADANGSIDIMPUAN',
            ),

            array (
                'code' => '12.77.01',
                'name' => 'Padangsidimpuan Utara',
            ),

            array (
                'code' => '12.77.02',
                'name' => 'Padangsidimpuan Selatan',
            ),

            array (
                'code' => '12.77.03',
                'name' => 'Padangsidimpuan Batunadua',
            ),

            array (
                'code' => '12.77.04',
                'name' => 'Padangsidimpuan Hutaimbaru',
            ),

            array (
                'code' => '12.77.05',
                'name' => 'Padangsidimpuan Tenggara',
            ),

            array (
                'code' => '12.77.06',
                'name' => 'Padangsidimpuan Angkola Julu',
            ),

            array (
                'code' => '12.78',
                'name' => 'KOTA GUNUNGSITOLI',
            ),

            array (
                'code' => '12.78.01',
                'name' => 'Gunungsitoli',
            ),

            array (
                'code' => '12.78.02',
                'name' => 'Gunungsitoli Selatan',
            ),

            array (
                'code' => '12.78.03',
                'name' => 'Gunungsitoli Utara',
            ),

            array (
                'code' => '12.78.04',
                'name' => 'Gunungsitoli Idanoi',
            ),

            array (
                'code' => '12.78.05',
                'name' => 'Gunungsitoli Alo\'oa  9',
            ),

            array (
                'code' => '12.78.06',
                'name' => 'Gunungsitoli Barat',
            ),

            array (
                'code' => '13',
                'name' => 'SUMATERA BARAT',
            ),

            array (
                'code' => '13.01',
                'name' => 'KAB. PESISIR SELATAN',
            ),

            array (
                'code' => '13.01.01',
                'name' => 'Pancung Soal',
            ),

            array (
                'code' => '13.01.02',
                'name' => 'Ranah Pesisir',
            ),

            array (
                'code' => '13.01.03',
                'name' => 'Lengayang',
            ),

            array (
                'code' => '13.01.04',
                'name' => 'Batang Kapas',
            ),

            array (
                'code' => '13.01.05',
                'name' => 'IV Jurai',
            ),

            array (
                'code' => '13.01.06',
                'name' => 'Bayang',
            ),

            array (
                'code' => '13.01.07',
                'name' => 'Koto XI Tarusan',
            ),

            array (
                'code' => '13.01.08',
                'name' => 'Sutera',
            ),

            array (
                'code' => '13.01.09',
                'name' => 'Linggo Sari Baganti',
            ),

            array (
                'code' => '13.01.10',
                'name' => 'Lunang',
            ),

            array (
                'code' => '13.01.11',
                'name' => 'Basa Ampek Balai Tapan',
            ),

            array (
                'code' => '13.01.12',
                'name' => 'IV Nagari Bayang Utara',
            ),

            array (
                'code' => '13.01.13',
                'name' => 'Airpura',
            ),

            array (
                'code' => '13.01.14',
                'name' => 'Ranah Ampek Hulu Tapan',
            ),

            array (
                'code' => '13.01.15',
                'name' => 'Silaut',
            ),

            array (
                'code' => '13.02',
                'name' => 'KAB. SOLOK',
            ),

            array (
                'code' => '13.02.03',
                'name' => 'Pantai Cermin',
            ),

            array (
                'code' => '13.02.04',
                'name' => 'Lembah Gumanti',
            ),

            array (
                'code' => '13.02.05',
                'name' => 'Payung Sekaki',
            ),

            array (
                'code' => '13.02.06',
                'name' => 'Lembang Jaya',
            ),

            array (
                'code' => '13.02.07',
                'name' => 'Gunung Talang',
            ),

            array (
                'code' => '13.02.08',
                'name' => 'Bukit Sundi',
            ),

            array (
                'code' => '13.02.09',
                'name' => 'IX Koto Sungai Lasi',
            ),

            array (
                'code' => '13.02.10',
                'name' => 'Kubung',
            ),

            array (
                'code' => '13.02.11',
                'name' => 'X Koto Singkarak',
            ),

            array (
                'code' => '13.02.12',
                'name' => 'X Koto Diatas',
            ),

            array (
                'code' => '13.02.13',
                'name' => 'Junjung Sirih',
            ),

            array (
                'code' => '13.02.17',
                'name' => 'Hiliran Gumanti',
            ),

            array (
                'code' => '13.02.18',
                'name' => 'Tigo Lurah',
            ),

            array (
                'code' => '13.02.19',
                'name' => 'Danau Kembar',
            ),

            array (
                'code' => '13.03',
                'name' => 'KAB. SIJUNJUNG',
            ),

            array (
                'code' => '13.03.03',
                'name' => 'Tanjung Gadang',
            ),

            array (
                'code' => '13.03.04',
                'name' => 'Sijunjung',
            ),

            array (
                'code' => '13.03.05',
                'name' => 'IV Nagari',
            ),

            array (
                'code' => '13.03.06',
                'name' => 'Kamang Baru',
            ),

            array (
                'code' => '13.03.07',
                'name' => 'Lubuak Tarok',
            ),

            array (
                'code' => '13.03.08',
                'name' => 'Koto VII',
            ),

            array (
                'code' => '13.03.09',
                'name' => 'Sumpur Kudus',
            ),

            array (
                'code' => '13.03.10',
                'name' => 'Kupitan',
            ),

            array (
                'code' => '13.04',
                'name' => 'KAB. TANAH DATAR',
            ),

            array (
                'code' => '13.04.01',
                'name' => 'X Koto',
            ),

            array (
                'code' => '13.04.02',
                'name' => 'Batipuh',
            ),

            array (
                'code' => '13.04.03',
                'name' => 'Rambatan',
            ),

            array (
                'code' => '13.04.04',
                'name' => 'Lima Kaum',
            ),

            array (
                'code' => '13.04.05',
                'name' => 'Tanjung Emas',
            ),

            array (
                'code' => '13.04.06',
                'name' => 'Lintau Buo',
            ),

            array (
                'code' => '13.04.07',
                'name' => 'Sungayang',
            ),

            array (
                'code' => '13.04.08',
                'name' => 'Sungai Tarab',
            ),

            array (
                'code' => '13.04.09',
                'name' => 'Pariangan',
            ),

            array (
                'code' => '13.04.10',
                'name' => 'Salimpauang',
            ),

            array (
                'code' => '13.04.11',
                'name' => 'Padang Ganting',
            ),

            array (
                'code' => '13.04.12',
                'name' => 'Tanjuang Baru',
            ),

            array (
                'code' => '13.04.13',
                'name' => 'Lintau Buo Utara',
            ),

            array (
                'code' => '13.04.14',
                'name' => 'Batipuah Selatan',
            ),

            array (
                'code' => '13.05',
                'name' => 'KAB. PADANG PARIAMAN',
            ),

            array (
                'code' => '13.05.01',
                'name' => 'Lubuk Alung',
            ),

            array (
                'code' => '13.05.02',
                'name' => 'Batang Anai',
            ),

            array (
                'code' => '13.05.03',
                'name' => 'Nan Sabaris',
            ),

            array (
                'code' => '13.05.04',
                'name' => ' x  Enam Lingkuang  3',
            ),

            array (
                'code' => '13.05.05',
                'name' => 'VII Koto Sungai Sarik',
            ),

            array (
                'code' => '13.05.06',
                'name' => 'V Koto Kampung Dalam',
            ),

            array (
                'code' => '13.05.07',
                'name' => 'Sungai Garingging',
            ),

            array (
                'code' => '13.05.08',
                'name' => 'Sungai Limau',
            ),

            array (
                'code' => '13.05.09',
                'name' => 'IV Koto Aur Malintang',
            ),

            array (
                'code' => '13.05.10',
                'name' => 'Ulakan Tapakih',
            ),

            array (
                'code' => '13.05.11',
                'name' => 'Sintuak Toboh Gadang',
            ),

            array (
                'code' => '13.05.12',
                'name' => 'Padang Sago',
            ),

            array (
                'code' => '13.05.13',
                'name' => 'Batang Gasan',
            ),

            array (
                'code' => '13.05.14',
                'name' => 'V Koto Timur',
            ),

            array (
                'code' => '13.05.15',
                'name' => ' x  Kayu Tanam  4',
            ),

            array (
                'code' => '13.05.16',
                'name' => 'Patamuan',
            ),

            array (
                'code' => '13.05.17',
                'name' => 'Enam Lingkung',
            ),

            array (
                'code' => '13.06',
                'name' => 'KAB. AGAM',
            ),

            array (
                'code' => '13.06.01',
                'name' => 'Tanjung Mutiara',
            ),

            array (
                'code' => '13.06.02',
                'name' => 'Lubuk Basung',
            ),

            array (
                'code' => '13.06.03',
                'name' => 'Tanjung Raya',
            ),

            array (
                'code' => '13.06.04',
                'name' => 'Matur',
            ),

            array (
                'code' => '13.06.05',
                'name' => 'IV Koto',
            ),

            array (
                'code' => '13.06.06',
                'name' => 'Banuhampu',
            ),

            array (
                'code' => '13.06.07',
                'name' => 'Ampek Angkek',
            ),

            array (
                'code' => '13.06.08',
                'name' => 'Baso',
            ),

            array (
                'code' => '13.06.09',
                'name' => 'Tilatang Kamang',
            ),

            array (
                'code' => '13.06.10',
                'name' => 'Palupuh',
            ),

            array (
                'code' => '13.06.11',
                'name' => 'Pelembayan',
            ),

            array (
                'code' => '13.06.12',
                'name' => 'Sungai Pua',
            ),

            array (
                'code' => '13.06.13',
                'name' => 'Ampek Nagari',
            ),

            array (
                'code' => '13.06.14',
                'name' => 'Candung',
            ),

            array (
                'code' => '13.06.15',
                'name' => 'Kamang Magek',
            ),

            array (
                'code' => '13.06.16',
                'name' => 'Malalak',
            ),

            array (
                'code' => '13.07',
                'name' => 'KAB. LIMA PULUH KOTA',
            ),

            array (
                'code' => '13.07.01',
                'name' => 'Suliki',
            ),

            array (
                'code' => '13.07.02',
                'name' => 'Guguak',
            ),

            array (
                'code' => '13.07.03',
                'name' => 'Payakumbuh',
            ),

            array (
                'code' => '13.07.04',
                'name' => 'Luak',
            ),

            array (
                'code' => '13.07.05',
                'name' => 'Harau',
            ),

            array (
                'code' => '13.07.06',
                'name' => 'Pangkalan Koto Baru',
            ),

            array (
                'code' => '13.07.07',
                'name' => 'Kapur IX',
            ),

            array (
                'code' => '13.07.08',
                'name' => 'Gunuang Omeh',
            ),

            array (
                'code' => '13.07.09',
                'name' => 'Lareh Sago Halaban',
            ),

            array (
                'code' => '13.07.10',
                'name' => 'Situjuah Limo Nagari',
            ),

            array (
                'code' => '13.07.11',
                'name' => 'Mungka',
            ),

            array (
                'code' => '13.07.12',
                'name' => 'Bukik Barisan',
            ),

            array (
                'code' => '13.07.13',
                'name' => 'Akabiluru',
            ),

            array (
                'code' => '13.08',
                'name' => 'KAB. PASAMAN',
            ),

            array (
                'code' => '13.08.04',
                'name' => 'Bonjol',
            ),

            array (
                'code' => '13.08.05',
                'name' => 'Lubuk Sikaping',
            ),

            array (
                'code' => '13.08.07',
                'name' => 'Panti',
            ),

            array (
                'code' => '13.08.08',
                'name' => 'Mapat Tunggul',
            ),

            array (
                'code' => '13.08.12',
                'name' => 'Duo Koto',
            ),

            array (
                'code' => '13.08.13',
                'name' => 'Tigo Nagari',
            ),

            array (
                'code' => '13.08.14',
                'name' => 'Rao',
            ),

            array (
                'code' => '13.08.15',
                'name' => 'Mapat Tunggul Selatan',
            ),

            array (
                'code' => '13.08.16',
                'name' => 'Simpang Alahan Mati',
            ),

            array (
                'code' => '13.08.17',
                'name' => 'Padang Gelugur',
            ),

            array (
                'code' => '13.08.18',
                'name' => 'Rao Utara',
            ),

            array (
                'code' => '13.08.19',
                'name' => 'Rao Selatan',
            ),

            array (
                'code' => '13.09',
                'name' => 'KAB. KEPULAUAN MENTAWAI',
            ),

            array (
                'code' => '13.09.01',
                'name' => 'Pagai Utara',
            ),

            array (
                'code' => '13.09.02',
                'name' => 'Sipora Selatan',
            ),

            array (
                'code' => '13.09.03',
                'name' => 'Siberut Selatan',
            ),

            array (
                'code' => '13.09.04',
                'name' => 'Siberut Utara',
            ),

            array (
                'code' => '13.09.05',
                'name' => 'Siberut Barat',
            ),

            array (
                'code' => '13.09.06',
                'name' => 'Siberut Barat Daya',
            ),

            array (
                'code' => '13.09.07',
                'name' => 'Siberut Tengah',
            ),

            array (
                'code' => '13.09.08',
                'name' => 'Sipora Utara',
            ),

            array (
                'code' => '13.09.09',
                'name' => 'Sikakap',
            ),

            array (
                'code' => '13.09.10',
                'name' => 'Pagai Selatan',
            ),

            array (
                'code' => '13.10',
                'name' => 'KAB. DHARMASRAYA',
            ),

            array (
                'code' => '13.10.01',
                'name' => 'Koto Baru',
            ),

            array (
                'code' => '13.10.02',
                'name' => 'Pulau Punjung',
            ),

            array (
                'code' => '13.10.03',
                'name' => 'Sungai Rumbai',
            ),

            array (
                'code' => '13.10.04',
                'name' => 'Sitiung',
            ),

            array (
                'code' => '13.10.05',
                'name' => 'Sembilan Koto',
            ),

            array (
                'code' => '13.10.06',
                'name' => 'Timpeh',
            ),

            array (
                'code' => '13.10.07',
                'name' => 'Koto Salak',
            ),

            array (
                'code' => '13.10.08',
                'name' => 'Tiumang',
            ),

            array (
                'code' => '13.10.09',
                'name' => 'Padang Laweh',
            ),

            array (
                'code' => '13.10.10',
                'name' => 'Asam Jujuhan',
            ),

            array (
                'code' => '13.10.11',
                'name' => 'Koto Besar',
            ),

            array (
                'code' => '13.11',
                'name' => 'KAB. SOLOK SELATAN',
            ),

            array (
                'code' => '13.11.01',
                'name' => 'Sangir',
            ),

            array (
                'code' => '13.11.02',
                'name' => 'Sungai Pagu',
            ),

            array (
                'code' => '13.11.03',
                'name' => 'Koto Parik Gadang Diateh',
            ),

            array (
                'code' => '13.11.04',
                'name' => 'Sangir Jujuan',
            ),

            array (
                'code' => '13.11.05',
                'name' => 'Sangir Batang Hari',
            ),

            array (
                'code' => '13.11.06',
                'name' => 'Pauh Duo',
            ),

            array (
                'code' => '13.11.07',
                'name' => 'Sangir Balai Janggo',
            ),

            array (
                'code' => '13.12',
                'name' => 'KAB. PASAMAN BARAT',
            ),

            array (
                'code' => '13.12.01',
                'name' => 'Sungaiberemas',
            ),

            array (
                'code' => '13.12.02',
                'name' => 'Lembah Melintang',
            ),

            array (
                'code' => '13.12.03',
                'name' => 'Pasaman',
            ),

            array (
                'code' => '13.12.04',
                'name' => 'Talamau',
            ),

            array (
                'code' => '13.12.05',
                'name' => 'Kinali',
            ),

            array (
                'code' => '13.12.06',
                'name' => 'Gunungtuleh',
            ),

            array (
                'code' => '13.12.07',
                'name' => 'Ranah Batahan',
            ),

            array (
                'code' => '13.12.08',
                'name' => 'Koto Balingka',
            ),

            array (
                'code' => '13.12.09',
                'name' => 'Sungaiaur',
            ),

            array (
                'code' => '13.12.10',
                'name' => 'Luhak Nan Duo',
            ),

            array (
                'code' => '13.12.11',
                'name' => 'Sasak Ranah Pesisir',
            ),

            array (
                'code' => '13.71',
                'name' => 'KOTA PADANG',
            ),

            array (
                'code' => '13.71.01',
                'name' => 'Padang Selatan',
            ),

            array (
                'code' => '13.71.02',
                'name' => 'Padang Timur',
            ),

            array (
                'code' => '13.71.03',
                'name' => 'Padang Barat',
            ),

            array (
                'code' => '13.71.04',
                'name' => 'Padang Utara',
            ),

            array (
                'code' => '13.71.05',
                'name' => 'Bungus Teluk Kabung',
            ),

            array (
                'code' => '13.71.06',
                'name' => 'Lubuk Begalung',
            ),

            array (
                'code' => '13.71.07',
                'name' => 'Lubuk Kilangan',
            ),

            array (
                'code' => '13.71.08',
                'name' => 'Pauh',
            ),

            array (
                'code' => '13.71.09',
                'name' => 'Kuranji',
            ),

            array (
                'code' => '13.71.10',
                'name' => 'Nanggalo',
            ),

            array (
                'code' => '13.71.11',
                'name' => 'Koto Tangah',
            ),

            array (
                'code' => '13.72',
                'name' => 'KOTA SOLOK',
            ),

            array (
                'code' => '13.72.01',
                'name' => 'Lubuk Sikarah',
            ),

            array (
                'code' => '13.72.02',
                'name' => 'Tanjung Harapan',
            ),

            array (
                'code' => '13.73',
                'name' => 'KOTA SAWAHLUNTO',
            ),

            array (
                'code' => '13.73.01',
                'name' => 'Lembah Segar',
            ),

            array (
                'code' => '13.73.02',
                'name' => 'Barangin',
            ),

            array (
                'code' => '13.73.03',
                'name' => 'Silungkang',
            ),

            array (
                'code' => '13.73.04',
                'name' => 'Talawi',
            ),

            array (
                'code' => '13.74',
                'name' => 'KOTA PADANG PANJANG',
            ),

            array (
                'code' => '13.74.01',
                'name' => 'Padang Panjang Timur',
            ),

            array (
                'code' => '13.74.02',
                'name' => 'Padang Panjang Barat',
            ),

            array (
                'code' => '13.75',
                'name' => 'KOTA BUKITTINGGI',
            ),

            array (
                'code' => '13.75.01',
                'name' => 'Guguak Panjang',
            ),

            array (
                'code' => '13.75.02',
                'name' => 'Mandiangin K. Selayan  9',
            ),

            array (
                'code' => '13.75.03',
                'name' => 'Aur Birugo Tigo Baleh',
            ),

            array (
                'code' => '13.76',
                'name' => 'KOTA PAYAKUMBUH',
            ),

            array (
                'code' => '13.76.01',
                'name' => 'Payakumbuh Barat',
            ),

            array (
                'code' => '13.76.02',
                'name' => 'Payakumbuh Utara',
            ),

            array (
                'code' => '13.76.03',
                'name' => 'Payakumbuh Timur',
            ),

            array (
                'code' => '13.76.04',
                'name' => 'Lamposi Tigo Nagori',
            ),

            array (
                'code' => '13.76.05',
                'name' => 'Payakumbuh Selatan',
            ),

            array (
                'code' => '13.77',
                'name' => 'KOTA PARIAMAN',
            ),

            array (
                'code' => '13.77.01',
                'name' => 'Pariaman Tengah',
            ),

            array (
                'code' => '13.77.02',
                'name' => 'Pariaman Utara',
            ),

            array (
                'code' => '13.77.03',
                'name' => 'Pariaman Selatan',
            ),

            array (
                'code' => '13.77.04',
                'name' => 'Pariaman Timur',
            ),

            array (
                'code' => '14',
                'name' => 'RIAU',
            ),

            array (
                'code' => '14.01',
                'name' => 'KAB. KAMPAR',
            ),

            array (
                'code' => '14.01.01',
                'name' => 'Bangkinang Kota',
            ),

            array (
                'code' => '14.01.02',
                'name' => 'Kampar',
            ),

            array (
                'code' => '14.01.03',
                'name' => 'Tambang',
            ),

            array (
                'code' => '14.01.04',
                'name' => 'XIII Koto Kampar',
            ),

            array (
                'code' => '14.01.05',
                'name' => 'Kuok',
            ),

            array (
                'code' => '14.01.06',
                'name' => 'Siak Hulu',
            ),

            array (
                'code' => '14.01.07',
                'name' => 'Kampar Kiri',
            ),

            array (
                'code' => '14.01.08',
                'name' => 'Kampar Kiri Hilir',
            ),

            array (
                'code' => '14.01.09',
                'name' => 'Kampar Kiri Hulu',
            ),

            array (
                'code' => '14.01.10',
                'name' => 'Tapung',
            ),

            array (
                'code' => '14.01.11',
                'name' => 'Tapung Hilir',
            ),

            array (
                'code' => '14.01.12',
                'name' => 'Tapung Hulu',
            ),

            array (
                'code' => '14.01.13',
                'name' => 'Salo',
            ),

            array (
                'code' => '14.01.14',
                'name' => 'Rumbio Jaya',
            ),

            array (
                'code' => '14.01.15',
                'name' => 'Bangkinang',
            ),

            array (
                'code' => '14.01.16',
                'name' => 'Perhentian Raja',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '14.01.17',
                'name' => 'Kampar Timur',
            ),

            array (
                'code' => '14.01.18',
                'name' => 'Kampar Utara',
            ),

            array (
                'code' => '14.01.19',
                'name' => 'Kampar Kiri Tengah',
            ),

            array (
                'code' => '14.01.20',
                'name' => 'Gunung Sahilan',
            ),

            array (
                'code' => '14.01.21',
                'name' => 'Koto Kampar Hulu',
            ),

            array (
                'code' => '14.02',
                'name' => 'KAB. INDRAGIRI HULU',
            ),

            array (
                'code' => '14.02.01',
                'name' => 'Rengat',
            ),

            array (
                'code' => '14.02.02',
                'name' => 'Rengat Barat',
            ),

            array (
                'code' => '14.02.03',
                'name' => 'Kelayang',
            ),

            array (
                'code' => '14.02.04',
                'name' => 'Pasir Penyu',
            ),

            array (
                'code' => '14.02.05',
                'name' => 'Peranap',
            ),

            array (
                'code' => '14.02.06',
                'name' => 'Siberida',
            ),

            array (
                'code' => '14.02.07',
                'name' => 'Batang Cenaku',
            ),

            array (
                'code' => '14.02.08',
                'name' => 'Batang Gangsal',
            ),

            array (
                'code' => '14.02.09',
                'name' => 'Lirik',
            ),

            array (
                'code' => '14.02.10',
                'name' => 'Kuala Cenaku',
            ),

            array (
                'code' => '14.02.11',
                'name' => 'Sungai Lala',
            ),

            array (
                'code' => '14.02.12',
                'name' => 'Lubuk Batu Jaya',
            ),

            array (
                'code' => '14.02.13',
                'name' => 'Rakit Kulim',
            ),

            array (
                'code' => '14.02.14',
                'name' => 'Batang Peranap',
            ),

            array (
                'code' => '14.03',
                'name' => 'KAB. BENGKALIS',
            ),

            array (
                'code' => '14.03.01',
                'name' => 'Bengkalis',
            ),

            array (
                'code' => '14.03.02',
                'name' => 'Bantan',
            ),

            array (
                'code' => '14.03.03',
                'name' => 'Bukit Batu',
            ),

            array (
                'code' => '14.03.09',
                'name' => 'Mandau',
            ),

            array (
                'code' => '14.03.10',
                'name' => 'Rupat',
            ),

            array (
                'code' => '14.03.11',
                'name' => 'Rupat Utara',
            ),

            array (
                'code' => '14.03.12',
                'name' => 'Siak Kecil',
            ),

            array (
                'code' => '14.03.13',
                'name' => 'Pinggir',
            ),

            array (
                'code' => '14.04',
                'name' => 'KAB. INDRAGIRI HILIR',
            ),

            array (
                'code' => '14.04.01',
                'name' => 'Reteh',
            ),

            array (
                'code' => '14.04.02',
                'name' => 'Enok',
            ),

            array (
                'code' => '14.04.03',
                'name' => 'Kuala Indragiri',
            ),

            array (
                'code' => '14.04.04',
                'name' => 'Tembilahan',
            ),

            array (
                'code' => '14.04.05',
                'name' => 'Tempuling',
            ),

            array (
                'code' => '14.04.06',
                'name' => 'Gaung Anak Serka',
            ),

            array (
                'code' => '14.04.07',
                'name' => 'Mandah',
            ),

            array (
                'code' => '14.04.08',
                'name' => 'Kateman',
            ),

            array (
                'code' => '14.04.09',
                'name' => 'Keritang',
            ),

            array (
                'code' => '14.04.10',
                'name' => 'Tanah Merah',
            ),

            array (
                'code' => '14.04.11',
                'name' => 'Batang Tuaka',
            ),

            array (
                'code' => '14.04.12',
                'name' => 'Gaung',
            ),

            array (
                'code' => '14.04.13',
                'name' => 'Tembilahan Hulu',
            ),

            array (
                'code' => '14.04.14',
                'name' => 'Kemuning',
            ),

            array (
                'code' => '14.04.15',
                'name' => 'Pelangiran',
            ),

            array (
                'code' => '14.04.16',
                'name' => 'Teluk Belengkong',
            ),

            array (
                'code' => '14.04.17',
                'name' => 'Pulau Burung',
            ),

            array (
                'code' => '14.04.18',
                'name' => 'Concong',
            ),

            array (
                'code' => '14.04.19',
                'name' => 'Kempas',
            ),

            array (
                'code' => '14.04.20',
                'name' => 'Sungai Batang',
            ),

            array (
                'code' => '14.05',
                'name' => 'KAB.  PELALAWAN',
            ),

            array (
                'code' => '14.05.01',
                'name' => 'Ukui',
            ),

            array (
                'code' => '14.05.02',
                'name' => 'Pangkalan Kerinci',
            ),

            array (
                'code' => '14.05.03',
                'name' => 'Pangkalan Kuras',
            ),

            array (
                'code' => '14.05.04',
                'name' => 'Pangkalan Lesung',
            ),

            array (
                'code' => '14.05.05',
                'name' => 'Langgam',
            ),

            array (
                'code' => '14.05.06',
                'name' => 'Pelalawan',
            ),

            array (
                'code' => '14.05.07',
                'name' => 'Kerumutan',
            ),

            array (
                'code' => '14.05.08',
                'name' => 'Bunut',
            ),

            array (
                'code' => '14.05.09',
                'name' => 'Teluk Meranti',
            ),

            array (
                'code' => '14.05.10',
                'name' => 'Kuala Kampar',
            ),

            array (
                'code' => '14.05.11',
                'name' => 'Bandar Sei Kijang',
            ),

            array (
                'code' => '14.05.12',
                'name' => 'Bandar Petalangan',
            ),

            array (
                'code' => '14.06',
                'name' => 'KAB.  ROKAN HULU',
            ),

            array (
                'code' => '14.06.01',
                'name' => 'Ujung Batu',
            ),

            array (
                'code' => '14.06.02',
                'name' => 'Rokan IV Koto',
            ),

            array (
                'code' => '14.06.03',
                'name' => 'Rambah',
            ),

            array (
                'code' => '14.06.04',
                'name' => 'Tambusai',
            ),

            array (
                'code' => '14.06.05',
                'name' => 'Kepenuhan',
            ),

            array (
                'code' => '14.06.06',
                'name' => 'Kunto Darussalam',
            ),

            array (
                'code' => '14.06.07',
                'name' => 'Rambah Samo',
            ),

            array (
                'code' => '14.06.08',
                'name' => 'Rambah Hilir',
            ),

            array (
                'code' => '14.06.09',
                'name' => 'Tambusai Utara',
            ),

            array (
                'code' => '14.06.10',
                'name' => 'Bangun Purba',
            ),

            array (
                'code' => '14.06.11',
                'name' => 'Tandun',
            ),

            array (
                'code' => '14.06.12',
                'name' => 'Kabun',
            ),

            array (
                'code' => '14.06.13',
                'name' => 'Bonai Darussalam',
            ),

            array (
                'code' => '14.06.14',
                'name' => 'Pagaran Tapah Darussalam',
            ),

            array (
                'code' => '14.06.15',
                'name' => 'Kepenuhan Hulu',
            ),

            array (
                'code' => '14.06.16',
                'name' => 'Pendalian IV Koto',
            ),

            array (
                'code' => '14.07',
                'name' => 'KAB.  ROKAN HILIR',
            ),

            array (
                'code' => '14.07.01',
                'name' => 'Kubu',
            ),

            array (
                'code' => '14.07.02',
                'name' => 'Bangko',
            ),

            array (
                'code' => '14.07.03',
                'name' => 'Tanah Putih',
            ),

            array (
                'code' => '14.07.04',
                'name' => 'Rimba Melintang',
            ),

            array (
                'code' => '14.07.05',
                'name' => 'Bagan Sinembah',
            ),

            array (
                'code' => '14.07.06',
                'name' => 'Pasir Limau Kapas',
            ),

            array (
                'code' => '14.07.07',
                'name' => 'Sinaboi',
            ),

            array (
                'code' => '14.07.08',
                'name' => 'Pujud',
            ),

            array (
                'code' => '14.07.09',
                'name' => 'Tanah Putih Tanjung Melawan',
            ),

            array (
                'code' => '14.07.10',
                'name' => 'Bangko Pusako',
            ),

            array (
                'code' => '14.07.11',
                'name' => 'Simpang Kanan',
            ),

            array (
                'code' => '14.07.12',
                'name' => 'Batu Hampar',
            ),

            array (
                'code' => '14.07.13',
                'name' => 'Rantau Kopar',
            ),

            array (
                'code' => '14.07.14',
                'name' => 'Pekaitan',
            ),

            array (
                'code' => '14.07.15',
                'name' => 'Kubu Babussalam',
            ),

            array (
                'code' => '14.08',
                'name' => 'KAB.  SIAK',
            ),

            array (
                'code' => '14.08.01',
                'name' => 'Siak',
            ),

            array (
                'code' => '14.08.02',
                'name' => 'Sungai Apit',
            ),

            array (
                'code' => '14.08.03',
                'name' => 'Minas',
            ),

            array (
                'code' => '14.08.04',
                'name' => 'Tualang',
            ),

            array (
                'code' => '14.08.05',
                'name' => 'Sungai Mandau',
            ),

            array (
                'code' => '14.08.06',
                'name' => 'Dayun',
            ),

            array (
                'code' => '14.08.07',
                'name' => 'Kerinci Kanan',
            ),

            array (
                'code' => '14.08.08',
                'name' => 'Bunga Raya',
            ),

            array (
                'code' => '14.08.09',
                'name' => 'Koto Gasib',
            ),

            array (
                'code' => '14.08.10',
                'name' => 'Kandis',
            ),

            array (
                'code' => '14.08.11',
                'name' => 'Lubuk Dalam',
            ),

            array (
                'code' => '14.08.12',
                'name' => 'Sabak Auh',
            ),

            array (
                'code' => '14.08.13',
                'name' => 'Mempura',
            ),

            array (
                'code' => '14.08.14',
                'name' => 'Pusako',
            ),

            array (
                'code' => '14.09',
                'name' => 'KAB. KUANTAN SINGINGI',
            ),

            array (
                'code' => '14.09.01',
                'name' => 'Kuantan Mudik',
            ),

            array (
                'code' => '14.09.02',
                'name' => 'Kuantan Tengah',
            ),

            array (
                'code' => '14.09.03',
                'name' => 'Singingi',
            ),

            array (
                'code' => '14.09.04',
                'name' => 'Kuantan Hilir',
            ),

            array (
                'code' => '14.09.05',
                'name' => 'Cerenti',
            ),

            array (
                'code' => '14.09.06',
                'name' => 'Benai',
            ),

            array (
                'code' => '14.09.07',
                'name' => 'Gunungtoar',
            ),

            array (
                'code' => '14.09.08',
                'name' => 'Singingi Hilir',
            ),

            array (
                'code' => '14.09.09',
                'name' => 'Pangean',
            ),

            array (
                'code' => '14.09.10',
                'name' => 'Logas Tanah Darat',
            ),

            array (
                'code' => '14.09.11',
                'name' => 'Inuman',
            ),

            array (
                'code' => '14.09.12',
                'name' => 'Hulu Kuantan',
            ),

            array (
                'code' => '14.09.13',
                'name' => 'Kuantan Hilir Seberang',
            ),

            array (
                'code' => '14.09.14',
                'name' => 'Sentajo Raya',
            ),

            array (
                'code' => '14.09.15',
                'name' => 'Pucuk Rantau',
            ),

            array (
                'code' => '14.10',
                'name' => 'KAB. KEPULAUAN MERANTI',
            ),

            array (
                'code' => '14.10.01',
                'name' => 'Tebing Tinggi',
            ),

            array (
                'code' => '14.10.02',
                'name' => 'Rangsang Barat',
            ),

            array (
                'code' => '14.10.03',
                'name' => 'Rangsang',
            ),

            array (
                'code' => '14.10.04',
                'name' => 'Tebing Tinggi Barat',
            ),

            array (
                'code' => '14.10.05',
                'name' => 'Merbau',
            ),

            array (
                'code' => '14.10.06',
                'name' => 'Pulaumerbau',
            ),

            array (
                'code' => '14.10.07',
                'name' => 'Tebing Tinggi Timur',
            ),

            array (
                'code' => '14.10.08',
                'name' => 'Tasik Putri Puyu',
            ),

            array (
                'code' => '14.10.09',
                'name' => 'Rangsang Pesisir',
            ),

            array (
                'code' => '14.71',
                'name' => 'KOTA PEKANBARU',
            ),

            array (
                'code' => '14.71.01',
                'name' => 'Sukajadi',
            ),

            array (
                'code' => '14.71.02',
                'name' => 'Pekanbaru Kota',
            ),

            array (
                'code' => '14.71.03',
                'name' => 'Sail',
            ),

            array (
                'code' => '14.71.04',
                'name' => 'Lima Puluh',
            ),

            array (
                'code' => '14.71.05',
                'name' => 'Senapelan',
            ),

            array (
                'code' => '14.71.06',
                'name' => 'Rumbai',
            ),

            array (
                'code' => '14.71.07',
                'name' => 'Bukit Raya',
            ),

            array (
                'code' => '14.71.08',
                'name' => 'Tampan',
            ),

            array (
                'code' => '14.71.09',
                'name' => 'Marpoyan Damai',
            ),

            array (
                'code' => '14.71.10',
                'name' => 'Tenayan Raya',
            ),

            array (
                'code' => '14.71.11',
                'name' => 'Payung Sekaki',
            ),

            array (
                'code' => '14.71.12',
                'name' => 'Rumbai Pesisir',
            ),

            array (
                'code' => '14.72',
                'name' => 'KOTA DUMAI',
            ),

            array (
                'code' => '14.72.01',
                'name' => 'Dumai Barat',
            ),

            array (
                'code' => '14.72.02',
                'name' => 'Dumai Timur',
            ),

            array (
                'code' => '14.72.03',
                'name' => 'Bukit Kapur',
            ),

            array (
                'code' => '14.72.04',
                'name' => 'Sungai Sembilan',
            ),

            array (
                'code' => '14.72.05',
                'name' => 'Medang Kampai',
            ),

            array (
                'code' => '14.72.06',
                'name' => 'Dumai Kota',
            ),

            array (
                'code' => '14.72.07',
                'name' => 'Dumai Selatan',
            ),

            array (
                'code' => '15',
                'name' => 'JAMBI',
            ),

            array (
                'code' => '15.01',
                'name' => 'KAB.  KERINCI',
            ),

            array (
                'code' => '15.01.01',
                'name' => 'Gunung Raya',
            ),

            array (
                'code' => '15.01.02',
                'name' => 'Danau Kerinci',
            ),

            array (
                'code' => '15.01.04',
                'name' => 'Sitinjau Laut',
            ),

            array (
                'code' => '15.01.05',
                'name' => 'Air Hangat',
            ),

            array (
                'code' => '15.01.06',
                'name' => 'Gunung Kerinci',
            ),

            array (
                'code' => '15.01.07',
                'name' => 'Batang Merangin',
            ),

            array (
                'code' => '15.01.08',
                'name' => 'Keliling Danau',
            ),

            array (
                'code' => '15.01.09',
                'name' => 'Kayu Aro',
            ),

            array (
                'code' => '15.01.11',
                'name' => 'Air Hangat Timur',
            ),

            array (
                'code' => '15.01.15',
                'name' => 'Gunung Tujuh',
            ),

            array (
                'code' => '15.01.16',
                'name' => 'Siulak',
            ),

            array (
                'code' => '15.01.17',
                'name' => 'Depati Tujuh',
            ),

            array (
                'code' => '15.01.18',
                'name' => 'Siulak Mukai',
            ),

            array (
                'code' => '15.01.19',
                'name' => 'Kayu Aro Barat',
            ),

            array (
                'code' => '15.01.20',
                'name' => 'Bukitkerman',
            ),

            array (
                'code' => '15.01.21',
                'name' => 'Air Hangat Barat',
            ),

            array (
                'code' => '15.02',
                'name' => 'KAB.  MERANGIN',
            ),

            array (
                'code' => '15.02.01',
                'name' => 'Jangkat',
            ),

            array (
                'code' => '15.02.02',
                'name' => 'Bangko',
            ),

            array (
                'code' => '15.02.03',
                'name' => 'Muara Siau',
            ),

            array (
                'code' => '15.02.04',
                'name' => 'Sungai Manau   Jambu',
            ),

            array (
                'code' => '15.02.05',
                'name' => 'Tabir',
            ),

            array (
                'code' => '15.02.06',
                'name' => 'Pamenang',
            ),

            array (
                'code' => '15.02.07',
                'name' => 'Tabir Ulu',
            ),

            array (
                'code' => '15.02.08',
                'name' => 'Tabir Selatan',
            ),

            array (
                'code' => '15.02.09',
                'name' => 'Lembah Masurai',
            ),

            array (
                'code' => '15.02.10',
                'name' => 'Bangko  Barat',
            ),

            array (
                'code' => '15.02.11',
                'name' => 'Nalo Tatan',
            ),

            array (
                'code' => '15.02.12',
                'name' => 'Batang Masumai',
            ),

            array (
                'code' => '15.02.13',
                'name' => 'Pamenang Barat',
            ),

            array (
                'code' => '15.02.14',
                'name' => 'Tabir Ilir',
            ),

            array (
                'code' => '15.02.15',
                'name' => 'Tabir Timur',
            ),

            array (
                'code' => '15.02.16',
                'name' => 'Renah Pembarap',
            ),

            array (
                'code' => '15.02.17',
                'name' => 'Pangkalan Jambu',
            ),

            array (
                'code' => '15.02.18',
                'name' => 'Jangkat Timur',
            ),

            array (
                'code' => '15.02.19',
                'name' => 'Renah Pamenang',
            ),

            array (
                'code' => '15.02.20',
                'name' => 'Pamenang Selatan',
            ),

            array (
                'code' => '15.02.21',
                'name' => 'Margo Tabir',
            ),

            array (
                'code' => '15.02.22',
                'name' => 'Tabir Lintas',
            ),

            array (
                'code' => '15.02.23',
                'name' => 'Tabir Barat',
            ),

            array (
                'code' => '15.02.24',
                'name' => 'Tiang Pumpung',
            ),

            array (
                'code' => '15.03',
                'name' => 'KAB. SAROLANGUN',
            ),

            array (
                'code' => '15.03.01',
                'name' => 'Batang Asai',
            ),

            array (
                'code' => '15.03.02',
                'name' => 'Limun',
            ),

            array (
                'code' => '15.03.03',
                'name' => 'Sarolangun',
            ),

            array (
                'code' => '15.03.04',
                'name' => 'Pauh',
            ),

            array (
                'code' => '15.03.05',
                'name' => 'Pelawan',
            ),

            array (
                'code' => '15.03.06',
                'name' => 'Mandiangin',
            ),

            array (
                'code' => '15.03.07',
                'name' => 'Air Hitam',
            ),

            array (
                'code' => '15.03.08',
                'name' => 'Bathin VIII',
            ),

            array (
                'code' => '15.03.09',
                'name' => 'Singkut',
            ),

            array (
                'code' => '15.03.10',
                'name' => 'Cermin Nan Gedang',
            ),

            array (
                'code' => '15.04',
                'name' => 'KAB. BATANGHARI',
            ),

            array (
                'code' => '15.04.01',
                'name' => 'Mersam',
            ),

            array (
                'code' => '15.04.02',
                'name' => 'Muara Tembesi',
            ),

            array (
                'code' => '15.04.03',
                'name' => 'Muara Bulian',
            ),

            array (
                'code' => '15.04.04',
                'name' => 'Batin XXIV',
            ),

            array (
                'code' => '15.04.05',
                'name' => 'Pemayung',
            ),

            array (
                'code' => '15.04.06',
                'name' => 'Maro Sebo Ulu',
            ),

            array (
                'code' => '15.04.07',
                'name' => 'Bajubang',
            ),

            array (
                'code' => '15.04.08',
                'name' => 'Maro Sebo Ilir',
            ),

            array (
                'code' => '15.05',
                'name' => 'KAB.  MUARO JAMBI',
            ),

            array (
                'code' => '15.05.01',
                'name' => 'Jambi Luar Kota',
            ),

            array (
                'code' => '15.05.02',
                'name' => 'Sekernan',
            ),

            array (
                'code' => '15.05.03',
                'name' => 'Kumpeh',
            ),

            array (
                'code' => '15.05.04',
                'name' => 'Maro Sebo',
            ),

            array (
                'code' => '15.05.05',
                'name' => 'Mestong',
            ),

            array (
                'code' => '15.05.06',
                'name' => 'Kumpeh Ulu',
            ),

            array (
                'code' => '15.05.07',
                'name' => 'Sungai Bahar',
            ),

            array (
                'code' => '15.05.08',
                'name' => 'Sungai Gelam',
            ),

            array (
                'code' => '15.05.09',
                'name' => 'Bahar Utara',
            ),

            array (
                'code' => '15.05.10',
                'name' => 'Bahar Selatan',
            ),

            array (
                'code' => '15.05.11',
                'name' => 'Taman Rajo',
            ),

            array (
                'code' => '15.06',
                'name' => 'KAB. TANJUNG JABUNG BARAT',
            ),

            array (
                'code' => '15.06.01',
                'name' => 'Tungkal Ulu',
            ),

            array (
                'code' => '15.06.02',
                'name' => 'Tungkal Ilir',
            ),

            array (
                'code' => '15.06.03',
                'name' => 'Pengabuan',
            ),

            array (
                'code' => '15.06.04',
                'name' => 'Betara',
            ),

            array (
                'code' => '15.06.05',
                'name' => 'Merlung',
            ),

            array (
                'code' => '15.06.06',
                'name' => 'Tebing Tinggi',
            ),

            array (
                'code' => '15.06.07',
                'name' => 'Batang Asam',
            ),

            array (
                'code' => '15.06.08',
                'name' => 'Renah Mendaluh',
            ),

            array (
                'code' => '15.06.09',
                'name' => 'Muara Papalik',
            ),

            array (
                'code' => '15.06.10',
                'name' => 'Seberang Kota',
            ),

            array (
                'code' => '15.06.11',
                'name' => 'Bram Itam',
            ),

            array (
                'code' => '15.06.12',
                'name' => 'Kuala Betara',
            ),

            array (
                'code' => '15.06.13',
                'name' => 'Senyerang',
            ),

            array (
                'code' => '15.07',
                'name' => 'KAB. TANJUNG JABUNG TIMUR',
            ),

            array (
                'code' => '15.07.01',
                'name' => 'Muara Sabak Timur',
            ),

            array (
                'code' => '15.07.02',
                'name' => 'Nipah Panjang',
            ),

            array (
                'code' => '15.07.03',
                'name' => 'Mendahara',
            ),

            array (
                'code' => '15.07.04',
                'name' => 'Rantau Rasau',
            ),

            array (
                'code' => '15.07.05',
                'name' => 'S a d u',
            ),

            array (
                'code' => '15.07.06',
                'name' => 'Dendang',
            ),

            array (
                'code' => '15.07.07',
                'name' => 'Muara Sabak Barat',
            ),

            array (
                'code' => '15.07.08',
                'name' => 'Kuala Jambi',
            ),

            array (
                'code' => '15.07.09',
                'name' => 'Mendahara Ulu',
            ),

            array (
                'code' => '15.07.10',
                'name' => 'Geragai',
            ),

            array (
                'code' => '15.07.11',
                'name' => 'Berbak',
            ),

            array (
                'code' => '15.08',
                'name' => 'KAB. BUNGO',
            ),

            array (
                'code' => '15.08.01',
                'name' => 'Tanah Tumbuh',
            ),

            array (
                'code' => '15.08.02',
                'name' => 'Rantau Pandan',
            ),

            array (
                'code' => '15.08.03',
                'name' => 'Pasar Muaro Bungo',
            ),

            array (
                'code' => '15.08.04',
                'name' => 'Jujuhan',
            ),

            array (
                'code' => '15.08.05',
                'name' => 'Tanah Sepenggal',
            ),

            array (
                'code' => '15.08.06',
                'name' => 'Pelepat',
            ),

            array (
                'code' => '15.08.07',
                'name' => 'Limbur Lubuk Mengkuang',
            ),

            array (
                'code' => '15.08.08',
                'name' => 'Muko-muko Bathin VII',
            ),

            array (
                'code' => '15.08.09',
                'name' => 'Pelepat Ilir',
            ),

            array (
                'code' => '15.08.10',
                'name' => 'Batin II Babeko',
            ),

            array (
                'code' => '15.08.11',
                'name' => 'Bathin III',
            ),

            array (
                'code' => '15.08.12',
                'name' => 'Bungo Dani',
            ),

            array (
                'code' => '15.08.13',
                'name' => 'Rimbo Tengah',
            ),

            array (
                'code' => '15.08.14',
                'name' => 'Bathin III Ulu',
            ),

            array (
                'code' => '15.08.15',
                'name' => 'Bathin II Pelayang',
            ),

            array (
                'code' => '15.08.16',
                'name' => 'Jujuhan Ilir',
            ),

            array (
                'code' => '15.08.17',
                'name' => 'Tanah Sepenggal Lintas',
            ),

            array (
                'code' => '15.09',
                'name' => 'KAB. TEBO',
            ),

            array (
                'code' => '15.09.01',
                'name' => 'Tebo Tengah',
            ),

            array (
                'code' => '15.09.02',
                'name' => 'Tebo Ilir',
            ),

            array (
                'code' => '15.09.03',
                'name' => 'Tebo Ulu',
            ),

            array (
                'code' => '15.09.04',
                'name' => 'Rimbo Bujang',
            ),

            array (
                'code' => '15.09.05',
                'name' => 'Sumay',
            ),

            array (
                'code' => '15.09.06',
                'name' => 'VII Koto',
            ),

            array (
                'code' => '15.09.07',
                'name' => 'Rimbo Ulu',
            ),

            array (
                'code' => '15.09.08',
                'name' => 'Rimbo Ilir',
            ),

            array (
                'code' => '15.09.09',
                'name' => 'Tengah Ilir',
            ),

            array (
                'code' => '15.09.10',
                'name' => 'Serai Serumpun',
            ),

            array (
                'code' => '15.09.11',
                'name' => 'VII Koto Ilir',
            ),

            array (
                'code' => '15.09.12',
                'name' => 'Muara Tabir',
            ),

            array (
                'code' => '15.71',
                'name' => 'KOTA JAMBI',
            ),

            array (
                'code' => '15.71.01',
                'name' => 'Telanaipura',
            ),

            array (
                'code' => '15.71.02',
                'name' => 'Jambi Selatan',
            ),

            array (
                'code' => '15.71.03',
                'name' => 'Jambi Timur',
            ),

            array (
                'code' => '15.71.04',
                'name' => 'Pasar Jambi',
            ),

            array (
                'code' => '15.71.05',
                'name' => 'Pelayangan',
            ),

            array (
                'code' => '15.71.06',
                'name' => 'Danau Teluk',
            ),

            array (
                'code' => '15.71.07',
                'name' => 'Kota Baru',
            ),

            array (
                'code' => '15.71.08',
                'name' => 'Jelutung',
            ),

            array (
                'code' => '15.71.09',
                'name' => 'Alam Barajo',
            ),

            array (
                'code' => '15.71.10',
                'name' => 'Danau Sipin',
            ),

            array (
                'code' => '15.71.11',
                'name' => 'Paal Merah',
            ),

            array (
                'code' => '15.72',
                'name' => 'KOTA SUNGAI PENUH',
            ),

            array (
                'code' => '15.72.01',
                'name' => 'Sungai Penuh',
            ),

            array (
                'code' => '15.72.02',
                'name' => 'Pesisir Bukit',
            ),

            array (
                'code' => '15.72.03',
                'name' => 'Hamparan Rawang',
            ),

            array (
                'code' => '15.72.04',
                'name' => 'Tanah Kampung',
            ),

            array (
                'code' => '15.72.05',
                'name' => 'Kumun Debai',
            ),

            array (
                'code' => '15.72.06',
                'name' => 'Pondok Tinggi',
            ),

            array (
                'code' => '15.72.07',
                'name' => 'Koto Baru',
            ),

            array (
                'code' => '15.72.08',
                'name' => 'Sungai Bungkal',
            ),

            array (
                'code' => '16',
                'name' => 'SUMATERA SELATAN',
            ),

            array (
                'code' => '16.01',
                'name' => 'KAB. OGAN KOMERING ULU',
            ),

            array (
                'code' => '16.01.07',
                'name' => 'Sosoh Buay Rayap',
            ),

            array (
                'code' => '16.01.08',
                'name' => 'Pengandonan',
            ),

            array (
                'code' => '16.01.09',
                'name' => 'Peninjauan',
            ),

            array (
                'code' => '16.01.13',
                'name' => 'Baturaja Barat',
            ),

            array (
                'code' => '16.01.14',
                'name' => 'Baturaja Timur',
            ),

            array (
                'code' => '16.01.20',
                'name' => 'Ulu Ogan',
            ),

            array (
                'code' => '16.01.21',
                'name' => 'Semidang Aji',
            ),

            array (
                'code' => '16.01.22',
                'name' => 'Lubuk Batang',
            ),

            array (
                'code' => '16.01.28',
                'name' => 'Lengkiti',
            ),

            array (
                'code' => '16.01.29',
                'name' => 'Sinar Peninjauan',
            ),

            array (
                'code' => '16.01.30',
                'name' => 'Lubuk Raja',
            ),

            array (
                'code' => '16.01.31',
                'name' => 'Muara Jaya',
            ),

            array (
                'code' => '16.02',
                'name' => 'KAB. OGAN KOMERING ILIR',
            ),

            array (
                'code' => '16.02.02',
                'name' => 'Tanjung Lubuk',
            ),

            array (
                'code' => '16.02.03',
                'name' => 'Pedamaran',
            ),

            array (
                'code' => '16.02.04',
                'name' => 'Mesuji',
            ),

            array (
                'code' => '16.02.05',
                'name' => 'Kayu Agung',
            ),

            array (
                'code' => '16.02.08',
                'name' => 'Sirah Pulau Padang',
            ),

            array (
                'code' => '16.02.11',
                'name' => 'Tulung Selapan',
            ),

            array (
                'code' => '16.02.12',
                'name' => 'Pampangan',
            ),

            array (
                'code' => '16.02.13',
                'name' => 'Lempuing',
            ),

            array (
                'code' => '16.02.14',
                'name' => 'Air Sugihan',
            ),

            array (
                'code' => '16.02.15',
                'name' => 'Sungai Menang',
            ),

            array (
                'code' => '16.02.17',
                'name' => 'Jejawi',
            ),

            array (
                'code' => '16.02.18',
                'name' => 'Cengal',
            ),

            array (
                'code' => '16.02.19',
                'name' => 'Pangkalan Lampam',
            ),

            array (
                'code' => '16.02.20',
                'name' => 'Mesuji Makmur',
            ),

            array (
                'code' => '16.02.21',
                'name' => 'Mesuji Raya',
            ),

            array (
                'code' => '16.02.22',
                'name' => 'Lempuing Jaya',
            ),

            array (
                'code' => '16.02.23',
                'name' => 'Teluk Gelam',
            ),

            array (
                'code' => '16.02.24',
                'name' => 'Pedamaran Timur',
            ),

            array (
                'code' => '16.03',
                'name' => 'KAB. MUARA ENIM',
            ),

            array (
                'code' => '16.03.01',
                'name' => 'Tanjung Agung',
            ),

            array (
                'code' => '16.03.02',
                'name' => 'Muara Enim',
            ),

            array (
                'code' => '16.03.03',
                'name' => 'Rambang Dangku',
            ),

            array (
                'code' => '16.03.04',
                'name' => 'Gunung Megang',
            ),

            array (
                'code' => '16.03.06',
                'name' => 'Gelumbang',
            ),

            array (
                'code' => '16.03.07',
                'name' => 'Lawang Kidul',
            ),

            array (
                'code' => '16.03.08',
                'name' => 'Semende Darat Laut',
            ),

            array (
                'code' => '16.03.09',
                'name' => 'Semende Darat Tengah',
            ),

            array (
                'code' => '16.03.10',
                'name' => 'Semende Darat Ulu',
            ),

            array (
                'code' => '16.03.11',
                'name' => 'Ujan Mas',
            ),

            array (
                'code' => '16.03.14',
                'name' => 'Lubai',
            ),

            array (
                'code' => '16.03.15',
                'name' => 'Rambang',
            ),

            array (
                'code' => '16.03.16',
                'name' => 'Sungai Rotan',
            ),

            array (
                'code' => '16.03.17',
                'name' => 'Lembak',
            ),

            array (
                'code' => '16.03.19',
                'name' => 'Benakat',
            ),

            array (
                'code' => '16.03.21',
                'name' => 'Kelekar',
            ),

            array (
                'code' => '16.03.22',
                'name' => 'Muara Belida',
            ),

            array (
                'code' => '16.03.23',
                'name' => 'Belimbing',
            ),

            array (
                'code' => '16.03.24',
                'name' => 'Belida Darat',
            ),

            array (
                'code' => '16.03.25',
                'name' => 'Lubai Ulu',
            ),

            array (
                'code' => '16.04',
                'name' => 'KAB. LAHAT',
            ),

            array (
                'code' => '16.04.01',
                'name' => 'Tanjungsakti Pumu',
            ),

            array (
                'code' => '16.04.06',
                'name' => 'Jarai',
            ),

            array (
                'code' => '16.04.07',
                'name' => 'Kota Agung',
            ),

            array (
                'code' => '16.04.08',
                'name' => 'Pulaupinang',
            ),

            array (
                'code' => '16.04.09',
                'name' => 'Merapi Barat',
            ),

            array (
                'code' => '16.04.10',
                'name' => 'Lahat',
            ),

            array (
                'code' => '16.04.12',
                'name' => 'Pajar Bulan',
            ),

            array (
                'code' => '16.04.15',
                'name' => 'Mulak Ulu',
            ),

            array (
                'code' => '16.04.16',
                'name' => 'Kikim Selatan',
            ),

            array (
                'code' => '16.04.17',
                'name' => 'Kikim Timur',
            ),

            array (
                'code' => '16.04.18',
                'name' => 'Kikim Tengah',
            ),

            array (
                'code' => '16.04.19',
                'name' => 'Kikim Barat',
            ),

            array (
                'code' => '16.04.20',
                'name' => 'Pseksu',
            ),

            array (
                'code' => '16.04.21',
                'name' => 'Gumay Talang',
            ),

            array (
                'code' => '16.04.22',
                'name' => 'Pagar Gunung',
            ),

            array (
                'code' => '16.04.23',
                'name' => 'Merapi Timur',
            ),

            array (
                'code' => '16.04.24',
                'name' => 'Tanjung Sakti Pumi',
            ),

            array (
                'code' => '16.04.25',
                'name' => 'Gumay Ulu',
            ),

            array (
                'code' => '16.04.26',
                'name' => 'Merapi Selatan',
            ),

            array (
                'code' => '16.04.27',
                'name' => 'Tanjungtebat',
            ),

            array (
                'code' => '16.04.28',
                'name' => 'Muarapayang',
            ),

            array (
                'code' => '16.04.29',
                'name' => 'Sukamerindu',
            ),

            array (
                'code' => '16.05',
                'name' => 'KAB. MUSI RAWAS',
            ),

            array (
                'code' => '16.05.01',
                'name' => 'Tugumulyo',
            ),

            array (
                'code' => '16.05.02',
                'name' => 'Muara Lakitan',
            ),

            array (
                'code' => '16.05.03',
                'name' => 'Muara Kelingi',
            ),

            array (
                'code' => '16.05.08',
                'name' => 'Jayaloka',
            ),

            array (
                'code' => '16.05.09',
                'name' => 'Muara Beliti',
            ),

            array (
                'code' => '16.05.10',
                'name' => 'STL Ulu Terawas',
            ),

            array (
                'code' => '16.05.11',
                'name' => 'Selangit',
            ),

            array (
                'code' => '16.05.12',
                'name' => 'Megang Sakti',
            ),

            array (
                'code' => '16.05.13',
                'name' => 'Purwodadi',
            ),

            array (
                'code' => '16.05.14',
                'name' => 'BTS. Ulu  1  18',
            ),

            array (
                'code' => '16.05.18',
                'name' => 'Tiang Pumpung Kepungut',
            ),

            array (
                'code' => '16.05.19',
                'name' => 'Sumber Harta',
            ),

            array (
                'code' => '16.05.20',
                'name' => 'Tuah Negeri',
            ),

            array (
                'code' => '16.05.21',
                'name' => 'Suka Karya',
            ),

            array (
                'code' => '16.06',
                'name' => 'KAB. MUSI BANYUASIN',
            ),

            array (
                'code' => '16.06.01',
                'name' => 'Sekayu',
            ),

            array (
                'code' => '16.06.02',
                'name' => 'Lais',
            ),

            array (
                'code' => '16.06.03',
                'name' => 'Sungai Keruh',
            ),

            array (
                'code' => '16.06.04',
                'name' => 'Batang Hari Leko',
            ),

            array (
                'code' => '16.06.05',
                'name' => 'Sanga Desa',
            ),

            array (
                'code' => '16.06.06',
                'name' => 'Babat Toman',
            ),

            array (
                'code' => '16.06.07',
                'name' => 'Sungai Lilin',
            ),

            array (
                'code' => '16.06.08',
                'name' => 'Keluang',
            ),

            array (
                'code' => '16.06.09',
                'name' => 'Bayung Lencir',
            ),

            array (
                'code' => '16.06.10',
                'name' => 'Plakat Tinggi',
            ),

            array (
                'code' => '16.06.11',
                'name' => 'Lalan',
            ),

            array (
                'code' => '16.06.12',
                'name' => 'Tungkal Jaya',
            ),

            array (
                'code' => '16.06.13',
                'name' => 'Lawang Wetan',
            ),

            array (
                'code' => '16.06.14',
                'name' => 'Babat Supat',
            ),

            array (
                'code' => '16.07',
                'name' => 'KAB. BANYUASIN',
            ),

            array (
                'code' => '16.07.01',
                'name' => 'Banyuasin I',
            ),

            array (
                'code' => '16.07.02',
                'name' => 'Banyuasin II',
            ),

            array (
                'code' => '16.07.03',
                'name' => 'Banyuasin III',
            ),

            array (
                'code' => '16.07.04',
                'name' => 'Pulau Rimau',
            ),

            array (
                'code' => '16.07.05',
                'name' => 'Betung',
            ),

            array (
                'code' => '16.07.06',
                'name' => 'Rambutan',
            ),

            array (
                'code' => '16.07.07',
                'name' => 'Muara Padang',
            ),

            array (
                'code' => '16.07.08',
                'name' => 'Muara Telang',
            ),

            array (
                'code' => '16.07.09',
                'name' => 'Makarti Jaya',
            ),

            array (
                'code' => '16.07.10',
                'name' => 'Talang Kelapa',
            ),

            array (
                'code' => '16.07.11',
                'name' => 'Rantau Bayur',
            ),

            array (
                'code' => '16.07.12',
                'name' => 'Tanjung Lago',
            ),

            array (
                'code' => '16.07.13',
                'name' => 'Muara Sugihan',
            ),

            array (
                'code' => '16.07.14',
                'name' => 'Air Salek',
            ),

            array (
                'code' => '16.07.15',
                'name' => 'Tungkal Ilir',
            ),

            array (
                'code' => '16.07.16',
                'name' => 'Suak Tapeh',
            ),

            array (
                'code' => '16.07.17',
                'name' => 'Sembawa',
            ),

            array (
                'code' => '16.07.18',
                'name' => 'Sumber Marga Telang',
            ),

            array (
                'code' => '16.07.19',
                'name' => 'Air Kumbang',
            ),

            array (
                'code' => '16.08',
                'name' => 'KAB. OGAN KOMERING ULU TIMUR',
            ),

            array (
                'code' => '16.08.01',
                'name' => 'Martapura',
            ),

            array (
                'code' => '16.08.02',
                'name' => 'Buay Madang',
            ),

            array (
                'code' => '16.08.03',
                'name' => 'Belitang',
            ),

            array (
                'code' => '16.08.04',
                'name' => 'Cempaka',
            ),

            array (
                'code' => '16.08.05',
                'name' => 'Buay Pemuka Peliung',
            ),

            array (
                'code' => '16.08.06',
                'name' => 'Madang Suku II',
            ),

            array (
                'code' => '16.08.07',
                'name' => 'Madang Suku I',
            ),

            array (
                'code' => '16.08.08',
                'name' => 'Semendawai Suku III',
            ),

            array (
                'code' => '16.08.09',
                'name' => 'Belitang II',
            ),

            array (
                'code' => '16.08.10',
                'name' => 'Belitang III',
            ),

            array (
                'code' => '16.08.11',
                'name' => 'Bunga Mayang',
            ),

            array (
                'code' => '16.08.12',
                'name' => 'Buay Madang Timur',
            ),

            array (
                'code' => '16.08.13',
                'name' => 'Madang Suku III',
            ),

            array (
                'code' => '16.08.14',
                'name' => 'Semendawai Barat',
            ),

            array (
                'code' => '16.08.15',
                'name' => 'Semendawai Timur',
            ),

            array (
                'code' => '16.08.16',
                'name' => 'Jayapura',
            ),

            array (
                'code' => '16.08.17',
                'name' => 'Belitang Jaya',
            ),

            array (
                'code' => '16.08.18',
                'name' => 'Belitang Madang Raya',
            ),

            array (
                'code' => '16.08.19',
                'name' => 'Belitang Mulya',
            ),

            array (
                'code' => '16.08.20',
                'name' => 'Buay Pemuka Bangsa Raja',
            ),

            array (
                'code' => '16.09',
                'name' => 'KAB. OGAN KOMERING ULU SELATAN',
            ),

            array (
                'code' => '16.09.01',
                'name' => 'Muara Dua',
            ),

            array (
                'code' => '16.09.02',
                'name' => 'Pulau Beringin',
            ),

            array (
                'code' => '16.09.03',
                'name' => 'Banding Agung',
            ),

            array (
                'code' => '16.09.04',
                'name' => 'Muara Dua Kisam',
            ),

            array (
                'code' => '16.09.05',
                'name' => 'Simpang',
            ),

            array (
                'code' => '16.09.06',
                'name' => 'Buay Sandang Aji',
            ),

            array (
                'code' => '16.09.07',
                'name' => 'Buay Runjung',
            ),

            array (
                'code' => '16.09.08',
                'name' => 'Mekakau Ilir',
            ),

            array (
                'code' => '16.09.09',
                'name' => 'Buay Pemaca',
            ),

            array (
                'code' => '16.09.10',
                'name' => 'Kisam Tinggi',
            ),

            array (
                'code' => '16.09.11',
                'name' => 'Kisam Ilir',
            ),

            array (
                'code' => '16.09.12',
                'name' => 'Buay Pematang Ribu Ranau Tengah',
            ),

            array (
                'code' => '16.09.13',
                'name' => 'Warkuk Ranau Selatan',
            ),

            array (
                'code' => '16.09.14',
                'name' => 'Runjung Agung',
            ),

            array (
                'code' => '16.09.15',
                'name' => 'Sungai Are',
            ),

            array (
                'code' => '16.09.16',
                'name' => 'Sindang Danau',
            ),

            array (
                'code' => '16.09.17',
                'name' => 'Buana Pemaca',
            ),

            array (
                'code' => '16.09.18',
                'name' => 'Tiga Dihaji',
            ),

            array (
                'code' => '16.09.19',
                'name' => 'Buay Rawan',
            ),

            array (
                'code' => '16.10',
                'name' => 'KAB. OGAN ILIR',
            ),

            array (
                'code' => '16.10.01',
                'name' => 'Muara Kuang',
            ),

            array (
                'code' => '16.10.02',
                'name' => 'Tanjung Batu',
            ),

            array (
                'code' => '16.10.03',
                'name' => 'Tanjung Raja',
            ),

            array (
                'code' => '16.10.04',
                'name' => 'Indralaya',
            ),

            array (
                'code' => '16.10.05',
                'name' => 'Pemulutan',
            ),

            array (
                'code' => '16.10.06',
                'name' => 'Rantau Alai',
            ),

            array (
                'code' => '16.10.07',
                'name' => 'Indralaya Utara',
            ),

            array (
                'code' => '16.10.08',
                'name' => 'Indralaya Selatan',
            ),

            array (
                'code' => '16.10.09',
                'name' => 'Pemulutan Selatan',
            ),

            array (
                'code' => '16.10.10',
                'name' => 'Pemulutan Barat',
            ),

            array (
                'code' => '16.10.11',
                'name' => 'Rantau Panjang',
            ),

            array (
                'code' => '16.10.12',
                'name' => 'Sungai Pinang',
            ),

            array (
                'code' => '16.10.13',
                'name' => 'Kandis',
            ),

            array (
                'code' => '16.10.14',
                'name' => 'Rambang Kuang',
            ),

            array (
                'code' => '16.10.15',
                'name' => 'Lubuk Keliat',
            ),

            array (
                'code' => '16.10.16',
                'name' => 'Payaraman',
            ),

            array (
                'code' => '16.11',
                'name' => 'KAB. EMPAT LAWANG',
            ),

            array (
                'code' => '16.11.01',
                'name' => 'Muara Pinang',
            ),

            array (
                'code' => '16.11.02',
                'name' => 'Pendopo',
            ),

            array (
                'code' => '16.11.03',
                'name' => 'Ulu Musi',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '16.11.04',
                'name' => 'Tebing Tinggi',
            ),

            array (
                'code' => '16.11.05',
                'name' => 'Lintang Kanan',
            ),

            array (
                'code' => '16.11.06',
                'name' => 'Talang Padang',
            ),

            array (
                'code' => '16.11.07',
                'name' => 'Pasemah Air Keruh',
            ),

            array (
                'code' => '16.11.08',
                'name' => 'Sikap Dalam',
            ),

            array (
                'code' => '16.11.09',
                'name' => 'Saling',
            ),

            array (
                'code' => '16.11.10',
                'name' => 'Pendopo Barat',
            ),

            array (
                'code' => '16.12',
                'name' => 'KAB. PENUKAL ABAB LEMATANG ILIR',
            ),

            array (
                'code' => '16.12.01',
                'name' => 'Talang Ubi',
            ),

            array (
                'code' => '16.12.02',
                'name' => 'Penukal Utara',
            ),

            array (
                'code' => '16.12.03',
                'name' => 'Penukal',
            ),

            array (
                'code' => '16.12.04',
                'name' => 'Abab',
            ),

            array (
                'code' => '16.12.05',
                'name' => 'Tanah Abang',
            ),

            array (
                'code' => '16.13',
                'name' => 'KAB. MUSI RAWAS UTARA',
            ),

            array (
                'code' => '16.13.01',
                'name' => 'Rupit',
            ),

            array (
                'code' => '16.13.02',
                'name' => 'Rawas Ulu',
            ),

            array (
                'code' => '16.13.03',
                'name' => 'Nibung',
            ),

            array (
                'code' => '16.13.04',
                'name' => 'Rawas Ilir',
            ),

            array (
                'code' => '16.13.05',
                'name' => 'Karang Dapo',
            ),

            array (
                'code' => '16.13.06',
                'name' => 'Karang Jaya',
            ),

            array (
                'code' => '16.13.07',
                'name' => 'Ulu Rawas',
            ),

            array (
                'code' => '16.71',
                'name' => 'KOTA PALEMBANG',
            ),

            array (
                'code' => '16.71.01',
                'name' => 'Ilir Barat II',
            ),

            array (
                'code' => '16.71.02',
                'name' => 'Seberang Ulu I',
            ),

            array (
                'code' => '16.71.03',
                'name' => 'Seberang Ulu II',
            ),

            array (
                'code' => '16.71.04',
                'name' => 'Ilir Barat I',
            ),

            array (
                'code' => '16.71.05',
                'name' => 'Ilir Timur I',
            ),

            array (
                'code' => '16.71.06',
                'name' => 'Ilir Timur II',
            ),

            array (
                'code' => '16.71.07',
                'name' => 'Sukarami',
            ),

            array (
                'code' => '16.71.08',
                'name' => 'Sako',
            ),

            array (
                'code' => '16.71.09',
                'name' => 'Kemuning',
            ),

            array (
                'code' => '16.71.10',
                'name' => 'Kalidoni',
            ),

            array (
                'code' => '16.71.11',
                'name' => 'Bukit Kecil',
            ),

            array (
                'code' => '16.71.12',
                'name' => 'Gandus',
            ),

            array (
                'code' => '16.71.13',
                'name' => 'Kertapati',
            ),

            array (
                'code' => '16.71.14',
                'name' => 'Plaju',
            ),

            array (
                'code' => '16.71.15',
                'name' => 'Alang-alang Lebar',
            ),

            array (
                'code' => '16.71.16',
                'name' => 'Sematang Borang',
            ),

            array (
                'code' => '16.72',
                'name' => 'KOTA PAGAR ALAM',
            ),

            array (
                'code' => '16.72.01',
                'name' => 'Pagar Alam Utara',
            ),

            array (
                'code' => '16.72.02',
                'name' => 'Pagar Alam Selatan',
            ),

            array (
                'code' => '16.72.03',
                'name' => 'Dempo Utara',
            ),

            array (
                'code' => '16.72.04',
                'name' => 'Dempo Selatan',
            ),

            array (
                'code' => '16.72.05',
                'name' => 'Dempo Tengah',
            ),

            array (
                'code' => '16.73',
                'name' => 'KOTA LUBUK LINGGAU',
            ),

            array (
                'code' => '16.73.01',
                'name' => 'Lubuk Linggau Timur I',
            ),

            array (
                'code' => '16.73.02',
                'name' => 'Lubuk Linggau Barat I',
            ),

            array (
                'code' => '16.73.03',
                'name' => 'Lubuk Linggau Selatan I',
            ),

            array (
                'code' => '16.73.04',
                'name' => 'Lubuk Linggau Utara I',
            ),

            array (
                'code' => '16.73.05',
                'name' => 'Lubuk Linggau Timur II',
            ),

            array (
                'code' => '16.73.06',
                'name' => 'Lubuk Linggau Barat II',
            ),

            array (
                'code' => '16.73.07',
                'name' => 'Lubuk Linggau Selatan II',
            ),

            array (
                'code' => '16.73.08',
                'name' => 'Lubuk Linggau Utara II',
            ),

            array (
                'code' => '16.74',
                'name' => 'KOTA PRABUMULIH',
            ),

            array (
                'code' => '16.74.01',
                'name' => 'Prabumulih Barat',
            ),

            array (
                'code' => '16.74.02',
                'name' => 'Prabumulih Timur',
            ),

            array (
                'code' => '16.74.03',
                'name' => 'Cambai',
            ),

            array (
                'code' => '16.74.04',
                'name' => 'Rambang Kpk Tengah',
            ),

            array (
                'code' => '16.74.05',
                'name' => 'Prabumulih Utara',
            ),

            array (
                'code' => '16.74.06',
                'name' => 'Prabumulih Selatan',
            ),

            array (
                'code' => '17',
                'name' => 'BENGKULU',
            ),

            array (
                'code' => '17.01',
                'name' => 'KAB. BENGKULU SELATAN',
            ),

            array (
                'code' => '17.01.01',
                'name' => 'Kedurang',
            ),

            array (
                'code' => '17.01.02',
                'name' => 'Seginim',
            ),

            array (
                'code' => '17.01.03',
                'name' => 'Pino',
            ),

            array (
                'code' => '17.01.04',
                'name' => 'Manna',
            ),

            array (
                'code' => '17.01.05',
                'name' => 'Kota Manna',
            ),

            array (
                'code' => '17.01.06',
                'name' => 'Pino Raya',
            ),

            array (
                'code' => '17.01.07',
                'name' => 'Kedurang Ilir',
            ),

            array (
                'code' => '17.01.08',
                'name' => 'Air Nipis',
            ),

            array (
                'code' => '17.01.09',
                'name' => 'Ulu Manna',
            ),

            array (
                'code' => '17.01.10',
                'name' => 'Bunga Mas',
            ),

            array (
                'code' => '17.01.11',
                'name' => 'Pasar Manna',
            ),

            array (
                'code' => '17.02',
                'name' => 'KAB. REJANG LEBONG',
            ),

            array (
                'code' => '17.02.06',
                'name' => 'Kota Padang',
            ),

            array (
                'code' => '17.02.07',
                'name' => 'Padang Ulak Tanding',
            ),

            array (
                'code' => '17.02.08',
                'name' => 'Sindang Kelingi',
            ),

            array (
                'code' => '17.02.09',
                'name' => 'Curup',
            ),

            array (
                'code' => '17.02.10',
                'name' => 'Bermani Ulu',
            ),

            array (
                'code' => '17.02.11',
                'name' => 'Selupu Rejang',
            ),

            array (
                'code' => '17.02.16',
                'name' => 'Curup Utara',
            ),

            array (
                'code' => '17.02.17',
                'name' => 'Curup Timur',
            ),

            array (
                'code' => '17.02.18',
                'name' => 'Curup Selatan',
            ),

            array (
                'code' => '17.02.19',
                'name' => 'Curup Tengah',
            ),

            array (
                'code' => '17.02.20',
                'name' => 'Binduriang',
            ),

            array (
                'code' => '17.02.21',
                'name' => 'Sindang Beliti Ulu',
            ),

            array (
                'code' => '17.02.22',
                'name' => 'Sindang Dataran',
            ),

            array (
                'code' => '17.02.23',
                'name' => 'Sindang Beliti Ilir',
            ),

            array (
                'code' => '17.02.24',
                'name' => 'Bermani Ulu Raya',
            ),

            array (
                'code' => '17.03',
                'name' => 'KAB. BENGKULU UTARA',
            ),

            array (
                'code' => '17.03.01',
                'name' => 'Enggano',
            ),

            array (
                'code' => '17.03.06',
                'name' => 'Kerkap',
            ),

            array (
                'code' => '17.03.07',
                'name' => 'Kota Arga Makmur',
            ),

            array (
                'code' => '17.03.08',
                'name' => 'Giri Mulya',
            ),

            array (
                'code' => '17.03.09',
                'name' => 'Padang Jaya',
            ),

            array (
                'code' => '17.03.10',
                'name' => 'Lais',
            ),

            array (
                'code' => '17.03.11',
                'name' => 'Batik Nau',
            ),

            array (
                'code' => '17.03.12',
                'name' => 'Ketahun',
            ),

            array (
                'code' => '17.03.13',
                'name' => 'Napal Putih',
            ),

            array (
                'code' => '17.03.14',
                'name' => 'Putri Hijau',
            ),

            array (
                'code' => '17.03.15',
                'name' => 'Air Besi',
            ),

            array (
                'code' => '17.03.16',
                'name' => 'Air Napal',
            ),

            array (
                'code' => '17.03.19',
                'name' => 'Hulu Palik',
            ),

            array (
                'code' => '17.03.20',
                'name' => 'Air Padang',
            ),

            array (
                'code' => '17.03.21',
                'name' => 'Arma Jaya',
            ),

            array (
                'code' => '17.03.22',
                'name' => 'Tanjung Agung Palik',
            ),

            array (
                'code' => '17.03.23',
                'name' => 'Ulok Kupai',
            ),

            array (
                'code' => '17.03.24',
                'name' => 'Pinang Raya',
            ),

            array (
                'code' => '17.03.25',
                'name' => 'Marga Sakti Sebelat',
            ),

            array (
                'code' => '17.04',
                'name' => 'KAB. KAUR',
            ),

            array (
                'code' => '17.04.01',
                'name' => 'Kinal',
            ),

            array (
                'code' => '17.04.02',
                'name' => 'Tanjung Kemuning',
            ),

            array (
                'code' => '17.04.03',
                'name' => 'Kaur Utara',
            ),

            array (
                'code' => '17.04.04',
                'name' => 'Kaur Tengah',
            ),

            array (
                'code' => '17.04.05',
                'name' => 'Kaur Selatan',
            ),

            array (
                'code' => '17.04.06',
                'name' => 'Maje',
            ),

            array (
                'code' => '17.04.07',
                'name' => 'Nasal',
            ),

            array (
                'code' => '17.04.08',
                'name' => 'Semidang Gumay',
            ),

            array (
                'code' => '17.04.09',
                'name' => 'Kelam Tengah',
            ),

            array (
                'code' => '17.04.10',
                'name' => 'Luas',
            ),

            array (
                'code' => '17.04.11',
                'name' => 'Muara Sahung',
            ),

            array (
                'code' => '17.04.12',
                'name' => 'Tetap',
            ),

            array (
                'code' => '17.04.13',
                'name' => 'Lungkang Kule',
            ),

            array (
                'code' => '17.04.14',
                'name' => 'Padang Guci Hilir',
            ),

            array (
                'code' => '17.04.15',
                'name' => 'Padang Guci Hulu',
            ),

            array (
                'code' => '17.05',
                'name' => 'KAB. SELUMA',
            ),

            array (
                'code' => '17.05.01',
                'name' => 'Sukaraja',
            ),

            array (
                'code' => '17.05.02',
                'name' => 'Seluma',
            ),

            array (
                'code' => '17.05.03',
                'name' => 'Talo',
            ),

            array (
                'code' => '17.05.04',
                'name' => 'Semidang Alas',
            ),

            array (
                'code' => '17.05.05',
                'name' => 'Semidang Alas Maras',
            ),

            array (
                'code' => '17.05.06',
                'name' => 'Air Periukan',
            ),

            array (
                'code' => '17.05.07',
                'name' => 'Lubuk Sandi',
            ),

            array (
                'code' => '17.05.08',
                'name' => 'Seluma Barat',
            ),

            array (
                'code' => '17.05.09',
                'name' => 'Seluma Timur',
            ),

            array (
                'code' => '17.05.10',
                'name' => 'Seluma Utara',
            ),

            array (
                'code' => '17.05.11',
                'name' => 'Seluma Selatan',
            ),

            array (
                'code' => '17.05.12',
                'name' => 'Talo Kecil',
            ),

            array (
                'code' => '17.05.13',
                'name' => 'Ulu Talo',
            ),

            array (
                'code' => '17.05.14',
                'name' => 'Ilir Talo',
            ),

            array (
                'code' => '17.06',
                'name' => 'KAB. MUKO MUKO',
            ),

            array (
                'code' => '17.06.01',
                'name' => 'Lubuk Pinang',
            ),

            array (
                'code' => '17.06.02',
                'name' => 'Kota Mukomuko',
            ),

            array (
                'code' => '17.06.03',
                'name' => 'Teras Terunjam',
            ),

            array (
                'code' => '17.06.04',
                'name' => 'Pondok Suguh',
            ),

            array (
                'code' => '17.06.05',
                'name' => 'Ipuh',
            ),

            array (
                'code' => '17.06.06',
                'name' => 'Malin Deman',
            ),

            array (
                'code' => '17.06.07',
                'name' => 'Air Rami',
            ),

            array (
                'code' => '17.06.08',
                'name' => 'Teramang Jaya',
            ),

            array (
                'code' => '17.06.09',
                'name' => 'Selagan Raya',
            ),

            array (
                'code' => '17.06.10',
                'name' => 'Penarik',
            ),

            array (
                'code' => '17.06.11',
                'name' => 'XIV Koto',
            ),

            array (
                'code' => '17.06.12',
                'name' => 'V Koto',
            ),

            array (
                'code' => '17.06.13',
                'name' => 'Air Majunto',
            ),

            array (
                'code' => '17.06.14',
                'name' => 'Air Dikit',
            ),

            array (
                'code' => '17.06.15',
                'name' => 'Sungai Rumbai',
            ),

            array (
                'code' => '17.07',
                'name' => 'KAB. LEBONG',
            ),

            array (
                'code' => '17.07.01',
                'name' => 'Lebong Utara',
            ),

            array (
                'code' => '17.07.02',
                'name' => 'Lebong Atas',
            ),

            array (
                'code' => '17.07.03',
                'name' => 'Lebong Tengah',
            ),

            array (
                'code' => '17.07.04',
                'name' => 'Lebong Selatan',
            ),

            array (
                'code' => '17.07.05',
                'name' => 'Rimbo Pengadang',
            ),

            array (
                'code' => '17.07.06',
                'name' => 'Topos',
            ),

            array (
                'code' => '17.07.07',
                'name' => 'Bingin Kuning',
            ),

            array (
                'code' => '17.07.08',
                'name' => 'Lebong Sakti',
            ),

            array (
                'code' => '17.07.09',
                'name' => 'Pelabai',
            ),

            array (
                'code' => '17.07.10',
                'name' => 'Amen',
            ),

            array (
                'code' => '17.07.11',
                'name' => 'Uram Jaya',
            ),

            array (
                'code' => '17.07.12',
                'name' => 'Pinang Belapis',
            ),

            array (
                'code' => '17.08',
                'name' => 'KAB. KEPAHIANG',
            ),

            array (
                'code' => '17.08.01',
                'name' => 'Bermani Ilir',
            ),

            array (
                'code' => '17.08.02',
                'name' => 'Ujan Mas',
            ),

            array (
                'code' => '17.08.03',
                'name' => 'Tebat Karai',
            ),

            array (
                'code' => '17.08.04',
                'name' => 'Kepahiang',
            ),

            array (
                'code' => '17.08.05',
                'name' => 'Merigi',
            ),

            array (
                'code' => '17.08.06',
                'name' => 'Kebawetan',
            ),

            array (
                'code' => '17.08.07',
                'name' => 'Seberang Musi',
            ),

            array (
                'code' => '17.08.08',
                'name' => 'Muara Kemumu',
            ),

            array (
                'code' => '17.09',
                'name' => 'KAB. BENGKULU TENGAH',
            ),

            array (
                'code' => '17.09.01',
                'name' => 'Karang Tinggi',
            ),

            array (
                'code' => '17.09.02',
                'name' => 'Talang Empat',
            ),

            array (
                'code' => '17.09.03',
                'name' => 'Pondok Kelapa',
            ),

            array (
                'code' => '17.09.04',
                'name' => 'Pematang Tiga',
            ),

            array (
                'code' => '17.09.05',
                'name' => 'Pagar Jati',
            ),

            array (
                'code' => '17.09.06',
                'name' => 'Taba Penanjung',
            ),

            array (
                'code' => '17.09.07',
                'name' => 'Merigi Kelindang',
            ),

            array (
                'code' => '17.09.08',
                'name' => 'Merigi Sakti',
            ),

            array (
                'code' => '17.09.09',
                'name' => 'Pondok Kubang',
            ),

            array (
                'code' => '17.09.10',
                'name' => 'Bang Haji',
            ),

            array (
                'code' => '17.71',
                'name' => 'KOTA BENGKULU',
            ),

            array (
                'code' => '17.71.01',
                'name' => 'Selebar',
            ),

            array (
                'code' => '17.71.02',
                'name' => 'Gading Cempaka',
            ),

            array (
                'code' => '17.71.03',
                'name' => 'Teluk Segara',
            ),

            array (
                'code' => '17.71.04',
                'name' => 'Muara Bangka Hulu',
            ),

            array (
                'code' => '17.71.05',
                'name' => 'Kampung Melayu',
            ),

            array (
                'code' => '17.71.06',
                'name' => 'Ratu Agung',
            ),

            array (
                'code' => '17.71.07',
                'name' => 'Ratu Samban',
            ),

            array (
                'code' => '17.71.08',
                'name' => 'Sungai Serut',
            ),

            array (
                'code' => '17.71.09',
                'name' => 'Singaran Pati',
            ),

            array (
                'code' => '18',
                'name' => 'LAMPUNG',
            ),

            array (
                'code' => '18.01',
                'name' => 'KAB. LAMPUNG SELATAN',
            ),

            array (
                'code' => '18.01.04',
                'name' => 'Natar',
            ),

            array (
                'code' => '18.01.05',
                'name' => 'Tanjung Bintang',
            ),

            array (
                'code' => '18.01.06',
                'name' => 'Kalianda',
            ),

            array (
                'code' => '18.01.07',
                'name' => 'Sidomulyo',
            ),

            array (
                'code' => '18.01.08',
                'name' => 'Katibung',
            ),

            array (
                'code' => '18.01.09',
                'name' => 'Penengahan',
            ),

            array (
                'code' => '18.01.10',
                'name' => 'Palas',
            ),

            array (
                'code' => '18.01.13',
                'name' => 'Jati Agung',
            ),

            array (
                'code' => '18.01.14',
                'name' => 'Ketapang',
            ),

            array (
                'code' => '18.01.15',
                'name' => 'Sragi',
            ),

            array (
                'code' => '18.01.16',
                'name' => 'Raja Basa',
            ),

            array (
                'code' => '18.01.17',
                'name' => 'Candipuro',
            ),

            array (
                'code' => '18.01.18',
                'name' => 'Merbau Mataram',
            ),

            array (
                'code' => '18.01.21',
                'name' => 'Bakauheni',
            ),

            array (
                'code' => '18.01.22',
                'name' => 'Tanjung Sari',
            ),

            array (
                'code' => '18.01.23',
                'name' => 'Way Sulan',
            ),

            array (
                'code' => '18.01.24',
                'name' => 'Way Panji',
            ),

            array (
                'code' => '18.02',
                'name' => 'KAB. LAMPUNG TENGAH',
            ),

            array (
                'code' => '18.02.01',
                'name' => 'Kalirejo',
            ),

            array (
                'code' => '18.02.02',
                'name' => 'Bangun Rejo',
            ),

            array (
                'code' => '18.02.03',
                'name' => 'Padang Ratu',
            ),

            array (
                'code' => '18.02.04',
                'name' => 'Gunung Sugih',
            ),

            array (
                'code' => '18.02.05',
                'name' => 'Trimurjo',
            ),

            array (
                'code' => '18.02.06',
                'name' => 'Punggur',
            ),

            array (
                'code' => '18.02.07',
                'name' => 'Terbanggi Besar',
            ),

            array (
                'code' => '18.02.08',
                'name' => 'Seputih Raman',
            ),

            array (
                'code' => '18.02.09',
                'name' => 'Rumbia',
            ),

            array (
                'code' => '18.02.10',
                'name' => 'Seputih Banyak',
            ),

            array (
                'code' => '18.02.11',
                'name' => 'Seputih Mataram',
            ),

            array (
                'code' => '18.02.12',
                'name' => 'Seputih Surabaya',
            ),

            array (
                'code' => '18.02.13',
                'name' => 'Terusan Nunyai',
            ),

            array (
                'code' => '18.02.14',
                'name' => 'Bumi Ratu Nuban',
            ),

            array (
                'code' => '18.02.15',
                'name' => 'Bekri',
            ),

            array (
                'code' => '18.02.16',
                'name' => 'Seputih Agung',
            ),

            array (
                'code' => '18.02.17',
                'name' => 'Way Pangubuan',
            ),

            array (
                'code' => '18.02.18',
                'name' => 'Bandar Mataram',
            ),

            array (
                'code' => '18.02.19',
                'name' => 'Pubian',
            ),

            array (
                'code' => '18.02.20',
                'name' => 'Selagai Lingga',
            ),

            array (
                'code' => '18.02.21',
                'name' => 'Anak Tuha',
            ),

            array (
                'code' => '18.02.22',
                'name' => 'Sendang Agung',
            ),

            array (
                'code' => '18.02.23',
                'name' => 'Kota Gajah',
            ),

            array (
                'code' => '18.02.24',
                'name' => 'Bumi Nabung',
            ),

            array (
                'code' => '18.02.25',
                'name' => 'Way Seputih',
            ),

            array (
                'code' => '18.02.26',
                'name' => 'Bandar Surabaya',
            ),

            array (
                'code' => '18.02.27',
                'name' => 'Anak Ratu Aji',
            ),

            array (
                'code' => '18.02.28',
                'name' => 'Putra Rumbia',
            ),

            array (
                'code' => '18.03',
                'name' => 'KAB. LAMPUNG UTARA',
            ),

            array (
                'code' => '18.03.01',
                'name' => 'Bukit Kemuning',
            ),

            array (
                'code' => '18.03.02',
                'name' => 'Kotabumi',
            ),

            array (
                'code' => '18.03.03',
                'name' => 'Sungkai Selatan',
            ),

            array (
                'code' => '18.03.04',
                'name' => 'Tanjung Raja',
            ),

            array (
                'code' => '18.03.05',
                'name' => 'Abung Timur',
            ),

            array (
                'code' => '18.03.06',
                'name' => 'Abung Barat',
            ),

            array (
                'code' => '18.03.07',
                'name' => 'Abung Selatan',
            ),

            array (
                'code' => '18.03.08',
                'name' => 'Sungkai Utara',
            ),

            array (
                'code' => '18.03.09',
                'name' => 'Kotabumi Utara',
            ),

            array (
                'code' => '18.03.10',
                'name' => 'Kotabumi Selatan',
            ),

            array (
                'code' => '18.03.11',
                'name' => 'Abung Tengah',
            ),

            array (
                'code' => '18.03.12',
                'name' => 'Abung Tinggi',
            ),

            array (
                'code' => '18.03.13',
                'name' => 'Abung Semuli',
            ),

            array (
                'code' => '18.03.14',
                'name' => 'Abung Surakarta',
            ),

            array (
                'code' => '18.03.15',
                'name' => 'Muara Sungkai',
            ),

            array (
                'code' => '18.03.16',
                'name' => 'Bunga Mayang',
            ),

            array (
                'code' => '18.03.17',
                'name' => 'Hulu Sungkai',
            ),

            array (
                'code' => '18.03.18',
                'name' => 'Sungkai Tengah',
            ),

            array (
                'code' => '18.03.19',
                'name' => 'Abung Pekurun',
            ),

            array (
                'code' => '18.03.20',
                'name' => 'Sungkai Jaya',
            ),

            array (
                'code' => '18.03.21',
                'name' => 'Sungkai Barat',
            ),

            array (
                'code' => '18.03.22',
                'name' => 'Abung Kunang',
            ),

            array (
                'code' => '18.03.23',
                'name' => 'Blambangan Pagar',
            ),

            array (
                'code' => '18.04',
                'name' => 'KAB. LAMPUNG BARAT',
            ),

            array (
                'code' => '18.04.04',
                'name' => 'Balik Bukit',
            ),

            array (
                'code' => '18.04.05',
                'name' => 'Sumber Jaya',
            ),

            array (
                'code' => '18.04.06',
                'name' => 'Belalau',
            ),

            array (
                'code' => '18.04.07',
                'name' => 'Way Tenong',
            ),

            array (
                'code' => '18.04.08',
                'name' => 'Sekincau',
            ),

            array (
                'code' => '18.04.09',
                'name' => 'Suoh',
            ),

            array (
                'code' => '18.04.10',
                'name' => 'Batu Brak',
            ),

            array (
                'code' => '18.04.11',
                'name' => 'Sukau',
            ),

            array (
                'code' => '18.04.15',
                'name' => 'Gedung Surian',
            ),

            array (
                'code' => '18.04.18',
                'name' => 'Kebun Tebu',
            ),

            array (
                'code' => '18.04.19',
                'name' => 'Air Hitam',
            ),

            array (
                'code' => '18.04.20',
                'name' => 'Pagar Dewa',
            ),

            array (
                'code' => '18.04.21',
                'name' => 'Batu Ketulis',
            ),

            array (
                'code' => '18.04.22',
                'name' => 'Lumbok Seminung',
            ),

            array (
                'code' => '18.04.23',
                'name' => 'Bandar Negeri Suoh',
            ),

            array (
                'code' => '18.05',
                'name' => 'KAB. TULANG BAWANG',
            ),

            array (
                'code' => '18.05.02',
                'name' => 'Menggala',
            ),

            array (
                'code' => '18.05.06',
                'name' => 'Gedung Aji',
            ),

            array (
                'code' => '18.05.08',
                'name' => 'Banjar Agung',
            ),

            array (
                'code' => '18.05.11',
                'name' => 'Gedung Meneng',
            ),

            array (
                'code' => '18.05.12',
                'name' => 'Rawa Jitu Selatan',
            ),

            array (
                'code' => '18.05.13',
                'name' => 'Penawar Tama',
            ),

            array (
                'code' => '18.05.18',
                'name' => 'Rawa Jitu Timur',
            ),

            array (
                'code' => '18.05.20',
                'name' => 'Banjar Margo',
            ),

            array (
                'code' => '18.05.22',
                'name' => 'Rawa Pitu',
            ),

            array (
                'code' => '18.05.23',
                'name' => 'Penawar Aji',
            ),

            array (
                'code' => '18.05.25',
                'name' => 'Dente Teladas',
            ),

            array (
                'code' => '18.05.26',
                'name' => 'Meraksa Aji',
            ),

            array (
                'code' => '18.05.27',
                'name' => 'Gedung Aji Baru',
            ),

            array (
                'code' => '18.05.29',
                'name' => 'Banjar Baru',
            ),

            array (
                'code' => '18.05.30',
                'name' => 'Menggala Timur',
            ),

            array (
                'code' => '18.06',
                'name' => 'KAB. TANGGAMUS',
            ),

            array (
                'code' => '18.06.01',
                'name' => 'Kota Agung',
            ),

            array (
                'code' => '18.06.02',
                'name' => 'Talang Padang',
            ),

            array (
                'code' => '18.06.03',
                'name' => 'Wonosobo',
            ),

            array (
                'code' => '18.06.04',
                'name' => 'Pulau Panggung',
            ),

            array (
                'code' => '18.06.09',
                'name' => 'Cukuh Balak',
            ),

            array (
                'code' => '18.06.11',
                'name' => 'Pugung',
            ),

            array (
                'code' => '18.06.12',
                'name' => 'Semaka',
            ),

            array (
                'code' => '18.06.13',
                'name' => 'Sumber Rejo',
            ),

            array (
                'code' => '18.06.15',
                'name' => 'Ulu Belu',
            ),

            array (
                'code' => '18.06.16',
                'name' => 'Pematang Sawa',
            ),

            array (
                'code' => '18.06.17',
                'name' => 'Klumbayan',
            ),

            array (
                'code' => '18.06.18',
                'name' => 'Kota Agung Barat',
            ),

            array (
                'code' => '18.06.19',
                'name' => 'Kota Agung Timur',
            ),

            array (
                'code' => '18.06.20',
                'name' => 'Gisting',
            ),

            array (
                'code' => '18.06.21',
                'name' => 'Gunung Alip',
            ),

            array (
                'code' => '18.06.24',
                'name' => 'Limau',
            ),

            array (
                'code' => '18.06.25',
                'name' => 'Bandar Negeri Semuong',
            ),

            array (
                'code' => '18.06.26',
                'name' => 'Air Naningan',
            ),

            array (
                'code' => '18.06.27',
                'name' => 'Bulok',
            ),

            array (
                'code' => '18.06.28',
                'name' => 'Klumbayan Barat',
            ),

            array (
                'code' => '18.07',
                'name' => 'KAB. LAMPUNG TIMUR',
            ),

            array (
                'code' => '18.07.01',
                'name' => 'Sukadana',
            ),

            array (
                'code' => '18.07.02',
                'name' => 'Labuhan Maringgai',
            ),

            array (
                'code' => '18.07.03',
                'name' => 'Jabung',
            ),

            array (
                'code' => '18.07.04',
                'name' => 'Pekalongan',
            ),

            array (
                'code' => '18.07.05',
                'name' => 'Sekampung',
            ),

            array (
                'code' => '18.07.06',
                'name' => 'Batanghari',
            ),

            array (
                'code' => '18.07.07',
                'name' => 'Way Jepara',
            ),

            array (
                'code' => '18.07.08',
                'name' => 'Purbolinggo',
            ),

            array (
                'code' => '18.07.09',
                'name' => 'Raman Utara',
            ),

            array (
                'code' => '18.07.10',
                'name' => 'Metro Kibang',
            ),

            array (
                'code' => '18.07.11',
                'name' => 'Marga Tiga',
            ),

            array (
                'code' => '18.07.12',
                'name' => 'Sekampung Udik',
            ),

            array (
                'code' => '18.07.13',
                'name' => 'Batanghari Nuban',
            ),

            array (
                'code' => '18.07.14',
                'name' => 'Bumi Agung',
            ),

            array (
                'code' => '18.07.15',
                'name' => 'Bandar Sribhawono',
            ),

            array (
                'code' => '18.07.16',
                'name' => 'Mataram Baru',
            ),

            array (
                'code' => '18.07.17',
                'name' => 'Melinting',
            ),

            array (
                'code' => '18.07.18',
                'name' => 'Gunung Pelindung',
            ),

            array (
                'code' => '18.07.19',
                'name' => 'Pasir Sakti',
            ),

            array (
                'code' => '18.07.20',
                'name' => 'Waway Karya',
            ),

            array (
                'code' => '18.07.21',
                'name' => 'Labuhan Ratu',
            ),

            array (
                'code' => '18.07.22',
                'name' => 'Braja Selebah',
            ),

            array (
                'code' => '18.07.23',
                'name' => 'Way Bungur',
            ),

            array (
                'code' => '18.07.24',
                'name' => 'Marga Sekampung',
            ),

            array (
                'code' => '18.08',
                'name' => 'KAB. WAY KANAN',
            ),

            array (
                'code' => '18.08.01',
                'name' => 'Blambangan Umpu',
            ),

            array (
                'code' => '18.08.02',
                'name' => 'Kasui',
            ),

            array (
                'code' => '18.08.03',
                'name' => 'Banjit',
            ),

            array (
                'code' => '18.08.04',
                'name' => 'Baradatu',
            ),

            array (
                'code' => '18.08.05',
                'name' => 'Bahuga',
            ),

            array (
                'code' => '18.08.06',
                'name' => 'Pakuan Ratu',
            ),

            array (
                'code' => '18.08.07',
                'name' => 'Negeri Agung',
            ),

            array (
                'code' => '18.08.08',
                'name' => 'Way Tuba',
            ),

            array (
                'code' => '18.08.09',
                'name' => 'Rebang Tangkas',
            ),

            array (
                'code' => '18.08.10',
                'name' => 'Gunung Labuhan',
            ),

            array (
                'code' => '18.08.11',
                'name' => 'Negara Batin',
            ),

            array (
                'code' => '18.08.12',
                'name' => 'Negeri Besar',
            ),

            array (
                'code' => '18.08.13',
                'name' => 'Buay Bahuga',
            ),

            array (
                'code' => '18.08.14',
                'name' => 'Bumi Agung',
            ),

            array (
                'code' => '18.09',
                'name' => 'KAB. PESAWARAN',
            ),

            array (
                'code' => '18.09.01',
                'name' => 'Gedong Tataan',
            ),

            array (
                'code' => '18.09.02',
                'name' => 'Negeri Katon',
            ),

            array (
                'code' => '18.09.03',
                'name' => 'Tegineneng',
            ),

            array (
                'code' => '18.09.04',
                'name' => 'Way Lima',
            ),

            array (
                'code' => '18.09.05',
                'name' => 'Padang Cermin',
            ),

            array (
                'code' => '18.09.06',
                'name' => 'Punduh Pidada',
            ),

            array (
                'code' => '18.09.07',
                'name' => 'Kedondong',
            ),

            array (
                'code' => '18.09.08',
                'name' => 'Marga Punduh',
            ),

            array (
                'code' => '18.09.09',
                'name' => 'Way Khilau',
            ),

            array (
                'code' => '18.09.10',
                'name' => 'Teluk Pandan',
            ),

            array (
                'code' => '18.09.11',
                'name' => 'Way Ratai',
            ),

            array (
                'code' => '18.10',
                'name' => 'KAB. PRINGSEWU',
            ),

            array (
                'code' => '18.10.01',
                'name' => 'Pringsewu',
            ),

            array (
                'code' => '18.10.02',
                'name' => 'Gading Rejo',
            ),

            array (
                'code' => '18.10.03',
                'name' => 'Ambarawa',
            ),

            array (
                'code' => '18.10.04',
                'name' => 'Pardasuka',
            ),

            array (
                'code' => '18.10.05',
                'name' => 'Pagelaran',
            ),

            array (
                'code' => '18.10.06',
                'name' => 'Banyumas',
            ),

            array (
                'code' => '18.10.07',
                'name' => 'Adiluwih',
            ),

            array (
                'code' => '18.10.08',
                'name' => 'Sukoharjo',
            ),

            array (
                'code' => '18.10.09',
                'name' => 'Pagelaran Utara',
            ),

            array (
                'code' => '18.11',
                'name' => 'KAB. MESUJI',
            ),

            array (
                'code' => '18.11.01',
                'name' => 'Mesuji',
            ),

            array (
                'code' => '18.11.02',
                'name' => 'Mesuji Timur',
            ),

            array (
                'code' => '18.11.03',
                'name' => 'Rawa Jitu Utara',
            ),

            array (
                'code' => '18.11.04',
                'name' => 'Way Serdang',
            ),

            array (
                'code' => '18.11.05',
                'name' => 'Simpang Pematang',
            ),

            array (
                'code' => '18.11.06',
                'name' => 'Panca Jaya',
            ),

            array (
                'code' => '18.11.07',
                'name' => 'Tanjung Raya',
            ),

            array (
                'code' => '18.12',
                'name' => 'KAB. TULANG BAWANG BARAT',
            ),

            array (
                'code' => '18.12.01',
                'name' => 'Tulang Bawang Tengah',
            ),

            array (
                'code' => '18.12.02',
                'name' => 'Tumijajar',
            ),

            array (
                'code' => '18.12.03',
                'name' => 'Tulang Bawang Udik',
            ),

            array (
                'code' => '18.12.04',
                'name' => 'Gunung Terang',
            ),

            array (
                'code' => '18.12.05',
                'name' => 'Gunung Agung',
            ),

            array (
                'code' => '18.12.06',
                'name' => 'Way Kenanga',
            ),

            array (
                'code' => '18.12.07',
                'name' => 'Lambu Kibang',
            ),

            array (
                'code' => '18.12.08',
                'name' => 'Pagar Dewa',
            ),

            array (
                'code' => '18.13',
                'name' => 'KAB. PESISIR BARAT',
            ),

            array (
                'code' => '18.13.01',
                'name' => 'Pesisir Tengah',
            ),

            array (
                'code' => '18.13.02',
                'name' => 'Pesisir Selatan',
            ),

            array (
                'code' => '18.13.03',
                'name' => 'Lemong',
            ),

            array (
                'code' => '18.13.04',
                'name' => 'Pesisir Utara',
            ),

            array (
                'code' => '18.13.05',
                'name' => 'Karya Penggawa',
            ),

            array (
                'code' => '18.13.06',
                'name' => 'Pulaupisang',
            ),

            array (
                'code' => '18.13.07',
                'name' => 'Way Krui',
            ),

            array (
                'code' => '18.13.08',
                'name' => 'Krui Selatan',
            ),

            array (
                'code' => '18.13.09',
                'name' => 'Ngambur',
            ),

            array (
                'code' => '18.13.10',
                'name' => 'Bengkunat',
            ),

            array (
                'code' => '18.13.11',
                'name' => 'Bengkunat Belimbing',
            ),

            array (
                'code' => '18.71',
                'name' => 'KOTA BANDAR LAMPUNG',
            ),

            array (
                'code' => '18.71.01',
                'name' => 'Kedaton',
            ),

            array (
                'code' => '18.71.02',
                'name' => 'Sukarame',
            ),

            array (
                'code' => '18.71.03',
                'name' => 'Tanjungkarang Barat',
            ),

            array (
                'code' => '18.71.04',
                'name' => 'Panjang',
            ),

            array (
                'code' => '18.71.05',
                'name' => 'Tanjungkarang Timur',
            ),

            array (
                'code' => '18.71.06',
                'name' => 'Tanjungkarang Pusat',
            ),

            array (
                'code' => '18.71.07',
                'name' => 'Telukbetung Selatan',
            ),

            array (
                'code' => '18.71.08',
                'name' => 'Telukbetung Barat',
            ),

            array (
                'code' => '18.71.09',
                'name' => 'Telukbetung Utara',
            ),

            array (
                'code' => '18.71.10',
                'name' => 'Rajabasa',
            ),

            array (
                'code' => '18.71.11',
                'name' => 'Tanjung Senang',
            ),

            array (
                'code' => '18.71.12',
                'name' => 'Sukabumi',
            ),

            array (
                'code' => '18.71.13',
                'name' => 'Kemiling',
            ),

            array (
                'code' => '18.71.14',
                'name' => 'Labuhan Ratu',
            ),

            array (
                'code' => '18.71.15',
                'name' => 'Way Halim',
            ),

            array (
                'code' => '18.71.16',
                'name' => 'Langkapura',
            ),

            array (
                'code' => '18.71.17',
                'name' => 'Enggal',
            ),

            array (
                'code' => '18.71.18',
                'name' => 'Kedamaian',
            ),

            array (
                'code' => '18.71.19',
                'name' => 'Telukbetung Timur',
            ),

            array (
                'code' => '18.71.20',
                'name' => 'Bumi Waras',
            ),

            array (
                'code' => '18.72',
                'name' => 'KOTA METRO',
            ),

            array (
                'code' => '18.72.01',
                'name' => 'Metro Pusat',
            ),

            array (
                'code' => '18.72.02',
                'name' => 'Metro Utara',
            ),

            array (
                'code' => '18.72.03',
                'name' => 'Metro Barat',
            ),

            array (
                'code' => '18.72.04',
                'name' => 'Metro Timur',
            ),

            array (
                'code' => '18.72.05',
                'name' => 'Metro Selatan',
            ),

            array (
                'code' => '19',
                'name' => 'KEPULAUAN BANGKA BELITUNG',
            ),

            array (
                'code' => '19.01',
                'name' => 'KAB. BANGKA',
            ),

            array (
                'code' => '19.01.01',
                'name' => 'Sungailiat',
            ),

            array (
                'code' => '19.01.02',
                'name' => 'Belinyu',
            ),

            array (
                'code' => '19.01.03',
                'name' => 'Merawang',
            ),

            array (
                'code' => '19.01.04',
                'name' => 'Mendo Barat',
            ),

            array (
                'code' => '19.01.05',
                'name' => 'Pemali',
            ),

            array (
                'code' => '19.01.06',
                'name' => 'Bakam',
            ),

            array (
                'code' => '19.01.07',
                'name' => 'Riau Silip',
            ),

            array (
                'code' => '19.01.08',
                'name' => 'Puding Besar',
            ),

            array (
                'code' => '19.02',
                'name' => 'KAB. BELITUNG',
            ),

            array (
                'code' => '19.02.01',
                'name' => 'Tanjung Pandan',
            ),

            array (
                'code' => '19.02.02',
                'name' => 'Membalong',
            ),

            array (
                'code' => '19.02.03',
                'name' => 'Selat Nasik',
            ),

            array (
                'code' => '19.02.04',
                'name' => 'Sijuk',
            ),

            array (
                'code' => '19.02.05',
                'name' => 'Badau',
            ),

            array (
                'code' => '19.03',
                'name' => 'KAB. BANGKA SELATAN',
            ),

            array (
                'code' => '19.03.01',
                'name' => 'Toboali',
            ),

            array (
                'code' => '19.03.02',
                'name' => 'Lepar Pongok',
            ),

            array (
                'code' => '19.03.03',
                'name' => 'Air Gegas',
            ),

            array (
                'code' => '19.03.04',
                'name' => 'Simpang Rimba',
            ),

            array (
                'code' => '19.03.05',
                'name' => 'Payung',
            ),

            array (
                'code' => '19.03.06',
                'name' => 'Tukak Sadai',
            ),

            array (
                'code' => '19.03.07',
                'name' => 'Pulaubesar',
            ),

            array (
                'code' => '19.03.08',
                'name' => 'Kepulauan Pongok',
            ),

            array (
                'code' => '19.04',
                'name' => 'KAB. BANGKA TENGAH',
            ),

            array (
                'code' => '19.04.01',
                'name' => 'Koba',
            ),

            array (
                'code' => '19.04.02',
                'name' => 'Pangkalan Baru',
            ),

            array (
                'code' => '19.04.03',
                'name' => 'Sungai Selan',
            ),

            array (
                'code' => '19.04.04',
                'name' => 'Simpang Katis',
            ),

            array (
                'code' => '19.04.05',
                'name' => 'Namang',
            ),

            array (
                'code' => '19.04.06',
                'name' => 'Lubuk Besar',
            ),

            array (
                'code' => '19.05',
                'name' => 'KAB. BANGKA BARAT',
            ),

            array (
                'code' => '19.05.01',
                'name' => 'Mentok',
            ),

            array (
                'code' => '19.05.02',
                'name' => 'Simpang Teritip',
            ),

            array (
                'code' => '19.05.03',
                'name' => 'Jebus',
            ),

            array (
                'code' => '19.05.04',
                'name' => 'Kelapa',
            ),

            array (
                'code' => '19.05.05',
                'name' => 'Tempilang',
            ),

            array (
                'code' => '19.05.06',
                'name' => 'Parittiga',
            ),

            array (
                'code' => '19.06',
                'name' => 'KAB. BELITUNG TIMUR',
            ),

            array (
                'code' => '19.06.01',
                'name' => 'Manggar',
            ),

            array (
                'code' => '19.06.02',
                'name' => 'Gantung',
            ),

            array (
                'code' => '19.06.03',
                'name' => 'Dendang',
            ),

            array (
                'code' => '19.06.04',
                'name' => 'Kelapa Kampit',
            ),

            array (
                'code' => '19.06.05',
                'name' => 'Damar',
            ),

            array (
                'code' => '19.06.06',
                'name' => 'Simpang Renggiang',
            ),

            array (
                'code' => '19.06.07',
                'name' => 'Simpang Pesak',
            ),

            array (
                'code' => '19.71',
                'name' => 'KOTA PANGKAL PINANG',
            ),

            array (
                'code' => '19.71.01',
                'name' => 'Bukitintan',
            ),

            array (
                'code' => '19.71.02',
                'name' => 'Taman Sari',
            ),

            array (
                'code' => '19.71.03',
                'name' => 'Pangkal Balam',
            ),

            array (
                'code' => '19.71.04',
                'name' => 'Rangkui',
            ),

            array (
                'code' => '19.71.05',
                'name' => 'Gerunggang',
            ),

            array (
                'code' => '19.71.06',
                'name' => 'Gabek',
            ),

            array (
                'code' => '19.71.07',
                'name' => 'Girimaya',
            ),

            array (
                'code' => '21',
                'name' => 'KEPULAUAN RIAU',
            ),

            array (
                'code' => '21.01',
                'name' => 'KAB. BINTAN',
            ),

            array (
                'code' => '21.01.04',
                'name' => 'Gunung Kijang',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '21.01.06',
                'name' => 'Bintan Timur',
            ),

            array (
                'code' => '21.01.07',
                'name' => 'Bintan Utara',
            ),

            array (
                'code' => '21.01.08',
                'name' => 'Teluk Bintan',
            ),

            array (
                'code' => '21.01.09',
                'name' => 'Tambelan',
            ),

            array (
                'code' => '21.01.10',
                'name' => 'Telok Sebong',
            ),

            array (
                'code' => '21.01.12',
                'name' => 'Toapaya',
            ),

            array (
                'code' => '21.01.13',
                'name' => 'Mantang',
            ),

            array (
                'code' => '21.01.14',
                'name' => 'Bintan Pesisir',
            ),

            array (
                'code' => '21.01.15',
                'name' => 'Seri Kuala Lobam',
            ),

            array (
                'code' => '21.02',
                'name' => 'KAB. KARIMUN',
            ),

            array (
                'code' => '21.02.01',
                'name' => 'Moro',
            ),

            array (
                'code' => '21.02.02',
                'name' => 'Kundur',
            ),

            array (
                'code' => '21.02.03',
                'name' => 'Karimun',
            ),

            array (
                'code' => '21.02.04',
                'name' => 'Meral',
            ),

            array (
                'code' => '21.02.05',
                'name' => 'Tebing',
            ),

            array (
                'code' => '21.02.06',
                'name' => 'Buru',
            ),

            array (
                'code' => '21.02.07',
                'name' => 'Kundur Utara',
            ),

            array (
                'code' => '21.02.08',
                'name' => 'Kundur Barat',
            ),

            array (
                'code' => '21.02.09',
                'name' => 'Durai',
            ),

            array (
                'code' => '21.02.10',
                'name' => 'Meral Barat',
            ),

            array (
                'code' => '21.02.11',
                'name' => 'Ungar',
            ),

            array (
                'code' => '21.02.12',
                'name' => 'Belat',
            ),

            array (
                'code' => '21.03',
                'name' => 'KAB. NATUNA',
            ),

            array (
                'code' => '21.03.04',
                'name' => 'Midai',
            ),

            array (
                'code' => '21.03.05',
                'name' => 'Bunguran Barat',
            ),

            array (
                'code' => '21.03.06',
                'name' => 'Serasan',
            ),

            array (
                'code' => '21.03.07',
                'name' => 'Bunguran Timur',
            ),

            array (
                'code' => '21.03.08',
                'name' => 'Bunguran Utara',
            ),

            array (
                'code' => '21.03.09',
                'name' => 'Subi',
            ),

            array (
                'code' => '21.03.10',
                'name' => 'Pulau Laut',
            ),

            array (
                'code' => '21.03.11',
                'name' => 'Pulau Tiga',
            ),

            array (
                'code' => '21.03.15',
                'name' => 'Bunguran Timur Laut',
            ),

            array (
                'code' => '21.03.16',
                'name' => 'Bunguran Tengah',
            ),

            array (
                'code' => '21.03.18',
                'name' => 'Bunguran Selatan',
            ),

            array (
                'code' => '21.03.19',
                'name' => 'Serasan Timur',
            ),

            array (
                'code' => '21.03.20',
                'name' => 'Bunguran Batubi',
            ),

            array (
                'code' => '21.03.21',
                'name' => 'Pulau Tiga Barat',
            ),

            array (
                'code' => '21.03.22',
                'name' => 'Suak Midai',
            ),

            array (
                'code' => '21.04',
                'name' => 'KAB. LINGGA',
            ),

            array (
                'code' => '21.04.01',
                'name' => 'Singkep',
            ),

            array (
                'code' => '21.04.02',
                'name' => 'Lingga',
            ),

            array (
                'code' => '21.04.03',
                'name' => 'Senayang',
            ),

            array (
                'code' => '21.04.04',
                'name' => 'Singkep Barat',
            ),

            array (
                'code' => '21.04.05',
                'name' => 'Lingga Utara',
            ),

            array (
                'code' => '21.04.06',
                'name' => 'Singkep Pesisir',
            ),

            array (
                'code' => '21.04.07',
                'name' => 'Lingga Timur',
            ),

            array (
                'code' => '21.04.08',
                'name' => 'Selayar',
            ),

            array (
                'code' => '21.04.09',
                'name' => 'Singkep Selatan',
            ),

            array (
                'code' => '21.04.10',
                'name' => 'Kepulauan Posek',
            ),

            array (
                'code' => '21.05',
                'name' => 'KAB. KEPULAUAN ANAMBAS',
            ),

            array (
                'code' => '21.05.01',
                'name' => 'Siantan',
            ),

            array (
                'code' => '21.05.02',
                'name' => 'Palmatak',
            ),

            array (
                'code' => '21.05.03',
                'name' => 'Siantan Timur',
            ),

            array (
                'code' => '21.05.04',
                'name' => 'Siantan Selatan',
            ),

            array (
                'code' => '21.05.05',
                'name' => 'Jemaja Timur',
            ),

            array (
                'code' => '21.05.06',
                'name' => 'Jemaja',
            ),

            array (
                'code' => '21.05.07',
                'name' => 'Siantan Tengah',
            ),

            array (
                'code' => '21.71',
                'name' => 'KOTA BATAM',
            ),

            array (
                'code' => '21.71.01',
                'name' => 'Belakang Padang',
            ),

            array (
                'code' => '21.71.02',
                'name' => 'Batu Ampar',
            ),

            array (
                'code' => '21.71.03',
                'name' => 'Sekupang',
            ),

            array (
                'code' => '21.71.04',
                'name' => 'Nongsa',
            ),

            array (
                'code' => '21.71.05',
                'name' => 'Bulang',
            ),

            array (
                'code' => '21.71.06',
                'name' => 'Lubuk Baja',
            ),

            array (
                'code' => '21.71.07',
                'name' => 'Sei Beduk',
            ),

            array (
                'code' => '21.71.08',
                'name' => 'Galang',
            ),

            array (
                'code' => '21.71.09',
                'name' => 'Bengkong',
            ),

            array (
                'code' => '21.71.10',
                'name' => 'Batam Kota',
            ),

            array (
                'code' => '21.71.11',
                'name' => 'Sagulung',
            ),

            array (
                'code' => '21.71.12',
                'name' => 'Batu Aji',
            ),

            array (
                'code' => '21.72',
                'name' => 'KOTA TANJUNG PINANG',
            ),

            array (
                'code' => '21.72.01',
                'name' => 'Tanjung Pinang Barat',
            ),

            array (
                'code' => '21.72.02',
                'name' => 'Tanjung Pinang Timur',
            ),

            array (
                'code' => '21.72.03',
                'name' => 'Tanjung Pinang Kota',
            ),

            array (
                'code' => '21.72.04',
                'name' => 'Bukit Bestari',
            ),

            array (
                'code' => '31',
                'name' => 'DKI JAKARTA',
            ),

            array (
                'code' => '31.01',
                'name' => 'KAB. ADM. KEP. SERIBU',
            ),

            array (
                'code' => '31.01.01',
                'name' => 'Kepulauan Seribu Utara',
            ),

            array (
                'code' => '31.01.02',
                'name' => 'Kepulauan Seribu Selatan',
            ),

            array (
                'code' => '31.71',
                'name' => 'KOTA ADM. JAKARTA PUSAT',
            ),

            array (
                'code' => '31.71.01',
                'name' => 'Gambir',
            ),

            array (
                'code' => '31.71.02',
                'name' => 'Sawah Besar',
            ),

            array (
                'code' => '31.71.03',
                'name' => 'Kemayoran',
            ),

            array (
                'code' => '31.71.04',
                'name' => 'Senen',
            ),

            array (
                'code' => '31.71.05',
                'name' => 'Cempaka Putih',
            ),

            array (
                'code' => '31.71.06',
                'name' => 'Menteng',
            ),

            array (
                'code' => '31.71.07',
                'name' => 'Tanah Abang',
            ),

            array (
                'code' => '31.71.08',
                'name' => 'Johar Baru',
            ),

            array (
                'code' => '31.72',
                'name' => 'KOTA ADM. JAKARTA UTARA',
            ),

            array (
                'code' => '31.72.01',
                'name' => 'Penjaringan',
            ),

            array (
                'code' => '31.72.02',
                'name' => 'Tanjung Priok',
            ),

            array (
                'code' => '31.72.03',
                'name' => 'Koja',
            ),

            array (
                'code' => '31.72.04',
                'name' => 'Cilincing',
            ),

            array (
                'code' => '31.72.05',
                'name' => 'Pademangan',
            ),

            array (
                'code' => '31.72.06',
                'name' => 'Kelapa Gading',
            ),

            array (
                'code' => '31.73',
                'name' => 'KOTA ADM. JAKARTA BARAT',
            ),

            array (
                'code' => '31.73.01',
                'name' => 'Cengkareng',
            ),

            array (
                'code' => '31.73.02',
                'name' => 'Grogol Petamburan',
            ),

            array (
                'code' => '31.73.03',
                'name' => 'Taman Sari',
            ),

            array (
                'code' => '31.73.04',
                'name' => 'Tambora',
            ),

            array (
                'code' => '31.73.05',
                'name' => 'Kebon Jeruk',
            ),

            array (
                'code' => '31.73.06',
                'name' => 'Kalideres',
            ),

            array (
                'code' => '31.73.07',
                'name' => 'Pal Merah',
            ),

            array (
                'code' => '31.73.08',
                'name' => 'Kembangan',
            ),

            array (
                'code' => '31.74',
                'name' => 'KOTA ADM. JAKARTA SELATAN',
            ),

            array (
                'code' => '31.74.01',
                'name' => 'Tebet',
            ),

            array (
                'code' => '31.74.02',
                'name' => 'Setiabudi',
            ),

            array (
                'code' => '31.74.03',
                'name' => 'Mampang Prapatan',
            ),

            array (
                'code' => '31.74.04',
                'name' => 'Pasar Minggu',
            ),

            array (
                'code' => '31.74.05',
                'name' => 'Kebayoran Lama',
            ),

            array (
                'code' => '31.74.06',
                'name' => 'Cilandak',
            ),

            array (
                'code' => '31.74.07',
                'name' => 'Kebayoran Baru',
            ),

            array (
                'code' => '31.74.08',
                'name' => 'Pancoran',
            ),

            array (
                'code' => '31.74.09',
                'name' => 'Jagakarsa',
            ),

            array (
                'code' => '31.74.10',
                'name' => 'Pesanggrahan',
            ),

            array (
                'code' => '31.75',
                'name' => 'KOTA ADM. JAKARTA TIMUR',
            ),

            array (
                'code' => '31.75.01',
                'name' => 'Matraman',
            ),

            array (
                'code' => '31.75.02',
                'name' => 'Pulogadung',
            ),

            array (
                'code' => '31.75.03',
                'name' => 'Jatinegara',
            ),

            array (
                'code' => '31.75.04',
                'name' => 'Kramatjati',
            ),

            array (
                'code' => '31.75.05',
                'name' => 'Pasar Rebo',
            ),

            array (
                'code' => '31.75.06',
                'name' => 'Cakung',
            ),

            array (
                'code' => '31.75.07',
                'name' => 'Duren Sawit',
            ),

            array (
                'code' => '31.75.08',
                'name' => 'Makasar',
            ),

            array (
                'code' => '31.75.09',
                'name' => 'Ciracas',
            ),

            array (
                'code' => '31.75.10',
                'name' => 'Cipayung',
            ),

            array (
                'code' => '32',
                'name' => 'JAWA BARAT',
            ),

            array (
                'code' => '32.01',
                'name' => 'KAB. BOGOR',
            ),

            array (
                'code' => '32.01.01',
                'name' => 'Cibinong',
            ),

            array (
                'code' => '32.01.02',
                'name' => 'Gunung Putri',
            ),

            array (
                'code' => '32.01.03',
                'name' => 'Citeureup',
            ),

            array (
                'code' => '32.01.04',
                'name' => 'Sukaraja',
            ),

            array (
                'code' => '32.01.05',
                'name' => 'Babakan Madang',
            ),

            array (
                'code' => '32.01.06',
                'name' => 'Jonggol',
            ),

            array (
                'code' => '32.01.07',
                'name' => 'Cileungsi',
            ),

            array (
                'code' => '32.01.08',
                'name' => 'Cariu',
            ),

            array (
                'code' => '32.01.09',
                'name' => 'Sukamakmur',
            ),

            array (
                'code' => '32.01.10',
                'name' => 'Parung',
            ),

            array (
                'code' => '32.01.11',
                'name' => 'Gunung Sindur',
            ),

            array (
                'code' => '32.01.12',
                'name' => 'Kemang',
            ),

            array (
                'code' => '32.01.13',
                'name' => 'Bojong Gede',
            ),

            array (
                'code' => '32.01.14',
                'name' => 'Leuwiliang',
            ),

            array (
                'code' => '32.01.15',
                'name' => 'Ciampea',
            ),

            array (
                'code' => '32.01.16',
                'name' => 'Cibungbulang',
            ),

            array (
                'code' => '32.01.17',
                'name' => 'Pamijahan',
            ),

            array (
                'code' => '32.01.18',
                'name' => 'Rumpin',
            ),

            array (
                'code' => '32.01.19',
                'name' => 'Jasinga',
            ),

            array (
                'code' => '32.01.20',
                'name' => 'Parung Panjang',
            ),

            array (
                'code' => '32.01.21',
                'name' => 'Nanggung',
            ),

            array (
                'code' => '32.01.22',
                'name' => 'Cigudeg',
            ),

            array (
                'code' => '32.01.23',
                'name' => 'Tenjo',
            ),

            array (
                'code' => '32.01.24',
                'name' => 'Ciawi',
            ),

            array (
                'code' => '32.01.25',
                'name' => 'Cisarua',
            ),

            array (
                'code' => '32.01.26',
                'name' => 'Megamendung',
            ),

            array (
                'code' => '32.01.27',
                'name' => 'Caringin',
            ),

            array (
                'code' => '32.01.28',
                'name' => 'Cijeruk',
            ),

            array (
                'code' => '32.01.29',
                'name' => 'Ciomas',
            ),

            array (
                'code' => '32.01.30',
                'name' => 'Dramaga',
            ),

            array (
                'code' => '32.01.31',
                'name' => 'Tamansari',
            ),

            array (
                'code' => '32.01.32',
                'name' => 'Klapanunggal',
            ),

            array (
                'code' => '32.01.33',
                'name' => 'Ciseeng',
            ),

            array (
                'code' => '32.01.34',
                'name' => 'Ranca Bungur',
            ),

            array (
                'code' => '32.01.35',
                'name' => 'Sukajaya',
            ),

            array (
                'code' => '32.01.36',
                'name' => 'Tanjungsari',
            ),

            array (
                'code' => '32.01.37',
                'name' => 'Tajurhalang',
            ),

            array (
                'code' => '32.01.38',
                'name' => 'Cigombong',
            ),

            array (
                'code' => '32.01.39',
                'name' => 'Leuwisadeng',
            ),

            array (
                'code' => '32.01.40',
                'name' => 'Tenjolaya',
            ),

            array (
                'code' => '32.02',
                'name' => 'KAB. SUKABUMI',
            ),

            array (
                'code' => '32.02.01',
                'name' => 'Pelabuhanratu',
            ),

            array (
                'code' => '32.02.02',
                'name' => 'Simpenan',
            ),

            array (
                'code' => '32.02.03',
                'name' => 'Cikakak',
            ),

            array (
                'code' => '32.02.04',
                'name' => 'Bantargadung',
            ),

            array (
                'code' => '32.02.05',
                'name' => 'Cisolok',
            ),

            array (
                'code' => '32.02.06',
                'name' => 'Cikidang',
            ),

            array (
                'code' => '32.02.07',
                'name' => 'Lengkong',
            ),

            array (
                'code' => '32.02.08',
                'name' => 'Jampang Tengah',
            ),

            array (
                'code' => '32.02.09',
                'name' => 'Warung Kiara',
            ),

            array (
                'code' => '32.02.10',
                'name' => 'Cikembar',
            ),

            array (
                'code' => '32.02.11',
                'name' => 'Cibadak',
            ),

            array (
                'code' => '32.02.12',
                'name' => 'Nagrak',
            ),

            array (
                'code' => '32.02.13',
                'name' => 'Parungkuda',
            ),

            array (
                'code' => '32.02.14',
                'name' => 'Bojonggenteng',
            ),

            array (
                'code' => '32.02.15',
                'name' => 'Parakansalak',
            ),

            array (
                'code' => '32.02.16',
                'name' => 'Cicurug',
            ),

            array (
                'code' => '32.02.17',
                'name' => 'Cidahu',
            ),

            array (
                'code' => '32.02.18',
                'name' => 'Kalapanunggal',
            ),

            array (
                'code' => '32.02.19',
                'name' => 'Kabandungan',
            ),

            array (
                'code' => '32.02.20',
                'name' => 'Waluran',
            ),

            array (
                'code' => '32.02.21',
                'name' => 'Jampang Kulon',
            ),

            array (
                'code' => '32.02.22',
                'name' => 'Ciemas',
            ),

            array (
                'code' => '32.02.23',
                'name' => 'Kalibunder',
            ),

            array (
                'code' => '32.02.24',
                'name' => 'Surade',
            ),

            array (
                'code' => '32.02.25',
                'name' => 'Cibitung',
            ),

            array (
                'code' => '32.02.26',
                'name' => 'Ciracap',
            ),

            array (
                'code' => '32.02.27',
                'name' => 'Gunung Guruh',
            ),

            array (
                'code' => '32.02.28',
                'name' => 'Cicantayan',
            ),

            array (
                'code' => '32.02.29',
                'name' => 'Cisaat',
            ),

            array (
                'code' => '32.02.30',
                'name' => 'Kadudampit',
            ),

            array (
                'code' => '32.02.31',
                'name' => 'Caringin',
            ),

            array (
                'code' => '32.02.32',
                'name' => 'Sukabumi',
            ),

            array (
                'code' => '32.02.33',
                'name' => 'Sukaraja',
            ),

            array (
                'code' => '32.02.34',
                'name' => 'Kebonpedes',
            ),

            array (
                'code' => '32.02.35',
                'name' => 'Cireunghas',
            ),

            array (
                'code' => '32.02.36',
                'name' => 'Sukalarang',
            ),

            array (
                'code' => '32.02.37',
                'name' => 'Pabuaran',
            ),

            array (
                'code' => '32.02.38',
                'name' => 'Purabaya',
            ),

            array (
                'code' => '32.02.39',
                'name' => 'Nyalindung',
            ),

            array (
                'code' => '32.02.40',
                'name' => 'Gegerbitung',
            ),

            array (
                'code' => '32.02.41',
                'name' => 'Sagaranten',
            ),

            array (
                'code' => '32.02.42',
                'name' => 'Curug Kembar',
            ),

            array (
                'code' => '32.02.43',
                'name' => 'Cidolog',
            ),

            array (
                'code' => '32.02.44',
                'name' => 'Cidadap',
            ),

            array (
                'code' => '32.02.45',
                'name' => 'Tegal Buleud',
            ),

            array (
                'code' => '32.02.46',
                'name' => 'Cimanggu',
            ),

            array (
                'code' => '32.02.47',
                'name' => 'Ciambar',
            ),

            array (
                'code' => '32.03',
                'name' => 'KAB. CIANJUR',
            ),

            array (
                'code' => '32.03.01',
                'name' => 'Cianjur',
            ),

            array (
                'code' => '32.03.02',
                'name' => 'Warungkondang',
            ),

            array (
                'code' => '32.03.03',
                'name' => 'Cibeber',
            ),

            array (
                'code' => '32.03.04',
                'name' => 'Cilaku',
            ),

            array (
                'code' => '32.03.05',
                'name' => 'Ciranjang',
            ),

            array (
                'code' => '32.03.06',
                'name' => 'Bojongpicung',
            ),

            array (
                'code' => '32.03.07',
                'name' => 'Karangtengah',
            ),

            array (
                'code' => '32.03.08',
                'name' => 'Mande',
            ),

            array (
                'code' => '32.03.09',
                'name' => 'Sukaluyu',
            ),

            array (
                'code' => '32.03.10',
                'name' => 'Pacet',
            ),

            array (
                'code' => '32.03.11',
                'name' => 'Cugenang',
            ),

            array (
                'code' => '32.03.12',
                'name' => 'Cikalongkulon',
            ),

            array (
                'code' => '32.03.13',
                'name' => 'Sukaresmi',
            ),

            array (
                'code' => '32.03.14',
                'name' => 'Sukanagara',
            ),

            array (
                'code' => '32.03.15',
                'name' => 'Campaka',
            ),

            array (
                'code' => '32.03.16',
                'name' => 'Takokak',
            ),

            array (
                'code' => '32.03.17',
                'name' => 'Kadupandak',
            ),

            array (
                'code' => '32.03.18',
                'name' => 'Pagelaran',
            ),

            array (
                'code' => '32.03.19',
                'name' => 'Tanggeung',
            ),

            array (
                'code' => '32.03.20',
                'name' => 'Cibinong',
            ),

            array (
                'code' => '32.03.21',
                'name' => 'Sindangbarang',
            ),

            array (
                'code' => '32.03.22',
                'name' => 'Agrabinta',
            ),

            array (
                'code' => '32.03.23',
                'name' => 'Cidaun',
            ),

            array (
                'code' => '32.03.24',
                'name' => 'Naringgul',
            ),

            array (
                'code' => '32.03.25',
                'name' => 'Campakamulya',
            ),

            array (
                'code' => '32.03.26',
                'name' => 'Cikadu',
            ),

            array (
                'code' => '32.03.27',
                'name' => 'Gekbrong',
            ),

            array (
                'code' => '32.03.28',
                'name' => 'Cipanas',
            ),

            array (
                'code' => '32.03.29',
                'name' => 'Cijati',
            ),

            array (
                'code' => '32.03.30',
                'name' => 'Leles',
            ),

            array (
                'code' => '32.03.31',
                'name' => 'Haurwangi',
            ),

            array (
                'code' => '32.03.32',
                'name' => 'Pasirkuda',
            ),

            array (
                'code' => '32.04',
                'name' => 'KAB. BANDUNG',
            ),

            array (
                'code' => '32.04.05',
                'name' => 'Cileunyi',
            ),

            array (
                'code' => '32.04.06',
                'name' => 'Cimenyan',
            ),

            array (
                'code' => '32.04.07',
                'name' => 'Cilengkrang',
            ),

            array (
                'code' => '32.04.08',
                'name' => 'Bojongsoang',
            ),

            array (
                'code' => '32.04.09',
                'name' => 'Margahayu',
            ),

            array (
                'code' => '32.04.10',
                'name' => 'Margaasih',
            ),

            array (
                'code' => '32.04.11',
                'name' => 'Katapang',
            ),

            array (
                'code' => '32.04.12',
                'name' => 'Dayeuhkolot',
            ),

            array (
                'code' => '32.04.13',
                'name' => 'Banjaran',
            ),

            array (
                'code' => '32.04.14',
                'name' => 'Pameungpeuk',
            ),

            array (
                'code' => '32.04.15',
                'name' => 'Pangalengan',
            ),

            array (
                'code' => '32.04.16',
                'name' => 'Arjasari',
            ),

            array (
                'code' => '32.04.17',
                'name' => 'Cimaung',
            ),

            array (
                'code' => '32.04.25',
                'name' => 'Cicalengka',
            ),

            array (
                'code' => '32.04.26',
                'name' => 'Nagreg',
            ),

            array (
                'code' => '32.04.27',
                'name' => 'Cikancung',
            ),

            array (
                'code' => '32.04.28',
                'name' => 'Rancaekek',
            ),

            array (
                'code' => '32.04.29',
                'name' => 'Ciparay',
            ),

            array (
                'code' => '32.04.30',
                'name' => 'Pacet',
            ),

            array (
                'code' => '32.04.31',
                'name' => 'Kertasari',
            ),

            array (
                'code' => '32.04.32',
                'name' => 'Baleendah',
            ),

            array (
                'code' => '32.04.33',
                'name' => 'Majalaya',
            ),

            array (
                'code' => '32.04.34',
                'name' => 'Solokanjeruk',
            ),

            array (
                'code' => '32.04.35',
                'name' => 'Paseh',
            ),

            array (
                'code' => '32.04.36',
                'name' => 'Ibun',
            ),

            array (
                'code' => '32.04.37',
                'name' => 'Soreang',
            ),

            array (
                'code' => '32.04.38',
                'name' => 'Pasirjambu',
            ),

            array (
                'code' => '32.04.39',
                'name' => 'Ciwidey',
            ),

            array (
                'code' => '32.04.40',
                'name' => 'Rancabali',
            ),

            array (
                'code' => '32.04.44',
                'name' => 'Cangkuang',
            ),

            array (
                'code' => '32.04.46',
                'name' => 'Kutawaringin',
            ),

            array (
                'code' => '32.05',
                'name' => 'KAB. GARUT',
            ),

            array (
                'code' => '32.05.01',
                'name' => 'Garut Kota',
            ),

            array (
                'code' => '32.05.02',
                'name' => 'Karangpawitan',
            ),

            array (
                'code' => '32.05.03',
                'name' => 'Wanaraja',
            ),

            array (
                'code' => '32.05.04',
                'name' => 'Tarogong Kaler',
            ),

            array (
                'code' => '32.05.05',
                'name' => 'Tarogong Kidul',
            ),

            array (
                'code' => '32.05.06',
                'name' => 'Banyuresmi',
            ),

            array (
                'code' => '32.05.07',
                'name' => 'Samarang',
            ),

            array (
                'code' => '32.05.08',
                'name' => 'Pasirwangi',
            ),

            array (
                'code' => '32.05.09',
                'name' => 'Leles',
            ),

            array (
                'code' => '32.05.10',
                'name' => 'Kadungora',
            ),

            array (
                'code' => '32.05.11',
                'name' => 'Leuwigoong',
            ),

            array (
                'code' => '32.05.12',
                'name' => 'Cibatu',
            ),

            array (
                'code' => '32.05.13',
                'name' => 'Kersamanah',
            ),

            array (
                'code' => '32.05.14',
                'name' => 'Malangbong',
            ),

            array (
                'code' => '32.05.15',
                'name' => 'Sukawening',
            ),

            array (
                'code' => '32.05.16',
                'name' => 'Karangtengah',
            ),

            array (
                'code' => '32.05.17',
                'name' => 'Bayongbong',
            ),

            array (
                'code' => '32.05.18',
                'name' => 'Cigedug',
            ),

            array (
                'code' => '32.05.19',
                'name' => 'Cilawu',
            ),

            array (
                'code' => '32.05.20',
                'name' => 'Cisurupan',
            ),

            array (
                'code' => '32.05.21',
                'name' => 'Sukaresmi',
            ),

            array (
                'code' => '32.05.22',
                'name' => 'Cikajang',
            ),

            array (
                'code' => '32.05.23',
                'name' => 'Banjarwangi',
            ),

            array (
                'code' => '32.05.24',
                'name' => 'Singajaya',
            ),

            array (
                'code' => '32.05.25',
                'name' => 'Cihurip',
            ),

            array (
                'code' => '32.05.26',
                'name' => 'Peundeuy',
            ),

            array (
                'code' => '32.05.27',
                'name' => 'Pameungpeuk',
            ),

            array (
                'code' => '32.05.28',
                'name' => 'Cisompet',
            ),

            array (
                'code' => '32.05.29',
                'name' => 'Cibalong',
            ),

            array (
                'code' => '32.05.30',
                'name' => 'Cikelet',
            ),

            array (
                'code' => '32.05.31',
                'name' => 'Bungbulang',
            ),

            array (
                'code' => '32.05.32',
                'name' => 'Mekarmukti',
            ),

            array (
                'code' => '32.05.33',
                'name' => 'Pakenjeng',
            ),

            array (
                'code' => '32.05.34',
                'name' => 'Pamulihan',
            ),

            array (
                'code' => '32.05.35',
                'name' => 'Cisewu',
            ),

            array (
                'code' => '32.05.36',
                'name' => 'Caringin',
            ),

            array (
                'code' => '32.05.37',
                'name' => 'Talegong',
            ),

            array (
                'code' => '32.05.38',
                'name' => 'Bl. Limbangan  14',
            ),

            array (
                'code' => '32.05.39',
                'name' => 'Selaawi',
            ),

            array (
                'code' => '32.05.40',
                'name' => 'Cibiuk',
            ),

            array (
                'code' => '32.05.41',
                'name' => 'Pangatikan',
            ),

            array (
                'code' => '32.05.42',
                'name' => 'Sucinaraja',
            ),

            array (
                'code' => '32.06',
                'name' => 'KAB. TASIKMALAYA',
            ),

            array (
                'code' => '32.06.01',
                'name' => 'Cipatujah',
            ),

            array (
                'code' => '32.06.02',
                'name' => 'Karangnunggal',
            ),

            array (
                'code' => '32.06.03',
                'name' => 'Cikalong',
            ),

            array (
                'code' => '32.06.04',
                'name' => 'Pancatengah',
            ),

            array (
                'code' => '32.06.05',
                'name' => 'Cikatomas',
            ),

            array (
                'code' => '32.06.06',
                'name' => 'Cibalong',
            ),

            array (
                'code' => '32.06.07',
                'name' => 'Parungponteng',
            ),

            array (
                'code' => '32.06.08',
                'name' => 'Bantarkalong',
            ),

            array (
                'code' => '32.06.09',
                'name' => 'Bojongasih',
            ),

            array (
                'code' => '32.06.10',
                'name' => 'Culamega',
            ),

            array (
                'code' => '32.06.11',
                'name' => 'Bojonggambir',
            ),

            array (
                'code' => '32.06.12',
                'name' => 'Sodonghilir',
            ),

            array (
                'code' => '32.06.13',
                'name' => 'Taraju',
            ),

            array (
                'code' => '32.06.14',
                'name' => 'Salawu',
            ),

            array (
                'code' => '32.06.15',
                'name' => 'Puspahiang',
            ),

            array (
                'code' => '32.06.16',
                'name' => 'Tanjungjaya',
            ),

            array (
                'code' => '32.06.17',
                'name' => 'Sukaraja',
            ),

            array (
                'code' => '32.06.18',
                'name' => 'Salopa',
            ),

            array (
                'code' => '32.06.19',
                'name' => 'Jatiwaras',
            ),

            array (
                'code' => '32.06.20',
                'name' => 'Cineam',
            ),

            array (
                'code' => '32.06.21',
                'name' => 'Karang Jaya',
            ),

            array (
                'code' => '32.06.22',
                'name' => 'Manonjaya',
            ),

            array (
                'code' => '32.06.23',
                'name' => 'Gunung Tanjung',
            ),

            array (
                'code' => '32.06.24',
                'name' => 'Singaparna',
            ),

            array (
                'code' => '32.06.25',
                'name' => 'Mangunreja',
            ),

            array (
                'code' => '32.06.26',
                'name' => 'Sukarame',
            ),

            array (
                'code' => '32.06.27',
                'name' => 'Cigalontang',
            ),

            array (
                'code' => '32.06.28',
                'name' => 'Leuwisari',
            ),

            array (
                'code' => '32.06.29',
                'name' => 'Padakembang',
            ),

            array (
                'code' => '32.06.30',
                'name' => 'Sariwangi',
            ),

            array (
                'code' => '32.06.31',
                'name' => 'Sukaratu',
            ),

            array (
                'code' => '32.06.32',
                'name' => 'Cisayong',
            ),

            array (
                'code' => '32.06.33',
                'name' => 'Sukahening',
            ),

            array (
                'code' => '32.06.34',
                'name' => 'Rajapolah',
            ),

            array (
                'code' => '32.06.35',
                'name' => 'Jamanis',
            ),

            array (
                'code' => '32.06.36',
                'name' => 'Ciawi',
            ),

            array (
                'code' => '32.06.37',
                'name' => 'Kadipaten',
            ),

            array (
                'code' => '32.06.38',
                'name' => 'Pagerageung',
            ),

            array (
                'code' => '32.06.39',
                'name' => 'Sukaresik',
            ),

            array (
                'code' => '32.07',
                'name' => 'KAB. CIAMIS',
            ),

            array (
                'code' => '32.07.01',
                'name' => 'Ciamis',
            ),

            array (
                'code' => '32.07.02',
                'name' => 'Cikoneng',
            ),

            array (
                'code' => '32.07.03',
                'name' => 'Cijeungjing',
            ),

            array (
                'code' => '32.07.04',
                'name' => 'Sadananya',
            ),

            array (
                'code' => '32.07.05',
                'name' => 'Cidolog',
            ),

            array (
                'code' => '32.07.06',
                'name' => 'Cihaurbeuti',
            ),

            array (
                'code' => '32.07.07',
                'name' => 'Panumbangan',
            ),

            array (
                'code' => '32.07.08',
                'name' => 'Panjalu',
            ),

            array (
                'code' => '32.07.09',
                'name' => 'Kawali',
            ),

            array (
                'code' => '32.07.10',
                'name' => 'Panawangan',
            ),

            array (
                'code' => '32.07.11',
                'name' => 'Cipaku',
            ),

            array (
                'code' => '32.07.12',
                'name' => 'Jatinagara',
            ),

            array (
                'code' => '32.07.13',
                'name' => 'Rajadesa',
            ),

            array (
                'code' => '32.07.14',
                'name' => 'Sukadana',
            ),

            array (
                'code' => '32.07.15',
                'name' => 'Rancah',
            ),

            array (
                'code' => '32.07.16',
                'name' => 'Tambaksari',
            ),

            array (
                'code' => '32.07.17',
                'name' => 'Lakbok',
            ),

            array (
                'code' => '32.07.18',
                'name' => 'Banjarsari',
            ),

            array (
                'code' => '32.07.19',
                'name' => 'Pamarican',
            ),

            array (
                'code' => '32.07.29',
                'name' => 'Cimaragas',
            ),

            array (
                'code' => '32.07.30',
                'name' => 'Cisaga',
            ),

            array (
                'code' => '32.07.31',
                'name' => 'Sindangkasih',
            ),

            array (
                'code' => '32.07.32',
                'name' => 'Baregbeg',
            ),

            array (
                'code' => '32.07.33',
                'name' => 'Sukamantri',
            ),

            array (
                'code' => '32.07.34',
                'name' => 'Lumbung',
            ),

            array (
                'code' => '32.07.35',
                'name' => 'Purwadadi',
            ),

            array (
                'code' => '32.08',
                'name' => 'KAB. KUNINGAN',
            ),

            array (
                'code' => '32.08.01',
                'name' => 'Kadugede',
            ),

            array (
                'code' => '32.08.02',
                'name' => 'Ciniru',
            ),

            array (
                'code' => '32.08.03',
                'name' => 'Subang',
            ),

            array (
                'code' => '32.08.04',
                'name' => 'Ciwaru',
            ),

            array (
                'code' => '32.08.05',
                'name' => 'Cibingbin',
            ),

            array (
                'code' => '32.08.06',
                'name' => 'Luragung',
            ),

            array (
                'code' => '32.08.07',
                'name' => 'Lebakwangi',
            ),

            array (
                'code' => '32.08.08',
                'name' => 'Garawangi',
            ),

            array (
                'code' => '32.08.09',
                'name' => 'Kuningan',
            ),

            array (
                'code' => '32.08.10',
                'name' => 'Ciawigebang',
            ),

            array (
                'code' => '32.08.11',
                'name' => 'Cidahu',
            ),

            array (
                'code' => '32.08.12',
                'name' => 'Jalaksana',
            ),

            array (
                'code' => '32.08.13',
                'name' => 'Cilimus',
            ),

            array (
                'code' => '32.08.14',
                'name' => 'Mandirancan',
            ),

            array (
                'code' => '32.08.15',
                'name' => 'Selajambe',
            ),

            array (
                'code' => '32.08.16',
                'name' => 'Kramatmulya',
            ),

            array (
                'code' => '32.08.17',
                'name' => 'Darma',
            ),

            array (
                'code' => '32.08.18',
                'name' => 'Cigugur',
            ),

            array (
                'code' => '32.08.19',
                'name' => 'Pasawahan',
            ),

            array (
                'code' => '32.08.20',
                'name' => 'Nusaherang',
            ),

            array (
                'code' => '32.08.21',
                'name' => 'Cipicung',
            ),

            array (
                'code' => '32.08.22',
                'name' => 'Pancalang',
            ),

            array (
                'code' => '32.08.23',
                'name' => 'Japara',
            ),

            array (
                'code' => '32.08.24',
                'name' => 'Cimahi',
            ),

            array (
                'code' => '32.08.25',
                'name' => 'Cilebak',
            ),

            array (
                'code' => '32.08.26',
                'name' => 'Hantara',
            ),

            array (
                'code' => '32.08.27',
                'name' => 'Kalimanggis',
            ),

            array (
                'code' => '32.08.28',
                'name' => 'Cibeureum',
            ),

            array (
                'code' => '32.08.29',
                'name' => 'Karang Kancana',
            ),

            array (
                'code' => '32.08.30',
                'name' => 'Maleber',
            ),

            array (
                'code' => '32.08.31',
                'name' => 'Sindang Agung',
            ),

            array (
                'code' => '32.08.32',
                'name' => 'Cigandamekar',
            ),

            array (
                'code' => '32.09',
                'name' => 'KAB. CIREBON',
            ),

            array (
                'code' => '32.09.01',
                'name' => 'Waled',
            ),

            array (
                'code' => '32.09.02',
                'name' => 'Ciledug',
            ),

            array (
                'code' => '32.09.03',
                'name' => 'Losari',
            ),

            array (
                'code' => '32.09.04',
                'name' => 'Pabedilan',
            ),

            array (
                'code' => '32.09.05',
                'name' => 'Babakan',
            ),

            array (
                'code' => '32.09.06',
                'name' => 'Karangsembung',
            ),

            array (
                'code' => '32.09.07',
                'name' => 'Lemahabang',
            ),

            array (
                'code' => '32.09.08',
                'name' => 'Susukan Lebak',
            ),

            array (
                'code' => '32.09.09',
                'name' => 'Sedong',
            ),

            array (
                'code' => '32.09.10',
                'name' => 'Astanajapura',
            ),

            array (
                'code' => '32.09.11',
                'name' => 'Pangenan',
            ),

            array (
                'code' => '32.09.12',
                'name' => 'Mundu',
            ),

            array (
                'code' => '32.09.13',
                'name' => 'Beber',
            ),

            array (
                'code' => '32.09.14',
                'name' => 'Talun',
            ),

            array (
                'code' => '32.09.15',
                'name' => 'Sumber',
            ),

            array (
                'code' => '32.09.16',
                'name' => 'Dukupuntang',
            ),

            array (
                'code' => '32.09.17',
                'name' => 'Palimanan',
            ),

            array (
                'code' => '32.09.18',
                'name' => 'Plumbon',
            ),

            array (
                'code' => '32.09.19',
                'name' => 'Weru',
            ),

            array (
                'code' => '32.09.20',
                'name' => 'Kedawung',
            ),

            array (
                'code' => '32.09.21',
                'name' => 'Gunung Jati',
            ),

            array (
                'code' => '32.09.22',
                'name' => 'Kapetakan',
            ),

            array (
                'code' => '32.09.23',
                'name' => 'Klangenan',
            ),

            array (
                'code' => '32.09.24',
                'name' => 'Arjawinangun',
            ),

            array (
                'code' => '32.09.25',
                'name' => 'Panguragan',
            ),

            array (
                'code' => '32.09.26',
                'name' => 'Ciwaringin',
            ),

            array (
                'code' => '32.09.27',
                'name' => 'Susukan',
            ),

            array (
                'code' => '32.09.28',
                'name' => 'Gegesik',
            ),

            array (
                'code' => '32.09.29',
                'name' => 'Kaliwedi',
            ),

            array (
                'code' => '32.09.30',
                'name' => 'Gebang',
            ),

            array (
                'code' => '32.09.31',
                'name' => 'Depok',
            ),

            array (
                'code' => '32.09.32',
                'name' => 'Pasaleman',
            ),

            array (
                'code' => '32.09.33',
                'name' => 'Pabuaran',
            ),

            array (
                'code' => '32.09.34',
                'name' => 'Karangwareng',
            ),

            array (
                'code' => '32.09.35',
                'name' => 'Tengah Tani',
            ),

            array (
                'code' => '32.09.36',
                'name' => 'Plered',
            ),

            array (
                'code' => '32.09.37',
                'name' => 'Gempol',
            ),

            array (
                'code' => '32.09.38',
                'name' => 'Greged',
            ),

            array (
                'code' => '32.09.39',
                'name' => 'Suranenggala',
            ),

            array (
                'code' => '32.09.40',
                'name' => 'Jamblang',
            ),

            array (
                'code' => '32.10',
                'name' => 'KAB. MAJALENGKA',
            ),

            array (
                'code' => '32.10.01',
                'name' => 'Lemahsugih',
            ),

            array (
                'code' => '32.10.02',
                'name' => 'Bantarujeg',
            ),

            array (
                'code' => '32.10.03',
                'name' => 'Cikijing',
            ),

            array (
                'code' => '32.10.04',
                'name' => 'Talaga',
            ),

            array (
                'code' => '32.10.05',
                'name' => 'Argapura',
            ),

            array (
                'code' => '32.10.06',
                'name' => 'Maja',
            ),

            array (
                'code' => '32.10.07',
                'name' => 'Majalengka',
            ),

            array (
                'code' => '32.10.08',
                'name' => 'Sukahaji',
            ),

            array (
                'code' => '32.10.09',
                'name' => 'Rajagaluh',
            ),

            array (
                'code' => '32.10.10',
                'name' => 'Leuwimunding',
            ),

            array (
                'code' => '32.10.11',
                'name' => 'Jatiwangi',
            ),

            array (
                'code' => '32.10.12',
                'name' => 'Dawuan',
            ),

            array (
                'code' => '32.10.13',
                'name' => 'Kadipaten',
            ),

            array (
                'code' => '32.10.14',
                'name' => 'Kertajati',
            ),

            array (
                'code' => '32.10.15',
                'name' => 'Jatitujuh',
            ),

            array (
                'code' => '32.10.16',
                'name' => 'Ligung',
            ),

            array (
                'code' => '32.10.17',
                'name' => 'Sumberjaya',
            ),

            array (
                'code' => '32.10.18',
                'name' => 'Panyingkiran',
            ),

            array (
                'code' => '32.10.19',
                'name' => 'Palasah',
            ),

            array (
                'code' => '32.10.20',
                'name' => 'Cigasong',
            ),

            array (
                'code' => '32.10.21',
                'name' => 'Sindangwangi',
            ),

            array (
                'code' => '32.10.22',
                'name' => 'Banjaran',
            ),

            array (
                'code' => '32.10.23',
                'name' => 'Cingambul',
            ),

            array (
                'code' => '32.10.24',
                'name' => 'Kasokandel',
            ),

            array (
                'code' => '32.10.25',
                'name' => 'Sindang',
            ),

            array (
                'code' => '32.10.26',
                'name' => 'Malausma',
            ),

            array (
                'code' => '32.11',
                'name' => 'KAB. SUMEDANG',
            ),

            array (
                'code' => '32.11.01',
                'name' => 'Wado',
            ),

            array (
                'code' => '32.11.02',
                'name' => 'Jatinunggal',
            ),

            array (
                'code' => '32.11.03',
                'name' => 'Darmaraja',
            ),

            array (
                'code' => '32.11.04',
                'name' => 'Cibugel',
            ),

            array (
                'code' => '32.11.05',
                'name' => 'Cisitu',
            ),

            array (
                'code' => '32.11.06',
                'name' => 'Situraja',
            ),

            array (
                'code' => '32.11.07',
                'name' => 'Conggeang',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '32.11.08',
                'name' => 'Paseh',
            ),

            array (
                'code' => '32.11.09',
                'name' => 'Surian',
            ),

            array (
                'code' => '32.11.10',
                'name' => 'Buahdua',
            ),

            array (
                'code' => '32.11.11',
                'name' => 'Tanjungsari',
            ),

            array (
                'code' => '32.11.12',
                'name' => 'Sukasari',
            ),

            array (
                'code' => '32.11.13',
                'name' => 'Pamulihan',
            ),

            array (
                'code' => '32.11.14',
                'name' => 'Cimanggung',
            ),

            array (
                'code' => '32.11.15',
                'name' => 'Jatinangor',
            ),

            array (
                'code' => '32.11.16',
                'name' => 'Rancakalong',
            ),

            array (
                'code' => '32.11.17',
                'name' => 'Sumedang Selatan',
            ),

            array (
                'code' => '32.11.18',
                'name' => 'Sumedang Utara',
            ),

            array (
                'code' => '32.11.19',
                'name' => 'Ganeas',
            ),

            array (
                'code' => '32.11.20',
                'name' => 'Tanjungkerta',
            ),

            array (
                'code' => '32.11.21',
                'name' => 'Tanjungmedar',
            ),

            array (
                'code' => '32.11.22',
                'name' => 'Cimalaka',
            ),

            array (
                'code' => '32.11.23',
                'name' => 'Cisarua',
            ),

            array (
                'code' => '32.11.24',
                'name' => 'Tomo',
            ),

            array (
                'code' => '32.11.25',
                'name' => 'Ujungjaya',
            ),

            array (
                'code' => '32.11.26',
                'name' => 'Jatigede',
            ),

            array (
                'code' => '32.12',
                'name' => 'KAB. INDRAMAYU',
            ),

            array (
                'code' => '32.12.01',
                'name' => 'Haurgeulis',
            ),

            array (
                'code' => '32.12.02',
                'name' => 'Kroya',
            ),

            array (
                'code' => '32.12.03',
                'name' => 'Gabuswetan',
            ),

            array (
                'code' => '32.12.04',
                'name' => 'Cikedung',
            ),

            array (
                'code' => '32.12.05',
                'name' => 'Lelea',
            ),

            array (
                'code' => '32.12.06',
                'name' => 'Bangodua',
            ),

            array (
                'code' => '32.12.07',
                'name' => 'Widasari',
            ),

            array (
                'code' => '32.12.08',
                'name' => 'Kertasemaya',
            ),

            array (
                'code' => '32.12.09',
                'name' => 'Krangkeng',
            ),

            array (
                'code' => '32.12.10',
                'name' => 'Karangampel',
            ),

            array (
                'code' => '32.12.11',
                'name' => 'Juntinyuat',
            ),

            array (
                'code' => '32.12.12',
                'name' => 'Sliyeg',
            ),

            array (
                'code' => '32.12.13',
                'name' => 'Jatibarang',
            ),

            array (
                'code' => '32.12.14',
                'name' => 'Balongan',
            ),

            array (
                'code' => '32.12.15',
                'name' => 'Indramayu',
            ),

            array (
                'code' => '32.12.16',
                'name' => 'Sindang',
            ),

            array (
                'code' => '32.12.17',
                'name' => 'Cantigi',
            ),

            array (
                'code' => '32.12.18',
                'name' => 'Lohbener',
            ),

            array (
                'code' => '32.12.19',
                'name' => 'Arahan',
            ),

            array (
                'code' => '32.12.20',
                'name' => 'Losarang',
            ),

            array (
                'code' => '32.12.21',
                'name' => 'Kandanghaur',
            ),

            array (
                'code' => '32.12.22',
                'name' => 'Bongas',
            ),

            array (
                'code' => '32.12.23',
                'name' => 'Anjatan',
            ),

            array (
                'code' => '32.12.24',
                'name' => 'Sukra',
            ),

            array (
                'code' => '32.12.25',
                'name' => 'Gantar',
            ),

            array (
                'code' => '32.12.26',
                'name' => 'Trisi',
            ),

            array (
                'code' => '32.12.27',
                'name' => 'Sukagumiwang',
            ),

            array (
                'code' => '32.12.28',
                'name' => 'Kedokan Bunder',
            ),

            array (
                'code' => '32.12.29',
                'name' => 'Pasekan',
            ),

            array (
                'code' => '32.12.30',
                'name' => 'Tukdana',
            ),

            array (
                'code' => '32.12.31',
                'name' => 'Patrol',
            ),

            array (
                'code' => '32.13',
                'name' => 'KAB. SUBANG',
            ),

            array (
                'code' => '32.13.01',
                'name' => 'Sagalaherang',
            ),

            array (
                'code' => '32.13.02',
                'name' => 'Cisalak',
            ),

            array (
                'code' => '32.13.03',
                'name' => 'Subang',
            ),

            array (
                'code' => '32.13.04',
                'name' => 'Kalijati',
            ),

            array (
                'code' => '32.13.05',
                'name' => 'Pabuaran',
            ),

            array (
                'code' => '32.13.06',
                'name' => 'Purwadadi',
            ),

            array (
                'code' => '32.13.07',
                'name' => 'Pagaden',
            ),

            array (
                'code' => '32.13.08',
                'name' => 'Binong',
            ),

            array (
                'code' => '32.13.09',
                'name' => 'Ciasem',
            ),

            array (
                'code' => '32.13.10',
                'name' => 'Pusakanagara',
            ),

            array (
                'code' => '32.13.11',
                'name' => 'Pamanukan',
            ),

            array (
                'code' => '32.13.12',
                'name' => 'Jalancagak',
            ),

            array (
                'code' => '32.13.13',
                'name' => 'Blanakan',
            ),

            array (
                'code' => '32.13.14',
                'name' => 'Tanjungsiang',
            ),

            array (
                'code' => '32.13.15',
                'name' => 'Compreng',
            ),

            array (
                'code' => '32.13.16',
                'name' => 'Patokbeusi',
            ),

            array (
                'code' => '32.13.17',
                'name' => 'Cibogo',
            ),

            array (
                'code' => '32.13.18',
                'name' => 'Cipunagara',
            ),

            array (
                'code' => '32.13.19',
                'name' => 'Cijambe',
            ),

            array (
                'code' => '32.13.20',
                'name' => 'Cipeunduey',
            ),

            array (
                'code' => '32.13.21',
                'name' => 'Legonkulon',
            ),

            array (
                'code' => '32.13.22',
                'name' => 'Cikaum',
            ),

            array (
                'code' => '32.13.23',
                'name' => 'Serangpanjang',
            ),

            array (
                'code' => '32.13.24',
                'name' => 'Sukasari',
            ),

            array (
                'code' => '32.13.25',
                'name' => 'Tambakdahan',
            ),

            array (
                'code' => '32.13.26',
                'name' => 'Kasomalang',
            ),

            array (
                'code' => '32.13.27',
                'name' => 'Dawuan',
            ),

            array (
                'code' => '32.13.28',
                'name' => 'Pagaden Barat',
            ),

            array (
                'code' => '32.13.29',
                'name' => 'Ciater',
            ),

            array (
                'code' => '32.13.30',
                'name' => 'Pusakajaya',
            ),

            array (
                'code' => '32.14',
                'name' => 'KAB. PURWAKARTA',
            ),

            array (
                'code' => '32.14.01',
                'name' => 'Purwakarta',
            ),

            array (
                'code' => '32.14.02',
                'name' => 'Campaka',
            ),

            array (
                'code' => '32.14.03',
                'name' => 'Jatiluhur',
            ),

            array (
                'code' => '32.14.04',
                'name' => 'Plered',
            ),

            array (
                'code' => '32.14.05',
                'name' => 'Sukatani',
            ),

            array (
                'code' => '32.14.06',
                'name' => 'Darangdan',
            ),

            array (
                'code' => '32.14.07',
                'name' => 'Maniis',
            ),

            array (
                'code' => '32.14.08',
                'name' => 'Tegalwaru',
            ),

            array (
                'code' => '32.14.09',
                'name' => 'Wanayasa',
            ),

            array (
                'code' => '32.14.10',
                'name' => 'Pasawahan',
            ),

            array (
                'code' => '32.14.11',
                'name' => 'Bojong',
            ),

            array (
                'code' => '32.14.12',
                'name' => 'Babakancikao',
            ),

            array (
                'code' => '32.14.13',
                'name' => 'Bungursari',
            ),

            array (
                'code' => '32.14.14',
                'name' => 'Cibatu',
            ),

            array (
                'code' => '32.14.15',
                'name' => 'Sukasari',
            ),

            array (
                'code' => '32.14.16',
                'name' => 'Pondoksalam',
            ),

            array (
                'code' => '32.14.17',
                'name' => 'Kiarapedes',
            ),

            array (
                'code' => '32.15',
                'name' => 'KAB. KARAWANG',
            ),

            array (
                'code' => '32.15.01',
                'name' => 'Karawang Barat',
            ),

            array (
                'code' => '32.15.02',
                'name' => 'Pangkalan',
            ),

            array (
                'code' => '32.15.03',
                'name' => 'Telukjambe Timur',
            ),

            array (
                'code' => '32.15.04',
                'name' => 'Ciampel',
            ),

            array (
                'code' => '32.15.05',
                'name' => 'Klari',
            ),

            array (
                'code' => '32.15.06',
                'name' => 'Rengasdengklok',
            ),

            array (
                'code' => '32.15.07',
                'name' => 'Kutawaluya',
            ),

            array (
                'code' => '32.15.08',
                'name' => 'Batujaya',
            ),

            array (
                'code' => '32.15.09',
                'name' => 'Tirtajaya',
            ),

            array (
                'code' => '32.15.10',
                'name' => 'Pedes',
            ),

            array (
                'code' => '32.15.11',
                'name' => 'Cibuaya',
            ),

            array (
                'code' => '32.15.12',
                'name' => 'Pakisjaya',
            ),

            array (
                'code' => '32.15.13',
                'name' => 'Cikampek',
            ),

            array (
                'code' => '32.15.14',
                'name' => 'Jatisari',
            ),

            array (
                'code' => '32.15.15',
                'name' => 'Cilamaya Wetan',
            ),

            array (
                'code' => '32.15.16',
                'name' => 'Tirtamulya',
            ),

            array (
                'code' => '32.15.17',
                'name' => 'Telagasari',
            ),

            array (
                'code' => '32.15.18',
                'name' => 'Rawamerta',
            ),

            array (
                'code' => '32.15.19',
                'name' => 'Lemahabang',
            ),

            array (
                'code' => '32.15.20',
                'name' => 'Tempuran',
            ),

            array (
                'code' => '32.15.21',
                'name' => 'Majalaya',
            ),

            array (
                'code' => '32.15.22',
                'name' => 'Jayakerta',
            ),

            array (
                'code' => '32.15.23',
                'name' => 'Cilamaya Kulon',
            ),

            array (
                'code' => '32.15.24',
                'name' => 'Banyusari',
            ),

            array (
                'code' => '32.15.25',
                'name' => 'Kota Baru',
            ),

            array (
                'code' => '32.15.26',
                'name' => 'Karawang Timur',
            ),

            array (
                'code' => '32.15.27',
                'name' => 'Telukjambe Barat',
            ),

            array (
                'code' => '32.15.28',
                'name' => 'Tegalwaru',
            ),

            array (
                'code' => '32.15.29',
                'name' => 'Purwasari',
            ),

            array (
                'code' => '32.15.30',
                'name' => 'Cilebar',
            ),

            array (
                'code' => '32.16',
                'name' => 'KAB. BEKASI',
            ),

            array (
                'code' => '32.16.01',
                'name' => 'Tarumajaya',
            ),

            array (
                'code' => '32.16.02',
                'name' => 'Babelan',
            ),

            array (
                'code' => '32.16.03',
                'name' => 'Sukawangi',
            ),

            array (
                'code' => '32.16.04',
                'name' => 'Tambelang',
            ),

            array (
                'code' => '32.16.05',
                'name' => 'Tambun Utara',
            ),

            array (
                'code' => '32.16.06',
                'name' => 'Tambun Selatan',
            ),

            array (
                'code' => '32.16.07',
                'name' => 'Cibitung',
            ),

            array (
                'code' => '32.16.08',
                'name' => 'Cikarang Barat',
            ),

            array (
                'code' => '32.16.09',
                'name' => 'Cikarang Utara',
            ),

            array (
                'code' => '32.16.10',
                'name' => 'Karang Bahagia',
            ),

            array (
                'code' => '32.16.11',
                'name' => 'Cikarang Timur',
            ),

            array (
                'code' => '32.16.12',
                'name' => 'Kedung Waringin',
            ),

            array (
                'code' => '32.16.13',
                'name' => 'Pebayuran',
            ),

            array (
                'code' => '32.16.14',
                'name' => 'Sukakarya',
            ),

            array (
                'code' => '32.16.15',
                'name' => 'Sukatani',
            ),

            array (
                'code' => '32.16.16',
                'name' => 'Cabangbungin',
            ),

            array (
                'code' => '32.16.17',
                'name' => 'Muaragembong',
            ),

            array (
                'code' => '32.16.18',
                'name' => 'Setu',
            ),

            array (
                'code' => '32.16.19',
                'name' => 'Cikarang Selatan',
            ),

            array (
                'code' => '32.16.20',
                'name' => 'Cikarang Pusat',
            ),

            array (
                'code' => '32.16.21',
                'name' => 'Serang Baru',
            ),

            array (
                'code' => '32.16.22',
                'name' => 'Cibarusah',
            ),

            array (
                'code' => '32.16.23',
                'name' => 'Bojongmangu',
            ),

            array (
                'code' => '32.17',
                'name' => 'KAB. BANDUNG BARAT',
            ),

            array (
                'code' => '32.17.01',
                'name' => 'Lembang',
            ),

            array (
                'code' => '32.17.02',
                'name' => 'Parongpong',
            ),

            array (
                'code' => '32.17.03',
                'name' => 'Cisarua',
            ),

            array (
                'code' => '32.17.04',
                'name' => 'Cikalongwetan',
            ),

            array (
                'code' => '32.17.05',
                'name' => 'Cipeundeuy',
            ),

            array (
                'code' => '32.17.06',
                'name' => 'Ngamprah',
            ),

            array (
                'code' => '32.17.07',
                'name' => 'Cipatat',
            ),

            array (
                'code' => '32.17.08',
                'name' => 'Padalarang',
            ),

            array (
                'code' => '32.17.09',
                'name' => 'Batujajar',
            ),

            array (
                'code' => '32.17.10',
                'name' => 'Cihampelas',
            ),

            array (
                'code' => '32.17.11',
                'name' => 'Cililin',
            ),

            array (
                'code' => '32.17.12',
                'name' => 'Cipongkor',
            ),

            array (
                'code' => '32.17.13',
                'name' => 'Rongga',
            ),

            array (
                'code' => '32.17.14',
                'name' => 'Sindangkerta',
            ),

            array (
                'code' => '32.17.15',
                'name' => 'Gununghalu',
            ),

            array (
                'code' => '32.17.16',
                'name' => 'Saguling',
            ),

            array (
                'code' => '32.18',
                'name' => 'KAB. PANGANDARAN',
            ),

            array (
                'code' => '32.18.01',
                'name' => 'Parigi',
            ),

            array (
                'code' => '32.18.02',
                'name' => 'Cijulang',
            ),

            array (
                'code' => '32.18.03',
                'name' => 'Cimerak',
            ),

            array (
                'code' => '32.18.04',
                'name' => 'Cigugur',
            ),

            array (
                'code' => '32.18.05',
                'name' => 'Langkaplancar',
            ),

            array (
                'code' => '32.18.06',
                'name' => 'Mangunjaya',
            ),

            array (
                'code' => '32.18.07',
                'name' => 'Padaherang',
            ),

            array (
                'code' => '32.18.08',
                'name' => 'Kalipucang',
            ),

            array (
                'code' => '32.18.09',
                'name' => 'Pangandaran',
            ),

            array (
                'code' => '32.18.10',
                'name' => 'Sidamulih',
            ),

            array (
                'code' => '32.71',
                'name' => 'KOTA BOGOR',
            ),

            array (
                'code' => '32.71.01',
                'name' => 'Bogor Selatan',
            ),

            array (
                'code' => '32.71.02',
                'name' => 'Bogor Timur',
            ),

            array (
                'code' => '32.71.03',
                'name' => 'Bogor Tengah',
            ),

            array (
                'code' => '32.71.04',
                'name' => 'Bogor Barat',
            ),

            array (
                'code' => '32.71.05',
                'name' => 'Bogor Utara',
            ),

            array (
                'code' => '32.71.06',
                'name' => 'Tanah Sareal',
            ),

            array (
                'code' => '32.72',
                'name' => 'KOTA SUKABUMI',
            ),

            array (
                'code' => '32.72.01',
                'name' => 'Gunung Puyuh',
            ),

            array (
                'code' => '32.72.02',
                'name' => 'Cikole',
            ),

            array (
                'code' => '32.72.03',
                'name' => 'Citamiang',
            ),

            array (
                'code' => '32.72.04',
                'name' => 'Warudoyong',
            ),

            array (
                'code' => '32.72.05',
                'name' => 'Baros',
            ),

            array (
                'code' => '32.72.06',
                'name' => 'Lembursitu',
            ),

            array (
                'code' => '32.72.07',
                'name' => 'Cibeureum',
            ),

            array (
                'code' => '32.73',
                'name' => 'KOTA BANDUNG',
            ),

            array (
                'code' => '32.73.01',
                'name' => 'Sukasari',
            ),

            array (
                'code' => '32.73.02',
                'name' => 'Coblong',
            ),

            array (
                'code' => '32.73.03',
                'name' => 'Babakan Ciparay',
            ),

            array (
                'code' => '32.73.04',
                'name' => 'Bojongloa Kaler',
            ),

            array (
                'code' => '32.73.05',
                'name' => 'Andir',
            ),

            array (
                'code' => '32.73.06',
                'name' => 'Cicendo',
            ),

            array (
                'code' => '32.73.07',
                'name' => 'Sukajadi',
            ),

            array (
                'code' => '32.73.08',
                'name' => 'Cidadap',
            ),

            array (
                'code' => '32.73.09',
                'name' => 'Bandung Wetan',
            ),

            array (
                'code' => '32.73.10',
                'name' => 'Astana Anyar',
            ),

            array (
                'code' => '32.73.11',
                'name' => 'Regol',
            ),

            array (
                'code' => '32.73.12',
                'name' => 'Batununggal',
            ),

            array (
                'code' => '32.73.13',
                'name' => 'Lengkong',
            ),

            array (
                'code' => '32.73.14',
                'name' => 'Cibeunying Kidul',
            ),

            array (
                'code' => '32.73.15',
                'name' => 'Bandung Kulon',
            ),

            array (
                'code' => '32.73.16',
                'name' => 'Kiaracondong',
            ),

            array (
                'code' => '32.73.17',
                'name' => 'Bojongloa Kidul',
            ),

            array (
                'code' => '32.73.18',
                'name' => 'Cibeunying Kaler',
            ),

            array (
                'code' => '32.73.19',
                'name' => 'Sumur Bandung',
            ),

            array (
                'code' => '32.73.20',
                'name' => 'Antapani',
            ),

            array (
                'code' => '32.73.21',
                'name' => 'Bandung Kidul',
            ),

            array (
                'code' => '32.73.22',
                'name' => 'Buahbatu',
            ),

            array (
                'code' => '32.73.23',
                'name' => 'Rancasari',
            ),

            array (
                'code' => '32.73.24',
                'name' => 'Arcamanik',
            ),

            array (
                'code' => '32.73.25',
                'name' => 'Cibiru',
            ),

            array (
                'code' => '32.73.26',
                'name' => 'Ujung Berung',
            ),

            array (
                'code' => '32.73.27',
                'name' => 'Gedebage',
            ),

            array (
                'code' => '32.73.28',
                'name' => 'Panyileukan',
            ),

            array (
                'code' => '32.73.29',
                'name' => 'Cinambo',
            ),

            array (
                'code' => '32.73.30',
                'name' => 'Mandalajati',
            ),

            array (
                'code' => '32.74',
                'name' => 'KOTA CIREBON',
            ),

            array (
                'code' => '32.74.01',
                'name' => 'Kejaksan',
            ),

            array (
                'code' => '32.74.02',
                'name' => 'Lemah Wungkuk',
            ),

            array (
                'code' => '32.74.03',
                'name' => 'Harjamukti',
            ),

            array (
                'code' => '32.74.04',
                'name' => 'Pekalipan',
            ),

            array (
                'code' => '32.74.05',
                'name' => 'Kesambi',
            ),

            array (
                'code' => '32.75',
                'name' => 'KOTA BEKASI',
            ),

            array (
                'code' => '32.75.01',
                'name' => 'Bekasi Timur',
            ),

            array (
                'code' => '32.75.02',
                'name' => 'Bekasi Barat',
            ),

            array (
                'code' => '32.75.03',
                'name' => 'Bekasi Utara',
            ),

            array (
                'code' => '32.75.04',
                'name' => 'Bekasi Selatan',
            ),

            array (
                'code' => '32.75.05',
                'name' => 'Rawa Lumbu',
            ),

            array (
                'code' => '32.75.06',
                'name' => 'Medan Satria',
            ),

            array (
                'code' => '32.75.07',
                'name' => 'Bantar Gebang',
            ),

            array (
                'code' => '32.75.08',
                'name' => 'Pondok Gede',
            ),

            array (
                'code' => '32.75.09',
                'name' => 'Jatiasih',
            ),

            array (
                'code' => '32.75.10',
                'name' => 'Jati Sempurna',
            ),

            array (
                'code' => '32.75.11',
                'name' => 'Mustika Jaya',
            ),

            array (
                'code' => '32.75.12',
                'name' => 'Pondok Melati',
            ),

            array (
                'code' => '32.76',
                'name' => 'KOTA DEPOK',
            ),

            array (
                'code' => '32.76.01',
                'name' => 'Pancoran Mas',
            ),

            array (
                'code' => '32.76.02',
                'name' => 'Cimanggis',
            ),

            array (
                'code' => '32.76.03',
                'name' => 'Sawangan',
            ),

            array (
                'code' => '32.76.04',
                'name' => 'Limo',
            ),

            array (
                'code' => '32.76.05',
                'name' => 'Sukmajaya',
            ),

            array (
                'code' => '32.76.06',
                'name' => 'Beji',
            ),

            array (
                'code' => '32.76.07',
                'name' => 'Cipayung',
            ),

            array (
                'code' => '32.76.08',
                'name' => 'Cilodong',
            ),

            array (
                'code' => '32.76.09',
                'name' => 'Cinere',
            ),

            array (
                'code' => '32.76.10',
                'name' => 'Tapos',
            ),

            array (
                'code' => '32.76.11',
                'name' => 'Bojongsari',
            ),

            array (
                'code' => '32.77',
                'name' => 'KOTA CIMAHI',
            ),

            array (
                'code' => '32.77.01',
                'name' => 'Cimahi Selatan',
            ),

            array (
                'code' => '32.77.02',
                'name' => 'Cimahi Tengah',
            ),

            array (
                'code' => '32.77.03',
                'name' => 'Cimahi Utara',
            ),

            array (
                'code' => '32.78',
                'name' => 'KOTA TASIKMALAYA',
            ),

            array (
                'code' => '32.78.01',
                'name' => 'Cihideung',
            ),

            array (
                'code' => '32.78.02',
                'name' => 'Cipedes',
            ),

            array (
                'code' => '32.78.03',
                'name' => 'Tawang',
            ),

            array (
                'code' => '32.78.04',
                'name' => 'Indihiang',
            ),

            array (
                'code' => '32.78.05',
                'name' => 'Kawalu',
            ),

            array (
                'code' => '32.78.06',
                'name' => 'Cibeureum',
            ),

            array (
                'code' => '32.78.07',
                'name' => 'Tamansari',
            ),

            array (
                'code' => '32.78.08',
                'name' => 'Mangkubumi',
            ),

            array (
                'code' => '32.78.09',
                'name' => 'Bungursari',
            ),

            array (
                'code' => '32.78.10',
                'name' => 'Purbaratu',
            ),

            array (
                'code' => '32.79',
                'name' => 'KOTA BANJAR',
            ),

            array (
                'code' => '32.79.01',
                'name' => 'Banjar',
            ),

            array (
                'code' => '32.79.02',
                'name' => 'Pataruman',
            ),

            array (
                'code' => '32.79.03',
                'name' => 'Purwaharja',
            ),

            array (
                'code' => '32.79.04',
                'name' => 'Langensari',
            ),

            array (
                'code' => '33',
                'name' => 'JAWA TENGAH',
            ),

            array (
                'code' => '33.01',
                'name' => 'KAB. CILACAP',
            ),

            array (
                'code' => '33.01.01',
                'name' => 'Kedungreja',
            ),

            array (
                'code' => '33.01.02',
                'name' => 'Kesugihan',
            ),

            array (
                'code' => '33.01.03',
                'name' => 'Adipala',
            ),

            array (
                'code' => '33.01.04',
                'name' => 'Binangun',
            ),

            array (
                'code' => '33.01.05',
                'name' => 'Nusawungu',
            ),

            array (
                'code' => '33.01.06',
                'name' => 'Kroya',
            ),

            array (
                'code' => '33.01.07',
                'name' => 'Maos',
            ),

            array (
                'code' => '33.01.08',
                'name' => 'Jeruklegi',
            ),

            array (
                'code' => '33.01.09',
                'name' => 'Kawunganten',
            ),

            array (
                'code' => '33.01.10',
                'name' => 'Gandrungmangu',
            ),

            array (
                'code' => '33.01.11',
                'name' => 'Sidareja',
            ),

            array (
                'code' => '33.01.12',
                'name' => 'Karangpucung',
            ),

            array (
                'code' => '33.01.13',
                'name' => 'Cimanggu',
            ),

            array (
                'code' => '33.01.14',
                'name' => 'Majenang',
            ),

            array (
                'code' => '33.01.15',
                'name' => 'Wanareja',
            ),

            array (
                'code' => '33.01.16',
                'name' => 'Dayeuhluhur',
            ),

            array (
                'code' => '33.01.17',
                'name' => 'Sampang',
            ),

            array (
                'code' => '33.01.18',
                'name' => 'Cipari',
            ),

            array (
                'code' => '33.01.19',
                'name' => 'Patimuan',
            ),

            array (
                'code' => '33.01.20',
                'name' => 'Bantarsari',
            ),

            array (
                'code' => '33.01.21',
                'name' => 'Cilacap Selatan',
            ),

            array (
                'code' => '33.01.22',
                'name' => 'Cilacap Tengah',
            ),

            array (
                'code' => '33.01.23',
                'name' => 'Cilacap Utara',
            ),

            array (
                'code' => '33.01.24',
                'name' => 'Kampung Laut',
            ),

            array (
                'code' => '33.02',
                'name' => 'KAB. BANYUMAS',
            ),

            array (
                'code' => '33.02.01',
                'name' => 'Lumbir',
            ),

            array (
                'code' => '33.02.02',
                'name' => 'Wangon',
            ),

            array (
                'code' => '33.02.03',
                'name' => 'Jatilawang',
            ),

            array (
                'code' => '33.02.04',
                'name' => 'Rawalo',
            ),

            array (
                'code' => '33.02.05',
                'name' => 'Kebasen',
            ),

            array (
                'code' => '33.02.06',
                'name' => 'Kemranjen',
            ),

            array (
                'code' => '33.02.07',
                'name' => 'Sumpiuh',
            ),

            array (
                'code' => '33.02.08',
                'name' => 'Tambak',
            ),

            array (
                'code' => '33.02.09',
                'name' => 'Somagede',
            ),

            array (
                'code' => '33.02.10',
                'name' => 'Kalibagor',
            ),

            array (
                'code' => '33.02.11',
                'name' => 'Banyumas',
            ),

            array (
                'code' => '33.02.12',
                'name' => 'Patikraja',
            ),

            array (
                'code' => '33.02.13',
                'name' => 'Purwojati',
            ),

            array (
                'code' => '33.02.14',
                'name' => 'Ajibarang',
            ),

            array (
                'code' => '33.02.15',
                'name' => 'Gumelar',
            ),

            array (
                'code' => '33.02.16',
                'name' => 'Pekuncen',
            ),

            array (
                'code' => '33.02.17',
                'name' => 'Cilongok',
            ),

            array (
                'code' => '33.02.18',
                'name' => 'Karanglewas',
            ),

            array (
                'code' => '33.02.19',
                'name' => 'Sokaraja',
            ),

            array (
                'code' => '33.02.20',
                'name' => 'Kembaran',
            ),

            array (
                'code' => '33.02.21',
                'name' => 'Sumbang',
            ),

            array (
                'code' => '33.02.22',
                'name' => 'Baturraden',
            ),

            array (
                'code' => '33.02.23',
                'name' => 'Kedungbanteng',
            ),

            array (
                'code' => '33.02.24',
                'name' => 'Purwokerto Selatan',
            ),

            array (
                'code' => '33.02.25',
                'name' => 'Purwokerto Barat',
            ),

            array (
                'code' => '33.02.26',
                'name' => 'Purwokerto Timur',
            ),

            array (
                'code' => '33.02.27',
                'name' => 'Purwokerto Utara',
            ),

            array (
                'code' => '33.03',
                'name' => 'KAB. PURBALINGGA',
            ),

            array (
                'code' => '33.03.01',
                'name' => 'Kemangkon',
            ),

            array (
                'code' => '33.03.02',
                'name' => 'Bukateja',
            ),

            array (
                'code' => '33.03.03',
                'name' => 'Kejobong',
            ),

            array (
                'code' => '33.03.04',
                'name' => 'Kaligondang',
            ),

            array (
                'code' => '33.03.05',
                'name' => 'Purbalingga',
            ),

            array (
                'code' => '33.03.06',
                'name' => 'Kalimanah',
            ),

            array (
                'code' => '33.03.07',
                'name' => 'Kutasari',
            ),

            array (
                'code' => '33.03.08',
                'name' => 'Mrebet',
            ),

            array (
                'code' => '33.03.09',
                'name' => 'Bobotsari',
            ),

            array (
                'code' => '33.03.10',
                'name' => 'Karangreja',
            ),

            array (
                'code' => '33.03.11',
                'name' => 'Karanganyar',
            ),

            array (
                'code' => '33.03.12',
                'name' => 'Karangmoncol',
            ),

            array (
                'code' => '33.03.13',
                'name' => 'Rembang',
            ),

            array (
                'code' => '33.03.14',
                'name' => 'Bojongsari',
            ),

            array (
                'code' => '33.03.15',
                'name' => 'Padamara',
            ),

            array (
                'code' => '33.03.16',
                'name' => 'Pengadegan',
            ),

            array (
                'code' => '33.03.17',
                'name' => 'Karangjambu',
            ),

            array (
                'code' => '33.03.18',
                'name' => 'Kertanegara',
            ),

            array (
                'code' => '33.04',
                'name' => 'KAB. BANJARNEGARA',
            ),

            array (
                'code' => '33.04.01',
                'name' => 'Susukan',
            ),

            array (
                'code' => '33.04.02',
                'name' => 'Purworeja Klampok',
            ),

            array (
                'code' => '33.04.03',
                'name' => 'Mandiraja',
            ),

            array (
                'code' => '33.04.04',
                'name' => 'Purwanegara',
            ),

            array (
                'code' => '33.04.05',
                'name' => 'Bawang',
            ),

            array (
                'code' => '33.04.06',
                'name' => 'Banjarnegara',
            ),

            array (
                'code' => '33.04.07',
                'name' => 'Sigaluh',
            ),

            array (
                'code' => '33.04.08',
                'name' => 'Madukara',
            ),

            array (
                'code' => '33.04.09',
                'name' => 'Banjarmangu',
            ),

            array (
                'code' => '33.04.10',
                'name' => 'Wanadadi',
            ),

            array (
                'code' => '33.04.11',
                'name' => 'Rakit',
            ),

            array (
                'code' => '33.04.12',
                'name' => 'Punggelan',
            ),

            array (
                'code' => '33.04.13',
                'name' => 'Karangkobar',
            ),

            array (
                'code' => '33.04.14',
                'name' => 'Pagentan',
            ),

            array (
                'code' => '33.04.15',
                'name' => 'Pejawaran',
            ),

            array (
                'code' => '33.04.16',
                'name' => 'Batur',
            ),

            array (
                'code' => '33.04.17',
                'name' => 'Wanayasa',
            ),

            array (
                'code' => '33.04.18',
                'name' => 'Kalibening',
            ),

            array (
                'code' => '33.04.19',
                'name' => 'Pandanarum',
            ),

            array (
                'code' => '33.04.20',
                'name' => 'Pagedongan',
            ),

            array (
                'code' => '33.05',
                'name' => 'KAB. KEBUMEN',
            ),

            array (
                'code' => '33.05.01',
                'name' => 'Ayah',
            ),

            array (
                'code' => '33.05.02',
                'name' => 'Buayan',
            ),

            array (
                'code' => '33.05.03',
                'name' => 'Puring',
            ),

            array (
                'code' => '33.05.04',
                'name' => 'Petanahan',
            ),

            array (
                'code' => '33.05.05',
                'name' => 'Klirong',
            ),

            array (
                'code' => '33.05.06',
                'name' => 'Buluspesantren',
            ),

            array (
                'code' => '33.05.07',
                'name' => 'Ambal',
            ),

            array (
                'code' => '33.05.08',
                'name' => 'Mirit',
            ),

            array (
                'code' => '33.05.09',
                'name' => 'Prembun',
            ),

            array (
                'code' => '33.05.10',
                'name' => 'Kutowinangun',
            ),

            array (
                'code' => '33.05.11',
                'name' => 'Alian',
            ),

            array (
                'code' => '33.05.12',
                'name' => 'Kebumen',
            ),

            array (
                'code' => '33.05.13',
                'name' => 'Pejagoan',
            ),

            array (
                'code' => '33.05.14',
                'name' => 'Sruweng',
            ),

            array (
                'code' => '33.05.15',
                'name' => 'Adimulyo',
            ),

            array (
                'code' => '33.05.16',
                'name' => 'Kuwarasan',
            ),

            array (
                'code' => '33.05.17',
                'name' => 'Rowokele',
            ),

            array (
                'code' => '33.05.18',
                'name' => 'Sempor',
            ),

            array (
                'code' => '33.05.19',
                'name' => 'Gombong',
            ),

            array (
                'code' => '33.05.20',
                'name' => 'Karanganyar',
            ),

            array (
                'code' => '33.05.21',
                'name' => 'Karanggayam',
            ),

            array (
                'code' => '33.05.22',
                'name' => 'Sadang',
            ),

            array (
                'code' => '33.05.23',
                'name' => 'Bonorowo',
            ),

            array (
                'code' => '33.05.24',
                'name' => 'Padureso',
            ),

            array (
                'code' => '33.05.25',
                'name' => 'Poncowarno',
            ),

            array (
                'code' => '33.05.26',
                'name' => 'Karangsambung',
            ),

            array (
                'code' => '33.06',
                'name' => 'KAB. PURWOREJO',
            ),

            array (
                'code' => '33.06.01',
                'name' => 'Grabag',
            ),

            array (
                'code' => '33.06.02',
                'name' => 'Ngombol',
            ),

            array (
                'code' => '33.06.03',
                'name' => 'Purwodadi',
            ),

            array (
                'code' => '33.06.04',
                'name' => 'Bagelen',
            ),

            array (
                'code' => '33.06.05',
                'name' => 'Kaligesing',
            ),

            array (
                'code' => '33.06.06',
                'name' => 'Purworejo',
            ),

            array (
                'code' => '33.06.07',
                'name' => 'Banyuurip',
            ),

            array (
                'code' => '33.06.08',
                'name' => 'Bayan',
            ),

            array (
                'code' => '33.06.09',
                'name' => 'Kutoarjo',
            ),

            array (
                'code' => '33.06.10',
                'name' => 'Butuh',
            ),

            array (
                'code' => '33.06.11',
                'name' => 'Pituruh',
            ),

            array (
                'code' => '33.06.12',
                'name' => 'Kemiri',
            ),

            array (
                'code' => '33.06.13',
                'name' => 'Bruno',
            ),

            array (
                'code' => '33.06.14',
                'name' => 'Gebang',
            ),

            array (
                'code' => '33.06.15',
                'name' => 'Loano',
            ),

            array (
                'code' => '33.06.16',
                'name' => 'Bener',
            ),

            array (
                'code' => '33.07',
                'name' => 'KAB. WONOSOBO',
            ),

            array (
                'code' => '33.07.01',
                'name' => 'Wadaslintang',
            ),

            array (
                'code' => '33.07.02',
                'name' => 'Kepil',
            ),

            array (
                'code' => '33.07.03',
                'name' => 'Sapuran',
            ),

            array (
                'code' => '33.07.04',
                'name' => 'Kaliwiro',
            ),

            array (
                'code' => '33.07.05',
                'name' => 'Leksono',
            ),

            array (
                'code' => '33.07.06',
                'name' => 'Selomerto',
            ),

            array (
                'code' => '33.07.07',
                'name' => 'Kalikajar',
            ),

            array (
                'code' => '33.07.08',
                'name' => 'Kertek',
            ),

            array (
                'code' => '33.07.09',
                'name' => 'Wonosobo',
            ),

            array (
                'code' => '33.07.10',
                'name' => 'Watumalang',
            ),

            array (
                'code' => '33.07.11',
                'name' => 'Mojotengah',
            ),

            array (
                'code' => '33.07.12',
                'name' => 'Garung',
            ),

            array (
                'code' => '33.07.13',
                'name' => 'Kejajar',
            ),

            array (
                'code' => '33.07.14',
                'name' => 'Sukoharjo',
            ),

            array (
                'code' => '33.07.15',
                'name' => 'Kalibawang',
            ),

            array (
                'code' => '33.08',
                'name' => 'KAB. MAGELANG',
            ),

            array (
                'code' => '33.08.01',
                'name' => 'Salaman',
            ),

            array (
                'code' => '33.08.02',
                'name' => 'Borobudur',
            ),

            array (
                'code' => '33.08.03',
                'name' => 'Ngluwar',
            ),

            array (
                'code' => '33.08.04',
                'name' => 'Salam',
            ),

            array (
                'code' => '33.08.05',
                'name' => 'Srumbung',
            ),

            array (
                'code' => '33.08.06',
                'name' => 'Dukun',
            ),

            array (
                'code' => '33.08.07',
                'name' => 'Sawangan',
            ),

            array (
                'code' => '33.08.08',
                'name' => 'Muntilan',
            ),

            array (
                'code' => '33.08.09',
                'name' => 'Mungkid',
            ),

            array (
                'code' => '33.08.10',
                'name' => 'Mertoyudan',
            ),

            array (
                'code' => '33.08.11',
                'name' => 'Tempuran',
            ),

            array (
                'code' => '33.08.12',
                'name' => 'Kajoran',
            ),

            array (
                'code' => '33.08.13',
                'name' => 'Kaliangkrik',
            ),

            array (
                'code' => '33.08.14',
                'name' => 'Bandongan',
            ),

            array (
                'code' => '33.08.15',
                'name' => 'Candimulyo',
            ),

            array (
                'code' => '33.08.16',
                'name' => 'Pakis',
            ),

            array (
                'code' => '33.08.17',
                'name' => 'Ngablak',
            ),

            array (
                'code' => '33.08.18',
                'name' => 'Grabag',
            ),

            array (
                'code' => '33.08.19',
                'name' => 'Tegalrejo',
            ),

            array (
                'code' => '33.08.20',
                'name' => 'Secang',
            ),

            array (
                'code' => '33.08.21',
                'name' => 'Windusari',
            ),

            array (
                'code' => '33.09',
                'name' => 'KAB. BOYOLALI',
            ),

            array (
                'code' => '33.09.01',
                'name' => 'Selo',
            ),

            array (
                'code' => '33.09.02',
                'name' => 'Ampel',
            ),

            array (
                'code' => '33.09.03',
                'name' => 'Cepogo',
            ),

            array (
                'code' => '33.09.04',
                'name' => 'Musuk',
            ),

            array (
                'code' => '33.09.05',
                'name' => 'Boyolali',
            ),

            array (
                'code' => '33.09.06',
                'name' => 'Mojosongo',
            ),

            array (
                'code' => '33.09.07',
                'name' => 'Teras',
            ),

            array (
                'code' => '33.09.08',
                'name' => 'Sawit',
            ),

            array (
                'code' => '33.09.09',
                'name' => 'Banyudono',
            ),

            array (
                'code' => '33.09.10',
                'name' => 'Sambi',
            ),

            array (
                'code' => '33.09.11',
                'name' => 'Ngemplak',
            ),

            array (
                'code' => '33.09.12',
                'name' => 'Nogosari',
            ),

            array (
                'code' => '33.09.13',
                'name' => 'Simo',
            ),

            array (
                'code' => '33.09.14',
                'name' => 'Karanggede',
            ),

            array (
                'code' => '33.09.15',
                'name' => 'Klego',
            ),

            array (
                'code' => '33.09.16',
                'name' => 'Andong',
            ),

            array (
                'code' => '33.09.17',
                'name' => 'Kemusu',
            ),

            array (
                'code' => '33.09.18',
                'name' => 'Wonosegoro',
            ),

            array (
                'code' => '33.09.19',
                'name' => 'Juwangi',
            ),

            array (
                'code' => '33.10',
                'name' => 'KAB. KLATEN',
            ),

            array (
                'code' => '33.10.01',
                'name' => 'Prambanan',
            ),

            array (
                'code' => '33.10.02',
                'name' => 'Gantiwarno',
            ),

            array (
                'code' => '33.10.03',
                'name' => 'Wedi',
            ),

            array (
                'code' => '33.10.04',
                'name' => 'Bayat',
            ),

            array (
                'code' => '33.10.05',
                'name' => 'Cawas',
            ),

            array (
                'code' => '33.10.06',
                'name' => 'Trucuk',
            ),

            array (
                'code' => '33.10.07',
                'name' => 'Kebonarum',
            ),

            array (
                'code' => '33.10.08',
                'name' => 'Jogonalan',
            ),

            array (
                'code' => '33.10.09',
                'name' => 'Manisrenggo',
            ),

            array (
                'code' => '33.10.10',
                'name' => 'Karangnongko',
            ),

            array (
                'code' => '33.10.11',
                'name' => 'Ceper',
            ),

            array (
                'code' => '33.10.12',
                'name' => 'Pedan',
            ),

            array (
                'code' => '33.10.13',
                'name' => 'Karangdowo',
            ),

            array (
                'code' => '33.10.14',
                'name' => 'Juwiring',
            ),

            array (
                'code' => '33.10.15',
                'name' => 'Wonosari',
            ),

            array (
                'code' => '33.10.16',
                'name' => 'Delanggu',
            ),

            array (
                'code' => '33.10.17',
                'name' => 'Polanharjo',
            ),

            array (
                'code' => '33.10.18',
                'name' => 'Karanganom',
            ),

            array (
                'code' => '33.10.19',
                'name' => 'Tulung',
            ),

            array (
                'code' => '33.10.20',
                'name' => 'Jatinom',
            ),

            array (
                'code' => '33.10.21',
                'name' => 'Kemalang',
            ),

            array (
                'code' => '33.10.22',
                'name' => 'Ngawen',
            ),

            array (
                'code' => '33.10.23',
                'name' => 'Kalikotes',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '33.10.24',
                'name' => 'Klaten Utara',
            ),

            array (
                'code' => '33.10.25',
                'name' => 'Klaten Tengah',
            ),

            array (
                'code' => '33.10.26',
                'name' => 'Klaten Selatan',
            ),

            array (
                'code' => '33.11',
                'name' => 'KAB. SUKOHARJO',
            ),

            array (
                'code' => '33.11.01',
                'name' => 'Weru',
            ),

            array (
                'code' => '33.11.02',
                'name' => 'Bulu',
            ),

            array (
                'code' => '33.11.03',
                'name' => 'Tawangsari',
            ),

            array (
                'code' => '33.11.04',
                'name' => 'Sukoharjo',
            ),

            array (
                'code' => '33.11.05',
                'name' => 'Nguter',
            ),

            array (
                'code' => '33.11.06',
                'name' => 'Bendosari',
            ),

            array (
                'code' => '33.11.07',
                'name' => 'Polokarto',
            ),

            array (
                'code' => '33.11.08',
                'name' => 'Mojolaban',
            ),

            array (
                'code' => '33.11.09',
                'name' => 'Grogol',
            ),

            array (
                'code' => '33.11.10',
                'name' => 'Baki',
            ),

            array (
                'code' => '33.11.11',
                'name' => 'Gatak',
            ),

            array (
                'code' => '33.11.12',
                'name' => 'Kartasura',
            ),

            array (
                'code' => '33.12',
                'name' => 'KAB. WONOGIRI',
            ),

            array (
                'code' => '33.12.01',
                'name' => 'Pracimantoro',
            ),

            array (
                'code' => '33.12.02',
                'name' => 'Giritontro',
            ),

            array (
                'code' => '33.12.03',
                'name' => 'Giriwoyo',
            ),

            array (
                'code' => '33.12.04',
                'name' => 'Batuwarno',
            ),

            array (
                'code' => '33.12.05',
                'name' => 'Tirtomoyo',
            ),

            array (
                'code' => '33.12.06',
                'name' => 'Nguntoronadi',
            ),

            array (
                'code' => '33.12.07',
                'name' => 'Baturetno',
            ),

            array (
                'code' => '33.12.08',
                'name' => 'Eromoko',
            ),

            array (
                'code' => '33.12.09',
                'name' => 'Wuryantoro',
            ),

            array (
                'code' => '33.12.10',
                'name' => 'Manyaran',
            ),

            array (
                'code' => '33.12.11',
                'name' => 'Selogiri',
            ),

            array (
                'code' => '33.12.12',
                'name' => 'Wonogiri',
            ),

            array (
                'code' => '33.12.13',
                'name' => 'Ngadirojo',
            ),

            array (
                'code' => '33.12.14',
                'name' => 'Sidoharjo',
            ),

            array (
                'code' => '33.12.15',
                'name' => 'Jatiroto',
            ),

            array (
                'code' => '33.12.16',
                'name' => 'Kismantoro',
            ),

            array (
                'code' => '33.12.17',
                'name' => 'Purwantoro',
            ),

            array (
                'code' => '33.12.18',
                'name' => 'Bulukerto',
            ),

            array (
                'code' => '33.12.19',
                'name' => 'Slogohimo',
            ),

            array (
                'code' => '33.12.20',
                'name' => 'Jatisrono',
            ),

            array (
                'code' => '33.12.21',
                'name' => 'Jatipurno',
            ),

            array (
                'code' => '33.12.22',
                'name' => 'Girimarto',
            ),

            array (
                'code' => '33.12.23',
                'name' => 'Karangtengah',
            ),

            array (
                'code' => '33.12.24',
                'name' => 'Paranggupito',
            ),

            array (
                'code' => '33.12.25',
                'name' => 'Puhpelem',
            ),

            array (
                'code' => '33.13',
                'name' => 'KAB. KARANGANYAR',
            ),

            array (
                'code' => '33.13.01',
                'name' => 'Jatipuro',
            ),

            array (
                'code' => '33.13.02',
                'name' => 'Jatiyoso',
            ),

            array (
                'code' => '33.13.03',
                'name' => 'Jumapolo',
            ),

            array (
                'code' => '33.13.04',
                'name' => 'Jumantono',
            ),

            array (
                'code' => '33.13.05',
                'name' => 'Matesih',
            ),

            array (
                'code' => '33.13.06',
                'name' => 'Tawangmangu',
            ),

            array (
                'code' => '33.13.07',
                'name' => 'Ngargoyoso',
            ),

            array (
                'code' => '33.13.08',
                'name' => 'Karangpandan',
            ),

            array (
                'code' => '33.13.09',
                'name' => 'Karanganyar',
            ),

            array (
                'code' => '33.13.10',
                'name' => 'Tasikmadu',
            ),

            array (
                'code' => '33.13.11',
                'name' => 'Jaten',
            ),

            array (
                'code' => '33.13.12',
                'name' => 'Colomadu',
            ),

            array (
                'code' => '33.13.13',
                'name' => 'Gondangrejo',
            ),

            array (
                'code' => '33.13.14',
                'name' => 'Kebakkramat',
            ),

            array (
                'code' => '33.13.15',
                'name' => 'Mojogedang',
            ),

            array (
                'code' => '33.13.16',
                'name' => 'Kerjo',
            ),

            array (
                'code' => '33.13.17',
                'name' => 'Jenawi',
            ),

            array (
                'code' => '33.14',
                'name' => 'KAB. SRAGEN',
            ),

            array (
                'code' => '33.14.01',
                'name' => 'Kalijambe',
            ),

            array (
                'code' => '33.14.02',
                'name' => 'Plupuh',
            ),

            array (
                'code' => '33.14.03',
                'name' => 'Masaran',
            ),

            array (
                'code' => '33.14.04',
                'name' => 'Kedawung',
            ),

            array (
                'code' => '33.14.05',
                'name' => 'Sambirejo',
            ),

            array (
                'code' => '33.14.06',
                'name' => 'Gondang',
            ),

            array (
                'code' => '33.14.07',
                'name' => 'Sambungmacan',
            ),

            array (
                'code' => '33.14.08',
                'name' => 'Ngrampal',
            ),

            array (
                'code' => '33.14.09',
                'name' => 'Karangmalang',
            ),

            array (
                'code' => '33.14.10',
                'name' => 'Sragen',
            ),

            array (
                'code' => '33.14.11',
                'name' => 'Sidoharjo',
            ),

            array (
                'code' => '33.14.12',
                'name' => 'Tanon',
            ),

            array (
                'code' => '33.14.13',
                'name' => 'Gemolong',
            ),

            array (
                'code' => '33.14.14',
                'name' => 'Miri',
            ),

            array (
                'code' => '33.14.15',
                'name' => 'Sumberlawang',
            ),

            array (
                'code' => '33.14.16',
                'name' => 'Mondokan',
            ),

            array (
                'code' => '33.14.17',
                'name' => 'Sukodono',
            ),

            array (
                'code' => '33.14.18',
                'name' => 'Gesi',
            ),

            array (
                'code' => '33.14.19',
                'name' => 'Tangen',
            ),

            array (
                'code' => '33.14.20',
                'name' => 'Jenar',
            ),

            array (
                'code' => '33.15',
                'name' => 'KAB. GROBOGAN',
            ),

            array (
                'code' => '33.15.01',
                'name' => 'Kedungjati',
            ),

            array (
                'code' => '33.15.02',
                'name' => 'Karangrayung',
            ),

            array (
                'code' => '33.15.03',
                'name' => 'Penawangan',
            ),

            array (
                'code' => '33.15.04',
                'name' => 'Toroh',
            ),

            array (
                'code' => '33.15.05',
                'name' => 'Geyer',
            ),

            array (
                'code' => '33.15.06',
                'name' => 'Pulokulon',
            ),

            array (
                'code' => '33.15.07',
                'name' => 'Kradenan',
            ),

            array (
                'code' => '33.15.08',
                'name' => 'Gabus',
            ),

            array (
                'code' => '33.15.09',
                'name' => 'Ngaringan',
            ),

            array (
                'code' => '33.15.10',
                'name' => 'Wirosari',
            ),

            array (
                'code' => '33.15.11',
                'name' => 'Tawangharjo',
            ),

            array (
                'code' => '33.15.12',
                'name' => 'Grobogan',
            ),

            array (
                'code' => '33.15.13',
                'name' => 'Purwodadi',
            ),

            array (
                'code' => '33.15.14',
                'name' => 'Brati',
            ),

            array (
                'code' => '33.15.15',
                'name' => 'Klambu',
            ),

            array (
                'code' => '33.15.16',
                'name' => 'Godong',
            ),

            array (
                'code' => '33.15.17',
                'name' => 'Gubug',
            ),

            array (
                'code' => '33.15.18',
                'name' => 'Tegowanu',
            ),

            array (
                'code' => '33.15.19',
                'name' => 'Tanggungharjo',
            ),

            array (
                'code' => '33.16',
                'name' => 'KAB. BLORA',
            ),

            array (
                'code' => '33.16.01',
                'name' => 'Jati',
            ),

            array (
                'code' => '33.16.02',
                'name' => 'Randublatung',
            ),

            array (
                'code' => '33.16.03',
                'name' => 'Kradenan',
            ),

            array (
                'code' => '33.16.04',
                'name' => 'Kedungtuban',
            ),

            array (
                'code' => '33.16.05',
                'name' => 'Cepu',
            ),

            array (
                'code' => '33.16.06',
                'name' => 'Sambong',
            ),

            array (
                'code' => '33.16.07',
                'name' => 'Jiken',
            ),

            array (
                'code' => '33.16.08',
                'name' => 'Jepon',
            ),

            array (
                'code' => '33.16.09',
                'name' => 'Blora',
            ),

            array (
                'code' => '33.16.10',
                'name' => 'Tunjungan',
            ),

            array (
                'code' => '33.16.11',
                'name' => 'Banjarejo',
            ),

            array (
                'code' => '33.16.12',
                'name' => 'Ngawen',
            ),

            array (
                'code' => '33.16.13',
                'name' => 'Kunduran',
            ),

            array (
                'code' => '33.16.14',
                'name' => 'Todanan',
            ),

            array (
                'code' => '33.16.15',
                'name' => 'Bogorejo',
            ),

            array (
                'code' => '33.16.16',
                'name' => 'Japah',
            ),

            array (
                'code' => '33.17',
                'name' => 'KAB. REMBANG',
            ),

            array (
                'code' => '33.17.01',
                'name' => 'Sumber',
            ),

            array (
                'code' => '33.17.02',
                'name' => 'Bulu',
            ),

            array (
                'code' => '33.17.03',
                'name' => 'Gunem',
            ),

            array (
                'code' => '33.17.04',
                'name' => 'Sale',
            ),

            array (
                'code' => '33.17.05',
                'name' => 'Sarang',
            ),

            array (
                'code' => '33.17.06',
                'name' => 'Sedan',
            ),

            array (
                'code' => '33.17.07',
                'name' => 'Pamotan',
            ),

            array (
                'code' => '33.17.08',
                'name' => 'Sulang',
            ),

            array (
                'code' => '33.17.09',
                'name' => 'Kaliori',
            ),

            array (
                'code' => '33.17.10',
                'name' => 'Rembang',
            ),

            array (
                'code' => '33.17.11',
                'name' => 'Pancur',
            ),

            array (
                'code' => '33.17.12',
                'name' => 'Kragan',
            ),

            array (
                'code' => '33.17.13',
                'name' => 'Sluke',
            ),

            array (
                'code' => '33.17.14',
                'name' => 'Lasem',
            ),

            array (
                'code' => '33.18',
                'name' => 'KAB. PATI',
            ),

            array (
                'code' => '33.18.01',
                'name' => 'Sukolilo',
            ),

            array (
                'code' => '33.18.02',
                'name' => 'Kayen',
            ),

            array (
                'code' => '33.18.03',
                'name' => 'Tambakromo',
            ),

            array (
                'code' => '33.18.04',
                'name' => 'Winong',
            ),

            array (
                'code' => '33.18.05',
                'name' => 'Pucakwangi',
            ),

            array (
                'code' => '33.18.06',
                'name' => 'Jaken',
            ),

            array (
                'code' => '33.18.07',
                'name' => 'Batangan',
            ),

            array (
                'code' => '33.18.08',
                'name' => 'Juwana',
            ),

            array (
                'code' => '33.18.09',
                'name' => 'Jakenan',
            ),

            array (
                'code' => '33.18.10',
                'name' => 'Pati',
            ),

            array (
                'code' => '33.18.11',
                'name' => 'Gabus',
            ),

            array (
                'code' => '33.18.12',
                'name' => 'Margorejo',
            ),

            array (
                'code' => '33.18.13',
                'name' => 'Gembong',
            ),

            array (
                'code' => '33.18.14',
                'name' => 'Tlogowungu',
            ),

            array (
                'code' => '33.18.15',
                'name' => 'Wedarijaksa',
            ),

            array (
                'code' => '33.18.16',
                'name' => 'Margoyoso',
            ),

            array (
                'code' => '33.18.17',
                'name' => 'Gunungwungkal',
            ),

            array (
                'code' => '33.18.18',
                'name' => 'Cluwak',
            ),

            array (
                'code' => '33.18.19',
                'name' => 'Tayu',
            ),

            array (
                'code' => '33.18.20',
                'name' => 'Dukuhseti',
            ),

            array (
                'code' => '33.18.21',
                'name' => 'Trangkil',
            ),

            array (
                'code' => '33.19',
                'name' => 'KAB. KUDUS',
            ),

            array (
                'code' => '33.19.01',
                'name' => 'Kaliwungu',
            ),

            array (
                'code' => '33.19.02',
                'name' => 'Kota Kudus',
            ),

            array (
                'code' => '33.19.03',
                'name' => 'Jati',
            ),

            array (
                'code' => '33.19.04',
                'name' => 'Undaan',
            ),

            array (
                'code' => '33.19.05',
                'name' => 'Mejobo',
            ),

            array (
                'code' => '33.19.06',
                'name' => 'Jekulo',
            ),

            array (
                'code' => '33.19.07',
                'name' => 'Bae',
            ),

            array (
                'code' => '33.19.08',
                'name' => 'Gebog',
            ),

            array (
                'code' => '33.19.09',
                'name' => 'Dawe',
            ),

            array (
                'code' => '33.20',
                'name' => 'KAB. JEPARA',
            ),

            array (
                'code' => '33.20.01',
                'name' => 'Kedung',
            ),

            array (
                'code' => '33.20.02',
                'name' => 'Pecangaan',
            ),

            array (
                'code' => '33.20.03',
                'name' => 'Welahan',
            ),

            array (
                'code' => '33.20.04',
                'name' => 'Mayong',
            ),

            array (
                'code' => '33.20.05',
                'name' => 'Batealit',
            ),

            array (
                'code' => '33.20.06',
                'name' => 'Jepara',
            ),

            array (
                'code' => '33.20.07',
                'name' => 'Mlonggo',
            ),

            array (
                'code' => '33.20.08',
                'name' => 'Bangsri',
            ),

            array (
                'code' => '33.20.09',
                'name' => 'Keling',
            ),

            array (
                'code' => '33.20.10',
                'name' => 'Karimun Jawa',
            ),

            array (
                'code' => '33.20.11',
                'name' => 'Tahunan',
            ),

            array (
                'code' => '33.20.12',
                'name' => 'Nalumsari',
            ),

            array (
                'code' => '33.20.13',
                'name' => 'Kalinyamatan',
            ),

            array (
                'code' => '33.20.14',
                'name' => 'Kembang',
            ),

            array (
                'code' => '33.20.15',
                'name' => 'Pakis Aji',
            ),

            array (
                'code' => '33.20.16',
                'name' => 'Donorojo',
            ),

            array (
                'code' => '33.21',
                'name' => 'KAB. DEMAK',
            ),

            array (
                'code' => '33.21.01',
                'name' => 'Mranggen',
            ),

            array (
                'code' => '33.21.02',
                'name' => 'Karangawen',
            ),

            array (
                'code' => '33.21.03',
                'name' => 'Guntur',
            ),

            array (
                'code' => '33.21.04',
                'name' => 'Sayung',
            ),

            array (
                'code' => '33.21.05',
                'name' => 'Karangtengah',
            ),

            array (
                'code' => '33.21.06',
                'name' => 'Wonosalam',
            ),

            array (
                'code' => '33.21.07',
                'name' => 'Dempet',
            ),

            array (
                'code' => '33.21.08',
                'name' => 'Gajah',
            ),

            array (
                'code' => '33.21.09',
                'name' => 'Karanganyar',
            ),

            array (
                'code' => '33.21.10',
                'name' => 'Mijen',
            ),

            array (
                'code' => '33.21.11',
                'name' => 'Demak',
            ),

            array (
                'code' => '33.21.12',
                'name' => 'Bonang',
            ),

            array (
                'code' => '33.21.13',
                'name' => 'Wedung',
            ),

            array (
                'code' => '33.21.14',
                'name' => 'Kebonagung',
            ),

            array (
                'code' => '33.22',
                'name' => 'KAB. SEMARANG',
            ),

            array (
                'code' => '33.22.01',
                'name' => 'Getasan',
            ),

            array (
                'code' => '33.22.02',
                'name' => 'Tengaran',
            ),

            array (
                'code' => '33.22.03',
                'name' => 'Susukan',
            ),

            array (
                'code' => '33.22.04',
                'name' => 'Suruh',
            ),

            array (
                'code' => '33.22.05',
                'name' => 'Pabelan',
            ),

            array (
                'code' => '33.22.06',
                'name' => 'Tuntang',
            ),

            array (
                'code' => '33.22.07',
                'name' => 'Banyubiru',
            ),

            array (
                'code' => '33.22.08',
                'name' => 'Jambu',
            ),

            array (
                'code' => '33.22.09',
                'name' => 'Sumowono',
            ),

            array (
                'code' => '33.22.10',
                'name' => 'Ambarawa',
            ),

            array (
                'code' => '33.22.11',
                'name' => 'Bawen',
            ),

            array (
                'code' => '33.22.12',
                'name' => 'Bringin',
            ),

            array (
                'code' => '33.22.13',
                'name' => 'Bergas',
            ),

            array (
                'code' => '33.22.15',
                'name' => 'Pringapus',
            ),

            array (
                'code' => '33.22.16',
                'name' => 'Bancak',
            ),

            array (
                'code' => '33.22.17',
                'name' => 'Kaliwungu',
            ),

            array (
                'code' => '33.22.18',
                'name' => 'Ungaran Barat',
            ),

            array (
                'code' => '33.22.19',
                'name' => 'Ungaran Timur',
            ),

            array (
                'code' => '33.22.20',
                'name' => 'Bandungan',
            ),

            array (
                'code' => '33.23',
                'name' => 'KAB. TEMANGGUNG',
            ),

            array (
                'code' => '33.23.01',
                'name' => 'Bulu',
            ),

            array (
                'code' => '33.23.02',
                'name' => 'Tembarak',
            ),

            array (
                'code' => '33.23.03',
                'name' => 'Temanggung',
            ),

            array (
                'code' => '33.23.04',
                'name' => 'Pringsurat',
            ),

            array (
                'code' => '33.23.05',
                'name' => 'Kaloran',
            ),

            array (
                'code' => '33.23.06',
                'name' => 'Kandangan',
            ),

            array (
                'code' => '33.23.07',
                'name' => 'Kedu',
            ),

            array (
                'code' => '33.23.08',
                'name' => 'Parakan',
            ),

            array (
                'code' => '33.23.09',
                'name' => 'Ngadirejo',
            ),

            array (
                'code' => '33.23.10',
                'name' => 'Jumo',
            ),

            array (
                'code' => '33.23.11',
                'name' => 'Tretep',
            ),

            array (
                'code' => '33.23.12',
                'name' => 'Candiroto',
            ),

            array (
                'code' => '33.23.13',
                'name' => 'Kranggan',
            ),

            array (
                'code' => '33.23.14',
                'name' => 'Tlogomulyo',
            ),

            array (
                'code' => '33.23.15',
                'name' => 'Selopampang',
            ),

            array (
                'code' => '33.23.16',
                'name' => 'Bansari',
            ),

            array (
                'code' => '33.23.17',
                'name' => 'Kledung',
            ),

            array (
                'code' => '33.23.18',
                'name' => 'Bejen',
            ),

            array (
                'code' => '33.23.19',
                'name' => 'Wonoboyo',
            ),

            array (
                'code' => '33.23.20',
                'name' => 'Gemawang',
            ),

            array (
                'code' => '33.24',
                'name' => 'KAB. KENDAL',
            ),

            array (
                'code' => '33.24.01',
                'name' => 'Plantungan',
            ),

            array (
                'code' => '33.24.02',
                'name' => 'Pageruyung',
            ),

            array (
                'code' => '33.24.03',
                'name' => 'Sukorejo',
            ),

            array (
                'code' => '33.24.04',
                'name' => 'Patean',
            ),

            array (
                'code' => '33.24.05',
                'name' => 'Singorojo',
            ),

            array (
                'code' => '33.24.06',
                'name' => 'Limbangan',
            ),

            array (
                'code' => '33.24.07',
                'name' => 'Boja',
            ),

            array (
                'code' => '33.24.08',
                'name' => 'Kaliwungu',
            ),

            array (
                'code' => '33.24.09',
                'name' => 'Brangsong',
            ),

            array (
                'code' => '33.24.10',
                'name' => 'Pegandon',
            ),

            array (
                'code' => '33.24.11',
                'name' => 'Gemuh',
            ),

            array (
                'code' => '33.24.12',
                'name' => 'Weleri',
            ),

            array (
                'code' => '33.24.13',
                'name' => 'Cepiring',
            ),

            array (
                'code' => '33.24.14',
                'name' => 'Patebon',
            ),

            array (
                'code' => '33.24.15',
                'name' => 'Kendal',
            ),

            array (
                'code' => '33.24.16',
                'name' => 'Rowosari',
            ),

            array (
                'code' => '33.24.17',
                'name' => 'Kangkung',
            ),

            array (
                'code' => '33.24.18',
                'name' => 'Ringinarum',
            ),

            array (
                'code' => '33.24.19',
                'name' => 'Ngampel',
            ),

            array (
                'code' => '33.24.20',
                'name' => 'Kaliwungu Selatan',
            ),

            array (
                'code' => '33.25',
                'name' => 'KAB. BATANG',
            ),

            array (
                'code' => '33.25.01',
                'name' => 'Wonotunggal',
            ),

            array (
                'code' => '33.25.02',
                'name' => 'Bandar',
            ),

            array (
                'code' => '33.25.03',
                'name' => 'Blado',
            ),

            array (
                'code' => '33.25.04',
                'name' => 'Reban',
            ),

            array (
                'code' => '33.25.05',
                'name' => 'Bawang',
            ),

            array (
                'code' => '33.25.06',
                'name' => 'Tersono',
            ),

            array (
                'code' => '33.25.07',
                'name' => 'Gringsing',
            ),

            array (
                'code' => '33.25.08',
                'name' => 'Limpung',
            ),

            array (
                'code' => '33.25.09',
                'name' => 'Subah',
            ),

            array (
                'code' => '33.25.10',
                'name' => 'Tulis',
            ),

            array (
                'code' => '33.25.11',
                'name' => 'Batang',
            ),

            array (
                'code' => '33.25.12',
                'name' => 'Warungasem',
            ),

            array (
                'code' => '33.25.13',
                'name' => 'Kandeman',
            ),

            array (
                'code' => '33.25.14',
                'name' => 'Pecalungan',
            ),

            array (
                'code' => '33.25.15',
                'name' => 'Banyuputih',
            ),

            array (
                'code' => '33.26',
                'name' => 'KAB. PEKALONGAN',
            ),

            array (
                'code' => '33.26.01',
                'name' => 'Kandangserang',
            ),

            array (
                'code' => '33.26.02',
                'name' => 'Paninggaran',
            ),

            array (
                'code' => '33.26.03',
                'name' => 'Lebakbarang',
            ),

            array (
                'code' => '33.26.04',
                'name' => 'Petungkriyono',
            ),

            array (
                'code' => '33.26.05',
                'name' => 'Talun',
            ),

            array (
                'code' => '33.26.06',
                'name' => 'Doro',
            ),

            array (
                'code' => '33.26.07',
                'name' => 'Karanganyar',
            ),

            array (
                'code' => '33.26.08',
                'name' => 'Kajen',
            ),

            array (
                'code' => '33.26.09',
                'name' => 'Kesesi',
            ),

            array (
                'code' => '33.26.10',
                'name' => 'Sragi',
            ),

            array (
                'code' => '33.26.11',
                'name' => 'Bojong',
            ),

            array (
                'code' => '33.26.12',
                'name' => 'Wonopringgo',
            ),

            array (
                'code' => '33.26.13',
                'name' => 'Kedungwuni',
            ),

            array (
                'code' => '33.26.14',
                'name' => 'Buaran',
            ),

            array (
                'code' => '33.26.15',
                'name' => 'Tirto',
            ),

            array (
                'code' => '33.26.16',
                'name' => 'Wiradesa',
            ),

            array (
                'code' => '33.26.17',
                'name' => 'Siwalan',
            ),

            array (
                'code' => '33.26.18',
                'name' => 'Karangdadap',
            ),

            array (
                'code' => '33.26.19',
                'name' => 'Wonokerto',
            ),

            array (
                'code' => '33.27',
                'name' => 'KAB. PEMALANG',
            ),

            array (
                'code' => '33.27.01',
                'name' => 'Moga',
            ),

            array (
                'code' => '33.27.02',
                'name' => 'Pulosari',
            ),

            array (
                'code' => '33.27.03',
                'name' => 'Belik',
            ),

            array (
                'code' => '33.27.04',
                'name' => 'Watukumpul',
            ),

            array (
                'code' => '33.27.05',
                'name' => 'Bodeh',
            ),

            array (
                'code' => '33.27.06',
                'name' => 'Bantarbolang',
            ),

            array (
                'code' => '33.27.07',
                'name' => 'Randudongkal',
            ),

            array (
                'code' => '33.27.08',
                'name' => 'Pemalang',
            ),

            array (
                'code' => '33.27.09',
                'name' => 'Taman',
            ),

            array (
                'code' => '33.27.10',
                'name' => 'Petarukan',
            ),

            array (
                'code' => '33.27.11',
                'name' => 'Ampelgading',
            ),

            array (
                'code' => '33.27.12',
                'name' => 'Comal',
            ),

            array (
                'code' => '33.27.13',
                'name' => 'Ulujami',
            ),

            array (
                'code' => '33.27.14',
                'name' => 'Warungpring',
            ),

            array (
                'code' => '33.28',
                'name' => 'KAB. TEGAL',
            ),

            array (
                'code' => '33.28.01',
                'name' => 'Margasari',
            ),

            array (
                'code' => '33.28.02',
                'name' => 'Bumijawa',
            ),

            array (
                'code' => '33.28.03',
                'name' => 'Bojong',
            ),

            array (
                'code' => '33.28.04',
                'name' => 'Balapulang',
            ),

            array (
                'code' => '33.28.05',
                'name' => 'Pagerbarang',
            ),

            array (
                'code' => '33.28.06',
                'name' => 'Lebaksiu',
            ),

            array (
                'code' => '33.28.07',
                'name' => 'Jatinegara',
            ),

            array (
                'code' => '33.28.08',
                'name' => 'Kedungbanteng',
            ),

            array (
                'code' => '33.28.09',
                'name' => 'Pangkah',
            ),

            array (
                'code' => '33.28.10',
                'name' => 'Slawi',
            ),

            array (
                'code' => '33.28.11',
                'name' => 'Adiwerna',
            ),

            array (
                'code' => '33.28.12',
                'name' => 'Talang',
            ),

            array (
                'code' => '33.28.13',
                'name' => 'Dukuhturi',
            ),

            array (
                'code' => '33.28.14',
                'name' => 'Tarub',
            ),

            array (
                'code' => '33.28.15',
                'name' => 'Kramat',
            ),

            array (
                'code' => '33.28.16',
                'name' => 'Suradadi',
            ),

            array (
                'code' => '33.28.17',
                'name' => 'Warureja',
            ),

            array (
                'code' => '33.28.18',
                'name' => 'Dukuhwaru',
            ),

            array (
                'code' => '33.29',
                'name' => 'KAB. BREBES',
            ),

            array (
                'code' => '33.29.01',
                'name' => 'Salem',
            ),

            array (
                'code' => '33.29.02',
                'name' => 'Bantarkawung',
            ),

            array (
                'code' => '33.29.03',
                'name' => 'Bumiayu',
            ),

            array (
                'code' => '33.29.04',
                'name' => 'Paguyangan',
            ),

            array (
                'code' => '33.29.05',
                'name' => 'Sirampog',
            ),

            array (
                'code' => '33.29.06',
                'name' => 'Tonjong',
            ),

            array (
                'code' => '33.29.07',
                'name' => 'Jatibarang',
            ),

            array (
                'code' => '33.29.08',
                'name' => 'Wanasari',
            ),

            array (
                'code' => '33.29.09',
                'name' => 'Brebes',
            ),

            array (
                'code' => '33.29.10',
                'name' => 'Songgom',
            ),

            array (
                'code' => '33.29.11',
                'name' => 'Kersana',
            ),

            array (
                'code' => '33.29.12',
                'name' => 'Losari',
            ),

            array (
                'code' => '33.29.13',
                'name' => 'Tanjung',
            ),

            array (
                'code' => '33.29.14',
                'name' => 'Bulakamba',
            ),

            array (
                'code' => '33.29.15',
                'name' => 'Larangan',
            ),

            array (
                'code' => '33.29.16',
                'name' => 'Ketanggungan',
            ),

            array (
                'code' => '33.29.17',
                'name' => 'Banjarharjo',
            ),

            array (
                'code' => '33.71',
                'name' => 'KOTA MAGELANG',
            ),

            array (
                'code' => '33.71.01',
                'name' => 'Magelang Selatan',
            ),

            array (
                'code' => '33.71.02',
                'name' => 'Magelang Utara',
            ),

            array (
                'code' => '33.71.03',
                'name' => 'Magelang Tengah',
            ),

            array (
                'code' => '33.72',
                'name' => 'KOTA SURAKARTA',
            ),

            array (
                'code' => '33.72.01',
                'name' => 'Laweyan',
            ),

            array (
                'code' => '33.72.02',
                'name' => 'Serengan',
            ),

            array (
                'code' => '33.72.03',
                'name' => 'Pasar Kliwon',
            ),

            array (
                'code' => '33.72.04',
                'name' => 'Jebres',
            ),

            array (
                'code' => '33.72.05',
                'name' => 'Banjarsari',
            ),

            array (
                'code' => '33.73',
                'name' => 'KOTA SALATIGA',
            ),

            array (
                'code' => '33.73.01',
                'name' => 'Sidorejo',
            ),

            array (
                'code' => '33.73.02',
                'name' => 'Tingkir',
            ),

            array (
                'code' => '33.73.03',
                'name' => 'Argomulyo',
            ),

            array (
                'code' => '33.73.04',
                'name' => 'Sidomukti',
            ),

            array (
                'code' => '33.74',
                'name' => 'KOTA SEMARANG',
            ),

            array (
                'code' => '33.74.01',
                'name' => 'Semarang Tengah',
            ),

            array (
                'code' => '33.74.02',
                'name' => 'Semarang Utara',
            ),

            array (
                'code' => '33.74.03',
                'name' => 'Semarang Timur',
            ),

            array (
                'code' => '33.74.04',
                'name' => 'Gayamsari',
            ),

            array (
                'code' => '33.74.05',
                'name' => 'Genuk',
            ),

            array (
                'code' => '33.74.06',
                'name' => 'Pedurungan',
            ),

            array (
                'code' => '33.74.07',
                'name' => 'Semarang Selatan',
            ),

            array (
                'code' => '33.74.08',
                'name' => 'Candisari',
            ),

            array (
                'code' => '33.74.09',
                'name' => 'Gajahmungkur',
            ),

            array (
                'code' => '33.74.10',
                'name' => 'Tembalang',
            ),

            array (
                'code' => '33.74.11',
                'name' => 'Banyumanik',
            ),

            array (
                'code' => '33.74.12',
                'name' => 'Gunungpati',
            ),

            array (
                'code' => '33.74.13',
                'name' => 'Semarang Barat',
            ),

            array (
                'code' => '33.74.14',
                'name' => 'Mijen',
            ),

            array (
                'code' => '33.74.15',
                'name' => 'Ngaliyan',
            ),

            array (
                'code' => '33.74.16',
                'name' => 'Tugu',
            ),

            array (
                'code' => '33.75',
                'name' => 'KOTA PEKALONGAN',
            ),

            array (
                'code' => '33.75.01',
                'name' => 'Pekalongan Barat',
            ),

            array (
                'code' => '33.75.02',
                'name' => 'Pekalongan Timur',
            ),

            array (
                'code' => '33.75.03',
                'name' => 'Pekalongan Utara',
            ),

            array (
                'code' => '33.75.04',
                'name' => 'Pekalongan Selatan',
            ),

            array (
                'code' => '33.76',
                'name' => 'KOTA TEGAL',
            ),

            array (
                'code' => '33.76.01',
                'name' => 'Tegal Barat',
            ),

            array (
                'code' => '33.76.02',
                'name' => 'Tegal Timur',
            ),

            array (
                'code' => '33.76.03',
                'name' => 'Tegal Selatan',
            ),

            array (
                'code' => '33.76.04',
                'name' => 'Margadana',
            ),

            array (
                'code' => '34',
                'name' => 'DAERAH ISTIMEWA',
            ),

            array (
                'code' => '34.01',
                'name' => 'KAB. KULON PROGO',
            ),

            array (
                'code' => '34.01.01',
                'name' => 'Temon',
            ),

            array (
                'code' => '34.01.02',
                'name' => 'Wates',
            ),

            array (
                'code' => '34.01.03',
                'name' => 'Panjatan',
            ),

            array (
                'code' => '34.01.04',
                'name' => 'Galur',
            ),

            array (
                'code' => '34.01.05',
                'name' => 'Lendah',
            ),

            array (
                'code' => '34.01.06',
                'name' => 'Sentolo',
            ),

            array (
                'code' => '34.01.07',
                'name' => 'Pengasih',
            ),

            array (
                'code' => '34.01.08',
                'name' => 'Kokap',
            ),

            array (
                'code' => '34.01.09',
                'name' => 'Girimulyo',
            ),

            array (
                'code' => '34.01.10',
                'name' => 'Nanggulan',
            ),

            array (
                'code' => '34.01.11',
                'name' => 'Samigaluh',
            ),

            array (
                'code' => '34.01.12',
                'name' => 'Kalibawang',
            ),

            array (
                'code' => '34.02',
                'name' => 'KAB. BANTUL',
            ),

            array (
                'code' => '34.02.01',
                'name' => 'Srandakan',
            ),

            array (
                'code' => '34.02.02',
                'name' => 'Sanden',
            ),

            array (
                'code' => '34.02.03',
                'name' => 'Kretek',
            ),

            array (
                'code' => '34.02.04',
                'name' => 'Pundong',
            ),

            array (
                'code' => '34.02.05',
                'name' => 'Bambanglipuro',
            ),

            array (
                'code' => '34.02.06',
                'name' => 'Pandak',
            ),

            array (
                'code' => '34.02.07',
                'name' => 'Pajangan',
            ),

            array (
                'code' => '34.02.08',
                'name' => 'Bantul',
            ),

            array (
                'code' => '34.02.09',
                'name' => 'Jetis',
            ),

            array (
                'code' => '34.02.10',
                'name' => 'Imogiri',
            ),

            array (
                'code' => '34.02.11',
                'name' => 'Dlingo',
            ),

            array (
                'code' => '34.02.12',
                'name' => 'Banguntapan',
            ),

            array (
                'code' => '34.02.13',
                'name' => 'Pleret',
            ),

            array (
                'code' => '34.02.14',
                'name' => 'Piyungan',
            ),

            array (
                'code' => '34.02.15',
                'name' => 'Sewon',
            ),

            array (
                'code' => '34.02.16',
                'name' => 'Kasihan',
            ),

            array (
                'code' => '34.02.17',
                'name' => 'Sedayu',
            ),

            array (
                'code' => '34.03',
                'name' => 'KAB. GUNUNGKIDUL',
            ),

            array (
                'code' => '34.03.01',
                'name' => 'Wonosari',
            ),

            array (
                'code' => '34.03.02',
                'name' => 'Nglipar',
            ),

            array (
                'code' => '34.03.03',
                'name' => 'Playen',
            ),

            array (
                'code' => '34.03.04',
                'name' => 'Patuk',
            ),

            array (
                'code' => '34.03.05',
                'name' => 'Paliyan',
            ),

            array (
                'code' => '34.03.06',
                'name' => 'Panggang',
            ),

            array (
                'code' => '34.03.07',
                'name' => 'Tepus',
            ),

            array (
                'code' => '34.03.08',
                'name' => 'Semanu',
            ),

            array (
                'code' => '34.03.09',
                'name' => 'Karangmojo',
            ),

            array (
                'code' => '34.03.10',
                'name' => 'Ponjong',
            ),

            array (
                'code' => '34.03.11',
                'name' => 'Rongkop',
            ),

            array (
                'code' => '34.03.12',
                'name' => 'Semin',
            ),

            array (
                'code' => '34.03.13',
                'name' => 'Ngawen',
            ),

            array (
                'code' => '34.03.14',
                'name' => 'Gedangsari',
            ),

            array (
                'code' => '34.03.15',
                'name' => 'Saptosari',
            ),

            array (
                'code' => '34.03.16',
                'name' => 'Girisubo',
            ),

            array (
                'code' => '34.03.17',
                'name' => 'Tanjungsari',
            ),

            array (
                'code' => '34.03.18',
                'name' => 'Purwosari',
            ),

            array (
                'code' => '34.04',
                'name' => 'KAB. SLEMAN',
            ),

            array (
                'code' => '34.04.01',
                'name' => 'Gamping',
            ),

            array (
                'code' => '34.04.02',
                'name' => 'Godean',
            ),

            array (
                'code' => '34.04.03',
                'name' => 'Moyudan',
            ),

            array (
                'code' => '34.04.04',
                'name' => 'Minggir',
            ),

            array (
                'code' => '34.04.05',
                'name' => 'Seyegan',
            ),

            array (
                'code' => '34.04.06',
                'name' => 'Mlati',
            ),

            array (
                'code' => '34.04.07',
                'name' => 'Depok',
            ),

            array (
                'code' => '34.04.08',
                'name' => 'Berbah',
            ),

            array (
                'code' => '34.04.09',
                'name' => 'Prambanan',
            ),

            array (
                'code' => '34.04.10',
                'name' => 'Kalasan',
            ),

            array (
                'code' => '34.04.11',
                'name' => 'Ngemplak',
            ),

            array (
                'code' => '34.04.12',
                'name' => 'Ngaglik',
            ),

            array (
                'code' => '34.04.13',
                'name' => 'Sleman',
            ),

            array (
                'code' => '34.04.14',
                'name' => 'Tempel',
            ),

            array (
                'code' => '34.04.15',
                'name' => 'Turi',
            ),

            array (
                'code' => '34.04.16',
                'name' => 'Pakem',
            ),

            array (
                'code' => '34.04.17',
                'name' => 'Cangkringan',
            ),

            array (
                'code' => '34.71',
                'name' => 'KOTA YOGYAKARTA',
            ),

            array (
                'code' => '34.71.01',
                'name' => 'Tegalrejo',
            ),

            array (
                'code' => '34.71.02',
                'name' => 'Jetis',
            ),

            array (
                'code' => '34.71.03',
                'name' => 'Gondokusuman',
            ),

            array (
                'code' => '34.71.04',
                'name' => 'Danurejan',
            ),

            array (
                'code' => '34.71.05',
                'name' => 'Gedongtengen',
            ),

            array (
                'code' => '34.71.06',
                'name' => 'Ngampilan',
            ),

            array (
                'code' => '34.71.07',
                'name' => 'Wirobrajan',
            ),

            array (
                'code' => '34.71.08',
                'name' => 'Mantrijeron',
            ),

            array (
                'code' => '34.71.09',
                'name' => 'Kraton',
            ),

            array (
                'code' => '34.71.10',
                'name' => 'Gondomanan',
            ),

            array (
                'code' => '34.71.11',
                'name' => 'Pakualaman',
            ),

            array (
                'code' => '34.71.12',
                'name' => 'Mergangsan',
            ),

            array (
                'code' => '34.71.13',
                'name' => 'Umbulharjo',
            ),

            array (
                'code' => '34.71.14',
                'name' => 'Kotagede',
            ),

            array (
                'code' => '35',
                'name' => 'JAWA TIMUR',
            ),

            array (
                'code' => '35.01',
                'name' => 'KAB. PACITAN',
            ),

            array (
                'code' => '35.01.01',
                'name' => 'Donorojo',
            ),

            array (
                'code' => '35.01.02',
                'name' => 'Pringkuku',
            ),

            array (
                'code' => '35.01.03',
                'name' => 'Punung',
            ),

            array (
                'code' => '35.01.04',
                'name' => 'Pacitan',
            ),

            array (
                'code' => '35.01.05',
                'name' => 'Kebonagung',
            ),

            array (
                'code' => '35.01.06',
                'name' => 'Arjosari',
            ),

            array (
                'code' => '35.01.07',
                'name' => 'Nawangan',
            ),

            array (
                'code' => '35.01.08',
                'name' => 'Bandar',
            ),

            array (
                'code' => '35.01.09',
                'name' => 'Tegalombo',
            ),

            array (
                'code' => '35.01.10',
                'name' => 'Tulakan',
            ),

            array (
                'code' => '35.01.11',
                'name' => 'Ngadirojo',
            ),

            array (
                'code' => '35.01.12',
                'name' => 'Sudimoro',
            ),

            array (
                'code' => '35.02',
                'name' => 'KAB. PONOROGO',
            ),

            array (
                'code' => '35.02.01',
                'name' => 'Slahung',
            ),

            array (
                'code' => '35.02.02',
                'name' => 'Ngrayun',
            ),

            array (
                'code' => '35.02.03',
                'name' => 'Bungkal',
            ),

            array (
                'code' => '35.02.04',
                'name' => 'Sambit',
            ),

            array (
                'code' => '35.02.05',
                'name' => 'Sawoo',
            ),

            array (
                'code' => '35.02.06',
                'name' => 'Sooko',
            ),

            array (
                'code' => '35.02.07',
                'name' => 'Pulung',
            ),

            array (
                'code' => '35.02.08',
                'name' => 'Mlarak',
            ),

            array (
                'code' => '35.02.09',
                'name' => 'Jetis',
            ),

            array (
                'code' => '35.02.10',
                'name' => 'Siman',
            ),

            array (
                'code' => '35.02.11',
                'name' => 'Balong',
            ),

            array (
                'code' => '35.02.12',
                'name' => 'Kauman',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '35.02.13',
                'name' => 'Badegan',
            ),

            array (
                'code' => '35.02.14',
                'name' => 'Sampung',
            ),

            array (
                'code' => '35.02.15',
                'name' => 'Sukorejo',
            ),

            array (
                'code' => '35.02.16',
                'name' => 'Babadan',
            ),

            array (
                'code' => '35.02.17',
                'name' => 'Ponorogo',
            ),

            array (
                'code' => '35.02.18',
                'name' => 'Jenangan',
            ),

            array (
                'code' => '35.02.19',
                'name' => 'Ngebel',
            ),

            array (
                'code' => '35.02.20',
                'name' => 'Jambon',
            ),

            array (
                'code' => '35.02.21',
                'name' => 'Pudak',
            ),

            array (
                'code' => '35.03',
                'name' => 'KAB. TRENGGALEK',
            ),

            array (
                'code' => '35.03.01',
                'name' => 'Panggul',
            ),

            array (
                'code' => '35.03.02',
                'name' => 'Munjungan',
            ),

            array (
                'code' => '35.03.03',
                'name' => 'Pule',
            ),

            array (
                'code' => '35.03.04',
                'name' => 'Dongko',
            ),

            array (
                'code' => '35.03.05',
                'name' => 'Tugu',
            ),

            array (
                'code' => '35.03.06',
                'name' => 'Karangan',
            ),

            array (
                'code' => '35.03.07',
                'name' => 'Kampak',
            ),

            array (
                'code' => '35.03.08',
                'name' => 'Watulimo',
            ),

            array (
                'code' => '35.03.09',
                'name' => 'Bendungan',
            ),

            array (
                'code' => '35.03.10',
                'name' => 'Gandusari',
            ),

            array (
                'code' => '35.03.11',
                'name' => 'Trenggalek',
            ),

            array (
                'code' => '35.03.12',
                'name' => 'Pogalan',
            ),

            array (
                'code' => '35.03.13',
                'name' => 'Durenan',
            ),

            array (
                'code' => '35.03.14',
                'name' => 'Suruh',
            ),

            array (
                'code' => '35.04',
                'name' => 'KAB. TULUNGAGUNG',
            ),

            array (
                'code' => '35.04.01',
                'name' => 'Tulungagung',
            ),

            array (
                'code' => '35.04.02',
                'name' => 'Boyolangu',
            ),

            array (
                'code' => '35.04.03',
                'name' => 'Kedungwaru',
            ),

            array (
                'code' => '35.04.04',
                'name' => 'Ngantru',
            ),

            array (
                'code' => '35.04.05',
                'name' => 'Kauman',
            ),

            array (
                'code' => '35.04.06',
                'name' => 'Pagerwojo',
            ),

            array (
                'code' => '35.04.07',
                'name' => 'Sendang',
            ),

            array (
                'code' => '35.04.08',
                'name' => 'Karangrejo',
            ),

            array (
                'code' => '35.04.09',
                'name' => 'Gondang',
            ),

            array (
                'code' => '35.04.10',
                'name' => 'Sumbergempol',
            ),

            array (
                'code' => '35.04.11',
                'name' => 'Ngunut',
            ),

            array (
                'code' => '35.04.12',
                'name' => 'Pucanglaban',
            ),

            array (
                'code' => '35.04.13',
                'name' => 'Rejotangan',
            ),

            array (
                'code' => '35.04.14',
                'name' => 'Kalidawir',
            ),

            array (
                'code' => '35.04.15',
                'name' => 'Besuki',
            ),

            array (
                'code' => '35.04.16',
                'name' => 'Campurdarat',
            ),

            array (
                'code' => '35.04.17',
                'name' => 'Bandung',
            ),

            array (
                'code' => '35.04.18',
                'name' => 'Pakel',
            ),

            array (
                'code' => '35.04.19',
                'name' => 'Tanggunggunung',
            ),

            array (
                'code' => '35.05',
                'name' => 'KAB. BLITAR',
            ),

            array (
                'code' => '35.05.01',
                'name' => 'Wonodadi',
            ),

            array (
                'code' => '35.05.02',
                'name' => 'Udanawu',
            ),

            array (
                'code' => '35.05.03',
                'name' => 'Srengat',
            ),

            array (
                'code' => '35.05.04',
                'name' => 'Kademangan',
            ),

            array (
                'code' => '35.05.05',
                'name' => 'Bakung',
            ),

            array (
                'code' => '35.05.06',
                'name' => 'Ponggok',
            ),

            array (
                'code' => '35.05.07',
                'name' => 'Sanankulon',
            ),

            array (
                'code' => '35.05.08',
                'name' => 'Wonotirto',
            ),

            array (
                'code' => '35.05.09',
                'name' => 'Nglegok',
            ),

            array (
                'code' => '35.05.10',
                'name' => 'Kanigoro',
            ),

            array (
                'code' => '35.05.11',
                'name' => 'Garum',
            ),

            array (
                'code' => '35.05.12',
                'name' => 'Sutojayan',
            ),

            array (
                'code' => '35.05.13',
                'name' => 'Panggungrejo',
            ),

            array (
                'code' => '35.05.14',
                'name' => 'Talun',
            ),

            array (
                'code' => '35.05.15',
                'name' => 'Gandusari',
            ),

            array (
                'code' => '35.05.16',
                'name' => 'Binangun',
            ),

            array (
                'code' => '35.05.17',
                'name' => 'Wlingi',
            ),

            array (
                'code' => '35.05.18',
                'name' => 'Doko',
            ),

            array (
                'code' => '35.05.19',
                'name' => 'Kesamben',
            ),

            array (
                'code' => '35.05.20',
                'name' => 'Wates',
            ),

            array (
                'code' => '35.05.21',
                'name' => 'Selorejo',
            ),

            array (
                'code' => '35.05.22',
                'name' => 'Selopuro',
            ),

            array (
                'code' => '35.06',
                'name' => 'KAB. KEDIRI',
            ),

            array (
                'code' => '35.06.01',
                'name' => 'Semen',
            ),

            array (
                'code' => '35.06.02',
                'name' => 'Mojo',
            ),

            array (
                'code' => '35.06.03',
                'name' => 'Kras',
            ),

            array (
                'code' => '35.06.04',
                'name' => 'Ngadiluwih',
            ),

            array (
                'code' => '35.06.05',
                'name' => 'Kandat',
            ),

            array (
                'code' => '35.06.06',
                'name' => 'Wates',
            ),

            array (
                'code' => '35.06.07',
                'name' => 'Ngancar',
            ),

            array (
                'code' => '35.06.08',
                'name' => 'Puncu',
            ),

            array (
                'code' => '35.06.09',
                'name' => 'Plosoklaten',
            ),

            array (
                'code' => '35.06.10',
                'name' => 'Gurah',
            ),

            array (
                'code' => '35.06.11',
                'name' => 'Pagu',
            ),

            array (
                'code' => '35.06.12',
                'name' => 'Gampengrejo',
            ),

            array (
                'code' => '35.06.13',
                'name' => 'Grogol',
            ),

            array (
                'code' => '35.06.14',
                'name' => 'Papar',
            ),

            array (
                'code' => '35.06.15',
                'name' => 'Purwoasri',
            ),

            array (
                'code' => '35.06.16',
                'name' => 'Plemahan',
            ),

            array (
                'code' => '35.06.17',
                'name' => 'Pare',
            ),

            array (
                'code' => '35.06.18',
                'name' => 'Kepung',
            ),

            array (
                'code' => '35.06.19',
                'name' => 'Kandangan',
            ),

            array (
                'code' => '35.06.20',
                'name' => 'Tarokan',
            ),

            array (
                'code' => '35.06.21',
                'name' => 'Kunjang',
            ),

            array (
                'code' => '35.06.22',
                'name' => 'Banyakan',
            ),

            array (
                'code' => '35.06.23',
                'name' => 'Ringinrejo',
            ),

            array (
                'code' => '35.06.24',
                'name' => 'Kayen Kidul',
            ),

            array (
                'code' => '35.06.25',
                'name' => 'Ngasem',
            ),

            array (
                'code' => '35.06.26',
                'name' => 'Badas',
            ),

            array (
                'code' => '35.07',
                'name' => 'KAB. MALANG',
            ),

            array (
                'code' => '35.07.01',
                'name' => 'Donomulyo',
            ),

            array (
                'code' => '35.07.02',
                'name' => 'Pagak',
            ),

            array (
                'code' => '35.07.03',
                'name' => 'Bantur',
            ),

            array (
                'code' => '35.07.04',
                'name' => 'Sumbermanjing Wetan',
            ),

            array (
                'code' => '35.07.05',
                'name' => 'Dampit',
            ),

            array (
                'code' => '35.07.06',
                'name' => 'Ampelgading',
            ),

            array (
                'code' => '35.07.07',
                'name' => 'Poncokusumo',
            ),

            array (
                'code' => '35.07.08',
                'name' => 'Wajak',
            ),

            array (
                'code' => '35.07.09',
                'name' => 'Turen',
            ),

            array (
                'code' => '35.07.10',
                'name' => 'Gondanglegi',
            ),

            array (
                'code' => '35.07.11',
                'name' => 'Kalipare',
            ),

            array (
                'code' => '35.07.12',
                'name' => 'Sumberpucung',
            ),

            array (
                'code' => '35.07.13',
                'name' => 'Kepanjen',
            ),

            array (
                'code' => '35.07.14',
                'name' => 'Bululawang',
            ),

            array (
                'code' => '35.07.15',
                'name' => 'Tajinan',
            ),

            array (
                'code' => '35.07.16',
                'name' => 'Tumpang',
            ),

            array (
                'code' => '35.07.17',
                'name' => 'Jabung',
            ),

            array (
                'code' => '35.07.18',
                'name' => 'Pakis',
            ),

            array (
                'code' => '35.07.19',
                'name' => 'Pakisaji',
            ),

            array (
                'code' => '35.07.20',
                'name' => 'Ngajung',
            ),

            array (
                'code' => '35.07.21',
                'name' => 'Wagir',
            ),

            array (
                'code' => '35.07.22',
                'name' => 'Dau',
            ),

            array (
                'code' => '35.07.23',
                'name' => 'Karang Ploso',
            ),

            array (
                'code' => '35.07.24',
                'name' => 'Singosari',
            ),

            array (
                'code' => '35.07.25',
                'name' => 'Lawang',
            ),

            array (
                'code' => '35.07.26',
                'name' => 'Pujon',
            ),

            array (
                'code' => '35.07.27',
                'name' => 'Ngantang',
            ),

            array (
                'code' => '35.07.28',
                'name' => 'Kasembon',
            ),

            array (
                'code' => '35.07.29',
                'name' => 'Gedangan',
            ),

            array (
                'code' => '35.07.30',
                'name' => 'Tirtoyudo',
            ),

            array (
                'code' => '35.07.31',
                'name' => 'Kromengan',
            ),

            array (
                'code' => '35.07.32',
                'name' => 'Wonosari',
            ),

            array (
                'code' => '35.07.33',
                'name' => 'Pagelaran',
            ),

            array (
                'code' => '35.08',
                'name' => 'KAB. LUMAJANG',
            ),

            array (
                'code' => '35.08.01',
                'name' => 'Tempursari',
            ),

            array (
                'code' => '35.08.02',
                'name' => 'Pronojiwo',
            ),

            array (
                'code' => '35.08.03',
                'name' => 'Candipuro',
            ),

            array (
                'code' => '35.08.04',
                'name' => 'Pasirian',
            ),

            array (
                'code' => '35.08.05',
                'name' => 'Tempeh',
            ),

            array (
                'code' => '35.08.06',
                'name' => 'Kunir',
            ),

            array (
                'code' => '35.08.07',
                'name' => 'Yosowilangun',
            ),

            array (
                'code' => '35.08.08',
                'name' => 'Rowokangkung',
            ),

            array (
                'code' => '35.08.09',
                'name' => 'Tekung',
            ),

            array (
                'code' => '35.08.10',
                'name' => 'Lumajang',
            ),

            array (
                'code' => '35.08.11',
                'name' => 'Pasrujambe',
            ),

            array (
                'code' => '35.08.12',
                'name' => 'Senduro',
            ),

            array (
                'code' => '35.08.13',
                'name' => 'Gucialit',
            ),

            array (
                'code' => '35.08.14',
                'name' => 'Padang',
            ),

            array (
                'code' => '35.08.15',
                'name' => 'Sukodono',
            ),

            array (
                'code' => '35.08.16',
                'name' => 'Kedungjajang',
            ),

            array (
                'code' => '35.08.17',
                'name' => 'Jatiroto',
            ),

            array (
                'code' => '35.08.18',
                'name' => 'Randuagung',
            ),

            array (
                'code' => '35.08.19',
                'name' => 'Klakah',
            ),

            array (
                'code' => '35.08.20',
                'name' => 'Ranuyoso',
            ),

            array (
                'code' => '35.08.21',
                'name' => 'Sumbersuko',
            ),

            array (
                'code' => '35.09',
                'name' => 'KAB. JEMBER',
            ),

            array (
                'code' => '35.09.01',
                'name' => 'Jombang',
            ),

            array (
                'code' => '35.09.02',
                'name' => 'Kencong',
            ),

            array (
                'code' => '35.09.03',
                'name' => 'Sumberbaru',
            ),

            array (
                'code' => '35.09.04',
                'name' => 'Gumukmas',
            ),

            array (
                'code' => '35.09.05',
                'name' => 'Umbulsari',
            ),

            array (
                'code' => '35.09.06',
                'name' => 'Tanggul',
            ),

            array (
                'code' => '35.09.07',
                'name' => 'Semboro',
            ),

            array (
                'code' => '35.09.08',
                'name' => 'Puger',
            ),

            array (
                'code' => '35.09.09',
                'name' => 'Bangsalsari',
            ),

            array (
                'code' => '35.09.10',
                'name' => 'Balung',
            ),

            array (
                'code' => '35.09.11',
                'name' => 'Wuluhan',
            ),

            array (
                'code' => '35.09.12',
                'name' => 'Ambulu',
            ),

            array (
                'code' => '35.09.13',
                'name' => 'Rambipuji',
            ),

            array (
                'code' => '35.09.14',
                'name' => 'Panti',
            ),

            array (
                'code' => '35.09.15',
                'name' => 'Sukorambi',
            ),

            array (
                'code' => '35.09.16',
                'name' => 'Jenggawah',
            ),

            array (
                'code' => '35.09.17',
                'name' => 'Ajung',
            ),

            array (
                'code' => '35.09.18',
                'name' => 'Tempurejo',
            ),

            array (
                'code' => '35.09.19',
                'name' => 'Kaliwates',
            ),

            array (
                'code' => '35.09.20',
                'name' => 'Patrang',
            ),

            array (
                'code' => '35.09.21',
                'name' => 'Sumbersari',
            ),

            array (
                'code' => '35.09.22',
                'name' => 'Arjasa',
            ),

            array (
                'code' => '35.09.23',
                'name' => 'Mumbulsari',
            ),

            array (
                'code' => '35.09.24',
                'name' => 'Pakusari',
            ),

            array (
                'code' => '35.09.25',
                'name' => 'Jelbuk',
            ),

            array (
                'code' => '35.09.26',
                'name' => 'Mayang',
            ),

            array (
                'code' => '35.09.27',
                'name' => 'Kalisat',
            ),

            array (
                'code' => '35.09.28',
                'name' => 'Ledokombo',
            ),

            array (
                'code' => '35.09.29',
                'name' => 'Sukowono',
            ),

            array (
                'code' => '35.09.30',
                'name' => 'Silo',
            ),

            array (
                'code' => '35.09.31',
                'name' => 'Sumberjambe',
            ),

            array (
                'code' => '35.10',
                'name' => 'KAB. BANYUWANGI',
            ),

            array (
                'code' => '35.10.01',
                'name' => 'Pesanggaran',
            ),

            array (
                'code' => '35.10.02',
                'name' => 'Bangorejo',
            ),

            array (
                'code' => '35.10.03',
                'name' => 'Purwoharjo',
            ),

            array (
                'code' => '35.10.04',
                'name' => 'Tegaldlimo',
            ),

            array (
                'code' => '35.10.05',
                'name' => 'Muncar',
            ),

            array (
                'code' => '35.10.06',
                'name' => 'Cluring',
            ),

            array (
                'code' => '35.10.07',
                'name' => 'Gambiran',
            ),

            array (
                'code' => '35.10.08',
                'name' => 'Srono',
            ),

            array (
                'code' => '35.10.09',
                'name' => 'Genteng',
            ),

            array (
                'code' => '35.10.10',
                'name' => 'Glenmore',
            ),

            array (
                'code' => '35.10.11',
                'name' => 'Kalibaru',
            ),

            array (
                'code' => '35.10.12',
                'name' => 'Singojuruh',
            ),

            array (
                'code' => '35.10.13',
                'name' => 'Rogojampi',
            ),

            array (
                'code' => '35.10.14',
                'name' => 'Kabat',
            ),

            array (
                'code' => '35.10.15',
                'name' => 'Glagah',
            ),

            array (
                'code' => '35.10.16',
                'name' => 'Banyuwangi',
            ),

            array (
                'code' => '35.10.17',
                'name' => 'Giri',
            ),

            array (
                'code' => '35.10.18',
                'name' => 'Wongsorejo',
            ),

            array (
                'code' => '35.10.19',
                'name' => 'Songgon',
            ),

            array (
                'code' => '35.10.20',
                'name' => 'Sempu',
            ),

            array (
                'code' => '35.10.21',
                'name' => 'Kalipuro',
            ),

            array (
                'code' => '35.10.22',
                'name' => 'Siliragung',
            ),

            array (
                'code' => '35.10.23',
                'name' => 'Tegalsari',
            ),

            array (
                'code' => '35.10.24',
                'name' => 'Licin',
            ),

            array (
                'code' => '35.11',
                'name' => 'KAB. BONDOWOSO',
            ),

            array (
                'code' => '35.11.01',
                'name' => 'Maesan',
            ),

            array (
                'code' => '35.11.02',
                'name' => 'Tamanan',
            ),

            array (
                'code' => '35.11.03',
                'name' => 'Tlogosari',
            ),

            array (
                'code' => '35.11.04',
                'name' => 'Sukosari',
            ),

            array (
                'code' => '35.11.05',
                'name' => 'Pujer',
            ),

            array (
                'code' => '35.11.06',
                'name' => 'Grujugan',
            ),

            array (
                'code' => '35.11.07',
                'name' => 'Curahdami',
            ),

            array (
                'code' => '35.11.08',
                'name' => 'Tenggarang',
            ),

            array (
                'code' => '35.11.09',
                'name' => 'Wonosari',
            ),

            array (
                'code' => '35.11.10',
                'name' => 'Tapen',
            ),

            array (
                'code' => '35.11.11',
                'name' => 'Bondowoso',
            ),

            array (
                'code' => '35.11.12',
                'name' => 'Wringin',
            ),

            array (
                'code' => '35.11.13',
                'name' => 'Tegalampel',
            ),

            array (
                'code' => '35.11.14',
                'name' => 'Klabang',
            ),

            array (
                'code' => '35.11.15',
                'name' => 'Cermee',
            ),

            array (
                'code' => '35.11.16',
                'name' => 'Prajekan',
            ),

            array (
                'code' => '35.11.17',
                'name' => 'Pakem',
            ),

            array (
                'code' => '35.11.18',
                'name' => 'Sumberwringin',
            ),

            array (
                'code' => '35.11.19',
                'name' => 'Sempol',
            ),

            array (
                'code' => '35.11.20',
                'name' => 'Binakal',
            ),

            array (
                'code' => '35.11.21',
                'name' => 'Taman Krocok',
            ),

            array (
                'code' => '35.11.22',
                'name' => 'Botolinggo',
            ),

            array (
                'code' => '35.11.23',
                'name' => 'Jambesari Darus Sholah',
            ),

            array (
                'code' => '35.12',
                'name' => 'KAB. SITUBONDO',
            ),

            array (
                'code' => '35.12.01',
                'name' => 'Jatibanteng',
            ),

            array (
                'code' => '35.12.02',
                'name' => 'Besuki',
            ),

            array (
                'code' => '35.12.03',
                'name' => 'Suboh',
            ),

            array (
                'code' => '35.12.04',
                'name' => 'Mlandingan',
            ),

            array (
                'code' => '35.12.05',
                'name' => 'Kendit',
            ),

            array (
                'code' => '35.12.06',
                'name' => 'Panarukan',
            ),

            array (
                'code' => '35.12.07',
                'name' => 'Situbondo',
            ),

            array (
                'code' => '35.12.08',
                'name' => 'Panji',
            ),

            array (
                'code' => '35.12.09',
                'name' => 'Mangaran',
            ),

            array (
                'code' => '35.12.10',
                'name' => 'Kapongan',
            ),

            array (
                'code' => '35.12.11',
                'name' => 'Arjasa',
            ),

            array (
                'code' => '35.12.12',
                'name' => 'Jangkar',
            ),

            array (
                'code' => '35.12.13',
                'name' => 'Asembagus',
            ),

            array (
                'code' => '35.12.14',
                'name' => 'Banyuputih',
            ),

            array (
                'code' => '35.12.15',
                'name' => 'Sumbermalang',
            ),

            array (
                'code' => '35.12.16',
                'name' => 'Banyuglugur',
            ),

            array (
                'code' => '35.12.17',
                'name' => 'Bungatan',
            ),

            array (
                'code' => '35.13',
                'name' => 'KAB. PROBOLINGGO',
            ),

            array (
                'code' => '35.13.01',
                'name' => 'Sukapura',
            ),

            array (
                'code' => '35.13.02',
                'name' => 'Sumber',
            ),

            array (
                'code' => '35.13.03',
                'name' => 'Kuripan',
            ),

            array (
                'code' => '35.13.04',
                'name' => 'Bantaran',
            ),

            array (
                'code' => '35.13.05',
                'name' => 'Leces',
            ),

            array (
                'code' => '35.13.06',
                'name' => 'Banyuanyar',
            ),

            array (
                'code' => '35.13.07',
                'name' => 'Tiris',
            ),

            array (
                'code' => '35.13.08',
                'name' => 'Krucil',
            ),

            array (
                'code' => '35.13.09',
                'name' => 'Gading',
            ),

            array (
                'code' => '35.13.10',
                'name' => 'Pakuniran',
            ),

            array (
                'code' => '35.13.11',
                'name' => 'Kotaanyar',
            ),

            array (
                'code' => '35.13.12',
                'name' => 'Paiton',
            ),

            array (
                'code' => '35.13.13',
                'name' => 'Besuk',
            ),

            array (
                'code' => '35.13.14',
                'name' => 'Kraksaan',
            ),

            array (
                'code' => '35.13.15',
                'name' => 'Krejengan',
            ),

            array (
                'code' => '35.13.16',
                'name' => 'Pejarakan',
            ),

            array (
                'code' => '35.13.17',
                'name' => 'Maron',
            ),

            array (
                'code' => '35.13.18',
                'name' => 'Gending',
            ),

            array (
                'code' => '35.13.19',
                'name' => 'Dringu',
            ),

            array (
                'code' => '35.13.20',
                'name' => 'Tegalsiwalan',
            ),

            array (
                'code' => '35.13.21',
                'name' => 'Sumberasih',
            ),

            array (
                'code' => '35.13.22',
                'name' => 'Wonomerto',
            ),

            array (
                'code' => '35.13.23',
                'name' => 'Tongas',
            ),

            array (
                'code' => '35.13.24',
                'name' => 'Lumbang',
            ),

            array (
                'code' => '35.14',
                'name' => 'KAB. PASURUAN',
            ),

            array (
                'code' => '35.14.01',
                'name' => 'Purwodadi',
            ),

            array (
                'code' => '35.14.02',
                'name' => 'Tutur',
            ),

            array (
                'code' => '35.14.03',
                'name' => 'Puspo',
            ),

            array (
                'code' => '35.14.04',
                'name' => 'Lumbang',
            ),

            array (
                'code' => '35.14.05',
                'name' => 'Pasrepan',
            ),

            array (
                'code' => '35.14.06',
                'name' => 'Kejayan',
            ),

            array (
                'code' => '35.14.07',
                'name' => 'Wonorejo',
            ),

            array (
                'code' => '35.14.08',
                'name' => 'Purwosari',
            ),

            array (
                'code' => '35.14.09',
                'name' => 'Sukorejo',
            ),

            array (
                'code' => '35.14.10',
                'name' => 'Prigen',
            ),

            array (
                'code' => '35.14.11',
                'name' => 'Pandaan',
            ),

            array (
                'code' => '35.14.12',
                'name' => 'Gempol',
            ),

            array (
                'code' => '35.14.13',
                'name' => 'Beji',
            ),

            array (
                'code' => '35.14.14',
                'name' => 'Bangil',
            ),

            array (
                'code' => '35.14.15',
                'name' => 'Rembang',
            ),

            array (
                'code' => '35.14.16',
                'name' => 'Kraton',
            ),

            array (
                'code' => '35.14.17',
                'name' => 'Pohjentrek',
            ),

            array (
                'code' => '35.14.18',
                'name' => 'Gondangwetan',
            ),

            array (
                'code' => '35.14.19',
                'name' => 'Winongan',
            ),

            array (
                'code' => '35.14.20',
                'name' => 'Grati',
            ),

            array (
                'code' => '35.14.21',
                'name' => 'Nguling',
            ),

            array (
                'code' => '35.14.22',
                'name' => 'Lekok',
            ),

            array (
                'code' => '35.14.23',
                'name' => 'Rejoso',
            ),

            array (
                'code' => '35.14.24',
                'name' => 'Tosari',
            ),

            array (
                'code' => '35.15',
                'name' => 'KAB. SIDOARJO',
            ),

            array (
                'code' => '35.15.01',
                'name' => 'Tarik',
            ),

            array (
                'code' => '35.15.02',
                'name' => 'Prambon',
            ),

            array (
                'code' => '35.15.03',
                'name' => 'Krembung',
            ),

            array (
                'code' => '35.15.04',
                'name' => 'Porong',
            ),

            array (
                'code' => '35.15.05',
                'name' => 'Jabon',
            ),

            array (
                'code' => '35.15.06',
                'name' => 'Tanggulangin',
            ),

            array (
                'code' => '35.15.07',
                'name' => 'Candi',
            ),

            array (
                'code' => '35.15.08',
                'name' => 'Sidoarjo',
            ),

            array (
                'code' => '35.15.09',
                'name' => 'Tulangan',
            ),

            array (
                'code' => '35.15.10',
                'name' => 'Wonoayu',
            ),

            array (
                'code' => '35.15.11',
                'name' => 'Krian',
            ),

            array (
                'code' => '35.15.12',
                'name' => 'Balongbendo',
            ),

            array (
                'code' => '35.15.13',
                'name' => 'Taman',
            ),

            array (
                'code' => '35.15.14',
                'name' => 'Sukodono',
            ),

            array (
                'code' => '35.15.15',
                'name' => 'Buduran',
            ),

            array (
                'code' => '35.15.16',
                'name' => 'Gedangan',
            ),

            array (
                'code' => '35.15.17',
                'name' => 'Sedati',
            ),

            array (
                'code' => '35.15.18',
                'name' => 'Waru',
            ),

            array (
                'code' => '35.16',
                'name' => 'KAB. MOJOKERTO',
            ),

            array (
                'code' => '35.16.01',
                'name' => 'Jatirejo',
            ),

            array (
                'code' => '35.16.02',
                'name' => 'Gondang',
            ),

            array (
                'code' => '35.16.03',
                'name' => 'Pacet',
            ),

            array (
                'code' => '35.16.04',
                'name' => 'Trawas',
            ),

            array (
                'code' => '35.16.05',
                'name' => 'Ngoro',
            ),

            array (
                'code' => '35.16.06',
                'name' => 'Pungging',
            ),

            array (
                'code' => '35.16.07',
                'name' => 'Kutorejo',
            ),

            array (
                'code' => '35.16.08',
                'name' => 'Mojosari',
            ),

            array (
                'code' => '35.16.09',
                'name' => 'Dlanggu',
            ),

            array (
                'code' => '35.16.10',
                'name' => 'Bangsal',
            ),

            array (
                'code' => '35.16.11',
                'name' => 'Puri',
            ),

            array (
                'code' => '35.16.12',
                'name' => 'Trowulan',
            ),

            array (
                'code' => '35.16.13',
                'name' => 'Sooko',
            ),

            array (
                'code' => '35.16.14',
                'name' => 'Gedeg',
            ),

            array (
                'code' => '35.16.15',
                'name' => 'Kemlagi',
            ),

            array (
                'code' => '35.16.16',
                'name' => 'Jetis',
            ),

            array (
                'code' => '35.16.17',
                'name' => 'Dawarblandong',
            ),

            array (
                'code' => '35.16.18',
                'name' => 'Mojoanyar',
            ),

            array (
                'code' => '35.17',
                'name' => 'KAB. JOMBANG',
            ),

            array (
                'code' => '35.17.01',
                'name' => 'Perak',
            ),

            array (
                'code' => '35.17.02',
                'name' => 'Gudo',
            ),

            array (
                'code' => '35.17.03',
                'name' => 'Ngoro',
            ),

            array (
                'code' => '35.17.04',
                'name' => 'Bareng',
            ),

            array (
                'code' => '35.17.05',
                'name' => 'Wonosalam',
            ),

            array (
                'code' => '35.17.06',
                'name' => 'Mojoagung',
            ),

            array (
                'code' => '35.17.07',
                'name' => 'Mojowarno',
            ),

            array (
                'code' => '35.17.08',
                'name' => 'Diwek',
            ),

            array (
                'code' => '35.17.09',
                'name' => 'Jombang',
            ),

            array (
                'code' => '35.17.10',
                'name' => 'Peterongan',
            ),

            array (
                'code' => '35.17.11',
                'name' => 'Sumobito',
            ),

            array (
                'code' => '35.17.12',
                'name' => 'Kesamben',
            ),

            array (
                'code' => '35.17.13',
                'name' => 'Tembelang',
            ),

            array (
                'code' => '35.17.14',
                'name' => 'Ploso',
            ),

            array (
                'code' => '35.17.15',
                'name' => 'Plandaan',
            ),

            array (
                'code' => '35.17.16',
                'name' => 'Kabuh',
            ),

            array (
                'code' => '35.17.17',
                'name' => 'Kudu',
            ),

            array (
                'code' => '35.17.18',
                'name' => 'Bandarkedungmulyo',
            ),

            array (
                'code' => '35.17.19',
                'name' => 'Jogoroto',
            ),

            array (
                'code' => '35.17.20',
                'name' => 'Megaluh',
            ),

            array (
                'code' => '35.17.21',
                'name' => 'Ngusikan',
            ),

            array (
                'code' => '35.18',
                'name' => 'KAB. NGANJUK',
            ),

            array (
                'code' => '35.18.01',
                'name' => 'Sawahan',
            ),

            array (
                'code' => '35.18.02',
                'name' => 'Ngetos',
            ),

            array (
                'code' => '35.18.03',
                'name' => 'Berbek',
            ),

            array (
                'code' => '35.18.04',
                'name' => 'Loceret',
            ),

            array (
                'code' => '35.18.05',
                'name' => 'Pace',
            ),

            array (
                'code' => '35.18.06',
                'name' => 'Prambon',
            ),

            array (
                'code' => '35.18.07',
                'name' => 'Ngronggot',
            ),

            array (
                'code' => '35.18.08',
                'name' => 'Kertosono',
            ),

            array (
                'code' => '35.18.09',
                'name' => 'Patianrowo',
            ),

            array (
                'code' => '35.18.10',
                'name' => 'Baron',
            ),

            array (
                'code' => '35.18.11',
                'name' => 'Tanjunganom',
            ),

            array (
                'code' => '35.18.12',
                'name' => 'Sukomoro',
            ),

            array (
                'code' => '35.18.13',
                'name' => 'Nganjuk',
            ),

            array (
                'code' => '35.18.14',
                'name' => 'Bagor',
            ),

            array (
                'code' => '35.18.15',
                'name' => 'Wilangan',
            ),

            array (
                'code' => '35.18.16',
                'name' => 'Rejoso',
            ),

            array (
                'code' => '35.18.17',
                'name' => 'Gondang',
            ),

            array (
                'code' => '35.18.18',
                'name' => 'Ngluyu',
            ),

            array (
                'code' => '35.18.19',
                'name' => 'Lengkong',
            ),

            array (
                'code' => '35.18.20',
                'name' => 'Jatikalen',
            ),

            array (
                'code' => '35.19',
                'name' => 'KAB. MADIUN',
            ),

            array (
                'code' => '35.19.01',
                'name' => 'Kebon Sari',
            ),

            array (
                'code' => '35.19.02',
                'name' => 'Dolopo',
            ),

            array (
                'code' => '35.19.03',
                'name' => 'Geger',
            ),

            array (
                'code' => '35.19.04',
                'name' => 'Dagangan',
            ),

            array (
                'code' => '35.19.05',
                'name' => 'Kare',
            ),

            array (
                'code' => '35.19.06',
                'name' => 'Gemarang',
            ),

            array (
                'code' => '35.19.07',
                'name' => 'Wungu',
            ),

            array (
                'code' => '35.19.08',
                'name' => 'Madiun',
            ),

            array (
                'code' => '35.19.09',
                'name' => 'Jiwan',
            ),

            array (
                'code' => '35.19.10',
                'name' => 'Balerejo',
            ),

            array (
                'code' => '35.19.11',
                'name' => 'Mejayan',
            ),

            array (
                'code' => '35.19.12',
                'name' => 'Saradan',
            ),

            array (
                'code' => '35.19.13',
                'name' => 'Pilangkenceng',
            ),

            array (
                'code' => '35.19.14',
                'name' => 'Sawahan',
            ),

            array (
                'code' => '35.19.15',
                'name' => 'Wonoasri',
            ),

            array (
                'code' => '35.20',
                'name' => 'KAB. MAGETAN',
            ),

            array (
                'code' => '35.20.01',
                'name' => 'Poncol',
            ),

            array (
                'code' => '35.20.02',
                'name' => 'Parang',
            ),

            array (
                'code' => '35.20.03',
                'name' => 'Lembeyan',
            ),

            array (
                'code' => '35.20.04',
                'name' => 'Takeran',
            ),

            array (
                'code' => '35.20.05',
                'name' => 'Kawedanan',
            ),

            array (
                'code' => '35.20.06',
                'name' => 'Magetan',
            ),

            array (
                'code' => '35.20.07',
                'name' => 'Plaosan',
            ),

            array (
                'code' => '35.20.08',
                'name' => 'Panekan',
            ),

            array (
                'code' => '35.20.09',
                'name' => 'Sukomoro',
            ),

            array (
                'code' => '35.20.10',
                'name' => 'Bendo',
            ),

            array (
                'code' => '35.20.11',
                'name' => 'Maospati',
            ),

            array (
                'code' => '35.20.12',
                'name' => 'Barat',
            ),

            array (
                'code' => '35.20.13',
                'name' => 'Karangrejo',
            ),

            array (
                'code' => '35.20.14',
                'name' => 'Karas',
            ),

            array (
                'code' => '35.20.15',
                'name' => 'Kartoharjo',
            ),

            array (
                'code' => '35.20.16',
                'name' => 'Ngariboyo',
            ),

            array (
                'code' => '35.20.17',
                'name' => 'Nguntoronadi',
            ),

            array (
                'code' => '35.20.18',
                'name' => 'Sidorejo',
            ),

            array (
                'code' => '35.21',
                'name' => 'KAB. NGAWI',
            ),

            array (
                'code' => '35.21.01',
                'name' => 'Sine',
            ),

            array (
                'code' => '35.21.02',
                'name' => 'Ngrambe',
            ),

            array (
                'code' => '35.21.03',
                'name' => 'Jogorogo',
            ),

            array (
                'code' => '35.21.04',
                'name' => 'Kendal',
            ),

            array (
                'code' => '35.21.05',
                'name' => 'Geneng',
            ),

            array (
                'code' => '35.21.06',
                'name' => 'Kwadungan',
            ),

            array (
                'code' => '35.21.07',
                'name' => 'Karangjati',
            ),

            array (
                'code' => '35.21.08',
                'name' => 'Padas',
            ),

            array (
                'code' => '35.21.09',
                'name' => 'Ngawi',
            ),

            array (
                'code' => '35.21.10',
                'name' => 'Paron',
            ),

            array (
                'code' => '35.21.11',
                'name' => 'Kedunggalar',
            ),

            array (
                'code' => '35.21.12',
                'name' => 'Widodaren',
            ),

            array (
                'code' => '35.21.13',
                'name' => 'Mantingan',
            ),

            array (
                'code' => '35.21.14',
                'name' => 'Pangkur',
            ),

            array (
                'code' => '35.21.15',
                'name' => 'Bringin',
            ),

            array (
                'code' => '35.21.16',
                'name' => 'Pitu',
            ),

            array (
                'code' => '35.21.17',
                'name' => 'Karanganyar',
            ),

            array (
                'code' => '35.21.18',
                'name' => 'Gerih',
            ),

            array (
                'code' => '35.21.19',
                'name' => 'Kasreman',
            ),

            array (
                'code' => '35.22',
                'name' => 'KAB. BOJONEGORO',
            ),

            array (
                'code' => '35.22.01',
                'name' => 'Ngraho',
            ),

            array (
                'code' => '35.22.02',
                'name' => 'Tambakrejo',
            ),

            array (
                'code' => '35.22.03',
                'name' => 'Ngambon',
            ),

            array (
                'code' => '35.22.04',
                'name' => 'Ngasem',
            ),

            array (
                'code' => '35.22.05',
                'name' => 'Bubulan',
            ),

            array (
                'code' => '35.22.06',
                'name' => 'Dander',
            ),

            array (
                'code' => '35.22.07',
                'name' => 'Sugihwaras',
            ),

            array (
                'code' => '35.22.08',
                'name' => 'Kedungadem',
            ),

            array (
                'code' => '35.22.09',
                'name' => 'Kepoh Baru',
            ),

            array (
                'code' => '35.22.10',
                'name' => 'Baureno',
            ),

            array (
                'code' => '35.22.11',
                'name' => 'Kanor',
            ),

            array (
                'code' => '35.22.12',
                'name' => 'Sumberejo',
            ),

            array (
                'code' => '35.22.13',
                'name' => 'Balen',
            ),

            array (
                'code' => '35.22.14',
                'name' => 'Kapas',
            ),

            array (
                'code' => '35.22.15',
                'name' => 'Bojonegoro',
            ),

            array (
                'code' => '35.22.16',
                'name' => 'Kalitidu',
            ),

            array (
                'code' => '35.22.17',
                'name' => 'Malo',
            ),

            array (
                'code' => '35.22.18',
                'name' => 'Purwosari',
            ),

            array (
                'code' => '35.22.19',
                'name' => 'Padangan',
            ),

            array (
                'code' => '35.22.20',
                'name' => 'Kasiman',
            ),

            array (
                'code' => '35.22.21',
                'name' => 'Temayang',
            ),

            array (
                'code' => '35.22.22',
                'name' => 'Margomulyo',
            ),

            array (
                'code' => '35.22.23',
                'name' => 'Trucuk',
            ),

            array (
                'code' => '35.22.24',
                'name' => 'Sukosewu',
            ),

            array (
                'code' => '35.22.25',
                'name' => 'Kedewan',
            ),

            array (
                'code' => '35.22.26',
                'name' => 'Gondang',
            ),

            array (
                'code' => '35.22.27',
                'name' => 'Sekar',
            ),

            array (
                'code' => '35.22.28',
                'name' => 'Gayam',
            ),

            array (
                'code' => '35.23',
                'name' => 'KAB. TUBAN',
            ),

            array (
                'code' => '35.23.01',
                'name' => 'Kenduruan',
            ),

            array (
                'code' => '35.23.02',
                'name' => 'Jatirogo',
            ),

            array (
                'code' => '35.23.03',
                'name' => 'Bangilan',
            ),

            array (
                'code' => '35.23.04',
                'name' => 'Bancar',
            ),

            array (
                'code' => '35.23.05',
                'name' => 'Senori',
            ),

            array (
                'code' => '35.23.06',
                'name' => 'Tambakboyo',
            ),

            array (
                'code' => '35.23.07',
                'name' => 'Singgahan',
            ),

            array (
                'code' => '35.23.08',
                'name' => 'Kerek',
            ),

            array (
                'code' => '35.23.09',
                'name' => 'Parengan',
            ),

            array (
                'code' => '35.23.10',
                'name' => 'Montong',
            ),

            array (
                'code' => '35.23.11',
                'name' => 'Soko',
            ),

            array (
                'code' => '35.23.12',
                'name' => 'Jenu',
            ),

            array (
                'code' => '35.23.13',
                'name' => 'Merakurak',
            ),

            array (
                'code' => '35.23.14',
                'name' => 'Rengel',
            ),

            array (
                'code' => '35.23.15',
                'name' => 'Semanding',
            ),

            array (
                'code' => '35.23.16',
                'name' => 'Tuban',
            ),

            array (
                'code' => '35.23.17',
                'name' => 'Plumpang',
            ),

            array (
                'code' => '35.23.18',
                'name' => 'Palang',
            ),

            array (
                'code' => '35.23.19',
                'name' => 'Widang',
            ),

            array (
                'code' => '35.23.20',
                'name' => 'Grabagan',
            ),

            array (
                'code' => '35.24',
                'name' => 'KAB. LAMONGAN',
            ),

            array (
                'code' => '35.24.01',
                'name' => 'Sukorame',
            ),

            array (
                'code' => '35.24.02',
                'name' => 'Bluluk',
            ),

            array (
                'code' => '35.24.03',
                'name' => 'Modo',
            ),

            array (
                'code' => '35.24.04',
                'name' => 'Ngimbang',
            ),

            array (
                'code' => '35.24.05',
                'name' => 'Babat',
            ),

            array (
                'code' => '35.24.06',
                'name' => 'Kedungpring',
            ),

            array (
                'code' => '35.24.07',
                'name' => 'Brondong',
            ),

            array (
                'code' => '35.24.08',
                'name' => 'Laren',
            ),

            array (
                'code' => '35.24.09',
                'name' => 'Sekaran',
            ),

            array (
                'code' => '35.24.10',
                'name' => 'Maduran',
            ),

            array (
                'code' => '35.24.11',
                'name' => 'Sambeng',
            ),

            array (
                'code' => '35.24.12',
                'name' => 'Sugio',
            ),

            array (
                'code' => '35.24.13',
                'name' => 'Pucuk',
            ),

            array (
                'code' => '35.24.14',
                'name' => 'Paciran',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '35.24.15',
                'name' => 'Solokuro',
            ),

            array (
                'code' => '35.24.16',
                'name' => 'Mantup',
            ),

            array (
                'code' => '35.24.17',
                'name' => 'Sukodadi',
            ),

            array (
                'code' => '35.24.18',
                'name' => 'Karanggeneng',
            ),

            array (
                'code' => '35.24.19',
                'name' => 'Kembangbahu',
            ),

            array (
                'code' => '35.24.20',
                'name' => 'Kalitengah',
            ),

            array (
                'code' => '35.24.21',
                'name' => 'Turi',
            ),

            array (
                'code' => '35.24.22',
                'name' => 'Lamongan',
            ),

            array (
                'code' => '35.24.23',
                'name' => 'Tikung',
            ),

            array (
                'code' => '35.24.24',
                'name' => 'Karangbinangun',
            ),

            array (
                'code' => '35.24.25',
                'name' => 'Deket',
            ),

            array (
                'code' => '35.24.26',
                'name' => 'Glagah',
            ),

            array (
                'code' => '35.24.27',
                'name' => 'Sarirejo',
            ),

            array (
                'code' => '35.25',
                'name' => 'KAB. GRESIK',
            ),

            array (
                'code' => '35.25.01',
                'name' => 'Dukun',
            ),

            array (
                'code' => '35.25.02',
                'name' => 'Balongpanggang',
            ),

            array (
                'code' => '35.25.03',
                'name' => 'Panceng',
            ),

            array (
                'code' => '35.25.04',
                'name' => 'Benjeng',
            ),

            array (
                'code' => '35.25.05',
                'name' => 'Duduksampeyan',
            ),

            array (
                'code' => '35.25.06',
                'name' => 'Wringinanom',
            ),

            array (
                'code' => '35.25.07',
                'name' => 'Ujungpangkah',
            ),

            array (
                'code' => '35.25.08',
                'name' => 'Kedamean',
            ),

            array (
                'code' => '35.25.09',
                'name' => 'Sidayu',
            ),

            array (
                'code' => '35.25.10',
                'name' => 'Manyar',
            ),

            array (
                'code' => '35.25.11',
                'name' => 'Cerme',
            ),

            array (
                'code' => '35.25.12',
                'name' => 'Bungah',
            ),

            array (
                'code' => '35.25.13',
                'name' => 'Menganti',
            ),

            array (
                'code' => '35.25.14',
                'name' => 'Kebomas',
            ),

            array (
                'code' => '35.25.15',
                'name' => 'Driyorejo',
            ),

            array (
                'code' => '35.25.16',
                'name' => 'Gresik',
            ),

            array (
                'code' => '35.25.17',
                'name' => 'Sangkapura',
            ),

            array (
                'code' => '35.25.18',
                'name' => 'Tambak',
            ),

            array (
                'code' => '35.26',
                'name' => 'KAB. BANGKALAN',
            ),

            array (
                'code' => '35.26.01',
                'name' => 'Bangkalan',
            ),

            array (
                'code' => '35.26.02',
                'name' => 'Socah',
            ),

            array (
                'code' => '35.26.03',
                'name' => 'Burneh',
            ),

            array (
                'code' => '35.26.04',
                'name' => 'Kamal',
            ),

            array (
                'code' => '35.26.05',
                'name' => 'Arosbaya',
            ),

            array (
                'code' => '35.26.06',
                'name' => 'Geger',
            ),

            array (
                'code' => '35.26.07',
                'name' => 'Klampis',
            ),

            array (
                'code' => '35.26.08',
                'name' => 'Sepulu',
            ),

            array (
                'code' => '35.26.09',
                'name' => 'Tanjung Bumi',
            ),

            array (
                'code' => '35.26.10',
                'name' => 'Kokop',
            ),

            array (
                'code' => '35.26.11',
                'name' => 'Kwanyar',
            ),

            array (
                'code' => '35.26.12',
                'name' => 'Labang',
            ),

            array (
                'code' => '35.26.13',
                'name' => 'Tanah Merah',
            ),

            array (
                'code' => '35.26.14',
                'name' => 'Tragah',
            ),

            array (
                'code' => '35.26.15',
                'name' => 'Blega',
            ),

            array (
                'code' => '35.26.16',
                'name' => 'Modung',
            ),

            array (
                'code' => '35.26.17',
                'name' => 'Konang',
            ),

            array (
                'code' => '35.26.18',
                'name' => 'Galis',
            ),

            array (
                'code' => '35.27',
                'name' => 'KAB. SAMPANG',
            ),

            array (
                'code' => '35.27.01',
                'name' => 'Sreseh',
            ),

            array (
                'code' => '35.27.02',
                'name' => 'Torjun',
            ),

            array (
                'code' => '35.27.03',
                'name' => 'Sampang',
            ),

            array (
                'code' => '35.27.04',
                'name' => 'Camplong',
            ),

            array (
                'code' => '35.27.05',
                'name' => 'Omben',
            ),

            array (
                'code' => '35.27.06',
                'name' => 'Kedungdung',
            ),

            array (
                'code' => '35.27.07',
                'name' => 'Jrengik',
            ),

            array (
                'code' => '35.27.08',
                'name' => 'Tambelangan',
            ),

            array (
                'code' => '35.27.09',
                'name' => 'Banyuates',
            ),

            array (
                'code' => '35.27.10',
                'name' => 'Robatal',
            ),

            array (
                'code' => '35.27.11',
                'name' => 'Sokobanah',
            ),

            array (
                'code' => '35.27.12',
                'name' => 'Ketapang',
            ),

            array (
                'code' => '35.27.13',
                'name' => 'Pangarengan',
            ),

            array (
                'code' => '35.27.14',
                'name' => 'Karangpenang',
            ),

            array (
                'code' => '35.28',
                'name' => 'KAB. PAMEKASAN',
            ),

            array (
                'code' => '35.28.01',
                'name' => 'Tlanakan',
            ),

            array (
                'code' => '35.28.02',
                'name' => 'Pademawu',
            ),

            array (
                'code' => '35.28.03',
                'name' => 'Galis',
            ),

            array (
                'code' => '35.28.04',
                'name' => 'Pamekasan',
            ),

            array (
                'code' => '35.28.05',
                'name' => 'Proppo',
            ),

            array (
                'code' => '35.28.06',
                'name' => 'Palenggaan',
            ),

            array (
                'code' => '35.28.07',
                'name' => 'Pegantenan',
            ),

            array (
                'code' => '35.28.08',
                'name' => 'Larangan',
            ),

            array (
                'code' => '35.28.09',
                'name' => 'Pakong',
            ),

            array (
                'code' => '35.28.10',
                'name' => 'Waru',
            ),

            array (
                'code' => '35.28.11',
                'name' => 'Batumarmar',
            ),

            array (
                'code' => '35.28.12',
                'name' => 'Kadur',
            ),

            array (
                'code' => '35.28.13',
                'name' => 'Pasean',
            ),

            array (
                'code' => '35.29',
                'name' => 'KAB. SUMENEP',
            ),

            array (
                'code' => '35.29.01',
                'name' => 'Kota Sumenep',
            ),

            array (
                'code' => '35.29.02',
                'name' => 'Kalianget',
            ),

            array (
                'code' => '35.29.03',
                'name' => 'Manding',
            ),

            array (
                'code' => '35.29.04',
                'name' => 'Talango',
            ),

            array (
                'code' => '35.29.05',
                'name' => 'Bluto',
            ),

            array (
                'code' => '35.29.06',
                'name' => 'Saronggi',
            ),

            array (
                'code' => '35.29.07',
                'name' => 'Lenteng',
            ),

            array (
                'code' => '35.29.08',
                'name' => 'Gili Ginting',
            ),

            array (
                'code' => '35.29.09',
                'name' => 'Guluk-Guluk',
            ),

            array (
                'code' => '35.29.10',
                'name' => 'Ganding',
            ),

            array (
                'code' => '35.29.11',
                'name' => 'Pragaan',
            ),

            array (
                'code' => '35.29.12',
                'name' => 'Ambunten',
            ),

            array (
                'code' => '35.29.13',
                'name' => 'Pasongsongan',
            ),

            array (
                'code' => '35.29.14',
                'name' => 'Dasuk',
            ),

            array (
                'code' => '35.29.15',
                'name' => 'Rubaru',
            ),

            array (
                'code' => '35.29.16',
                'name' => 'Batang Batang',
            ),

            array (
                'code' => '35.29.17',
                'name' => 'Batu Putih',
            ),

            array (
                'code' => '35.29.18',
                'name' => 'Dungkek',
            ),

            array (
                'code' => '35.29.19',
                'name' => 'Gapura',
            ),

            array (
                'code' => '35.29.20',
                'name' => 'Gayam',
            ),

            array (
                'code' => '35.29.21',
                'name' => 'Nonggunong',
            ),

            array (
                'code' => '35.29.22',
                'name' => 'Raas',
            ),

            array (
                'code' => '35.29.23',
                'name' => 'Masalembu',
            ),

            array (
                'code' => '35.29.24',
                'name' => 'Arjasa',
            ),

            array (
                'code' => '35.29.25',
                'name' => 'Sapeken',
            ),

            array (
                'code' => '35.29.26',
                'name' => 'Batuan',
            ),

            array (
                'code' => '35.29.27',
                'name' => 'Kangayan',
            ),

            array (
                'code' => '35.71',
                'name' => 'KOTA KEDIRI',
            ),

            array (
                'code' => '35.71.01',
                'name' => 'Mojoroto',
            ),

            array (
                'code' => '35.71.02',
                'name' => 'Kota',
            ),

            array (
                'code' => '35.71.03',
                'name' => 'Pesantren',
            ),

            array (
                'code' => '35.72',
                'name' => 'KOTA BLITAR',
            ),

            array (
                'code' => '35.72.01',
                'name' => 'Kepanjenkidul',
            ),

            array (
                'code' => '35.72.02',
                'name' => 'Sukorejo',
            ),

            array (
                'code' => '35.72.03',
                'name' => 'Sananwetan',
            ),

            array (
                'code' => '35.73',
                'name' => 'KOTA MALANG',
            ),

            array (
                'code' => '35.73.01',
                'name' => 'Blimbing',
            ),

            array (
                'code' => '35.73.02',
                'name' => 'Klojen',
            ),

            array (
                'code' => '35.73.03',
                'name' => 'Kedungkandang',
            ),

            array (
                'code' => '35.73.04',
                'name' => 'Sukun',
            ),

            array (
                'code' => '35.73.05',
                'name' => 'Lowokwaru',
            ),

            array (
                'code' => '35.74',
                'name' => 'KOTA PROBOLINGGO',
            ),

            array (
                'code' => '35.74.01',
                'name' => 'Kademangan',
            ),

            array (
                'code' => '35.74.02',
                'name' => 'Wonoasih',
            ),

            array (
                'code' => '35.74.03',
                'name' => 'Mayangan',
            ),

            array (
                'code' => '35.74.04',
                'name' => 'Kanigaran',
            ),

            array (
                'code' => '35.74.05',
                'name' => 'Kedopak',
            ),

            array (
                'code' => '35.75',
                'name' => 'KOTA PASURUAN',
            ),

            array (
                'code' => '35.75.01',
                'name' => 'Gadingrejo',
            ),

            array (
                'code' => '35.75.02',
                'name' => 'Purworejo',
            ),

            array (
                'code' => '35.75.03',
                'name' => 'Bugul Kidul',
            ),

            array (
                'code' => '35.75.04',
                'name' => 'Panggungrejo',
            ),

            array (
                'code' => '35.76',
                'name' => 'KOTA MOJOKERTO',
            ),

            array (
                'code' => '35.76.01',
                'name' => 'Prajurit Kulon',
            ),

            array (
                'code' => '35.76.02',
                'name' => 'Magersari',
            ),

            array (
                'code' => '35.77',
                'name' => 'KOTA MADIUN',
            ),

            array (
                'code' => '35.77.01',
                'name' => 'Kartoharjo',
            ),

            array (
                'code' => '35.77.02',
                'name' => 'Manguharjo',
            ),

            array (
                'code' => '35.77.03',
                'name' => 'Taman',
            ),

            array (
                'code' => '35.78',
                'name' => 'KOTA SURABAYA',
            ),

            array (
                'code' => '35.78.01',
                'name' => 'Karangpilang',
            ),

            array (
                'code' => '35.78.02',
                'name' => 'Wonocolo',
            ),

            array (
                'code' => '35.78.03',
                'name' => 'Rungkut',
            ),

            array (
                'code' => '35.78.04',
                'name' => 'Wonokromo',
            ),

            array (
                'code' => '35.78.05',
                'name' => 'Tegalsari',
            ),

            array (
                'code' => '35.78.06',
                'name' => 'Sawahan',
            ),

            array (
                'code' => '35.78.07',
                'name' => 'Genteng',
            ),

            array (
                'code' => '35.78.08',
                'name' => 'Gubeng',
            ),

            array (
                'code' => '35.78.09',
                'name' => 'Sukolilo',
            ),

            array (
                'code' => '35.78.10',
                'name' => 'Tambaksari',
            ),

            array (
                'code' => '35.78.11',
                'name' => 'Simokerto',
            ),

            array (
                'code' => '35.78.12',
                'name' => 'Pabean Cantikan',
            ),

            array (
                'code' => '35.78.13',
                'name' => 'Bubutan',
            ),

            array (
                'code' => '35.78.14',
                'name' => 'Tandes',
            ),

            array (
                'code' => '35.78.15',
                'name' => 'Krembangan',
            ),

            array (
                'code' => '35.78.16',
                'name' => 'Semampir',
            ),

            array (
                'code' => '35.78.17',
                'name' => 'Kenjeran',
            ),

            array (
                'code' => '35.78.18',
                'name' => 'Lakarsantri',
            ),

            array (
                'code' => '35.78.19',
                'name' => 'Benowo',
            ),

            array (
                'code' => '35.78.20',
                'name' => 'Wiyung',
            ),

            array (
                'code' => '35.78.21',
                'name' => 'Dukuhpakis',
            ),

            array (
                'code' => '35.78.22',
                'name' => 'Gayungan',
            ),

            array (
                'code' => '35.78.23',
                'name' => 'Jambangan',
            ),

            array (
                'code' => '35.78.24',
                'name' => 'Tenggilis Mejoyo',
            ),

            array (
                'code' => '35.78.25',
                'name' => 'Gunung Anyar',
            ),

            array (
                'code' => '35.78.26',
                'name' => 'Mulyorejo',
            ),

            array (
                'code' => '35.78.27',
                'name' => 'Sukomanunggal',
            ),

            array (
                'code' => '35.78.28',
                'name' => 'Asem Rowo',
            ),

            array (
                'code' => '35.78.29',
                'name' => 'Bulak',
            ),

            array (
                'code' => '35.78.30',
                'name' => 'Pakal',
            ),

            array (
                'code' => '35.78.31',
                'name' => 'Sambikerep',
            ),

            array (
                'code' => '35.79',
                'name' => 'KOTA BATU',
            ),

            array (
                'code' => '35.79.01',
                'name' => 'Batu',
            ),

            array (
                'code' => '35.79.02',
                'name' => 'Bumiaji',
            ),

            array (
                'code' => '35.79.03',
                'name' => 'Junrejo',
            ),

            array (
                'code' => '36',
                'name' => 'BANTEN',
            ),

            array (
                'code' => '36.01',
                'name' => 'KAB. PANDEGLANG',
            ),

            array (
                'code' => '36.01.01',
                'name' => 'Sumur',
            ),

            array (
                'code' => '36.01.02',
                'name' => 'Cimanggu',
            ),

            array (
                'code' => '36.01.03',
                'name' => 'Cibaliung',
            ),

            array (
                'code' => '36.01.04',
                'name' => 'Cikeusik',
            ),

            array (
                'code' => '36.01.05',
                'name' => 'Cigeulis',
            ),

            array (
                'code' => '36.01.06',
                'name' => 'Panimbang',
            ),

            array (
                'code' => '36.01.07',
                'name' => 'Angsana',
            ),

            array (
                'code' => '36.01.08',
                'name' => 'Munjul',
            ),

            array (
                'code' => '36.01.09',
                'name' => 'Pagelaran',
            ),

            array (
                'code' => '36.01.10',
                'name' => 'Bojong',
            ),

            array (
                'code' => '36.01.11',
                'name' => 'Picung',
            ),

            array (
                'code' => '36.01.12',
                'name' => 'Labuan',
            ),

            array (
                'code' => '36.01.13',
                'name' => 'Menes',
            ),

            array (
                'code' => '36.01.14',
                'name' => 'Saketi',
            ),

            array (
                'code' => '36.01.15',
                'name' => 'Cipeucang',
            ),

            array (
                'code' => '36.01.16',
                'name' => 'Jiput',
            ),

            array (
                'code' => '36.01.17',
                'name' => 'Mandalawangi',
            ),

            array (
                'code' => '36.01.18',
                'name' => 'Cimanuk',
            ),

            array (
                'code' => '36.01.19',
                'name' => 'Kaduhejo',
            ),

            array (
                'code' => '36.01.20',
                'name' => 'Banjar',
            ),

            array (
                'code' => '36.01.21',
                'name' => 'Pandeglang',
            ),

            array (
                'code' => '36.01.22',
                'name' => 'Cadasari',
            ),

            array (
                'code' => '36.01.23',
                'name' => 'Cisata',
            ),

            array (
                'code' => '36.01.24',
                'name' => 'Patia',
            ),

            array (
                'code' => '36.01.25',
                'name' => 'Karang Tanjung',
            ),

            array (
                'code' => '36.01.26',
                'name' => 'Cikedal',
            ),

            array (
                'code' => '36.01.27',
                'name' => 'Cibitung',
            ),

            array (
                'code' => '36.01.28',
                'name' => 'Carita',
            ),

            array (
                'code' => '36.01.29',
                'name' => 'Sukaresmi',
            ),

            array (
                'code' => '36.01.30',
                'name' => 'Mekarjaya',
            ),

            array (
                'code' => '36.01.31',
                'name' => 'Sindangresmi',
            ),

            array (
                'code' => '36.01.32',
                'name' => 'Pulosari',
            ),

            array (
                'code' => '36.01.33',
                'name' => 'Koroncong',
            ),

            array (
                'code' => '36.01.34',
                'name' => 'Majasari',
            ),

            array (
                'code' => '36.01.35',
                'name' => 'Sobang',
            ),

            array (
                'code' => '36.02',
                'name' => 'KAB. LEBAK',
            ),

            array (
                'code' => '36.02.01',
                'name' => 'Malingping',
            ),

            array (
                'code' => '36.02.02',
                'name' => 'Panggarangan',
            ),

            array (
                'code' => '36.02.03',
                'name' => 'Bayah',
            ),

            array (
                'code' => '36.02.04',
                'name' => 'Cipanas',
            ),

            array (
                'code' => '36.02.05',
                'name' => 'Muncang',
            ),

            array (
                'code' => '36.02.06',
                'name' => 'Leuwidamar',
            ),

            array (
                'code' => '36.02.07',
                'name' => 'Bojongmanik',
            ),

            array (
                'code' => '36.02.08',
                'name' => 'Gunungkencana',
            ),

            array (
                'code' => '36.02.09',
                'name' => 'Banjarsari',
            ),

            array (
                'code' => '36.02.10',
                'name' => 'Cileles',
            ),

            array (
                'code' => '36.02.11',
                'name' => 'Cimarga',
            ),

            array (
                'code' => '36.02.12',
                'name' => 'Sajira',
            ),

            array (
                'code' => '36.02.13',
                'name' => 'Maja',
            ),

            array (
                'code' => '36.02.14',
                'name' => 'Rangkasbitung',
            ),

            array (
                'code' => '36.02.15',
                'name' => 'Warunggunung',
            ),

            array (
                'code' => '36.02.16',
                'name' => 'Cijaku',
            ),

            array (
                'code' => '36.02.17',
                'name' => 'Cikulur',
            ),

            array (
                'code' => '36.02.18',
                'name' => 'Cibadak',
            ),

            array (
                'code' => '36.02.19',
                'name' => 'Cibeber',
            ),

            array (
                'code' => '36.02.20',
                'name' => 'Cilograng',
            ),

            array (
                'code' => '36.02.21',
                'name' => 'Wanasalam',
            ),

            array (
                'code' => '36.02.22',
                'name' => 'Sobang',
            ),

            array (
                'code' => '36.02.23',
                'name' => 'Curug bitung',
            ),

            array (
                'code' => '36.02.24',
                'name' => 'Kalanganyar',
            ),

            array (
                'code' => '36.02.25',
                'name' => 'Lebakgedong',
            ),

            array (
                'code' => '36.02.26',
                'name' => 'Cihara',
            ),

            array (
                'code' => '36.02.27',
                'name' => 'Cirinten',
            ),

            array (
                'code' => '36.02.28',
                'name' => 'Cigemlong',
            ),

            array (
                'code' => '36.03',
                'name' => 'KAB. TANGERANG',
            ),

            array (
                'code' => '36.03.01',
                'name' => 'Balaraja',
            ),

            array (
                'code' => '36.03.02',
                'name' => 'Jayanti',
            ),

            array (
                'code' => '36.03.03',
                'name' => 'Tigaraksa',
            ),

            array (
                'code' => '36.03.04',
                'name' => 'Jambe',
            ),

            array (
                'code' => '36.03.05',
                'name' => 'Cisoka',
            ),

            array (
                'code' => '36.03.06',
                'name' => 'Kresek',
            ),

            array (
                'code' => '36.03.07',
                'name' => 'Kronjo',
            ),

            array (
                'code' => '36.03.08',
                'name' => 'Mauk',
            ),

            array (
                'code' => '36.03.09',
                'name' => 'Kemiri',
            ),

            array (
                'code' => '36.03.10',
                'name' => 'Sukadiri',
            ),

            array (
                'code' => '36.03.11',
                'name' => 'Rajeg',
            ),

            array (
                'code' => '36.03.12',
                'name' => 'Pasar Kemis',
            ),

            array (
                'code' => '36.03.13',
                'name' => 'Teluknaga',
            ),

            array (
                'code' => '36.03.14',
                'name' => 'Kosambi',
            ),

            array (
                'code' => '36.03.15',
                'name' => 'Pakuhaji',
            ),

            array (
                'code' => '36.03.16',
                'name' => 'Sepatan',
            ),

            array (
                'code' => '36.03.17',
                'name' => 'Curug',
            ),

            array (
                'code' => '36.03.18',
                'name' => 'Cikupa',
            ),

            array (
                'code' => '36.03.19',
                'name' => 'Panongan',
            ),

            array (
                'code' => '36.03.20',
                'name' => 'Legok',
            ),

            array (
                'code' => '36.03.22',
                'name' => 'Pagedangan',
            ),

            array (
                'code' => '36.03.23',
                'name' => 'Cisauk',
            ),

            array (
                'code' => '36.03.27',
                'name' => 'Sukamulya',
            ),

            array (
                'code' => '36.03.28',
                'name' => 'Kelapa Dua',
            ),

            array (
                'code' => '36.03.29',
                'name' => 'Sindang Jaya',
            ),

            array (
                'code' => '36.03.30',
                'name' => 'Sepatan Timur',
            ),

            array (
                'code' => '36.03.31',
                'name' => 'Solear',
            ),

            array (
                'code' => '36.03.32',
                'name' => 'Gunung Kaler',
            ),

            array (
                'code' => '36.03.33',
                'name' => 'Mekar Baru',
            ),

            array (
                'code' => '36.04',
                'name' => 'KAB. SERANG',
            ),

            array (
                'code' => '36.04.05',
                'name' => 'Kramatwatu',
            ),

            array (
                'code' => '36.04.06',
                'name' => 'Waringinkurung',
            ),

            array (
                'code' => '36.04.07',
                'name' => 'Bojonegara',
            ),

            array (
                'code' => '36.04.08',
                'name' => 'Pulo Ampel',
            ),

            array (
                'code' => '36.04.09',
                'name' => 'Ciruas',
            ),

            array (
                'code' => '36.04.11',
                'name' => 'Kragilan',
            ),

            array (
                'code' => '36.04.12',
                'name' => 'Pontang',
            ),

            array (
                'code' => '36.04.13',
                'name' => 'Tirtayasa',
            ),

            array (
                'code' => '36.04.14',
                'name' => 'Tanara',
            ),

            array (
                'code' => '36.04.15',
                'name' => 'Cikande',
            ),

            array (
                'code' => '36.04.16',
                'name' => 'Kibin',
            ),

            array (
                'code' => '36.04.17',
                'name' => 'Carenang',
            ),

            array (
                'code' => '36.04.18',
                'name' => 'Binuang',
            ),

            array (
                'code' => '36.04.19',
                'name' => 'Petir',
            ),

            array (
                'code' => '36.04.20',
                'name' => 'Tunjung Teja',
            ),

            array (
                'code' => '36.04.22',
                'name' => 'Baros',
            ),

            array (
                'code' => '36.04.23',
                'name' => 'Cikeusal',
            ),

            array (
                'code' => '36.04.24',
                'name' => 'Pamarayan',
            ),

            array (
                'code' => '36.04.25',
                'name' => 'Kopo',
            ),

            array (
                'code' => '36.04.26',
                'name' => 'Jawilan',
            ),

            array (
                'code' => '36.04.27',
                'name' => 'Ciomas',
            ),

            array (
                'code' => '36.04.28',
                'name' => 'Pabuaran',
            ),

            array (
                'code' => '36.04.29',
                'name' => 'Padarincang',
            ),

            array (
                'code' => '36.04.30',
                'name' => 'Anyar',
            ),

            array (
                'code' => '36.04.31',
                'name' => 'Cinangka',
            ),

            array (
                'code' => '36.04.32',
                'name' => 'Mancak',
            ),

            array (
                'code' => '36.04.33',
                'name' => 'Gunung Sari',
            ),

            array (
                'code' => '36.04.34',
                'name' => 'Bandung',
            ),

            array (
                'code' => '36.04.35',
                'name' => 'Lebak Wangi',
            ),

            array (
                'code' => '36.71',
                'name' => 'KOTA TANGERANG',
            ),

            array (
                'code' => '36.71.01',
                'name' => 'Tangerang',
            ),

            array (
                'code' => '36.71.02',
                'name' => 'Jatiuwung',
            ),

            array (
                'code' => '36.71.03',
                'name' => 'Batuceper',
            ),

            array (
                'code' => '36.71.04',
                'name' => 'Benda',
            ),

            array (
                'code' => '36.71.05',
                'name' => 'Cipondoh',
            ),

            array (
                'code' => '36.71.06',
                'name' => 'Ciledug',
            ),

            array (
                'code' => '36.71.07',
                'name' => 'Karawaci',
            ),

            array (
                'code' => '36.71.08',
                'name' => 'Periuk',
            ),

            array (
                'code' => '36.71.09',
                'name' => 'Cibodas',
            ),

            array (
                'code' => '36.71.10',
                'name' => 'Neglasari',
            ),

            array (
                'code' => '36.71.11',
                'name' => 'Pinang',
            ),

            array (
                'code' => '36.71.12',
                'name' => 'Karang Tengah',
            ),

            array (
                'code' => '36.71.13',
                'name' => 'Larangan',
            ),

            array (
                'code' => '36.72',
                'name' => 'KOTA CILEGON',
            ),

            array (
                'code' => '36.72.01',
                'name' => 'Cibeber',
            ),

            array (
                'code' => '36.72.02',
                'name' => 'Cilegon',
            ),

            array (
                'code' => '36.72.03',
                'name' => 'Pulomerak',
            ),

            array (
                'code' => '36.72.04',
                'name' => 'Ciwandan',
            ),

            array (
                'code' => '36.72.05',
                'name' => 'Jombang',
            ),

            array (
                'code' => '36.72.06',
                'name' => 'Gerogol',
            ),

            array (
                'code' => '36.72.07',
                'name' => 'Purwakarta',
            ),

            array (
                'code' => '36.72.08',
                'name' => 'Citangkil',
            ),

            array (
                'code' => '36.73',
                'name' => 'KOTA SERANG',
            ),

            array (
                'code' => '36.73.01',
                'name' => 'Serang',
            ),

            array (
                'code' => '36.73.02',
                'name' => 'Kasemen',
            ),

            array (
                'code' => '36.73.03',
                'name' => 'Walantaka',
            ),

            array (
                'code' => '36.73.04',
                'name' => 'Curug',
            ),

            array (
                'code' => '36.73.05',
                'name' => 'Cipocok Jaya',
            ),

            array (
                'code' => '36.73.06',
                'name' => 'Taktakan',
            ),

            array (
                'code' => '36.74',
                'name' => 'KOTA TANGERANG SELATAN',
            ),

            array (
                'code' => '36.74.01',
                'name' => 'Serpong',
            ),

            array (
                'code' => '36.74.02',
                'name' => 'Serpong Utara',
            ),

            array (
                'code' => '36.74.03',
                'name' => 'Pondok Aren',
            ),

            array (
                'code' => '36.74.04',
                'name' => 'Ciputat',
            ),

            array (
                'code' => '36.74.05',
                'name' => 'Ciputat Timur',
            ),

            array (
                'code' => '36.74.06',
                'name' => 'Pamulang',
            ),

            array (
                'code' => '36.74.07',
                'name' => 'Setu',
            ),

            array (
                'code' => '51',
                'name' => 'BALI',
            ),

            array (
                'code' => '51.01',
                'name' => 'KAB. JEMBRANA',
            ),

            array (
                'code' => '51.01.01',
                'name' => 'Negara',
            ),

            array (
                'code' => '51.01.02',
                'name' => 'Mendoyo',
            ),

            array (
                'code' => '51.01.03',
                'name' => 'Pekutatan',
            ),

            array (
                'code' => '51.01.04',
                'name' => 'Melaya',
            ),

            array (
                'code' => '51.01.05',
                'name' => 'Jembrana',
            ),

            array (
                'code' => '51.02',
                'name' => 'KAB. TABANAN',
            ),

            array (
                'code' => '51.02.01',
                'name' => 'Selemadeg',
            ),

            array (
                'code' => '51.02.02',
                'name' => 'Salamadeg Timur',
            ),

            array (
                'code' => '51.02.03',
                'name' => 'Salemadeg Barat',
            ),

            array (
                'code' => '51.02.04',
                'name' => 'Kerambitan',
            ),

            array (
                'code' => '51.02.05',
                'name' => 'Tabanan',
            ),

            array (
                'code' => '51.02.06',
                'name' => 'Kediri',
            ),

            array (
                'code' => '51.02.07',
                'name' => 'Marga',
            ),

            array (
                'code' => '51.02.08',
                'name' => 'Penebel',
            ),

            array (
                'code' => '51.02.09',
                'name' => 'Baturiti',
            ),

            array (
                'code' => '51.02.10',
                'name' => 'Pupuan',
            ),

            array (
                'code' => '51.03',
                'name' => 'KAB. BADUNG',
            ),

            array (
                'code' => '51.03.01',
                'name' => 'Kuta',
            ),

            array (
                'code' => '51.03.02',
                'name' => 'Mengwi',
            ),

            array (
                'code' => '51.03.03',
                'name' => 'Abiansemal',
            ),

            array (
                'code' => '51.03.04',
                'name' => 'Petang',
            ),

            array (
                'code' => '51.03.05',
                'name' => 'Kuta Selatan',
            ),

            array (
                'code' => '51.03.06',
                'name' => 'Kuta Utara',
            ),

            array (
                'code' => '51.04',
                'name' => 'KAB. GIANYAR',
            ),

            array (
                'code' => '51.04.01',
                'name' => 'Sukawati',
            ),

            array (
                'code' => '51.04.02',
                'name' => 'Blahbatuh',
            ),

            array (
                'code' => '51.04.03',
                'name' => 'Gianyar',
            ),

            array (
                'code' => '51.04.04',
                'name' => 'Tampaksiring',
            ),

            array (
                'code' => '51.04.05',
                'name' => 'Ubud',
            ),

            array (
                'code' => '51.04.06',
                'name' => 'Tegalallang',
            ),

            array (
                'code' => '51.04.07',
                'name' => 'Payangan',
            ),

            array (
                'code' => '51.05',
                'name' => 'KAB. KLUNGKUNG',
            ),

            array (
                'code' => '51.05.01',
                'name' => 'Nusa Penida',
            ),

            array (
                'code' => '51.05.02',
                'name' => 'Banjarangkan',
            ),

            array (
                'code' => '51.05.03',
                'name' => 'Klungkung',
            ),

            array (
                'code' => '51.05.04',
                'name' => 'Dawan',
            ),

            array (
                'code' => '51.06',
                'name' => 'KAB. BANGLI',
            ),

            array (
                'code' => '51.06.01',
                'name' => 'Susut',
            ),

            array (
                'code' => '51.06.02',
                'name' => 'Bangli',
            ),

            array (
                'code' => '51.06.03',
                'name' => 'Tembuku',
            ),

            array (
                'code' => '51.06.04',
                'name' => 'Kintamani',
            ),

            array (
                'code' => '51.07',
                'name' => 'KAB. KARANGASEM',
            ),

            array (
                'code' => '51.07.01',
                'name' => 'Rendang',
            ),

            array (
                'code' => '51.07.02',
                'name' => 'Sidemen',
            ),

            array (
                'code' => '51.07.03',
                'name' => 'Manggis',
            ),

            array (
                'code' => '51.07.04',
                'name' => 'Karangasem',
            ),

            array (
                'code' => '51.07.05',
                'name' => 'Abang',
            ),

            array (
                'code' => '51.07.06',
                'name' => 'Bebandem',
            ),

            array (
                'code' => '51.07.07',
                'name' => 'Selat',
            ),

            array (
                'code' => '51.07.08',
                'name' => 'Kubu',
            ),

            array (
                'code' => '51.08',
                'name' => 'KAB. BULELENG',
            ),

            array (
                'code' => '51.08.01',
                'name' => 'Gerokgak',
            ),

            array (
                'code' => '51.08.02',
                'name' => 'Seririt',
            ),

            array (
                'code' => '51.08.03',
                'name' => 'Busung biu',
            ),

            array (
                'code' => '51.08.04',
                'name' => 'Banjar',
            ),

            array (
                'code' => '51.08.05',
                'name' => 'Sukasada',
            ),

            array (
                'code' => '51.08.06',
                'name' => 'Buleleng',
            ),

            array (
                'code' => '51.08.07',
                'name' => 'Sawan',
            ),

            array (
                'code' => '51.08.08',
                'name' => 'Kubutambahan',
            ),

            array (
                'code' => '51.08.09',
                'name' => 'Tejakula',
            ),

            array (
                'code' => '51.71',
                'name' => 'KOTA DENPASAR',
            ),

            array (
                'code' => '51.71.01',
                'name' => 'Denpasar Selatan',
            ),

            array (
                'code' => '51.71.02',
                'name' => 'Denpasar Timur',
            ),

            array (
                'code' => '51.71.03',
                'name' => 'Denpasar Barat',
            ),

            array (
                'code' => '51.71.04',
                'name' => 'Denpasar Utara',
            ),

            array (
                'code' => '52',
                'name' => 'NUSA TENGGARA BARAT',
            ),

            array (
                'code' => '52.01',
                'name' => 'KAB. LOMBOK BARAT',
            ),

            array (
                'code' => '52.01.01',
                'name' => 'Gerung',
            ),

            array (
                'code' => '52.01.02',
                'name' => 'Kediri',
            ),

            array (
                'code' => '52.01.03',
                'name' => 'Narmada',
            ),

            array (
                'code' => '52.01.07',
                'name' => 'Sekotong',
            ),

            array (
                'code' => '52.01.08',
                'name' => 'Labuapi',
            ),

            array (
                'code' => '52.01.09',
                'name' => 'Gunungsari',
            ),

            array (
                'code' => '52.01.12',
                'name' => 'Lingsar',
            ),

            array (
                'code' => '52.01.13',
                'name' => 'Lembar',
            ),

            array (
                'code' => '52.01.14',
                'name' => 'Batu Layar',
            ),

            array (
                'code' => '52.01.15',
                'name' => 'Kuripan',
            ),

            array (
                'code' => '52.02',
                'name' => 'KAB. LOMBOK TENGAH',
            ),

            array (
                'code' => '52.02.01',
                'name' => 'Praya',
            ),

            array (
                'code' => '52.02.02',
                'name' => 'Jonggat',
            ),

            array (
                'code' => '52.02.03',
                'name' => 'Batukliang',
            ),

            array (
                'code' => '52.02.04',
                'name' => 'Pujut',
            ),

            array (
                'code' => '52.02.05',
                'name' => 'Praya Barat',
            ),

            array (
                'code' => '52.02.06',
                'name' => 'Praya Timur',
            ),

            array (
                'code' => '52.02.07',
                'name' => 'Janapria',
            ),

            array (
                'code' => '52.02.08',
                'name' => 'Pringgarata',
            ),

            array (
                'code' => '52.02.09',
                'name' => 'Kopang',
            ),

            array (
                'code' => '52.02.10',
                'name' => 'Praya Tengah',
            ),

            array (
                'code' => '52.02.11',
                'name' => 'Praya Barat Daya',
            ),

            array (
                'code' => '52.02.12',
                'name' => 'Batukliang Utara',
            ),

            array (
                'code' => '52.03',
                'name' => 'KAB. LOMBOK TIMUR',
            ),

            array (
                'code' => '52.03.01',
                'name' => 'Keruak',
            ),

            array (
                'code' => '52.03.02',
                'name' => 'Sakra',
            ),

            array (
                'code' => '52.03.03',
                'name' => 'Terara',
            ),

            array (
                'code' => '52.03.04',
                'name' => 'Sikur',
            ),

            array (
                'code' => '52.03.05',
                'name' => 'Masbagik',
            ),

            array (
                'code' => '52.03.06',
                'name' => 'Sukamulia',
            ),

            array (
                'code' => '52.03.07',
                'name' => 'Selong',
            ),

            array (
                'code' => '52.03.08',
                'name' => 'Pringgabaya',
            ),

            array (
                'code' => '52.03.09',
                'name' => 'Aikmel',
            ),

            array (
                'code' => '52.03.10',
                'name' => 'Sambelia',
            ),

            array (
                'code' => '52.03.11',
                'name' => 'Montong Gading',
            ),

            array (
                'code' => '52.03.12',
                'name' => 'Pringgasela',
            ),

            array (
                'code' => '52.03.13',
                'name' => 'Suralaga',
            ),

            array (
                'code' => '52.03.14',
                'name' => 'Wanasaba',
            ),

            array (
                'code' => '52.03.15',
                'name' => 'Sembalun',
            ),

            array (
                'code' => '52.03.16',
                'name' => 'Suwela',
            ),

            array (
                'code' => '52.03.17',
                'name' => 'Labuhan Haji',
            ),

            array (
                'code' => '52.03.18',
                'name' => 'Sakra Timur',
            ),

            array (
                'code' => '52.03.19',
                'name' => 'Sakra Barat',
            ),

            array (
                'code' => '52.03.20',
                'name' => 'Jerowaru',
            ),

            array (
                'code' => '52.04',
                'name' => 'KAB. SUMBAWA',
            ),

            array (
                'code' => '52.04.02',
                'name' => 'Lunyuk',
            ),

            array (
                'code' => '52.04.05',
                'name' => 'Alas',
            ),

            array (
                'code' => '52.04.06',
                'name' => 'Utan',
            ),

            array (
                'code' => '52.04.07',
                'name' => 'Batu Lanteh',
            ),

            array (
                'code' => '52.04.08',
                'name' => 'Sumbawa',
            ),

            array (
                'code' => '52.04.09',
                'name' => 'Moyo Hilir',
            ),

            array (
                'code' => '52.04.10',
                'name' => 'Moyo Hulu',
            ),

            array (
                'code' => '52.04.11',
                'name' => 'Ropang',
            ),

            array (
                'code' => '52.04.12',
                'name' => 'Lape',
            ),

            array (
                'code' => '52.04.13',
                'name' => 'Plampang',
            ),

            array (
                'code' => '52.04.14',
                'name' => 'Empang',
            ),

            array (
                'code' => '52.04.17',
                'name' => 'Alas Barat',
            ),

            array (
                'code' => '52.04.18',
                'name' => 'Labuhan Badas',
            ),

            array (
                'code' => '52.04.19',
                'name' => 'Labangka',
            ),

            array (
                'code' => '52.04.20',
                'name' => 'Buer',
            ),

            array (
                'code' => '52.04.21',
                'name' => 'Rhee',
            ),

            array (
                'code' => '52.04.22',
                'name' => 'Unter Iwes',
            ),

            array (
                'code' => '52.04.23',
                'name' => 'Moyo Utara',
            ),

            array (
                'code' => '52.04.24',
                'name' => 'Maronge',
            ),

            array (
                'code' => '52.04.25',
                'name' => 'Tarano',
            ),

            array (
                'code' => '52.04.26',
                'name' => 'Lopok',
            ),

            array (
                'code' => '52.04.27',
                'name' => 'Lenangguar',
            ),

            array (
                'code' => '52.04.28',
                'name' => 'Orong Telu',
            ),

            array (
                'code' => '52.04.29',
                'name' => 'Lantung',
            ),

            array (
                'code' => '52.05',
                'name' => 'KAB. DOMPU',
            ),

            array (
                'code' => '52.05.01',
                'name' => 'Dompu',
            ),

            array (
                'code' => '52.05.02',
                'name' => 'Kempo',
            ),

            array (
                'code' => '52.05.03',
                'name' => 'Hu\'u  8',
            ),

            array (
                'code' => '52.05.04',
                'name' => 'Kilo',
            ),

            array (
                'code' => '52.05.05',
                'name' => 'Woja',
            ),

            array (
                'code' => '52.05.06',
                'name' => 'Pekat',
            ),

            array (
                'code' => '52.05.07',
                'name' => 'Manggalewa',
            ),

            array (
                'code' => '52.05.08',
                'name' => 'Pajo',
            ),

            array (
                'code' => '52.06',
                'name' => 'KAB. BIMA',
            ),

            array (
                'code' => '52.06.01',
                'name' => 'Monta',
            ),

            array (
                'code' => '52.06.02',
                'name' => 'Bolo',
            ),

            array (
                'code' => '52.06.03',
                'name' => 'Woha',
            ),

            array (
                'code' => '52.06.04',
                'name' => 'Belo',
            ),

            array (
                'code' => '52.06.05',
                'name' => 'Wawo',
            ),

            array (
                'code' => '52.06.06',
                'name' => 'Sape',
            ),

            array (
                'code' => '52.06.07',
                'name' => 'Wera',
            ),

            array (
                'code' => '52.06.08',
                'name' => 'Donggo',
            ),

            array (
                'code' => '52.06.09',
                'name' => 'Sanggar',
            ),

            array (
                'code' => '52.06.10',
                'name' => 'Ambalawi',
            ),

            array (
                'code' => '52.06.11',
                'name' => 'Langgudu',
            ),

            array (
                'code' => '52.06.12',
                'name' => 'Lambu',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '52.06.13',
                'name' => 'Madapangga',
            ),

            array (
                'code' => '52.06.14',
                'name' => 'Tambora',
            ),

            array (
                'code' => '52.06.15',
                'name' => 'Soromandi',
            ),

            array (
                'code' => '52.06.16',
                'name' => 'Parado',
            ),

            array (
                'code' => '52.06.17',
                'name' => 'Lambitu',
            ),

            array (
                'code' => '52.06.18',
                'name' => 'Palibelo',
            ),

            array (
                'code' => '52.07',
                'name' => 'KAB. SUMBAWA BARAT',
            ),

            array (
                'code' => '52.07.01',
                'name' => 'Jereweh',
            ),

            array (
                'code' => '52.07.02',
                'name' => 'Taliwang',
            ),

            array (
                'code' => '52.07.03',
                'name' => 'Seteluk',
            ),

            array (
                'code' => '52.07.04',
                'name' => 'Sekongkang',
            ),

            array (
                'code' => '52.07.05',
                'name' => 'Brang Rea',
            ),

            array (
                'code' => '52.07.06',
                'name' => 'Poto Tano',
            ),

            array (
                'code' => '52.07.07',
                'name' => 'Brang Ene',
            ),

            array (
                'code' => '52.07.08',
                'name' => 'Maluk',
            ),

            array (
                'code' => '52.08',
                'name' => 'KAB. LOMBOK UTARA',
            ),

            array (
                'code' => '52.08.01',
                'name' => 'Tanjung',
            ),

            array (
                'code' => '52.08.02',
                'name' => 'Gangga',
            ),

            array (
                'code' => '52.08.03',
                'name' => 'Kayangan',
            ),

            array (
                'code' => '52.08.04',
                'name' => 'Bayan',
            ),

            array (
                'code' => '52.08.05',
                'name' => 'Pemenang',
            ),

            array (
                'code' => '52.71',
                'name' => 'KOTA MATARAM',
            ),

            array (
                'code' => '52.71.01',
                'name' => 'Ampenan',
            ),

            array (
                'code' => '52.71.02',
                'name' => 'Mataram',
            ),

            array (
                'code' => '52.71.03',
                'name' => 'Cakranegara',
            ),

            array (
                'code' => '52.71.04',
                'name' => 'Sekarbela',
            ),

            array (
                'code' => '52.71.05',
                'name' => 'Selaprang',
            ),

            array (
                'code' => '52.71.06',
                'name' => 'Sandubaya',
            ),

            array (
                'code' => '52.72',
                'name' => 'KOTA BIMA',
            ),

            array (
                'code' => '52.72.01',
                'name' => 'RasanaE Barat',
            ),

            array (
                'code' => '52.72.02',
                'name' => 'RasanaE Timur',
            ),

            array (
                'code' => '52.72.03',
                'name' => 'Asakota',
            ),

            array (
                'code' => '52.72.04',
                'name' => 'Raba',
            ),

            array (
                'code' => '52.72.05',
                'name' => 'Mpunda',
            ),

            array (
                'code' => '53',
                'name' => 'NUSA TENGGARA TIMUR',
            ),

            array (
                'code' => '53.01',
                'name' => 'KAB. KUPANG',
            ),

            array (
                'code' => '53.01.04',
                'name' => 'Semau',
            ),

            array (
                'code' => '53.01.05',
                'name' => 'Kupang Barat',
            ),

            array (
                'code' => '53.01.06',
                'name' => 'Kupang Timur',
            ),

            array (
                'code' => '53.01.07',
                'name' => 'Sulamu',
            ),

            array (
                'code' => '53.01.08',
                'name' => 'Kupang Tengah',
            ),

            array (
                'code' => '53.01.09',
                'name' => 'Amarasi',
            ),

            array (
                'code' => '53.01.10',
                'name' => 'Fatuleu',
            ),

            array (
                'code' => '53.01.11',
                'name' => 'Takari',
            ),

            array (
                'code' => '53.01.12',
                'name' => 'Amfoang Selatan',
            ),

            array (
                'code' => '53.01.13',
                'name' => 'Amfoang Utara',
            ),

            array (
                'code' => '53.01.16',
                'name' => 'Nekamese',
            ),

            array (
                'code' => '53.01.17',
                'name' => 'Amarasi Barat',
            ),

            array (
                'code' => '53.01.18',
                'name' => 'Amarasi Selatan',
            ),

            array (
                'code' => '53.01.19',
                'name' => 'Amarasi Timur',
            ),

            array (
                'code' => '53.01.20',
                'name' => 'Amabi Oefeto Timur',
            ),

            array (
                'code' => '53.01.21',
                'name' => 'Amfoang Barat Daya',
            ),

            array (
                'code' => '53.01.22',
                'name' => 'Amfoang Barat Laut',
            ),

            array (
                'code' => '53.01.23',
                'name' => 'Semau Selatan',
            ),

            array (
                'code' => '53.01.24',
                'name' => 'Taebenu',
            ),

            array (
                'code' => '53.01.25',
                'name' => 'Amabi Oefeto',
            ),

            array (
                'code' => '53.01.26',
                'name' => 'Amfoang Timur',
            ),

            array (
                'code' => '53.01.27',
                'name' => 'Fatuleu Barat',
            ),

            array (
                'code' => '53.01.28',
                'name' => 'Fatuleu Tengah',
            ),

            array (
                'code' => '53.01.30',
                'name' => 'Amfoang Tengah',
            ),

            array (
                'code' => '53.02',
                'name' => 'KAB TIMOR TENGAH SELATAN',
            ),

            array (
                'code' => '53.02.01',
                'name' => 'Kota Soe',
            ),

            array (
                'code' => '53.02.02',
                'name' => 'Mollo Selatan',
            ),

            array (
                'code' => '53.02.03',
                'name' => 'Mollo Utara',
            ),

            array (
                'code' => '53.02.04',
                'name' => 'Amanuban Timur',
            ),

            array (
                'code' => '53.02.05',
                'name' => 'Amanuban Tengah',
            ),

            array (
                'code' => '53.02.06',
                'name' => 'Amanuban Selatan',
            ),

            array (
                'code' => '53.02.07',
                'name' => 'Amanuban Barat',
            ),

            array (
                'code' => '53.02.08',
                'name' => 'Amanatun Selatan',
            ),

            array (
                'code' => '53.02.09',
                'name' => 'Amanatun Utara',
            ),

            array (
                'code' => '53.02.10',
                'name' => 'KI\'E  13',
            ),

            array (
                'code' => '53.02.11',
                'name' => 'Kuanfatu',
            ),

            array (
                'code' => '53.02.12',
                'name' => 'Fatumnasi',
            ),

            array (
                'code' => '53.02.13',
                'name' => 'Polen',
            ),

            array (
                'code' => '53.02.14',
                'name' => 'Batu Putih',
            ),

            array (
                'code' => '53.02.15',
                'name' => 'Boking',
            ),

            array (
                'code' => '53.02.16',
                'name' => 'Toianas',
            ),

            array (
                'code' => '53.02.17',
                'name' => 'Nunkolo',
            ),

            array (
                'code' => '53.02.18',
                'name' => 'Oenino',
            ),

            array (
                'code' => '53.02.19',
                'name' => 'Kolbano',
            ),

            array (
                'code' => '53.02.20',
                'name' => 'Kot olin',
            ),

            array (
                'code' => '53.02.21',
                'name' => 'Kualin',
            ),

            array (
                'code' => '53.02.22',
                'name' => 'Mollo Barat',
            ),

            array (
                'code' => '53.02.23',
                'name' => 'Kok Baun',
            ),

            array (
                'code' => '53.02.24',
                'name' => 'Noebana',
            ),

            array (
                'code' => '53.02.25',
                'name' => 'Santian',
            ),

            array (
                'code' => '53.02.26',
                'name' => 'Noebeba',
            ),

            array (
                'code' => '53.02.27',
                'name' => 'Kuatnana',
            ),

            array (
                'code' => '53.02.28',
                'name' => 'Fautmolo',
            ),

            array (
                'code' => '53.02.29',
                'name' => 'Fatukopa',
            ),

            array (
                'code' => '53.02.30',
                'name' => 'Mollo Tengah',
            ),

            array (
                'code' => '53.02.31',
                'name' => 'Tobu',
            ),

            array (
                'code' => '53.02.32',
                'name' => 'Nunbena',
            ),

            array (
                'code' => '53.03',
                'name' => 'KAB. TIMOR TENGAH UTARA',
            ),

            array (
                'code' => '53.03.01',
                'name' => 'Miomafo Timur',
            ),

            array (
                'code' => '53.03.02',
                'name' => 'Miomafo Barat',
            ),

            array (
                'code' => '53.03.03',
                'name' => 'Biboki Selatan',
            ),

            array (
                'code' => '53.03.04',
                'name' => 'Noemuti',
            ),

            array (
                'code' => '53.03.05',
                'name' => 'Kota Kefamenanu',
            ),

            array (
                'code' => '53.03.06',
                'name' => 'Biboki Utara',
            ),

            array (
                'code' => '53.03.07',
                'name' => 'Biboki Anleu',
            ),

            array (
                'code' => '53.03.08',
                'name' => 'Insana',
            ),

            array (
                'code' => '53.03.09',
                'name' => 'Insana Utara',
            ),

            array (
                'code' => '53.03.10',
                'name' => 'Noemuti Timur',
            ),

            array (
                'code' => '53.03.11',
                'name' => 'Miomaffo Tengah',
            ),

            array (
                'code' => '53.03.12',
                'name' => 'Musi',
            ),

            array (
                'code' => '53.03.13',
                'name' => 'Mutis',
            ),

            array (
                'code' => '53.03.14',
                'name' => 'Bikomi Selatan',
            ),

            array (
                'code' => '53.03.15',
                'name' => 'Bikomi Tengah',
            ),

            array (
                'code' => '53.03.16',
                'name' => 'Bikomi Nilulat',
            ),

            array (
                'code' => '53.03.17',
                'name' => 'Bikomi Utara',
            ),

            array (
                'code' => '53.03.18',
                'name' => 'Naibenu',
            ),

            array (
                'code' => '53.03.19',
                'name' => 'Insana Fafinesu',
            ),

            array (
                'code' => '53.03.20',
                'name' => 'Insana Barat',
            ),

            array (
                'code' => '53.03.21',
                'name' => 'Insana Tengah',
            ),

            array (
                'code' => '53.03.22',
                'name' => 'Biboki Tan Pah',
            ),

            array (
                'code' => '53.03.23',
                'name' => 'Biboki Moenleu',
            ),

            array (
                'code' => '53.03.24',
                'name' => 'Biboki Feotleu',
            ),

            array (
                'code' => '53.04',
                'name' => 'KAB. BELU',
            ),

            array (
                'code' => '53.04.01',
                'name' => 'Lamaknen',
            ),

            array (
                'code' => '53.04.02',
                'name' => 'TasifetoTimur',
            ),

            array (
                'code' => '53.04.03',
                'name' => 'Raihat',
            ),

            array (
                'code' => '53.04.04',
                'name' => 'Tasifeto Barat',
            ),

            array (
                'code' => '53.04.05',
                'name' => 'Kakuluk Mesak',
            ),

            array (
                'code' => '53.04.12',
                'name' => 'Kota Atambua',
            ),

            array (
                'code' => '53.04.13',
                'name' => 'Raimanuk',
            ),

            array (
                'code' => '53.04.17',
                'name' => 'Lasiolat',
            ),

            array (
                'code' => '53.04.18',
                'name' => 'Lamaknen Selatan',
            ),

            array (
                'code' => '53.04.21',
                'name' => 'Atambua Barat',
            ),

            array (
                'code' => '53.04.22',
                'name' => 'Atambua Selatan',
            ),

            array (
                'code' => '53.04.23',
                'name' => 'Nanaet Duabesi',
            ),

            array (
                'code' => '53.05',
                'name' => 'KAB. ALOR',
            ),

            array (
                'code' => '53.05.01',
                'name' => 'Teluk Mutiara',
            ),

            array (
                'code' => '53.05.02',
                'name' => 'Alor Barat Laut',
            ),

            array (
                'code' => '53.05.03',
                'name' => 'Alor Barat Daya',
            ),

            array (
                'code' => '53.05.04',
                'name' => 'Alor Selatan',
            ),

            array (
                'code' => '53.05.05',
                'name' => 'Alor Timur',
            ),

            array (
                'code' => '53.05.06',
                'name' => 'Pantar',
            ),

            array (
                'code' => '53.05.07',
                'name' => 'Alor Tengah Utara',
            ),

            array (
                'code' => '53.05.08',
                'name' => 'Alor Timur Laut',
            ),

            array (
                'code' => '53.05.09',
                'name' => 'Pantar Barat',
            ),

            array (
                'code' => '53.05.10',
                'name' => 'Kabola',
            ),

            array (
                'code' => '53.05.11',
                'name' => 'Pulau Pura',
            ),

            array (
                'code' => '53.05.12',
                'name' => 'Mataru',
            ),

            array (
                'code' => '53.05.13',
                'name' => 'Pureman',
            ),

            array (
                'code' => '53.05.14',
                'name' => 'Pantar Timur',
            ),

            array (
                'code' => '53.05.15',
                'name' => 'Lembur',
            ),

            array (
                'code' => '53.05.16',
                'name' => 'Pantar Tengah',
            ),

            array (
                'code' => '53.05.17',
                'name' => 'Pantar Baru Laut',
            ),

            array (
                'code' => '53.06',
                'name' => 'KAB. FLORES TIMUR',
            ),

            array (
                'code' => '53.06.01',
                'name' => 'Wulanggitang',
            ),

            array (
                'code' => '53.06.02',
                'name' => 'Titehena',
            ),

            array (
                'code' => '53.06.03',
                'name' => 'Larantuka',
            ),

            array (
                'code' => '53.06.04',
                'name' => 'Ile Mandiri',
            ),

            array (
                'code' => '53.06.05',
                'name' => 'Tanjung Bunga',
            ),

            array (
                'code' => '53.06.06',
                'name' => 'Solor Barat',
            ),

            array (
                'code' => '53.06.07',
                'name' => 'Solor Timur',
            ),

            array (
                'code' => '53.06.08',
                'name' => 'Adonara Barat',
            ),

            array (
                'code' => '53.06.09',
                'name' => 'Wotan Ulumando',
            ),

            array (
                'code' => '53.06.10',
                'name' => 'Adonara Timur',
            ),

            array (
                'code' => '53.06.11',
                'name' => 'Kelubagolit',
            ),

            array (
                'code' => '53.06.12',
                'name' => 'Witihama',
            ),

            array (
                'code' => '53.06.13',
                'name' => 'Ile Boleng',
            ),

            array (
                'code' => '53.06.14',
                'name' => 'Demon Pagong',
            ),

            array (
                'code' => '53.06.15',
                'name' => 'Lewolema',
            ),

            array (
                'code' => '53.06.16',
                'name' => 'Ile Bura',
            ),

            array (
                'code' => '53.06.17',
                'name' => 'Adonara',
            ),

            array (
                'code' => '53.06.18',
                'name' => 'Adonara Tengah',
            ),

            array (
                'code' => '53.06.19',
                'name' => 'Solor Selatan',
            ),

            array (
                'code' => '53.07',
                'name' => 'KAB. SIKKA',
            ),

            array (
                'code' => '53.07.01',
                'name' => 'Paga',
            ),

            array (
                'code' => '53.07.02',
                'name' => 'Mego',
            ),

            array (
                'code' => '53.07.03',
                'name' => 'Lela',
            ),

            array (
                'code' => '53.07.04',
                'name' => 'Nita',
            ),

            array (
                'code' => '53.07.05',
                'name' => 'Alok',
            ),

            array (
                'code' => '53.07.06',
                'name' => 'Palue',
            ),

            array (
                'code' => '53.07.07',
                'name' => 'Nelle',
            ),

            array (
                'code' => '53.07.08',
                'name' => 'Talibura',
            ),

            array (
                'code' => '53.07.09',
                'name' => 'Waigete',
            ),

            array (
                'code' => '53.07.10',
                'name' => 'Kewapante',
            ),

            array (
                'code' => '53.07.11',
                'name' => 'Bola',
            ),

            array (
                'code' => '53.07.12',
                'name' => 'Magepanda',
            ),

            array (
                'code' => '53.07.13',
                'name' => 'Waiblama',
            ),

            array (
                'code' => '53.07.14',
                'name' => 'Alok Barat',
            ),

            array (
                'code' => '53.07.15',
                'name' => 'Alok Timur',
            ),

            array (
                'code' => '53.07.16',
                'name' => 'Koting',
            ),

            array (
                'code' => '53.07.17',
                'name' => 'Tana Wawo',
            ),

            array (
                'code' => '53.07.18',
                'name' => 'Hewokloang',
            ),

            array (
                'code' => '53.07.19',
                'name' => 'Kangae',
            ),

            array (
                'code' => '53.07.20',
                'name' => 'Doreng',
            ),

            array (
                'code' => '53.07.21',
                'name' => 'Mapitara',
            ),

            array (
                'code' => '53.08',
                'name' => 'KAB. ENDE',
            ),

            array (
                'code' => '53.08.01',
                'name' => 'Nangapanda',
            ),

            array (
                'code' => '53.08.02',
                'name' => 'Pulau Ende',
            ),

            array (
                'code' => '53.08.03',
                'name' => 'Ende',
            ),

            array (
                'code' => '53.08.04',
                'name' => 'Ende Selatan',
            ),

            array (
                'code' => '53.08.05',
                'name' => 'Ndona',
            ),

            array (
                'code' => '53.08.06',
                'name' => 'Detusoko',
            ),

            array (
                'code' => '53.08.07',
                'name' => 'Wewaria',
            ),

            array (
                'code' => '53.08.08',
                'name' => 'Wolowaru',
            ),

            array (
                'code' => '53.08.09',
                'name' => 'Wolojita',
            ),

            array (
                'code' => '53.08.10',
                'name' => 'Maurole',
            ),

            array (
                'code' => '53.08.11',
                'name' => 'Maukaro',
            ),

            array (
                'code' => '53.08.12',
                'name' => 'Lio Timur',
            ),

            array (
                'code' => '53.08.13',
                'name' => 'Kota Baru',
            ),

            array (
                'code' => '53.08.14',
                'name' => 'Kelimutu',
            ),

            array (
                'code' => '53.08.15',
                'name' => 'Detukeli',
            ),

            array (
                'code' => '53.08.16',
                'name' => 'Ndona Timur',
            ),

            array (
                'code' => '53.08.17',
                'name' => 'Ndori',
            ),

            array (
                'code' => '53.08.18',
                'name' => 'Ende Utara',
            ),

            array (
                'code' => '53.08.19',
                'name' => 'Ende Tengah',
            ),

            array (
                'code' => '53.08.20',
                'name' => 'Ende Timur',
            ),

            array (
                'code' => '53.08.21',
                'name' => 'Lepembusu Kelisoke',
            ),

            array (
                'code' => '53.09',
                'name' => 'KAB. NGADA',
            ),

            array (
                'code' => '53.09.01',
                'name' => 'Aimere',
            ),

            array (
                'code' => '53.09.02',
                'name' => 'Golewa',
            ),

            array (
                'code' => '53.09.06',
                'name' => 'Bajawa',
            ),

            array (
                'code' => '53.09.07',
                'name' => 'Soa',
            ),

            array (
                'code' => '53.09.09',
                'name' => 'Riung',
            ),

            array (
                'code' => '53.09.12',
                'name' => 'Jerebuu',
            ),

            array (
                'code' => '53.09.14',
                'name' => 'Riung Barat',
            ),

            array (
                'code' => '53.09.15',
                'name' => 'Bajawa Utara',
            ),

            array (
                'code' => '53.09.16',
                'name' => 'Wolomeze',
            ),

            array (
                'code' => '53.09.18',
                'name' => 'Golewa Selatan',
            ),

            array (
                'code' => '53.09.19',
                'name' => 'Golewa Barat',
            ),

            array (
                'code' => '53.09.20',
                'name' => 'Inerie',
            ),

            array (
                'code' => '53.10',
                'name' => 'KAB. MANGGARAI',
            ),

            array (
                'code' => '53.10.01',
                'name' => 'Wae Rii',
            ),

            array (
                'code' => '53.10.03',
                'name' => 'Ruteng',
            ),

            array (
                'code' => '53.10.05',
                'name' => 'Satar Mese',
            ),

            array (
                'code' => '53.10.06',
                'name' => 'Cibal',
            ),

            array (
                'code' => '53.10.11',
                'name' => 'Reok',
            ),

            array (
                'code' => '53.10.12',
                'name' => 'Langke Rembong',
            ),

            array (
                'code' => '53.10.13',
                'name' => 'Satar Mese Barat',
            ),

            array (
                'code' => '53.10.14',
                'name' => 'Rahong Utara',
            ),

            array (
                'code' => '53.10.15',
                'name' => 'Lelak',
            ),

            array (
                'code' => '53.10.16',
                'name' => 'Reok Barat',
            ),

            array (
                'code' => '53.10.17',
                'name' => 'Cibal barat',
            ),

            array (
                'code' => '53.11',
                'name' => 'KAB. SUMBA TIMUR',
            ),

            array (
                'code' => '53.11.01',
                'name' => 'Kota Waingapu',
            ),

            array (
                'code' => '53.11.02',
                'name' => 'Haharu',
            ),

            array (
                'code' => '53.11.03',
                'name' => 'Lewa',
            ),

            array (
                'code' => '53.11.04',
                'name' => 'Nggaha Ori Angu',
            ),

            array (
                'code' => '53.11.05',
                'name' => 'Tabundung',
            ),

            array (
                'code' => '53.11.06',
                'name' => 'Pinu Pahar',
            ),

            array (
                'code' => '53.11.07',
                'name' => 'Pandawai',
            ),

            array (
                'code' => '53.11.08',
                'name' => 'Umalulu',
            ),

            array (
                'code' => '53.11.09',
                'name' => 'Rindi',
            ),

            array (
                'code' => '53.11.10',
                'name' => 'Pahunga Lodu',
            ),

            array (
                'code' => '53.11.11',
                'name' => 'Wulla Waijelu',
            ),

            array (
                'code' => '53.11.12',
                'name' => 'Paberiwai',
            ),

            array (
                'code' => '53.11.13',
                'name' => 'Karera',
            ),

            array (
                'code' => '53.11.14',
                'name' => 'Kahaungu Eti',
            ),

            array (
                'code' => '53.11.15',
                'name' => 'Matawai La Pawu',
            ),

            array (
                'code' => '53.11.16',
                'name' => 'Kambera',
            ),

            array (
                'code' => '53.11.17',
                'name' => 'Kambata Mapambuhang',
            ),

            array (
                'code' => '53.11.18',
                'name' => 'Lewa Tidahu',
            ),

            array (
                'code' => '53.11.19',
                'name' => 'Katala Hamu Lingu',
            ),

            array (
                'code' => '53.11.20',
                'name' => 'Kanatang',
            ),

            array (
                'code' => '53.11.21',
                'name' => 'Ngadu Ngala',
            ),

            array (
                'code' => '53.11.22',
                'name' => 'Mahu',
            ),

            array (
                'code' => '53.12',
                'name' => 'KAB. SUMBA BARAT',
            ),

            array (
                'code' => '53.12.04',
                'name' => 'Tana Righu',
            ),

            array (
                'code' => '53.12.10',
                'name' => 'Loli',
            ),

            array (
                'code' => '53.12.11',
                'name' => 'Wanokaka',
            ),

            array (
                'code' => '53.12.12',
                'name' => 'Lamboya',
            ),

            array (
                'code' => '53.12.15',
                'name' => 'Kota Waikabubak',
            ),

            array (
                'code' => '53.12.18',
                'name' => 'Laboya Barat',
            ),

            array (
                'code' => '53.13',
                'name' => 'KAB. LEMBATA',
            ),

            array (
                'code' => '53.13.01',
                'name' => 'Naga Wutung',
            ),

            array (
                'code' => '53.13.02',
                'name' => 'Atadei',
            ),

            array (
                'code' => '53.13.03',
                'name' => 'Ile Ape',
            ),

            array (
                'code' => '53.13.04',
                'name' => 'Lebatukan',
            ),

            array (
                'code' => '53.13.05',
                'name' => 'Nubatukan',
            ),

            array (
                'code' => '53.13.06',
                'name' => 'Omesuri',
            ),

            array (
                'code' => '53.13.07',
                'name' => 'Buyasuri',
            ),

            array (
                'code' => '53.13.08',
                'name' => 'Wulandoni',
            ),

            array (
                'code' => '53.13.09',
                'name' => 'Ile Ape Timur',
            ),

            array (
                'code' => '53.14',
                'name' => 'KAB. ROTE NDAO',
            ),

            array (
                'code' => '53.14.01',
                'name' => 'Rote Barat Daya',
            ),

            array (
                'code' => '53.14.02',
                'name' => 'Rote Barat Laut',
            ),

            array (
                'code' => '53.14.03',
                'name' => 'Lobalain',
            ),

            array (
                'code' => '53.14.04',
                'name' => 'Rote Tengah',
            ),

            array (
                'code' => '53.14.05',
                'name' => 'Pantai Baru',
            ),

            array (
                'code' => '53.14.06',
                'name' => 'Rote Timur',
            ),

            array (
                'code' => '53.14.07',
                'name' => 'Rote Barat',
            ),

            array (
                'code' => '53.14.08',
                'name' => 'Rote  Selatan',
            ),

            array (
                'code' => '53.14.09',
                'name' => 'Ndao Nuse',
            ),

            array (
                'code' => '53.14.10',
                'name' => 'Landu Leko',
            ),

            array (
                'code' => '53.15',
                'name' => 'KAB. MANGGARAI BARAT',
            ),

            array (
                'code' => '53.15.01',
                'name' => 'Macang Pacar',
            ),

            array (
                'code' => '53.15.02',
                'name' => 'Kuwus',
            ),

            array (
                'code' => '53.15.03',
                'name' => 'Lembor',
            ),

            array (
                'code' => '53.15.04',
                'name' => 'Sano Nggoang',
            ),

            array (
                'code' => '53.15.05',
                'name' => 'Komodo',
            ),

            array (
                'code' => '53.15.06',
                'name' => 'Boleng',
            ),

            array (
                'code' => '53.15.07',
                'name' => 'Welak',
            ),

            array (
                'code' => '53.15.08',
                'name' => 'Ndoso',
            ),

            array (
                'code' => '53.15.09',
                'name' => 'Lembor Selatan',
            ),

            array (
                'code' => '53.15.10',
                'name' => 'Mbeliling',
            ),

            array (
                'code' => '53.16',
                'name' => 'KAB. NAGEKEO',
            ),

            array (
                'code' => '53.16.01',
                'name' => 'Aesesa',
            ),

            array (
                'code' => '53.16.02',
                'name' => 'Nangaroro',
            ),

            array (
                'code' => '53.16.03',
                'name' => 'Boawae',
            ),

            array (
                'code' => '53.16.04',
                'name' => 'Mauponggo',
            ),

            array (
                'code' => '53.16.05',
                'name' => 'Wolowae',
            ),

            array (
                'code' => '53.16.06',
                'name' => 'Keo Tengah',
            ),

            array (
                'code' => '53.16.07',
                'name' => 'Aesesa Selatan',
            ),

            array (
                'code' => '53.17',
                'name' => 'KAB. SUMBA TENGAH',
            ),

            array (
                'code' => '53.17.01',
                'name' => 'Katiku Tana',
            ),

            array (
                'code' => '53.17.02',
                'name' => 'Umbu Ratu Nggay Barat',
            ),

            array (
                'code' => '53.17.03',
                'name' => 'Mamboro',
            ),

            array (
                'code' => '53.17.04',
                'name' => 'Umbu Ratu Nggay',
            ),

            array (
                'code' => '53.17.05',
                'name' => 'Katiku Tana Selatan',
            ),

            array (
                'code' => '53.18',
                'name' => 'KAB. SUMBA BARAT DAYA',
            ),

            array (
                'code' => '53.18.01',
                'name' => 'Loura',
            ),

            array (
                'code' => '53.18.02',
                'name' => 'Wewewa Utara',
            ),

            array (
                'code' => '53.18.03',
                'name' => 'Wewewa Timur',
            ),

            array (
                'code' => '53.18.04',
                'name' => 'Wewewa Barat',
            ),

            array (
                'code' => '53.18.05',
                'name' => 'Wewewa Selatan',
            ),

            array (
                'code' => '53.18.06',
                'name' => 'Kodi Bangedo',
            ),

            array (
                'code' => '53.18.07',
                'name' => 'Kodi',
            ),

            array (
                'code' => '53.18.08',
                'name' => 'Kodi Utara',
            ),

            array (
                'code' => '53.18.09',
                'name' => 'Kota Tambolaka',
            ),

            array (
                'code' => '53.18.10',
                'name' => 'Wewewa Tengah',
            ),

            array (
                'code' => '53.18.11',
                'name' => 'Kodi Balaghar',
            ),

            array (
                'code' => '53.19',
                'name' => 'KAB. MANGGARAI TIMUR',
            ),

            array (
                'code' => '53.19.01',
                'name' => 'Borong',
            ),

            array (
                'code' => '53.19.02',
                'name' => 'Poco Ranaka',
            ),

            array (
                'code' => '53.19.03',
                'name' => 'Lamba Leda',
            ),

            array (
                'code' => '53.19.04',
                'name' => 'Sambi Rampas',
            ),

            array (
                'code' => '53.19.05',
                'name' => 'Elar',
            ),

            array (
                'code' => '53.19.06',
                'name' => 'Kota Komba',
            ),

            array (
                'code' => '53.19.07',
                'name' => 'Rana Mese',
            ),

            array (
                'code' => '53.19.08',
                'name' => 'Poco Ranaka Timur',
            ),

            array (
                'code' => '53.19.09',
                'name' => 'Elar Selatan',
            ),

            array (
                'code' => '53.20',
                'name' => 'KAB. SABU RAIJUA',
            ),

            array (
                'code' => '53.20.01',
                'name' => 'Sabu Barat',
            ),

            array (
                'code' => '53.20.02',
                'name' => 'Sabu Tengah',
            ),

            array (
                'code' => '53.20.03',
                'name' => 'Sabu Timur',
            ),

            array (
                'code' => '53.20.04',
                'name' => 'Sabu Liae',
            ),

            array (
                'code' => '53.20.05',
                'name' => 'Hawu Mehara',
            ),

            array (
                'code' => '53.20.06',
                'name' => 'Raijua',
            ),

            array (
                'code' => '53.21',
                'name' => 'KAB. MALAKA',
            ),

            array (
                'code' => '53.21.01',
                'name' => 'Malaka Tengah',
            ),

            array (
                'code' => '53.21.02',
                'name' => 'Malaka Barat',
            ),

            array (
                'code' => '53.21.03',
                'name' => 'Wewiku',
            ),

            array (
                'code' => '53.21.04',
                'name' => 'Weliman',
            ),

            array (
                'code' => '53.21.05',
                'name' => 'Rinhat',
            ),

            array (
                'code' => '53.21.06',
                'name' => 'Io Kufeu',
            ),

            array (
                'code' => '53.21.07',
                'name' => 'Sasitamean',
            ),

            array (
                'code' => '53.21.08',
                'name' => 'Laenmanen',
            ),

            array (
                'code' => '53.21.09',
                'name' => 'Malaka Timur',
            ),

            array (
                'code' => '53.21.10',
                'name' => 'Kobalima Timur',
            ),

            array (
                'code' => '53.21.11',
                'name' => 'Kobalima',
            ),

            array (
                'code' => '53.21.12',
                'name' => 'Botin Leobele',
            ),

            array (
                'code' => '53.71',
                'name' => 'KOTA KUPANG',
            ),

            array (
                'code' => '53.71.01',
                'name' => 'Alak',
            ),

            array (
                'code' => '53.71.02',
                'name' => 'Maulafa',
            ),

            array (
                'code' => '53.71.03',
                'name' => 'Kelapa Lima',
            ),

            array (
                'code' => '53.71.04',
                'name' => 'Oebobo',
            ),

            array (
                'code' => '53.71.05',
                'name' => 'Kota Raja',
            ),

            array (
                'code' => '53.71.06',
                'name' => 'Kota Lama',
            ),

            array (
                'code' => '61',
                'name' => 'KALIMANTAN BARAT',
            ),

            array (
                'code' => '61.01',
                'name' => 'KAB. SAMBAS',
            ),

            array (
                'code' => '61.01.01',
                'name' => 'Sambas',
            ),

            array (
                'code' => '61.01.02',
                'name' => 'Teluk Keramat',
            ),

            array (
                'code' => '61.01.03',
                'name' => 'Jawai',
            ),

            array (
                'code' => '61.01.04',
                'name' => 'Tebas',
            ),

            array (
                'code' => '61.01.05',
                'name' => 'Pemangkat',
            ),

            array (
                'code' => '61.01.06',
                'name' => 'Sejangkung',
            ),

            array (
                'code' => '61.01.07',
                'name' => 'Selakau',
            ),

            array (
                'code' => '61.01.08',
                'name' => 'Paloh',
            ),

            array (
                'code' => '61.01.09',
                'name' => 'Sajingan Besar',
            ),

            array (
                'code' => '61.01.10',
                'name' => 'Subah',
            ),

            array (
                'code' => '61.01.11',
                'name' => 'Galing',
            ),

            array (
                'code' => '61.01.12',
                'name' => 'Tekarang',
            ),

            array (
                'code' => '61.01.13',
                'name' => 'Semparuk',
            ),

            array (
                'code' => '61.01.14',
                'name' => 'Sajad',
            ),

            array (
                'code' => '61.01.15',
                'name' => 'Sebawi',
            ),

            array (
                'code' => '61.01.16',
                'name' => 'Jawai Selatan',
            ),

            array (
                'code' => '61.01.17',
                'name' => 'Tangaran',
            ),

            array (
                'code' => '61.01.18',
                'name' => 'Salatiga',
            ),

            array (
                'code' => '61.01.19',
                'name' => 'Selakau Timur',
            ),

            array (
                'code' => '61.02',
                'name' => 'KAB. MEMPAWAH',
            ),

            array (
                'code' => '61.02.01',
                'name' => 'Mempawah Hilir',
            ),

            array (
                'code' => '61.02.06',
                'name' => 'Toho',
            ),

            array (
                'code' => '61.02.07',
                'name' => 'Sungai Pinyuh',
            ),

            array (
                'code' => '61.02.08',
                'name' => 'Siantan',
            ),

            array (
                'code' => '61.02.12',
                'name' => 'Sungai Kunyit',
            ),

            array (
                'code' => '61.02.15',
                'name' => 'Segedong',
            ),

            array (
                'code' => '61.02.16',
                'name' => 'Anjongan',
            ),

            array (
                'code' => '61.02.17',
                'name' => 'Sadaniang',
            ),

            array (
                'code' => '61.02.18',
                'name' => 'Mempawah Timur',
            ),

            array (
                'code' => '61.03',
                'name' => 'KAB. SANGGAU',
            ),

            array (
                'code' => '61.03.01',
                'name' => 'Kapuas',
            ),

            array (
                'code' => '61.03.02',
                'name' => 'Mukok',
            ),

            array (
                'code' => '61.03.03',
                'name' => 'Noyan',
            ),

            array (
                'code' => '61.03.04',
                'name' => 'Jangkang',
            ),

            array (
                'code' => '61.03.05',
                'name' => 'Bonti',
            ),

            array (
                'code' => '61.03.06',
                'name' => 'Beduai',
            ),

            array (
                'code' => '61.03.07',
                'name' => 'Sekayam',
            ),

            array (
                'code' => '61.03.08',
                'name' => 'Kembayan',
            ),

            array (
                'code' => '61.03.09',
                'name' => 'Parindu',
            ),

            array (
                'code' => '61.03.10',
                'name' => 'Tayan Hulu',
            ),

            array (
                'code' => '61.03.11',
                'name' => 'Tayan Hilir',
            ),

            array (
                'code' => '61.03.12',
                'name' => 'Balai',
            ),

            array (
                'code' => '61.03.13',
                'name' => 'Toba',
            ),

            array (
                'code' => '61.03.20',
                'name' => 'Meliau',
            ),

            array (
                'code' => '61.03.21',
                'name' => 'Entikong',
            ),

            array (
                'code' => '61.04',
                'name' => 'KAB. KETAPANG',
            ),

            array (
                'code' => '61.04.01',
                'name' => 'Matan Hilir Utara',
            ),

            array (
                'code' => '61.04.02',
                'name' => 'Marau',
            ),

            array (
                'code' => '61.04.03',
                'name' => 'Manis Mata',
            ),

            array (
                'code' => '61.04.04',
                'name' => 'Kendawangan',
            ),

            array (
                'code' => '61.04.05',
                'name' => 'Sandai',
            ),

            array (
                'code' => '61.04.07',
                'name' => 'Sungai Laur',
            ),

            array (
                'code' => '61.04.08',
                'name' => 'Simpang Hulu',
            ),

            array (
                'code' => '61.04.11',
                'name' => 'Nanga Tayap',
            ),

            array (
                'code' => '61.04.12',
                'name' => 'Matan Hilir Selatan',
            ),

            array (
                'code' => '61.04.13',
                'name' => 'Tumbang Titi',
            ),

            array (
                'code' => '61.04.14',
                'name' => 'Jelai Hulu',
            ),

            array (
                'code' => '61.04.16',
                'name' => 'Delta Pawan',
            ),

            array (
                'code' => '61.04.17',
                'name' => 'Muara Pawan',
            ),

            array (
                'code' => '61.04.18',
                'name' => 'Benua Kayong',
            ),

            array (
                'code' => '61.04.19',
                'name' => 'Hulu Sungai',
            ),

            array (
                'code' => '61.04.20',
                'name' => 'Simpang Dua',
            ),

            array (
                'code' => '61.04.21',
                'name' => 'Air Upas',
            ),

            array (
                'code' => '61.04.22',
                'name' => 'Singkup',
            ),

            array (
                'code' => '61.04.24',
                'name' => 'Pemahan',
            ),

            array (
                'code' => '61.04.25',
                'name' => 'Sungai Melayu Rayak',
            ),

            array (
                'code' => '61.05',
                'name' => 'KAB. SINTANG',
            ),

            array (
                'code' => '61.05.01',
                'name' => 'Sintang',
            ),

            array (
                'code' => '61.05.02',
                'name' => 'Tempunak',
            ),

            array (
                'code' => '61.05.03',
                'name' => 'Sepauk',
            ),

            array (
                'code' => '61.05.04',
                'name' => 'Ketungau Hilir',
            ),

            array (
                'code' => '61.05.05',
                'name' => 'Ketungau Tengah',
            ),

            array (
                'code' => '61.05.06',
                'name' => 'Ketungau Hulu',
            ),

            array (
                'code' => '61.05.07',
                'name' => 'Dedai',
            ),

            array (
                'code' => '61.05.08',
                'name' => 'Kayan Hilir',
            ),

            array (
                'code' => '61.05.09',
                'name' => 'Kayan Hulu',
            ),

            array (
                'code' => '61.05.14',
                'name' => 'Serawai',
            ),

            array (
                'code' => '61.05.15',
                'name' => 'Ambalau',
            ),

            array (
                'code' => '61.05.19',
                'name' => 'Kelam Permai',
            ),

            array (
                'code' => '61.05.20',
                'name' => 'Sungai Tebelian',
            ),

            array (
                'code' => '61.05.21',
                'name' => 'Binjai Hulu',
            ),

            array (
                'code' => '61.06',
                'name' => 'KAB. KAPUAS HULU',
            ),

            array (
                'code' => '61.06.01',
                'name' => 'Putussibau Utara',
            ),

            array (
                'code' => '61.06.02',
                'name' => 'Bika',
            ),

            array (
                'code' => '61.06.03',
                'name' => 'Embaloh Hilir',
            ),

            array (
                'code' => '61.06.04',
                'name' => 'Embaloh Hulu',
            ),

            array (
                'code' => '61.06.05',
                'name' => 'Bunut Hilir',
            ),

            array (
                'code' => '61.06.06',
                'name' => 'Bunut Hulu',
            ),

            array (
                'code' => '61.06.07',
                'name' => 'Jongkong',
            ),

            array (
                'code' => '61.06.08',
                'name' => 'Hulu Gurung',
            ),

            array (
                'code' => '61.06.09',
                'name' => 'Selimbau',
            ),

            array (
                'code' => '61.06.10',
                'name' => 'Semitau',
            ),

            array (
                'code' => '61.06.11',
                'name' => 'Seberuang',
            ),

            array (
                'code' => '61.06.12',
                'name' => 'Batang Lupar',
            ),

            array (
                'code' => '61.06.13',
                'name' => 'Empanang',
            ),

            array (
                'code' => '61.06.14',
                'name' => 'Badau',
            ),

            array (
                'code' => '61.06.15',
                'name' => 'Silat Hilir',
            ),

            array (
                'code' => '61.06.16',
                'name' => 'Silat Hulu',
            ),

            array (
                'code' => '61.06.17',
                'name' => 'Putussibau Selatan',
            ),

            array (
                'code' => '61.06.18',
                'name' => 'Kalis',
            ),

            array (
                'code' => '61.06.19',
                'name' => 'Boyan Tanjung',
            ),

            array (
                'code' => '61.06.20',
                'name' => 'Mentebah',
            ),

            array (
                'code' => '61.06.21',
                'name' => 'Pengkadan',
            ),

            array (
                'code' => '61.06.22',
                'name' => 'Suhaid',
            ),

            array (
                'code' => '61.06.23',
                'name' => 'Puring Kencana',
            ),

            array (
                'code' => '61.07',
                'name' => 'KAB. BENGKAYANG',
            ),

            array (
                'code' => '61.07.01',
                'name' => 'Sungai Raya',
            ),

            array (
                'code' => '61.07.02',
                'name' => 'Samalantan',
            ),

            array (
                'code' => '61.07.03',
                'name' => 'Ledo',
            ),

            array (
                'code' => '61.07.04',
                'name' => 'Bengkayang',
            ),

            array (
                'code' => '61.07.05',
                'name' => 'Seluas',
            ),

            array (
                'code' => '61.07.06',
                'name' => 'Sanggau Ledo',
            ),

            array (
                'code' => '61.07.07',
                'name' => 'Jagoi Babang',
            ),

            array (
                'code' => '61.07.08',
                'name' => 'Monterado',
            ),

            array (
                'code' => '61.07.09',
                'name' => 'Teriak',
            ),

            array (
                'code' => '61.07.10',
                'name' => 'Suti Semarang',
            ),

            array (
                'code' => '61.07.11',
                'name' => 'Capkala',
            ),

            array (
                'code' => '61.07.12',
                'name' => 'Siding',
            ),

            array (
                'code' => '61.07.13',
                'name' => 'Lumar',
            ),

            array (
                'code' => '61.07.14',
                'name' => 'Sungai Betung',
            ),

            array (
                'code' => '61.07.15',
                'name' => 'Sungai Raya Kepulauan',
            ),

            array (
                'code' => '61.07.16',
                'name' => 'Lembah Bawang',
            ),

            array (
                'code' => '61.07.17',
                'name' => 'Tujuh Belas',
            ),

            array (
                'code' => '61.08',
                'name' => 'KAB. LANDAK',
            ),

            array (
                'code' => '61.08.01',
                'name' => 'Ngabang',
            ),

            array (
                'code' => '61.08.02',
                'name' => 'Mempawah Hulu',
            ),

            array (
                'code' => '61.08.03',
                'name' => 'Menjalin',
            ),

            array (
                'code' => '61.08.04',
                'name' => 'Mandor',
            ),

            array (
                'code' => '61.08.05',
                'name' => 'Air Besar',
            ),

            array (
                'code' => '61.08.06',
                'name' => 'Menyuke',
            ),

            array (
                'code' => '61.08.07',
                'name' => 'Sengah Temila',
            ),

            array (
                'code' => '61.08.08',
                'name' => 'Meranti',
            ),

            array (
                'code' => '61.08.09',
                'name' => 'Kuala Behe',
            ),

            array (
                'code' => '61.08.10',
                'name' => 'Sebangki',
            ),

            array (
                'code' => '61.08.11',
                'name' => 'Jelimpo',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '61.08.12',
                'name' => 'Banyuke Hulu',
            ),

            array (
                'code' => '61.08.13',
                'name' => 'Sompak',
            ),

            array (
                'code' => '61.09',
                'name' => 'KAB. SEKADAU',
            ),

            array (
                'code' => '61.09.01',
                'name' => 'Sekadau Hilir',
            ),

            array (
                'code' => '61.09.02',
                'name' => 'Sekadau Hulu',
            ),

            array (
                'code' => '61.09.03',
                'name' => 'Nanga Taman',
            ),

            array (
                'code' => '61.09.04',
                'name' => 'Nanga Mahap',
            ),

            array (
                'code' => '61.09.05',
                'name' => 'Belitang Hilir',
            ),

            array (
                'code' => '61.09.06',
                'name' => 'Belitang Hulu',
            ),

            array (
                'code' => '61.09.07',
                'name' => 'Belitang',
            ),

            array (
                'code' => '61.10',
                'name' => 'KAB. MELAWI',
            ),

            array (
                'code' => '61.10.01',
                'name' => 'Belimbing',
            ),

            array (
                'code' => '61.10.02',
                'name' => 'Nanga Pinoh',
            ),

            array (
                'code' => '61.10.03',
                'name' => 'Ella Hilir',
            ),

            array (
                'code' => '61.10.04',
                'name' => 'Menukung',
            ),

            array (
                'code' => '61.10.05',
                'name' => 'Sayan',
            ),

            array (
                'code' => '61.10.06',
                'name' => 'Tanah Pinoh',
            ),

            array (
                'code' => '61.10.07',
                'name' => 'Sokan',
            ),

            array (
                'code' => '61.10.08',
                'name' => 'Pinoh Utara',
            ),

            array (
                'code' => '61.10.09',
                'name' => 'Pinoh Selatan',
            ),

            array (
                'code' => '61.10.10',
                'name' => 'Belimbing Hulu',
            ),

            array (
                'code' => '61.10.11',
                'name' => 'Tanah Pinoh Barat',
            ),

            array (
                'code' => '61.11',
                'name' => 'KAB. KAYONG UTARA',
            ),

            array (
                'code' => '61.11.01',
                'name' => 'Sukadana',
            ),

            array (
                'code' => '61.11.02',
                'name' => 'Simpang Hilir',
            ),

            array (
                'code' => '61.11.03',
                'name' => 'Teluk Batang',
            ),

            array (
                'code' => '61.11.04',
                'name' => 'Pulau Maya',
            ),

            array (
                'code' => '61.11.05',
                'name' => 'Seponti',
            ),

            array (
                'code' => '61.11.06',
                'name' => 'Kepulauan Karimata',
            ),

            array (
                'code' => '61.12',
                'name' => 'KAB. KUBU RAYA',
            ),

            array (
                'code' => '61.12.01',
                'name' => 'Sungai Raya',
            ),

            array (
                'code' => '61.12.02',
                'name' => 'Kuala Mandor B',
            ),

            array (
                'code' => '61.12.03',
                'name' => 'Sungai Ambawang',
            ),

            array (
                'code' => '61.12.04',
                'name' => 'Terentang',
            ),

            array (
                'code' => '61.12.05',
                'name' => 'Batu Ampar',
            ),

            array (
                'code' => '61.12.06',
                'name' => 'Kubu',
            ),

            array (
                'code' => '61.12.07',
                'name' => 'Rasau Jaya',
            ),

            array (
                'code' => '61.12.08',
                'name' => 'Teluk Pakedai',
            ),

            array (
                'code' => '61.12.09',
                'name' => 'Sungai Kakap',
            ),

            array (
                'code' => '61.71',
                'name' => 'KOTA PONTIANAK',
            ),

            array (
                'code' => '61.71.01',
                'name' => 'Pontianak Selatan',
            ),

            array (
                'code' => '61.71.02',
                'name' => 'Pontianak Timur',
            ),

            array (
                'code' => '61.71.03',
                'name' => 'Pontianak Barat',
            ),

            array (
                'code' => '61.71.04',
                'name' => 'Pontianak Utara',
            ),

            array (
                'code' => '61.71.05',
                'name' => 'Pontianak Kota',
            ),

            array (
                'code' => '61.71.06',
                'name' => 'Pontianak Tenggara',
            ),

            array (
                'code' => '61.72',
                'name' => 'KOTA SINGKAWANG',
            ),

            array (
                'code' => '61.72.01',
                'name' => 'Singkawang Tengah',
            ),

            array (
                'code' => '61.72.02',
                'name' => 'Singkawang Barat',
            ),

            array (
                'code' => '61.72.03',
                'name' => 'Singkawang Timur',
            ),

            array (
                'code' => '61.72.04',
                'name' => 'Singkawang Utara',
            ),

            array (
                'code' => '61.72.05',
                'name' => 'Singkawang Selatan',
            ),

            array (
                'code' => '62',
                'name' => 'KALIMANTAN TENGAH',
            ),

            array (
                'code' => '62.01',
                'name' => 'KAB. KOTAWARINGIN BARAT',
            ),

            array (
                'code' => '62.01.01',
                'name' => 'Kumai',
            ),

            array (
                'code' => '62.01.02',
                'name' => 'Arut Selatan',
            ),

            array (
                'code' => '62.01.03',
                'name' => 'Kotawaringin Lama',
            ),

            array (
                'code' => '62.01.04',
                'name' => 'Arut Utara',
            ),

            array (
                'code' => '62.01.05',
                'name' => 'Pangkalan Lada',
            ),

            array (
                'code' => '62.01.06',
                'name' => 'Pangkalan Banteng',
            ),

            array (
                'code' => '62.02',
                'name' => 'KAB. KOTAWARINGIN TIMUR',
            ),

            array (
                'code' => '62.02.01',
                'name' => 'Kota Besi',
            ),

            array (
                'code' => '62.02.02',
                'name' => 'Cempaga',
            ),

            array (
                'code' => '62.02.03',
                'name' => 'Mentaya Hulu',
            ),

            array (
                'code' => '62.02.04',
                'name' => 'Parenggean',
            ),

            array (
                'code' => '62.02.05',
                'name' => 'Baamang',
            ),

            array (
                'code' => '62.02.06',
                'name' => 'Mentawa Baru Ketapang',
            ),

            array (
                'code' => '62.02.07',
                'name' => 'Mentaya Hilir Utara',
            ),

            array (
                'code' => '62.02.08',
                'name' => 'Mentaya Hilir Selatan',
            ),

            array (
                'code' => '62.02.09',
                'name' => 'Pulau Hanaut',
            ),

            array (
                'code' => '62.02.10',
                'name' => 'Antang Kalang',
            ),

            array (
                'code' => '62.02.11',
                'name' => 'Teluk Sampit',
            ),

            array (
                'code' => '62.02.12',
                'name' => 'Seranau',
            ),

            array (
                'code' => '62.02.13',
                'name' => 'Cempaga Hulu',
            ),

            array (
                'code' => '62.02.14',
                'name' => 'Telawang',
            ),

            array (
                'code' => '62.02.15',
                'name' => 'Bukit Santuai',
            ),

            array (
                'code' => '62.02.16',
                'name' => 'Tualan Hulu',
            ),

            array (
                'code' => '62.02.17',
                'name' => 'Telaga Antang',
            ),

            array (
                'code' => '62.03',
                'name' => 'KAB. KAPUAS',
            ),

            array (
                'code' => '62.03.01',
                'name' => 'Selat',
            ),

            array (
                'code' => '62.03.02',
                'name' => 'Kapuas Hilir',
            ),

            array (
                'code' => '62.03.03',
                'name' => 'Kapuas Timur',
            ),

            array (
                'code' => '62.03.04',
                'name' => 'Kapuas Kuala',
            ),

            array (
                'code' => '62.03.05',
                'name' => 'Kapuas Barat',
            ),

            array (
                'code' => '62.03.06',
                'name' => 'Pulau Petak',
            ),

            array (
                'code' => '62.03.07',
                'name' => 'Kapuas Murung',
            ),

            array (
                'code' => '62.03.08',
                'name' => 'Basarang',
            ),

            array (
                'code' => '62.03.09',
                'name' => 'Mantangai',
            ),

            array (
                'code' => '62.03.10',
                'name' => 'Timpah',
            ),

            array (
                'code' => '62.03.11',
                'name' => 'Kapuas Tengah',
            ),

            array (
                'code' => '62.03.12',
                'name' => 'Kapuas Hulu',
            ),

            array (
                'code' => '62.03.13',
                'name' => 'Tamban Catur',
            ),

            array (
                'code' => '62.03.14',
                'name' => 'Pasak Talawang',
            ),

            array (
                'code' => '62.03.15',
                'name' => 'Mandau Talawang',
            ),

            array (
                'code' => '62.03.16',
                'name' => 'Dadahup',
            ),

            array (
                'code' => '62.03.17',
                'name' => 'Bataguh',
            ),

            array (
                'code' => '62.04',
                'name' => 'KAB. BARITO SELATAN',
            ),

            array (
                'code' => '62.04.01',
                'name' => 'Jenamas',
            ),

            array (
                'code' => '62.04.02',
                'name' => 'Dusun Hilir',
            ),

            array (
                'code' => '62.04.03',
                'name' => 'Karau Kuala',
            ),

            array (
                'code' => '62.04.04',
                'name' => 'Dusun Utara',
            ),

            array (
                'code' => '62.04.05',
                'name' => 'Gn. Bintang Awai  21',
            ),

            array (
                'code' => '62.04.06',
                'name' => 'Dusun Selatan',
            ),

            array (
                'code' => '62.05',
                'name' => 'KAB. BARITO UTARA',
            ),

            array (
                'code' => '62.05.01',
                'name' => 'Montallat',
            ),

            array (
                'code' => '62.05.02',
                'name' => 'Gunung Timang',
            ),

            array (
                'code' => '62.05.03',
                'name' => 'Gunung Purei',
            ),

            array (
                'code' => '62.05.04',
                'name' => 'Teweh Timur',
            ),

            array (
                'code' => '62.05.05',
                'name' => 'Teweh Tengah',
            ),

            array (
                'code' => '62.05.06',
                'name' => 'Lahei',
            ),

            array (
                'code' => '62.05.07',
                'name' => 'Teweh Baru',
            ),

            array (
                'code' => '62.05.08',
                'name' => 'Teweh Selatan',
            ),

            array (
                'code' => '62.05.09',
                'name' => 'Lahei Barat',
            ),

            array (
                'code' => '62.06',
                'name' => 'KAB. KATINGAN',
            ),

            array (
                'code' => '62.06.01',
                'name' => 'Kamipang',
            ),

            array (
                'code' => '62.06.02',
                'name' => 'Katingan Hilir',
            ),

            array (
                'code' => '62.06.03',
                'name' => 'Tewang Sangalang Garing',
            ),

            array (
                'code' => '62.06.04',
                'name' => 'Pulau Malan',
            ),

            array (
                'code' => '62.06.05',
                'name' => 'Katingan Tengah',
            ),

            array (
                'code' => '62.06.06',
                'name' => 'Sanaman Mantikei',
            ),

            array (
                'code' => '62.06.07',
                'name' => 'Marikit',
            ),

            array (
                'code' => '62.06.08',
                'name' => 'Katingan Hulu',
            ),

            array (
                'code' => '62.06.09',
                'name' => 'Mendawai',
            ),

            array (
                'code' => '62.06.10',
                'name' => 'Katingan Kuala',
            ),

            array (
                'code' => '62.06.11',
                'name' => 'Tasik Payawan',
            ),

            array (
                'code' => '62.06.12',
                'name' => 'Petak Malai',
            ),

            array (
                'code' => '62.06.13',
                'name' => 'Bukit Raya',
            ),

            array (
                'code' => '62.07',
                'name' => 'KAB. SERUYAN',
            ),

            array (
                'code' => '62.07.01',
                'name' => 'Seruyan Hilir',
            ),

            array (
                'code' => '62.07.02',
                'name' => 'Seruyan Tengah',
            ),

            array (
                'code' => '62.07.03',
                'name' => 'Danau Sembuluh',
            ),

            array (
                'code' => '62.07.04',
                'name' => 'Hanau',
            ),

            array (
                'code' => '62.07.05',
                'name' => 'Seruyan Hulu',
            ),

            array (
                'code' => '62.07.06',
                'name' => 'Seruyan Hilir Timur',
            ),

            array (
                'code' => '62.07.07',
                'name' => 'Seruyan Raya',
            ),

            array (
                'code' => '62.07.08',
                'name' => 'Danau Seluluk',
            ),

            array (
                'code' => '62.07.09',
                'name' => 'Batu Ampar',
            ),

            array (
                'code' => '62.07.10',
                'name' => 'Suling Tambun',
            ),

            array (
                'code' => '62.08',
                'name' => 'KAB. SUKAMARA',
            ),

            array (
                'code' => '62.08.01',
                'name' => 'Sukamara',
            ),

            array (
                'code' => '62.08.02',
                'name' => 'Jelai',
            ),

            array (
                'code' => '62.08.03',
                'name' => 'Balai Riam',
            ),

            array (
                'code' => '62.08.04',
                'name' => 'Pantai Lunci',
            ),

            array (
                'code' => '62.08.05',
                'name' => 'Permata Kecubung',
            ),

            array (
                'code' => '62.09',
                'name' => 'KAB. LAMANDAU',
            ),

            array (
                'code' => '62.09.01',
                'name' => 'Lamandau',
            ),

            array (
                'code' => '62.09.02',
                'name' => 'Delang',
            ),

            array (
                'code' => '62.09.03',
                'name' => 'Bulik',
            ),

            array (
                'code' => '62.09.04',
                'name' => 'Bulik Timur',
            ),

            array (
                'code' => '62.09.05',
                'name' => 'Menthobi Raya',
            ),

            array (
                'code' => '62.09.06',
                'name' => 'Sematu Jaya',
            ),

            array (
                'code' => '62.09.07',
                'name' => 'Belantikan Raya',
            ),

            array (
                'code' => '62.09.08',
                'name' => 'Batang Kawa',
            ),

            array (
                'code' => '62.10',
                'name' => 'KAB. GUNUNG MAS',
            ),

            array (
                'code' => '62.10.01',
                'name' => 'Sepang Simin',
            ),

            array (
                'code' => '62.10.02',
                'name' => 'Kurun',
            ),

            array (
                'code' => '62.10.03',
                'name' => 'Tewah',
            ),

            array (
                'code' => '62.10.04',
                'name' => 'Kahayan Hulu Utara',
            ),

            array (
                'code' => '62.10.05',
                'name' => 'Rungan',
            ),

            array (
                'code' => '62.10.06',
                'name' => 'Manuhing',
            ),

            array (
                'code' => '62.10.07',
                'name' => 'Mihing Raya',
            ),

            array (
                'code' => '62.10.08',
                'name' => 'Damang Batu',
            ),

            array (
                'code' => '62.10.09',
                'name' => 'Miri Manasa',
            ),

            array (
                'code' => '62.10.10',
                'name' => 'Rungan Hulu',
            ),

            array (
                'code' => '62.10.11',
                'name' => 'Mahuning Raya',
            ),

            array (
                'code' => '62.10.12',
                'name' => 'Rungan Barat',
            ),

            array (
                'code' => '62.11',
                'name' => 'KAB. PULANG PISAU',
            ),

            array (
                'code' => '62.11.01',
                'name' => 'Pandih Batu',
            ),

            array (
                'code' => '62.11.02',
                'name' => 'Kahayan Kuala',
            ),

            array (
                'code' => '62.11.03',
                'name' => 'Kahayan Tengah',
            ),

            array (
                'code' => '62.11.04',
                'name' => 'Banama Tingang',
            ),

            array (
                'code' => '62.11.05',
                'name' => 'Kahayan Hilir',
            ),

            array (
                'code' => '62.11.06',
                'name' => 'Maliku',
            ),

            array (
                'code' => '62.11.07',
                'name' => 'Jabiren',
            ),

            array (
                'code' => '62.11.08',
                'name' => 'Sebangau Kuala',
            ),

            array (
                'code' => '62.12',
                'name' => 'KAB. MURUNG RAYA',
            ),

            array (
                'code' => '62.12.01',
                'name' => 'Murung',
            ),

            array (
                'code' => '62.12.02',
                'name' => 'Tanah Siang',
            ),

            array (
                'code' => '62.12.03',
                'name' => 'Laung Tuhup',
            ),

            array (
                'code' => '62.12.04',
                'name' => 'Permata Intan',
            ),

            array (
                'code' => '62.12.05',
                'name' => 'Sumber Barito',
            ),

            array (
                'code' => '62.12.06',
                'name' => 'Barito Tuhup Raya',
            ),

            array (
                'code' => '62.12.07',
                'name' => 'Tanah Siang Selatan',
            ),

            array (
                'code' => '62.12.08',
                'name' => 'Sungai Babuat',
            ),

            array (
                'code' => '62.12.09',
                'name' => 'Seribu Riam',
            ),

            array (
                'code' => '62.12.10',
                'name' => 'Uut Murung',
            ),

            array (
                'code' => '62.13',
                'name' => 'KAB. BARITO TIMUR',
            ),

            array (
                'code' => '62.13.01',
                'name' => 'Dusun Timur',
            ),

            array (
                'code' => '62.13.02',
                'name' => 'Banua Lima',
            ),

            array (
                'code' => '62.13.03',
                'name' => 'Patangkep Tutui',
            ),

            array (
                'code' => '62.13.04',
                'name' => 'Awang',
            ),

            array (
                'code' => '62.13.05',
                'name' => 'Dusun Tengah',
            ),

            array (
                'code' => '62.13.06',
                'name' => 'Pematang Karau',
            ),

            array (
                'code' => '62.13.07',
                'name' => 'Paju Epat',
            ),

            array (
                'code' => '62.13.08',
                'name' => 'Raren Batuah',
            ),

            array (
                'code' => '62.13.09',
                'name' => 'Paku',
            ),

            array (
                'code' => '62.13.10',
                'name' => 'Karusen Janang',
            ),

            array (
                'code' => '62.71',
                'name' => 'KOTA PALANGKARAYA',
            ),

            array (
                'code' => '62.71.01',
                'name' => 'Pahandut',
            ),

            array (
                'code' => '62.71.02',
                'name' => 'Bukit Batu',
            ),

            array (
                'code' => '62.71.03',
                'name' => 'Jekan Raya',
            ),

            array (
                'code' => '62.71.04',
                'name' => 'Sabangau',
            ),

            array (
                'code' => '62.71.05',
                'name' => 'Rakumpit',
            ),

            array (
                'code' => '63',
                'name' => 'KALIMANTAN SELATAN',
            ),

            array (
                'code' => '63.01',
                'name' => 'KAB. TANAH LAUT',
            ),

            array (
                'code' => '63.01.01',
                'name' => 'Takisung',
            ),

            array (
                'code' => '63.01.02',
                'name' => 'Jorong',
            ),

            array (
                'code' => '63.01.03',
                'name' => 'Pelaihari',
            ),

            array (
                'code' => '63.01.04',
                'name' => 'Kurau',
            ),

            array (
                'code' => '63.01.05',
                'name' => 'Bati Bati',
            ),

            array (
                'code' => '63.01.06',
                'name' => 'Panyipatan',
            ),

            array (
                'code' => '63.01.07',
                'name' => 'Kintap',
            ),

            array (
                'code' => '63.01.08',
                'name' => 'Tambang Ulang',
            ),

            array (
                'code' => '63.01.09',
                'name' => 'Batu Ampar',
            ),

            array (
                'code' => '63.01.10',
                'name' => 'Bajuin',
            ),

            array (
                'code' => '63.01.11',
                'name' => 'Bumi Makmur',
            ),

            array (
                'code' => '63.02',
                'name' => 'KAB. KOTABARU',
            ),

            array (
                'code' => '63.02.01',
                'name' => 'Pulausembilan',
            ),

            array (
                'code' => '63.02.02',
                'name' => 'Pulaulaut Barat',
            ),

            array (
                'code' => '63.02.03',
                'name' => 'Pulaulaut Selatan',
            ),

            array (
                'code' => '63.02.04',
                'name' => 'Pulaulaut Timur',
            ),

            array (
                'code' => '63.02.05',
                'name' => 'Pulausebuku',
            ),

            array (
                'code' => '63.02.06',
                'name' => 'Pulaulaut Utara',
            ),

            array (
                'code' => '63.02.07',
                'name' => 'Kelumpang Selatan',
            ),

            array (
                'code' => '63.02.08',
                'name' => 'Kelumpang Hulu',
            ),

            array (
                'code' => '63.02.09',
                'name' => 'Kelumpang Tengah',
            ),

            array (
                'code' => '63.02.10',
                'name' => 'Kelumpang Utara',
            ),

            array (
                'code' => '63.02.11',
                'name' => 'Pamukan Selatan',
            ),

            array (
                'code' => '63.02.12',
                'name' => 'Sampanahan',
            ),

            array (
                'code' => '63.02.13',
                'name' => 'Pamukan Utara',
            ),

            array (
                'code' => '63.02.14',
                'name' => 'Hampang',
            ),

            array (
                'code' => '63.02.15',
                'name' => 'Sungaidurian',
            ),

            array (
                'code' => '63.02.16',
                'name' => 'Pulaulaut Tengah',
            ),

            array (
                'code' => '63.02.17',
                'name' => 'Kelumpang Hilir',
            ),

            array (
                'code' => '63.02.18',
                'name' => 'Kelumpang Barat',
            ),

            array (
                'code' => '63.02.19',
                'name' => 'Pamukan Barat',
            ),

            array (
                'code' => '63.02.20',
                'name' => 'Pulaulaut Kepulauan',
            ),

            array (
                'code' => '63.02.21',
                'name' => 'Pulaulaut Tanjung Selayar',
            ),

            array (
                'code' => '63.03',
                'name' => 'KAB. BANJAR',
            ),

            array (
                'code' => '63.03.01',
                'name' => 'Aluh Aluh',
            ),

            array (
                'code' => '63.03.02',
                'name' => 'Kertak Hanyar',
            ),

            array (
                'code' => '63.03.03',
                'name' => 'Gambut',
            ),

            array (
                'code' => '63.03.04',
                'name' => 'Sungai Tabuk',
            ),

            array (
                'code' => '63.03.05',
                'name' => 'Martapura',
            ),

            array (
                'code' => '63.03.06',
                'name' => 'Karang Intan',
            ),

            array (
                'code' => '63.03.07',
                'name' => 'Astambul',
            ),

            array (
                'code' => '63.03.08',
                'name' => 'Simpang Empat',
            ),

            array (
                'code' => '63.03.09',
                'name' => 'Pengarom',
            ),

            array (
                'code' => '63.03.10',
                'name' => 'Sungai Pinang',
            ),

            array (
                'code' => '63.03.11',
                'name' => 'Aranio',
            ),

            array (
                'code' => '63.03.12',
                'name' => 'Mataraman',
            ),

            array (
                'code' => '63.03.13',
                'name' => 'Beruntung Baru',
            ),

            array (
                'code' => '63.03.14',
                'name' => 'Martapura Barat',
            ),

            array (
                'code' => '63.03.15',
                'name' => 'Martapura Timur',
            ),

            array (
                'code' => '63.03.16',
                'name' => 'Sambung Makmur',
            ),

            array (
                'code' => '63.03.17',
                'name' => 'Paramasan',
            ),

            array (
                'code' => '63.03.18',
                'name' => 'Telaga Bauntung',
            ),

            array (
                'code' => '63.03.19',
                'name' => 'Tatah Makmur',
            ),

            array (
                'code' => '63.04',
                'name' => 'KAB. BARITO KUALA',
            ),

            array (
                'code' => '63.04.01',
                'name' => 'Tabunganen',
            ),

            array (
                'code' => '63.04.02',
                'name' => 'Tamban',
            ),

            array (
                'code' => '63.04.03',
                'name' => 'Anjir Pasar',
            ),

            array (
                'code' => '63.04.04',
                'name' => 'Anjir Muara',
            ),

            array (
                'code' => '63.04.05',
                'name' => 'Alalak',
            ),

            array (
                'code' => '63.04.06',
                'name' => 'Mandastana',
            ),

            array (
                'code' => '63.04.07',
                'name' => 'Rantau Badauh',
            ),

            array (
                'code' => '63.04.08',
                'name' => 'Belawang',
            ),

            array (
                'code' => '63.04.09',
                'name' => 'Cerbon',
            ),

            array (
                'code' => '63.04.10',
                'name' => 'Bakumpai',
            ),

            array (
                'code' => '63.04.11',
                'name' => 'Kuripan',
            ),

            array (
                'code' => '63.04.12',
                'name' => 'Tabukan',
            ),

            array (
                'code' => '63.04.13',
                'name' => 'Mekarsari',
            ),

            array (
                'code' => '63.04.14',
                'name' => 'Barambai',
            ),

            array (
                'code' => '63.04.15',
                'name' => 'Marabahan',
            ),

            array (
                'code' => '63.04.16',
                'name' => 'Wanaraya',
            ),

            array (
                'code' => '63.04.17',
                'name' => 'Jejangkit',
            ),

            array (
                'code' => '63.05',
                'name' => 'KAB. TAPIN',
            ),

            array (
                'code' => '63.05.01',
                'name' => 'Binuang',
            ),

            array (
                'code' => '63.05.02',
                'name' => 'Tapin Selatan',
            ),

            array (
                'code' => '63.05.03',
                'name' => 'Tapin Tengah',
            ),

            array (
                'code' => '63.05.04',
                'name' => 'Tapin Utara',
            ),

            array (
                'code' => '63.05.05',
                'name' => 'Candi Laras Selatan',
            ),

            array (
                'code' => '63.05.06',
                'name' => 'Candi Laras Utara',
            ),

            array (
                'code' => '63.05.07',
                'name' => 'Bakarangan',
            ),

            array (
                'code' => '63.05.08',
                'name' => 'Piani',
            ),

            array (
                'code' => '63.05.09',
                'name' => 'Bungur',
            ),

            array (
                'code' => '63.05.10',
                'name' => 'Lokpaikat',
            ),

            array (
                'code' => '63.05.11',
                'name' => 'Salam Babaris',
            ),

            array (
                'code' => '63.05.12',
                'name' => 'Hatungun',
            ),

            array (
                'code' => '63.06',
                'name' => 'KAB. HULU SUNGAI SELATAN',
            ),

            array (
                'code' => '63.06.01',
                'name' => 'Sungai Raya',
            ),

            array (
                'code' => '63.06.02',
                'name' => 'Padang Batung',
            ),

            array (
                'code' => '63.06.03',
                'name' => 'Telaga Langsat',
            ),

            array (
                'code' => '63.06.04',
                'name' => 'Angkinang',
            ),

            array (
                'code' => '63.06.05',
                'name' => 'Kandangan',
            ),

            array (
                'code' => '63.06.06',
                'name' => 'Simpur',
            ),

            array (
                'code' => '63.06.07',
                'name' => 'Daha Selatan',
            ),

            array (
                'code' => '63.06.08',
                'name' => 'Daha Utara',
            ),

            array (
                'code' => '63.06.09',
                'name' => 'Kalumpang',
            ),

            array (
                'code' => '63.06.10',
                'name' => 'Loksado',
            ),

            array (
                'code' => '63.06.11',
                'name' => 'Daha Barat',
            ),

            array (
                'code' => '63.07',
                'name' => 'KAB. HULU SUNGAI TENGAH',
            ),

            array (
                'code' => '63.07.01',
                'name' => 'Haruyan',
            ),

            array (
                'code' => '63.07.02',
                'name' => 'Batu Benawa',
            ),

            array (
                'code' => '63.07.03',
                'name' => 'Labuan Amas Selatan',
            ),

            array (
                'code' => '63.07.04',
                'name' => 'Labuan Amas Utara',
            ),

            array (
                'code' => '63.07.05',
                'name' => 'Pandawan',
            ),

            array (
                'code' => '63.07.06',
                'name' => 'Barabai',
            ),

            array (
                'code' => '63.07.07',
                'name' => 'Batang Alai Selatan',
            ),

            array (
                'code' => '63.07.08',
                'name' => 'Batang Alai Utara',
            ),

            array (
                'code' => '63.07.09',
                'name' => 'Hantakan',
            ),

            array (
                'code' => '63.07.10',
                'name' => 'Batang Alai Timur',
            ),

            array (
                'code' => '63.07.11',
                'name' => 'Limpasu',
            ),

            array (
                'code' => '63.08',
                'name' => 'KAB. HULU SUNGAI UTARA',
            ),

            array (
                'code' => '63.08.01',
                'name' => 'Danau Panggang',
            ),

            array (
                'code' => '63.08.02',
                'name' => 'Babirik',
            ),

            array (
                'code' => '63.08.03',
                'name' => 'Sungai Pandan',
            ),

            array (
                'code' => '63.08.04',
                'name' => 'Amuntai Selatan',
            ),

            array (
                'code' => '63.08.05',
                'name' => 'Amuntai Tengah',
            ),

            array (
                'code' => '63.08.06',
                'name' => 'Amuntai Utara',
            ),

            array (
                'code' => '63.08.07',
                'name' => 'Banjang',
            ),

            array (
                'code' => '63.08.08',
                'name' => 'Haur Gading',
            ),

            array (
                'code' => '63.08.09',
                'name' => 'Paminggir',
            ),

            array (
                'code' => '63.08.10',
                'name' => 'Sungai Tabukan',
            ),

            array (
                'code' => '63.09',
                'name' => 'KAB. TABALONG',
            ),

            array (
                'code' => '63.09.01',
                'name' => 'Banua Lawas',
            ),

            array (
                'code' => '63.09.02',
                'name' => 'Kelua',
            ),

            array (
                'code' => '63.09.03',
                'name' => 'Tanta',
            ),

            array (
                'code' => '63.09.04',
                'name' => 'Tanjung',
            ),

            array (
                'code' => '63.09.05',
                'name' => 'Haruai',
            ),

            array (
                'code' => '63.09.06',
                'name' => 'Murung Pudak',
            ),

            array (
                'code' => '63.09.07',
                'name' => 'Muara Uya',
            ),

            array (
                'code' => '63.09.08',
                'name' => 'Muara Harus',
            ),

            array (
                'code' => '63.09.09',
                'name' => 'Pugaan',
            ),

            array (
                'code' => '63.09.10',
                'name' => 'Upau',
            ),

            array (
                'code' => '63.09.11',
                'name' => 'Jaro',
            ),

            array (
                'code' => '63.09.12',
                'name' => 'Bintang Ara',
            ),

            array (
                'code' => '63.10',
                'name' => 'KAB. TANAH BUMBU',
            ),

            array (
                'code' => '63.10.01',
                'name' => 'Batu Licin',
            ),

            array (
                'code' => '63.10.02',
                'name' => 'Kusan Hilir',
            ),

            array (
                'code' => '63.10.03',
                'name' => 'Sungai Loban',
            ),

            array (
                'code' => '63.10.04',
                'name' => 'Satui',
            ),

            array (
                'code' => '63.10.05',
                'name' => 'Kusan Hulu',
            ),

            array (
                'code' => '63.10.06',
                'name' => 'Simpang Empat',
            ),

            array (
                'code' => '63.10.07',
                'name' => 'Karang Bintang',
            ),

            array (
                'code' => '63.10.08',
                'name' => 'Mantewe',
            ),

            array (
                'code' => '63.10.09',
                'name' => 'Angsana',
            ),

            array (
                'code' => '63.10.10',
                'name' => 'Kuranji',
            ),

            array (
                'code' => '63.11',
                'name' => 'KAB. BALANGAN',
            ),

            array (
                'code' => '63.11.01',
                'name' => 'Juai',
            ),

            array (
                'code' => '63.11.02',
                'name' => 'Halong',
            ),

            array (
                'code' => '63.11.03',
                'name' => 'Awayan',
            ),

            array (
                'code' => '63.11.04',
                'name' => 'Batu Mandi',
            ),

            array (
                'code' => '63.11.05',
                'name' => 'Lampihong',
            ),

            array (
                'code' => '63.11.06',
                'name' => 'Paringin',
            ),

            array (
                'code' => '63.11.07',
                'name' => 'Paringin Selatan',
            ),

            array (
                'code' => '63.11.08',
                'name' => 'Tebing Tinggi',
            ),

            array (
                'code' => '63.71',
                'name' => 'KOTA BANJARMASIN',
            ),

            array (
                'code' => '63.71.01',
                'name' => 'Banjarmasin Selatan',
            ),

            array (
                'code' => '63.71.02',
                'name' => 'Banjarmasin Timur',
            ),

            array (
                'code' => '63.71.03',
                'name' => 'Banjarmasin Barat',
            ),

            array (
                'code' => '63.71.04',
                'name' => 'Banjarmasin Utara',
            ),

            array (
                'code' => '63.71.05',
                'name' => 'Banjarmasin Tengah',
            ),

            array (
                'code' => '63.72',
                'name' => 'KOTA BANJARBARU',
            ),

            array (
                'code' => '63.72.02',
                'name' => 'Landasan Ulin',
            ),

            array (
                'code' => '63.72.03',
                'name' => 'Cempaka',
            ),

            array (
                'code' => '63.72.04',
                'name' => 'Banjarbaru Utara',
            ),

            array (
                'code' => '63.72.05',
                'name' => 'Banjarbaru Selatan',
            ),

            array (
                'code' => '63.72.06',
                'name' => 'Liang Anggang',
            ),

            array (
                'code' => '64',
                'name' => 'KALIMANTAN TIMUR',
            ),

            array (
                'code' => '64.01',
                'name' => 'KAB. PASER',
            ),

            array (
                'code' => '64.01.01',
                'name' => 'Batu Sopang',
            ),

            array (
                'code' => '64.01.02',
                'name' => 'Tanjung Harapan',
            ),

            array (
                'code' => '64.01.03',
                'name' => 'Pasir Balengkong',
            ),

            array (
                'code' => '64.01.04',
                'name' => 'Tanah Grogot',
            ),

            array (
                'code' => '64.01.05',
                'name' => 'Kuaro',
            ),

            array (
                'code' => '64.01.06',
                'name' => 'Long Ikis',
            ),

            array (
                'code' => '64.01.07',
                'name' => 'Muara Komam',
            ),

            array (
                'code' => '64.01.08',
                'name' => 'Long Kali',
            ),

            array (
                'code' => '64.01.09',
                'name' => 'Batu Engau',
            ),

            array (
                'code' => '64.01.10',
                'name' => 'Muara Samu',
            ),

            array (
                'code' => '64.02',
                'name' => 'KAB. KUTAI KARTANEGARA',
            ),

            array (
                'code' => '64.02.01',
                'name' => 'Muara Muntai',
            ),

            array (
                'code' => '64.02.02',
                'name' => 'Loa Kulu',
            ),

            array (
                'code' => '64.02.03',
                'name' => 'Loa Janan',
            ),

            array (
                'code' => '64.02.04',
                'name' => 'Anggana',
            ),

            array (
                'code' => '64.02.05',
                'name' => 'Muara Badak',
            ),

            array (
                'code' => '64.02.06',
                'name' => 'Tenggarong',
            ),

            array (
                'code' => '64.02.07',
                'name' => 'Sebulu',
            ),

            array (
                'code' => '64.02.08',
                'name' => 'Kota Bangun',
            ),

            array (
                'code' => '64.02.09',
                'name' => 'Kenohan',
            ),

            array (
                'code' => '64.02.10',
                'name' => 'Kembang Janggut',
            ),

            array (
                'code' => '64.02.11',
                'name' => 'Muara Kaman',
            ),

            array (
                'code' => '64.02.12',
                'name' => 'Tabang',
            ),

            array (
                'code' => '64.02.13',
                'name' => 'Samboja',
            ),

            array (
                'code' => '64.02.14',
                'name' => 'Muara Jawa',
            ),

            array (
                'code' => '64.02.15',
                'name' => 'Sanga Sanga',
            ),

            array (
                'code' => '64.02.16',
                'name' => 'Tenggarong Seberang',
            ),

            array (
                'code' => '64.02.17',
                'name' => 'Marang Kayu',
            ),

            array (
                'code' => '64.02.18',
                'name' => 'Muara Wis',
            ),

            array (
                'code' => '64.03',
                'name' => 'KAB. BERAU',
            ),

            array (
                'code' => '64.03.01',
                'name' => 'Kelay',
            ),

            array (
                'code' => '64.03.02',
                'name' => 'Talisayan',
            ),

            array (
                'code' => '64.03.03',
                'name' => 'Sambaliung',
            ),

            array (
                'code' => '64.03.04',
                'name' => 'Segah',
            ),

            array (
                'code' => '64.03.05',
                'name' => 'Tanjung Redeb',
            ),

            array (
                'code' => '64.03.06',
                'name' => 'Gunung Tabur',
            ),

            array (
                'code' => '64.03.07',
                'name' => 'Pulau Derawan',
            ),

            array (
                'code' => '64.03.08',
                'name' => 'Biduk-Biduk',
            ),

            array (
                'code' => '64.03.09',
                'name' => 'Teluk Bayur',
            ),

            array (
                'code' => '64.03.10',
                'name' => 'Tabalar',
            ),

            array (
                'code' => '64.03.11',
                'name' => 'Maratua',
            ),

            array (
                'code' => '64.03.12',
                'name' => 'Batu Putih',
            ),

            array (
                'code' => '64.03.13',
                'name' => 'Biatan',
            ),

            array (
                'code' => '64.07',
                'name' => 'KAB. KUTAI BARAT',
            ),

            array (
                'code' => '64.07.05',
                'name' => 'Long Iram',
            ),

            array (
                'code' => '64.07.06',
                'name' => 'Melak',
            ),

            array (
                'code' => '64.07.07',
                'name' => 'Barong Tongkok',
            ),

            array (
                'code' => '64.07.08',
                'name' => 'Damai',
            ),

            array (
                'code' => '64.07.09',
                'name' => 'Muara Lawa',
            ),

            array (
                'code' => '64.07.10',
                'name' => 'Muara Pahu',
            ),

            array (
                'code' => '64.07.11',
                'name' => 'Jempang',
            ),

            array (
                'code' => '64.07.12',
                'name' => 'Bongan',
            ),

            array (
                'code' => '64.07.13',
                'name' => 'Penyinggahan',
            ),

            array (
                'code' => '64.07.14',
                'name' => 'Bentian Besar',
            ),

            array (
                'code' => '64.07.15',
                'name' => 'Linggang Bigung',
            ),

            array (
                'code' => '64.07.16',
                'name' => 'Nyuatan',
            ),

            array (
                'code' => '64.07.17',
                'name' => 'Siluq Ngurai',
            ),

            array (
                'code' => '64.07.18',
                'name' => 'Mook Manaar Bulatn',
            ),

            array (
                'code' => '64.07.19',
                'name' => 'Tering',
            ),

            array (
                'code' => '64.07.20',
                'name' => 'Sekolaq Darat',
            ),

            array (
                'code' => '64.08',
                'name' => 'KAB. KUTAI TIMUR',
            ),

            array (
                'code' => '64.08.01',
                'name' => 'Muara Ancalong',
            ),

            array (
                'code' => '64.08.02',
                'name' => 'Muara Wahau',
            ),

            array (
                'code' => '64.08.03',
                'name' => 'Muara Bengkal',
            ),

            array (
                'code' => '64.08.04',
                'name' => 'Sangatta Utara',
            ),

            array (
                'code' => '64.08.05',
                'name' => 'Sangkulirang',
            ),

            array (
                'code' => '64.08.06',
                'name' => 'Busang',
            ),

            array (
                'code' => '64.08.07',
                'name' => 'Telen',
            ),

            array (
                'code' => '64.08.08',
                'name' => 'Kombeng',
            ),

            array (
                'code' => '64.08.09',
                'name' => 'Bengalon',
            ),

            array (
                'code' => '64.08.10',
                'name' => 'Kaliorang',
            ),

            array (
                'code' => '64.08.11',
                'name' => 'Sandaran',
            ),

            array (
                'code' => '64.08.12',
                'name' => 'Sangatta Selatan',
            ),

            array (
                'code' => '64.08.13',
                'name' => 'Teluk Pandan',
            ),

            array (
                'code' => '64.08.14',
                'name' => 'Rantau Pulung',
            ),

            array (
                'code' => '64.08.15',
                'name' => 'Kaubun',
            ),

            array (
                'code' => '64.08.16',
                'name' => 'Karangan',
            ),

            array (
                'code' => '64.08.17',
                'name' => 'Batu Ampar',
            ),

            array (
                'code' => '64.08.18',
                'name' => 'Long Mesangat',
            ),

            array (
                'code' => '64.09',
                'name' => 'KAB. PENAJAM PASER UTARA',
            ),

            array (
                'code' => '64.09.01',
                'name' => 'Penajam',
            ),

            array (
                'code' => '64.09.02',
                'name' => 'Waru',
            ),

            array (
                'code' => '64.09.03',
                'name' => 'Babulu',
            ),

            array (
                'code' => '64.09.04',
                'name' => 'Sepaku',
            ),

            array (
                'code' => '64.11',
                'name' => 'KAB. MAHAKAM ULU',
            ),

            array (
                'code' => '64.11.01',
                'name' => 'Long Bagun',
            ),

            array (
                'code' => '64.11.02',
                'name' => 'Long Hubung',
            ),

            array (
                'code' => '64.11.03',
                'name' => 'Laham',
            ),

            array (
                'code' => '64.11.04',
                'name' => 'Long Apari',
            ),

            array (
                'code' => '64.11.05',
                'name' => 'Long Pahangai',
            ),

            array (
                'code' => '64.71',
                'name' => 'KOTA BALIKPAPAN',
            ),

            array (
                'code' => '64.71.01',
                'name' => 'Balikpapan Timur',
            ),

            array (
                'code' => '64.71.02',
                'name' => 'Balikpapan Barat',
            ),

            array (
                'code' => '64.71.03',
                'name' => 'Balikpapan Utara',
            ),

            array (
                'code' => '64.71.04',
                'name' => 'Balikpapan Tengah',
            ),

            array (
                'code' => '64.71.05',
                'name' => 'Balikpapan Selatan',
            ),

            array (
                'code' => '64.71.06',
                'name' => 'Balikpapan Kota',
            ),

            array (
                'code' => '64.72',
                'name' => 'KOTA SAMARINDA',
            ),

            array (
                'code' => '64.72.01',
                'name' => 'Palaran',
            ),

            array (
                'code' => '64.72.02',
                'name' => 'Samarinda Seberang',
            ),

            array (
                'code' => '64.72.03',
                'name' => 'Samarinda Ulu',
            ),

            array (
                'code' => '64.72.04',
                'name' => 'Samarinda Ilir',
            ),

            array (
                'code' => '64.72.05',
                'name' => 'Samarinda Utara',
            ),

            array (
                'code' => '64.72.06',
                'name' => 'Sungai Kunjang',
            ),

            array (
                'code' => '64.72.07',
                'name' => 'Sambutan',
            ),

            array (
                'code' => '64.72.08',
                'name' => 'Sungai Pinang',
            ),

            array (
                'code' => '64.72.09',
                'name' => 'Samarinda Kota',
            ),

            array (
                'code' => '64.72.10',
                'name' => 'Loa Janan Ilir',
            ),

            array (
                'code' => '64.74',
                'name' => 'KOTA BONTANG',
            ),

            array (
                'code' => '64.74.01',
                'name' => 'Bontang Utara',
            ),

            array (
                'code' => '64.74.02',
                'name' => 'Bontang Selatan',
            ),

            array (
                'code' => '64.74.03',
                'name' => 'Bontang Barat',
            ),

            array (
                'code' => '65',
                'name' => 'KALIMANTAN UTARA',
            ),

            array (
                'code' => '65.01',
                'name' => 'KAB. BULUNGAN',
            ),

            array (
                'code' => '65.01.01',
                'name' => 'Tanjung Palas',
            ),

            array (
                'code' => '65.01.02',
                'name' => 'Tanjung Palas Barat',
            ),

            array (
                'code' => '65.01.03',
                'name' => 'Tanjung Palas Utara',
            ),

            array (
                'code' => '65.01.04',
                'name' => 'Tanjung Palas Timur',
            ),

            array (
                'code' => '65.01.05',
                'name' => 'Tanjung Selor',
            ),

            array (
                'code' => '65.01.06',
                'name' => 'Tanjung Palas Tengah',
            ),

            array (
                'code' => '65.01.07',
                'name' => 'Peso',
            ),

            array (
                'code' => '65.01.08',
                'name' => 'Peso Hilir',
            ),

            array (
                'code' => '65.01.09',
                'name' => 'Sekatak',
            ),

            array (
                'code' => '65.01.10',
                'name' => 'Bunyu',
            ),

            array (
                'code' => '65.02',
                'name' => 'KAB. MALINAU',
            ),

            array (
                'code' => '65.02.01',
                'name' => 'Mentarang',
            ),

            array (
                'code' => '65.02.02',
                'name' => 'Malinau Kota',
            ),

            array (
                'code' => '65.02.03',
                'name' => 'Pujungan',
            ),

            array (
                'code' => '65.02.04',
                'name' => 'Kayan Hilir',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '65.02.05',
                'name' => 'Kayan Hulu',
            ),

            array (
                'code' => '65.02.06',
                'name' => 'Malinau Selatan',
            ),

            array (
                'code' => '65.02.07',
                'name' => 'Malinau Utara',
            ),

            array (
                'code' => '65.02.08',
                'name' => 'Malinau Barat',
            ),

            array (
                'code' => '65.02.09',
                'name' => 'Sungai Boh',
            ),

            array (
                'code' => '65.02.10',
                'name' => 'Kayan Selatan',
            ),

            array (
                'code' => '65.02.11',
                'name' => 'Bahau Hulu',
            ),

            array (
                'code' => '65.02.12',
                'name' => 'Mentarang Hulu',
            ),

            array (
                'code' => '65.02.13',
                'name' => 'Malinau Selatan Hilir',
            ),

            array (
                'code' => '65.02.14',
                'name' => 'Malinau Selatan Hulu',
            ),

            array (
                'code' => '65.02.15',
                'name' => 'Sungai Tubu',
            ),

            array (
                'code' => '65.03',
                'name' => 'KAB. NUNUKAN',
            ),

            array (
                'code' => '65.03.01',
                'name' => 'Sebatik',
            ),

            array (
                'code' => '65.03.02',
                'name' => 'Nunukan',
            ),

            array (
                'code' => '65.03.03',
                'name' => 'Sembakung',
            ),

            array (
                'code' => '65.03.04',
                'name' => 'Lumbis',
            ),

            array (
                'code' => '65.03.05',
                'name' => 'Krayan',
            ),

            array (
                'code' => '65.03.06',
                'name' => 'Sebuku',
            ),

            array (
                'code' => '65.03.07',
                'name' => 'Krayan Selatan',
            ),

            array (
                'code' => '65.03.08',
                'name' => 'Sebatik Barat',
            ),

            array (
                'code' => '65.03.09',
                'name' => 'Nunukan Selatan',
            ),

            array (
                'code' => '65.03.10',
                'name' => 'Sebatik Timur',
            ),

            array (
                'code' => '65.03.11',
                'name' => 'Sebatik Utara',
            ),

            array (
                'code' => '65.03.12',
                'name' => 'Sebatik Tengah',
            ),

            array (
                'code' => '65.03.13',
                'name' => 'Sei Menggaris',
            ),

            array (
                'code' => '65.03.14',
                'name' => 'Tulin Onsoi',
            ),

            array (
                'code' => '65.03.15',
                'name' => 'Lumbis Ogong',
            ),

            array (
                'code' => '65.03.16',
                'name' => 'Sembakung Atulai',
            ),

            array (
                'code' => '65.04',
                'name' => 'KAB. TANA TIDUNG',
            ),

            array (
                'code' => '65.04.01',
                'name' => 'Sesayap',
            ),

            array (
                'code' => '65.04.02',
                'name' => 'Sesayap Hilir',
            ),

            array (
                'code' => '65.04.03',
                'name' => 'Tana Lia',
            ),

            array (
                'code' => '65.04.04',
                'name' => 'Betayau',
            ),

            array (
                'code' => '65.04.05',
                'name' => 'Muruk Rian',
            ),

            array (
                'code' => '65.71',
                'name' => 'KOTA TARAKAN',
            ),

            array (
                'code' => '65.71.01',
                'name' => 'Tarakan Barat',
            ),

            array (
                'code' => '65.71.02',
                'name' => 'Tarakan Tengah',
            ),

            array (
                'code' => '65.71.03',
                'name' => 'Tarakan Timur',
            ),

            array (
                'code' => '65.71.04',
                'name' => 'Tarakan Utara',
            ),

            array (
                'code' => '71',
                'name' => 'SULAWESI UTARA',
            ),

            array (
                'code' => '71.01',
                'name' => 'KAB. BOLAANG MONGONDOW',
            ),

            array (
                'code' => '71.01.05',
                'name' => 'Sang Tombolang',
            ),

            array (
                'code' => '71.01.09',
                'name' => 'Dumoga Barat',
            ),

            array (
                'code' => '71.01.10',
                'name' => 'Dumoga Timur',
            ),

            array (
                'code' => '71.01.11',
                'name' => 'Dumoga Utara',
            ),

            array (
                'code' => '71.01.12',
                'name' => 'Lolak',
            ),

            array (
                'code' => '71.01.13',
                'name' => 'Bolaang',
            ),

            array (
                'code' => '71.01.14',
                'name' => 'Lolayan',
            ),

            array (
                'code' => '71.01.19',
                'name' => 'Passi Barat',
            ),

            array (
                'code' => '71.01.20',
                'name' => 'Poigar',
            ),

            array (
                'code' => '71.01.22',
                'name' => 'Passi Timur',
            ),

            array (
                'code' => '71.01.31',
                'name' => 'Bolaang Timur',
            ),

            array (
                'code' => '71.01.32',
                'name' => 'Bilalang',
            ),

            array (
                'code' => '71.01.33',
                'name' => 'Dumoga',
            ),

            array (
                'code' => '71.01.34',
                'name' => 'Dumoga Tenggara',
            ),

            array (
                'code' => '71.01.35',
                'name' => 'Dumoga Tengah',
            ),

            array (
                'code' => '71.02',
                'name' => 'KAB. MINAHASA',
            ),

            array (
                'code' => '71.02.01',
                'name' => 'Tondano Barat',
            ),

            array (
                'code' => '71.02.02',
                'name' => 'Tondano Timur',
            ),

            array (
                'code' => '71.02.03',
                'name' => 'Eris',
            ),

            array (
                'code' => '71.02.04',
                'name' => 'Kombi',
            ),

            array (
                'code' => '71.02.05',
                'name' => 'Lembean Timur',
            ),

            array (
                'code' => '71.02.06',
                'name' => 'Kakas',
            ),

            array (
                'code' => '71.02.07',
                'name' => 'Tompaso',
            ),

            array (
                'code' => '71.02.08',
                'name' => 'Remboken',
            ),

            array (
                'code' => '71.02.09',
                'name' => 'Langowan Timur',
            ),

            array (
                'code' => '71.02.10',
                'name' => 'Langowan Barat',
            ),

            array (
                'code' => '71.02.11',
                'name' => 'Sonder',
            ),

            array (
                'code' => '71.02.12',
                'name' => 'Kawangkoan',
            ),

            array (
                'code' => '71.02.13',
                'name' => 'Pineleng',
            ),

            array (
                'code' => '71.02.14',
                'name' => 'Tombulu',
            ),

            array (
                'code' => '71.02.15',
                'name' => 'Tombariri',
            ),

            array (
                'code' => '71.02.16',
                'name' => 'Tondano Utara',
            ),

            array (
                'code' => '71.02.17',
                'name' => 'Langowan Selatan',
            ),

            array (
                'code' => '71.02.18',
                'name' => 'Tondano Selatan',
            ),

            array (
                'code' => '71.02.19',
                'name' => 'Langowan Utara',
            ),

            array (
                'code' => '71.02.20',
                'name' => 'Kakas Barat',
            ),

            array (
                'code' => '71.02.21',
                'name' => 'Kawangkoan Utara',
            ),

            array (
                'code' => '71.02.22',
                'name' => 'Kawangkoan Barat',
            ),

            array (
                'code' => '71.02.23',
                'name' => 'Mandolang',
            ),

            array (
                'code' => '71.02.24',
                'name' => 'Tombariri Timur',
            ),

            array (
                'code' => '71.02.25',
                'name' => 'Tompaso Barat',
            ),

            array (
                'code' => '71.03',
                'name' => 'KAB. KEPULAUAN SANGIHE',
            ),

            array (
                'code' => '71.03.08',
                'name' => 'Tabukan Utara',
            ),

            array (
                'code' => '71.03.09',
                'name' => 'Nusa Tabukan',
            ),

            array (
                'code' => '71.03.10',
                'name' => 'Manganitu Selatan',
            ),

            array (
                'code' => '71.03.11',
                'name' => 'Tatoareng',
            ),

            array (
                'code' => '71.03.12',
                'name' => 'Tamako',
            ),

            array (
                'code' => '71.03.13',
                'name' => 'Manganitu',
            ),

            array (
                'code' => '71.03.14',
                'name' => 'Tabukan Tengah',
            ),

            array (
                'code' => '71.03.15',
                'name' => 'Tabukan Selatan',
            ),

            array (
                'code' => '71.03.16',
                'name' => 'Kendahe',
            ),

            array (
                'code' => '71.03.17',
                'name' => 'Tahuna',
            ),

            array (
                'code' => '71.03.19',
                'name' => 'Tabukan Selatan Tengah',
            ),

            array (
                'code' => '71.03.20',
                'name' => 'Tabukan Selatan Tenggara',
            ),

            array (
                'code' => '71.03.23',
                'name' => 'Tahuna Barat',
            ),

            array (
                'code' => '71.03.24',
                'name' => 'Tahuna Timur',
            ),

            array (
                'code' => '71.03.25',
                'name' => 'Kepulauan Marore',
            ),

            array (
                'code' => '71.04',
                'name' => 'KAB. KEPULAUAN TALAUD',
            ),

            array (
                'code' => '71.04.01',
                'name' => 'Lirung',
            ),

            array (
                'code' => '71.04.02',
                'name' => 'Beo',
            ),

            array (
                'code' => '71.04.03',
                'name' => 'Rainis',
            ),

            array (
                'code' => '71.04.04',
                'name' => 'Essang',
            ),

            array (
                'code' => '71.04.05',
                'name' => 'Nanusa',
            ),

            array (
                'code' => '71.04.06',
                'name' => 'Kabaruan',
            ),

            array (
                'code' => '71.04.07',
                'name' => 'Melonguane',
            ),

            array (
                'code' => '71.04.08',
                'name' => 'Gemeh',
            ),

            array (
                'code' => '71.04.09',
                'name' => 'Damau',
            ),

            array (
                'code' => '71.04.10',
                'name' => 'Tampan\' Amma  11',
            ),

            array (
                'code' => '71.04.11',
                'name' => 'Salibabu',
            ),

            array (
                'code' => '71.04.12',
                'name' => 'Kalongan',
            ),

            array (
                'code' => '71.04.13',
                'name' => 'Miangas',
            ),

            array (
                'code' => '71.04.14',
                'name' => 'Beo Utara',
            ),

            array (
                'code' => '71.04.15',
                'name' => 'Pulutan',
            ),

            array (
                'code' => '71.04.16',
                'name' => 'Melonguane Timur',
            ),

            array (
                'code' => '71.04.17',
                'name' => 'Moronge',
            ),

            array (
                'code' => '71.04.18',
                'name' => 'Beo Selatan',
            ),

            array (
                'code' => '71.04.19',
                'name' => 'Essang Selatan',
            ),

            array (
                'code' => '71.05',
                'name' => 'KAB. MINAHASA SELATAN',
            ),

            array (
                'code' => '71.05.01',
                'name' => 'Modoinding',
            ),

            array (
                'code' => '71.05.02',
                'name' => 'Tompaso Baru',
            ),

            array (
                'code' => '71.05.03',
                'name' => 'Ranoyapo',
            ),

            array (
                'code' => '71.05.07',
                'name' => 'Motoling',
            ),

            array (
                'code' => '71.05.08',
                'name' => 'Sinonsayang',
            ),

            array (
                'code' => '71.05.09',
                'name' => 'Tenga',
            ),

            array (
                'code' => '71.05.10',
                'name' => 'Amurang',
            ),

            array (
                'code' => '71.05.12',
                'name' => 'Tumpaan',
            ),

            array (
                'code' => '71.05.13',
                'name' => 'Tareran',
            ),

            array (
                'code' => '71.05.15',
                'name' => 'Kumelembuai',
            ),

            array (
                'code' => '71.05.16',
                'name' => 'Maesaan',
            ),

            array (
                'code' => '71.05.17',
                'name' => 'Amurang Barat',
            ),

            array (
                'code' => '71.05.18',
                'name' => 'Amurang Timur',
            ),

            array (
                'code' => '71.05.19',
                'name' => 'Tatapaan',
            ),

            array (
                'code' => '71.05.21',
                'name' => 'Motoling Barat',
            ),

            array (
                'code' => '71.05.22',
                'name' => 'Motoling Timur',
            ),

            array (
                'code' => '71.05.23',
                'name' => 'Suluun Tareran',
            ),

            array (
                'code' => '71.06',
                'name' => 'KAB. MINAHASA UTARA',
            ),

            array (
                'code' => '71.06.01',
                'name' => 'Kema',
            ),

            array (
                'code' => '71.06.02',
                'name' => 'Kauditan',
            ),

            array (
                'code' => '71.06.03',
                'name' => 'Airmadidi',
            ),

            array (
                'code' => '71.06.04',
                'name' => 'Wori',
            ),

            array (
                'code' => '71.06.05',
                'name' => 'Dimembe',
            ),

            array (
                'code' => '71.06.06',
                'name' => 'Likupang Barat',
            ),

            array (
                'code' => '71.06.07',
                'name' => 'Likupang Timur',
            ),

            array (
                'code' => '71.06.08',
                'name' => 'Kalawat',
            ),

            array (
                'code' => '71.06.09',
                'name' => 'Talawaan',
            ),

            array (
                'code' => '71.06.10',
                'name' => 'Likupang Selatan',
            ),

            array (
                'code' => '71.07',
                'name' => 'KAB. MINAHASA TENGGARA',
            ),

            array (
                'code' => '71.07.01',
                'name' => 'Ratahan',
            ),

            array (
                'code' => '71.07.02',
                'name' => 'Pusomaen',
            ),

            array (
                'code' => '71.07.03',
                'name' => 'Belang',
            ),

            array (
                'code' => '71.07.04',
                'name' => 'Ratatotok',
            ),

            array (
                'code' => '71.07.05',
                'name' => 'Tombatu',
            ),

            array (
                'code' => '71.07.06',
                'name' => 'Touluaan',
            ),

            array (
                'code' => '71.07.07',
                'name' => 'Touluaan Selatan',
            ),

            array (
                'code' => '71.07.08',
                'name' => 'Silian Raya',
            ),

            array (
                'code' => '71.07.09',
                'name' => 'Tombatu Timur',
            ),

            array (
                'code' => '71.07.10',
                'name' => 'Tombatu Utara',
            ),

            array (
                'code' => '71.07.11',
                'name' => 'Pasan',
            ),

            array (
                'code' => '71.07.12',
                'name' => 'Ratahan Timur',
            ),

            array (
                'code' => '71.08',
                'name' => 'KAB. BOLAANG MONGONDOW UTARA',
            ),

            array (
                'code' => '71.08.01',
                'name' => 'Sangkub',
            ),

            array (
                'code' => '71.08.02',
                'name' => 'Bintauna',
            ),

            array (
                'code' => '71.08.03',
                'name' => 'Bolangitang Timur',
            ),

            array (
                'code' => '71.08.04',
                'name' => 'Bolangitang Barat',
            ),

            array (
                'code' => '71.08.05',
                'name' => 'Kaidipang',
            ),

            array (
                'code' => '71.08.06',
                'name' => 'Pinogaluman',
            ),

            array (
                'code' => '71.09',
                'name' => 'KAB. KEP. SIAU TAGULANDANG BIARO',
            ),

            array (
                'code' => '71.09.01',
                'name' => 'Siau Timur',
            ),

            array (
                'code' => '71.09.02',
                'name' => 'Siau Barat',
            ),

            array (
                'code' => '71.09.03',
                'name' => 'Tagulandang',
            ),

            array (
                'code' => '71.09.04',
                'name' => 'Siau Timur Selatan',
            ),

            array (
                'code' => '71.09.05',
                'name' => 'Siau Barat Selatan',
            ),

            array (
                'code' => '71.09.06',
                'name' => 'Tagulandang Utara',
            ),

            array (
                'code' => '71.09.07',
                'name' => 'Biaro',
            ),

            array (
                'code' => '71.09.08',
                'name' => 'Siau Barat Utara',
            ),

            array (
                'code' => '71.09.09',
                'name' => 'Siau Tengah',
            ),

            array (
                'code' => '71.09.10',
                'name' => 'Tagulandang Selatan',
            ),

            array (
                'code' => '71.10',
                'name' => 'KAB. BOLAANG MONGONDOW TIMUR',
            ),

            array (
                'code' => '71.10.01',
                'name' => 'Tutuyan',
            ),

            array (
                'code' => '71.10.02',
                'name' => 'Kotabunan',
            ),

            array (
                'code' => '71.10.03',
                'name' => 'Nuangan',
            ),

            array (
                'code' => '71.10.04',
                'name' => 'Modayag',
            ),

            array (
                'code' => '71.10.05',
                'name' => 'Modayag Barat',
            ),

            array (
                'code' => '71.11',
                'name' => 'KAB. BOLAANG MONGONDOW SELATAN',
            ),

            array (
                'code' => '71.11.01',
                'name' => 'Bolaang Uki',
            ),

            array (
                'code' => '71.11.02',
                'name' => 'Posigadan',
            ),

            array (
                'code' => '71.11.03',
                'name' => 'Pinolosian',
            ),

            array (
                'code' => '71.11.04',
                'name' => 'Pinolosian Tengah',
            ),

            array (
                'code' => '71.11.05',
                'name' => 'Pinolosian Timur',
            ),

            array (
                'code' => '71.71',
                'name' => 'KOTA MANADO',
            ),

            array (
                'code' => '71.71.01',
                'name' => 'Bunaken',
            ),

            array (
                'code' => '71.71.02',
                'name' => 'Tuminiting',
            ),

            array (
                'code' => '71.71.03',
                'name' => 'Singkil',
            ),

            array (
                'code' => '71.71.04',
                'name' => 'Wenang',
            ),

            array (
                'code' => '71.71.05',
                'name' => 'Tikala',
            ),

            array (
                'code' => '71.71.06',
                'name' => 'Sario',
            ),

            array (
                'code' => '71.71.07',
                'name' => 'Wanea',
            ),

            array (
                'code' => '71.71.08',
                'name' => 'Mapanget',
            ),

            array (
                'code' => '71.71.09',
                'name' => 'Malalayang',
            ),

            array (
                'code' => '71.71.10',
                'name' => 'Bunaken Kepulauan',
            ),

            array (
                'code' => '71.71.11',
                'name' => 'Paal Dua',
            ),

            array (
                'code' => '71.72',
                'name' => 'KOTA BITUNG',
            ),

            array (
                'code' => '71.72.01',
                'name' => 'Lembeh Selatan',
            ),

            array (
                'code' => '71.72.02',
                'name' => 'Madidir',
            ),

            array (
                'code' => '71.72.03',
                'name' => 'Ranowulu',
            ),

            array (
                'code' => '71.72.04',
                'name' => 'Aertembaga',
            ),

            array (
                'code' => '71.72.05',
                'name' => 'Matuari',
            ),

            array (
                'code' => '71.72.06',
                'name' => 'Girian',
            ),

            array (
                'code' => '71.72.07',
                'name' => 'Maesa',
            ),

            array (
                'code' => '71.72.08',
                'name' => 'Lembeh Utara',
            ),

            array (
                'code' => '71.73',
                'name' => 'KOTA TOMOHON',
            ),

            array (
                'code' => '71.73.01',
                'name' => 'Tomohon Selatan',
            ),

            array (
                'code' => '71.73.02',
                'name' => 'Tomohon Tengah',
            ),

            array (
                'code' => '71.73.03',
                'name' => 'Tomohon Utara',
            ),

            array (
                'code' => '71.73.04',
                'name' => 'Tomohon Barat',
            ),

            array (
                'code' => '71.73.05',
                'name' => 'Tomohon Timur',
            ),

            array (
                'code' => '71.74',
                'name' => 'KOTA KOTAMOBAGU',
            ),

            array (
                'code' => '71.74.01',
                'name' => 'Kotamobagu Utara',
            ),

            array (
                'code' => '71.74.02',
                'name' => 'Kotamobagu Timur',
            ),

            array (
                'code' => '71.74.03',
                'name' => 'Kotamobagu Selatan',
            ),

            array (
                'code' => '71.74.04',
                'name' => 'Kotamobagu Barat',
            ),

            array (
                'code' => '72',
                'name' => 'SULAWESI TENGAH',
            ),

            array (
                'code' => '72.01',
                'name' => 'KAB. BANGGAI',
            ),

            array (
                'code' => '72.01.01',
                'name' => 'Batui',
            ),

            array (
                'code' => '72.01.02',
                'name' => 'Bunta',
            ),

            array (
                'code' => '72.01.03',
                'name' => 'Kintom',
            ),

            array (
                'code' => '72.01.04',
                'name' => 'Luwuk',
            ),

            array (
                'code' => '72.01.05',
                'name' => 'Lamala',
            ),

            array (
                'code' => '72.01.06',
                'name' => 'Balantak',
            ),

            array (
                'code' => '72.01.07',
                'name' => 'Pagimana',
            ),

            array (
                'code' => '72.01.08',
                'name' => 'Bualemo',
            ),

            array (
                'code' => '72.01.09',
                'name' => 'Toili',
            ),

            array (
                'code' => '72.01.10',
                'name' => 'Masama',
            ),

            array (
                'code' => '72.01.11',
                'name' => 'Luwuk Timur',
            ),

            array (
                'code' => '72.01.12',
                'name' => 'Toili Barat',
            ),

            array (
                'code' => '72.01.13',
                'name' => 'Nuhon',
            ),

            array (
                'code' => '72.01.14',
                'name' => 'Moilong',
            ),

            array (
                'code' => '72.01.15',
                'name' => 'Batui Selatan',
            ),

            array (
                'code' => '72.01.16',
                'name' => 'Lobu',
            ),

            array (
                'code' => '72.01.17',
                'name' => 'Simpang Raya',
            ),

            array (
                'code' => '72.01.18',
                'name' => 'Balantak Selatan',
            ),

            array (
                'code' => '72.01.19',
                'name' => 'Balantak Utara',
            ),

            array (
                'code' => '72.01.20',
                'name' => 'Luwuk Selatan',
            ),

            array (
                'code' => '72.01.21',
                'name' => 'Luwuk Utara',
            ),

            array (
                'code' => '72.01.22',
                'name' => 'Mantoh',
            ),

            array (
                'code' => '72.01.23',
                'name' => 'Nambo',
            ),

            array (
                'code' => '72.02',
                'name' => 'KAB. POSO',
            ),

            array (
                'code' => '72.02.01',
                'name' => 'Poso Kota',
            ),

            array (
                'code' => '72.02.02',
                'name' => 'Poso Pesisir',
            ),

            array (
                'code' => '72.02.03',
                'name' => 'Lage',
            ),

            array (
                'code' => '72.02.04',
                'name' => 'Pamona Puselemba',
            ),

            array (
                'code' => '72.02.05',
                'name' => 'Pamona Timur',
            ),

            array (
                'code' => '72.02.06',
                'name' => 'Pamona Selatan',
            ),

            array (
                'code' => '72.02.07',
                'name' => 'Lore Utara',
            ),

            array (
                'code' => '72.02.08',
                'name' => 'Lore Tengah',
            ),

            array (
                'code' => '72.02.09',
                'name' => 'Lore Selatan',
            ),

            array (
                'code' => '72.02.18',
                'name' => 'Poso Pesisir Utara',
            ),

            array (
                'code' => '72.02.19',
                'name' => 'Poso Pesisir Selatan',
            ),

            array (
                'code' => '72.02.20',
                'name' => 'Pamona Barat',
            ),

            array (
                'code' => '72.02.21',
                'name' => 'Poso Kota Selatan',
            ),

            array (
                'code' => '72.02.22',
                'name' => 'Poso Kota Utara',
            ),

            array (
                'code' => '72.02.23',
                'name' => 'Lore Barat',
            ),

            array (
                'code' => '72.02.24',
                'name' => 'Lore Timur',
            ),

            array (
                'code' => '72.02.25',
                'name' => 'Lore Piore',
            ),

            array (
                'code' => '72.02.26',
                'name' => 'Pamona Tenggara',
            ),

            array (
                'code' => '72.02.27',
                'name' => 'Pamona Utara',
            ),

            array (
                'code' => '72.03',
                'name' => 'KAB. DONGGALA',
            ),

            array (
                'code' => '72.03.04',
                'name' => 'Rio Pakava',
            ),

            array (
                'code' => '72.03.06',
                'name' => 'Dampelas',
            ),

            array (
                'code' => '72.03.08',
                'name' => 'Banawa',
            ),

            array (
                'code' => '72.03.09',
                'name' => 'Labuan',
            ),

            array (
                'code' => '72.03.10',
                'name' => 'Sindue',
            ),

            array (
                'code' => '72.03.11',
                'name' => 'Sirenja',
            ),

            array (
                'code' => '72.03.12',
                'name' => 'Balaesang',
            ),

            array (
                'code' => '72.03.14',
                'name' => 'Sojol',
            ),

            array (
                'code' => '72.03.18',
                'name' => 'Banawa Selatan',
            ),

            array (
                'code' => '72.03.19',
                'name' => 'Tanantovea',
            ),

            array (
                'code' => '72.03.21',
                'name' => 'Pinembani',
            ),

            array (
                'code' => '72.03.24',
                'name' => 'Sindue Tombusabora',
            ),

            array (
                'code' => '72.03.25',
                'name' => 'Sindue Tobata',
            ),

            array (
                'code' => '72.03.27',
                'name' => 'Banawa Tengah',
            ),

            array (
                'code' => '72.03.30',
                'name' => 'Sojol Utara',
            ),

            array (
                'code' => '72.03.31',
                'name' => 'Balaesang Tanjung',
            ),

            array (
                'code' => '72.04',
                'name' => 'KAB. TOLI TOLI',
            ),

            array (
                'code' => '72.04.01',
                'name' => 'Dampal Selatan',
            ),

            array (
                'code' => '72.04.02',
                'name' => 'Dampal Utara',
            ),

            array (
                'code' => '72.04.03',
                'name' => 'Dondo',
            ),

            array (
                'code' => '72.04.04',
                'name' => 'Basidondo',
            ),

            array (
                'code' => '72.04.05',
                'name' => 'Ogodeide',
            ),

            array (
                'code' => '72.04.06',
                'name' => 'Lampasio',
            ),

            array (
                'code' => '72.04.07',
                'name' => 'Baolan',
            ),

            array (
                'code' => '72.04.08',
                'name' => 'Galang',
            ),

            array (
                'code' => '72.04.09',
                'name' => 'Toli-Toli Utara',
            ),

            array (
                'code' => '72.04.10',
                'name' => 'Dako Pemean',
            ),

            array (
                'code' => '72.05',
                'name' => 'KAB. BUOL',
            ),

            array (
                'code' => '72.05.01',
                'name' => 'Momunu',
            ),

            array (
                'code' => '72.05.02',
                'name' => 'Lakea',
            ),

            array (
                'code' => '72.05.03',
                'name' => 'Bokat',
            ),

            array (
                'code' => '72.05.04',
                'name' => 'Bunobogu',
            ),

            array (
                'code' => '72.05.05',
                'name' => 'Paleleh',
            ),

            array (
                'code' => '72.05.06',
                'name' => 'Biau',
            ),

            array (
                'code' => '72.05.07',
                'name' => 'Tiloan',
            ),

            array (
                'code' => '72.05.08',
                'name' => 'Bukal',
            ),

            array (
                'code' => '72.05.09',
                'name' => 'Gadung',
            ),

            array (
                'code' => '72.05.10',
                'name' => 'Karamat',
            ),

            array (
                'code' => '72.05.11',
                'name' => 'Paleleh Barat',
            ),

            array (
                'code' => '72.06',
                'name' => 'KAB. MOROWALI',
            ),

            array (
                'code' => '72.06.05',
                'name' => 'Bungku Tengah',
            ),

            array (
                'code' => '72.06.06',
                'name' => 'Bungku Selatan',
            ),

            array (
                'code' => '72.06.07',
                'name' => 'Menui Kepulauan',
            ),

            array (
                'code' => '72.06.08',
                'name' => 'Bungku Barat',
            ),

            array (
                'code' => '72.06.09',
                'name' => 'Bumi Raya',
            ),

            array (
                'code' => '72.06.10',
                'name' => 'Bahodopi',
            ),

            array (
                'code' => '72.06.12',
                'name' => 'Wita Ponda',
            ),

            array (
                'code' => '72.06.15',
                'name' => 'Bungku Pesisir',
            ),

            array (
                'code' => '72.06.18',
                'name' => 'Bungku Timur',
            ),

            array (
                'code' => '72.07',
                'name' => 'KAB. BANGGAI KEPULAUAN',
            ),

            array (
                'code' => '72.07.03',
                'name' => 'Totikum',
            ),

            array (
                'code' => '72.07.04',
                'name' => 'Tinangkung',
            ),

            array (
                'code' => '72.07.05',
                'name' => 'Liang',
            ),

            array (
                'code' => '72.07.06',
                'name' => 'Bulagi',
            ),

            array (
                'code' => '72.07.07',
                'name' => 'Buko',
            ),

            array (
                'code' => '72.07.09',
                'name' => 'Bulagi Selatan',
            ),

            array (
                'code' => '72.07.11',
                'name' => 'Tinangkung Selatan',
            ),

            array (
                'code' => '72.07.15',
                'name' => 'Totikum Selatan',
            ),

            array (
                'code' => '72.07.16',
                'name' => 'Peling Tengah',
            ),

            array (
                'code' => '72.07.17',
                'name' => 'Bulagi Utara',
            ),

            array (
                'code' => '72.07.18',
                'name' => 'Buko Selatan',
            ),

            array (
                'code' => '72.07.19',
                'name' => 'Tinangkung Utara',
            ),

            array (
                'code' => '72.08',
                'name' => 'KAB. PARIGI MOUTONG',
            ),

            array (
                'code' => '72.08.01',
                'name' => 'Parigi',
            ),

            array (
                'code' => '72.08.02',
                'name' => 'Ampibabo',
            ),

            array (
                'code' => '72.08.03',
                'name' => 'Tinombo',
            ),

            array (
                'code' => '72.08.04',
                'name' => 'Moutong',
            ),

            array (
                'code' => '72.08.05',
                'name' => 'Tomini',
            ),

            array (
                'code' => '72.08.06',
                'name' => 'Sausu',
            ),

            array (
                'code' => '72.08.07',
                'name' => 'Bolano Lambunu',
            ),

            array (
                'code' => '72.08.08',
                'name' => 'Kasimbar',
            ),

            array (
                'code' => '72.08.09',
                'name' => 'Torue',
            ),

            array (
                'code' => '72.08.10',
                'name' => 'Tinombo Selatan',
            ),

            array (
                'code' => '72.08.11',
                'name' => 'Parigi Selatan',
            ),

            array (
                'code' => '72.08.12',
                'name' => 'Mepanga',
            ),

            array (
                'code' => '72.08.13',
                'name' => 'Toribulu',
            ),

            array (
                'code' => '72.08.14',
                'name' => 'Taopa',
            ),

            array (
                'code' => '72.08.15',
                'name' => 'Balinggi',
            ),

            array (
                'code' => '72.08.16',
                'name' => 'Parigi Barat',
            ),

            array (
                'code' => '72.08.17',
                'name' => 'Siniu',
            ),

            array (
                'code' => '72.08.18',
                'name' => 'Palasa',
            ),

            array (
                'code' => '72.08.19',
                'name' => 'Parigi Utara',
            ),

            array (
                'code' => '72.08.20',
                'name' => 'Parigi Tengah',
            ),

            array (
                'code' => '72.08.21',
                'name' => 'Bolano',
            ),

            array (
                'code' => '72.08.22',
                'name' => 'Ongka Malino',
            ),

            array (
                'code' => '72.08.23',
                'name' => 'Sidoan',
            ),

            array (
                'code' => '72.09',
                'name' => 'KAB. TOJO UNA UNA',
            ),

            array (
                'code' => '72.09.01',
                'name' => 'Una Una',
            ),

            array (
                'code' => '72.09.02',
                'name' => 'Togean',
            ),

            array (
                'code' => '72.09.03',
                'name' => 'Walea Kepulauan',
            ),

            array (
                'code' => '72.09.04',
                'name' => 'Ampana Tete',
            ),

            array (
                'code' => '72.09.05',
                'name' => 'Ampana Kota',
            ),

            array (
                'code' => '72.09.06',
                'name' => 'Ulubongka',
            ),

            array (
                'code' => '72.09.07',
                'name' => 'Tojo Barat',
            ),

            array (
                'code' => '72.09.08',
                'name' => 'Tojo',
            ),

            array (
                'code' => '72.09.09',
                'name' => 'Walea Besar',
            ),

            array (
                'code' => '72.09.10',
                'name' => 'Ratolindo',
            ),

            array (
                'code' => '72.09.11',
                'name' => 'Batudaka',
            ),

            array (
                'code' => '72.09.12',
                'name' => 'Talatako',
            ),

            array (
                'code' => '72.10',
                'name' => 'KAB. SIGI',
            ),

            array (
                'code' => '72.10.01',
                'name' => 'Sigi Biromaru',
            ),

            array (
                'code' => '72.10.02',
                'name' => 'Palolo',
            ),

            array (
                'code' => '72.10.03',
                'name' => 'Nokilalaki',
            ),

            array (
                'code' => '72.10.04',
                'name' => 'Lindu',
            ),

            array (
                'code' => '72.10.05',
                'name' => 'Kulawi',
            ),

            array (
                'code' => '72.10.06',
                'name' => 'Kulawi Selatan',
            ),

            array (
                'code' => '72.10.07',
                'name' => 'Pipikoro',
            ),

            array (
                'code' => '72.10.08',
                'name' => 'Gumbasa',
            ),

            array (
                'code' => '72.10.09',
                'name' => 'Dolo Selatan',
            ),

            array (
                'code' => '72.10.10',
                'name' => 'Tanambulava',
            ),

            array (
                'code' => '72.10.11',
                'name' => 'Dolo Barat',
            ),

            array (
                'code' => '72.10.12',
                'name' => 'Dolo',
            ),

            array (
                'code' => '72.10.13',
                'name' => 'Kinovaro',
            ),

            array (
                'code' => '72.10.14',
                'name' => 'Marawola',
            ),

            array (
                'code' => '72.10.15',
                'name' => 'Marawola Barat',
            ),

            array (
                'code' => '72.11',
                'name' => 'KAB. BANGGAI LAUT',
            ),

            array (
                'code' => '72.11.01',
                'name' => 'Banggai',
            ),

            array (
                'code' => '72.11.02',
                'name' => 'Banggai Utara',
            ),

            array (
                'code' => '72.11.03',
                'name' => 'Bokan Kepulauan',
            ),

            array (
                'code' => '72.11.04',
                'name' => 'Bangkurung',
            ),

            array (
                'code' => '72.11.05',
                'name' => 'Labobo',
            ),

            array (
                'code' => '72.11.06',
                'name' => 'Banggai Selatan',
            ),

            array (
                'code' => '72.11.07',
                'name' => 'Banggai Tengah',
            ),

            array (
                'code' => '72.12',
                'name' => 'KAB. MOROWALI UTARA',
            ),

            array (
                'code' => '72.12.01',
                'name' => 'Petasia',
            ),

            array (
                'code' => '72.12.02',
                'name' => 'Petasia Timur',
            ),

            array (
                'code' => '72.12.03',
                'name' => 'Lembo Raya',
            ),

            array (
                'code' => '72.12.04',
                'name' => 'Lembo',
            ),

            array (
                'code' => '72.12.05',
                'name' => 'Mori Atas',
            ),

            array (
                'code' => '72.12.06',
                'name' => 'Mori Utara',
            ),

            array (
                'code' => '72.12.07',
                'name' => 'Soyo Jaya',
            ),

            array (
                'code' => '72.12.08',
                'name' => 'Bungku Utara',
            ),

            array (
                'code' => '72.12.09',
                'name' => 'Mamosalato',
            ),

            array (
                'code' => '72.12.10',
                'name' => 'Petasia Barat',
            ),

            array (
                'code' => '72.71',
                'name' => 'KOTA PALU',
            ),

            array (
                'code' => '72.71.01',
                'name' => 'Palu Timur',
            ),

            array (
                'code' => '72.71.02',
                'name' => 'Palu Barat',
            ),

            array (
                'code' => '72.71.03',
                'name' => 'Palu Selatan',
            ),

            array (
                'code' => '72.71.04',
                'name' => 'Palu Utara',
            ),

            array (
                'code' => '72.71.05',
                'name' => 'Ulujadi',
            ),

            array (
                'code' => '72.71.06',
                'name' => 'Tatanga',
            ),

            array (
                'code' => '72.71.07',
                'name' => 'Tawaeli',
            ),

            array (
                'code' => '72.71.08',
                'name' => 'Mantikulore',
            ),

            array (
                'code' => '73',
                'name' => 'SULAWESI SELATAN',
            ),

            array (
                'code' => '73.01',
                'name' => 'KAB. KEPULAUAN SELAYAR',
            ),

            array (
                'code' => '73.01.01',
                'name' => 'Benteng',
            ),

            array (
                'code' => '73.01.02',
                'name' => 'Bontoharu',
            ),

            array (
                'code' => '73.01.03',
                'name' => 'Bontomatene',
            ),

            array (
                'code' => '73.01.04',
                'name' => 'Bontomanai',
            ),

            array (
                'code' => '73.01.05',
                'name' => 'Bontosikuyu',
            ),

            array (
                'code' => '73.01.06',
                'name' => 'Pasimasunggu',
            ),

            array (
                'code' => '73.01.07',
                'name' => 'Pasimarannu',
            ),

            array (
                'code' => '73.01.08',
                'name' => 'Taka Bonerate',
            ),

            array (
                'code' => '73.01.09',
                'name' => 'Pasilambena',
            ),

            array (
                'code' => '73.01.10',
                'name' => 'Pasimasunggu Timur',
            ),

            array (
                'code' => '73.01.11',
                'name' => 'Buki',
            ),

            array (
                'code' => '73.02',
                'name' => 'KAB. BULUKUMBA',
            ),

            array (
                'code' => '73.02.01',
                'name' => 'Gantorang',
            ),

            array (
                'code' => '73.02.02',
                'name' => 'Ujung Bulu',
            ),

            array (
                'code' => '73.02.03',
                'name' => 'Bonto Bahari',
            ),

            array (
                'code' => '73.02.04',
                'name' => 'Bonto Tiro',
            ),

            array (
                'code' => '73.02.05',
                'name' => 'Herlang',
            ),

            array (
                'code' => '73.02.06',
                'name' => 'Kajang',
            ),

            array (
                'code' => '73.02.07',
                'name' => 'Bulukumpa',
            ),

            array (
                'code' => '73.02.08',
                'name' => 'Kindang',
            ),

            array (
                'code' => '73.02.09',
                'name' => 'Ujungloe',
            ),

            array (
                'code' => '73.02.10',
                'name' => 'Rilauale',
            ),

            array (
                'code' => '73.03',
                'name' => 'KAB. BANTAENG',
            ),

            array (
                'code' => '73.03.01',
                'name' => 'Bissappu',
            ),

            array (
                'code' => '73.03.02',
                'name' => 'Bantaeng',
            ),

            array (
                'code' => '73.03.03',
                'name' => 'Eremerasa',
            ),

            array (
                'code' => '73.03.04',
                'name' => 'Tompo Bulu',
            ),

            array (
                'code' => '73.03.05',
                'name' => 'Pajukukang',
            ),

            array (
                'code' => '73.03.06',
                'name' => 'Uluere',
            ),

            array (
                'code' => '73.03.07',
                'name' => 'Gantarang Keke',
            ),

            array (
                'code' => '73.03.08',
                'name' => 'Sinoa',
            ),

            array (
                'code' => '73.04',
                'name' => 'KAB. JENEPONTO',
            ),

            array (
                'code' => '73.04.01',
                'name' => 'Bangkala',
            ),

            array (
                'code' => '73.04.02',
                'name' => 'Tamalatea',
            ),

            array (
                'code' => '73.04.03',
                'name' => 'Binamu',
            ),

            array (
                'code' => '73.04.04',
                'name' => 'Batang',
            ),

            array (
                'code' => '73.04.05',
                'name' => 'Kelara',
            ),

            array (
                'code' => '73.04.06',
                'name' => 'Bangkala Barat',
            ),

            array (
                'code' => '73.04.07',
                'name' => 'Bontoramba',
            ),

            array (
                'code' => '73.04.08',
                'name' => 'Turatea',
            ),

            array (
                'code' => '73.04.09',
                'name' => 'Arungkeke',
            ),

            array (
                'code' => '73.04.10',
                'name' => 'Rumbia',
            ),

            array (
                'code' => '73.04.11',
                'name' => 'Tarowang',
            ),

            array (
                'code' => '73.05',
                'name' => 'KAB. TAKALAR',
            ),

            array (
                'code' => '73.05.01',
                'name' => 'Mappakasunggu',
            ),

            array (
                'code' => '73.05.02',
                'name' => 'Mangarabombang',
            ),

            array (
                'code' => '73.05.03',
                'name' => 'Polombangkeng Selatan',
            ),

            array (
                'code' => '73.05.04',
                'name' => 'Polombangkeng Utara',
            ),

            array (
                'code' => '73.05.05',
                'name' => 'Galesong Selatan',
            ),

            array (
                'code' => '73.05.06',
                'name' => 'Galesong Utara',
            ),

            array (
                'code' => '73.05.07',
                'name' => 'Pattallassang',
            ),

            array (
                'code' => '73.05.08',
                'name' => 'Sanrobone',
            ),

            array (
                'code' => '73.05.09',
                'name' => 'Galesong',
            ),

            array (
                'code' => '73.06',
                'name' => 'KAB. GOWA',
            ),

            array (
                'code' => '73.06.01',
                'name' => 'Bontonompo',
            ),

            array (
                'code' => '73.06.02',
                'name' => 'Bajeng',
            ),

            array (
                'code' => '73.06.03',
                'name' => 'Tompobullu',
            ),

            array (
                'code' => '73.06.04',
                'name' => 'Tinggimoncong',
            ),

            array (
                'code' => '73.06.05',
                'name' => 'Parangloe',
            ),

            array (
                'code' => '73.06.06',
                'name' => 'Bontomarannu',
            ),

            array (
                'code' => '73.06.07',
                'name' => 'Palangga',
            ),

            array (
                'code' => '73.06.08',
                'name' => 'Somba Upu',
            ),

            array (
                'code' => '73.06.09',
                'name' => 'Bungaya',
            ),

            array (
                'code' => '73.06.10',
                'name' => 'Tombolopao',
            ),

            array (
                'code' => '73.06.11',
                'name' => 'Biringbulu',
            ),

            array (
                'code' => '73.06.12',
                'name' => 'Barombong',
            ),

            array (
                'code' => '73.06.13',
                'name' => 'Pattalasang',
            ),

            array (
                'code' => '73.06.14',
                'name' => 'Manuju',
            ),

            array (
                'code' => '73.06.15',
                'name' => 'Bontolempangang',
            ),

            array (
                'code' => '73.06.16',
                'name' => 'Bontonompo Selatan',
            ),

            array (
                'code' => '73.06.17',
                'name' => 'Parigi',
            ),

            array (
                'code' => '73.06.18',
                'name' => 'Bajeng Barat',
            ),

            array (
                'code' => '73.07',
                'name' => 'KAB. SINJAI',
            ),

            array (
                'code' => '73.07.01',
                'name' => 'Sinjai Barat',
            ),

            array (
                'code' => '73.07.02',
                'name' => 'Sinjai  Selatan',
            ),

            array (
                'code' => '73.07.03',
                'name' => 'Sinjai Timur',
            ),

            array (
                'code' => '73.07.04',
                'name' => 'Sinjai Tengah',
            ),

            array (
                'code' => '73.07.05',
                'name' => 'Sinjai Utara',
            ),

            array (
                'code' => '73.07.06',
                'name' => 'Bulupoddo',
            ),

            array (
                'code' => '73.07.07',
                'name' => 'Sinjai Borong',
            ),

            array (
                'code' => '73.07.08',
                'name' => 'Tellu Limpoe',
            ),

            array (
                'code' => '73.07.09',
                'name' => 'Pulau Sembilan',
            ),

            array (
                'code' => '73.08',
                'name' => 'KAB. BONE',
            ),

            array (
                'code' => '73.08.01',
                'name' => 'Bontocani',
            ),

            array (
                'code' => '73.08.02',
                'name' => 'Kahu',
            ),

            array (
                'code' => '73.08.03',
                'name' => 'Kajuara',
            ),

            array (
                'code' => '73.08.04',
                'name' => 'Salomekko',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '73.08.05',
                'name' => 'Tonra',
            ),

            array (
                'code' => '73.08.06',
                'name' => 'Libureng',
            ),

            array (
                'code' => '73.08.07',
                'name' => 'Mare',
            ),

            array (
                'code' => '73.08.08',
                'name' => 'Sibulue',
            ),

            array (
                'code' => '73.08.09',
                'name' => 'Barebbo',
            ),

            array (
                'code' => '73.08.10',
                'name' => 'Cina',
            ),

            array (
                'code' => '73.08.11',
                'name' => 'Ponre',
            ),

            array (
                'code' => '73.08.12',
                'name' => 'Lappariaja',
            ),

            array (
                'code' => '73.08.13',
                'name' => 'Lamuru',
            ),

            array (
                'code' => '73.08.14',
                'name' => 'Ulaweng',
            ),

            array (
                'code' => '73.08.15',
                'name' => 'Palakka',
            ),

            array (
                'code' => '73.08.16',
                'name' => 'Awangpone',
            ),

            array (
                'code' => '73.08.17',
                'name' => 'Tellu Siattinge',
            ),

            array (
                'code' => '73.08.18',
                'name' => 'Ajangale',
            ),

            array (
                'code' => '73.08.19',
                'name' => 'Dua Boccoe',
            ),

            array (
                'code' => '73.08.20',
                'name' => 'Cenrana',
            ),

            array (
                'code' => '73.08.21',
                'name' => 'Tanete Riattang',
            ),

            array (
                'code' => '73.08.22',
                'name' => 'Tanete Riattang Barat',
            ),

            array (
                'code' => '73.08.23',
                'name' => 'Tanete Riattang Timur',
            ),

            array (
                'code' => '73.08.24',
                'name' => 'Amali',
            ),

            array (
                'code' => '73.08.25',
                'name' => 'Tellulimpoe',
            ),

            array (
                'code' => '73.08.26',
                'name' => 'Bengo',
            ),

            array (
                'code' => '73.08.27',
                'name' => 'Patimpeng',
            ),

            array (
                'code' => '73.09',
                'name' => 'KAB. MAROS',
            ),

            array (
                'code' => '73.09.01',
                'name' => 'Mandai',
            ),

            array (
                'code' => '73.09.02',
                'name' => 'Camba',
            ),

            array (
                'code' => '73.09.03',
                'name' => 'Bantimurung',
            ),

            array (
                'code' => '73.09.04',
                'name' => 'Maros Baru',
            ),

            array (
                'code' => '73.09.05',
                'name' => 'Bontoa',
            ),

            array (
                'code' => '73.09.06',
                'name' => 'Malllawa',
            ),

            array (
                'code' => '73.09.07',
                'name' => 'Tanralili',
            ),

            array (
                'code' => '73.09.08',
                'name' => 'Marusu',
            ),

            array (
                'code' => '73.09.09',
                'name' => 'Simbang',
            ),

            array (
                'code' => '73.09.10',
                'name' => 'Cenrana',
            ),

            array (
                'code' => '73.09.11',
                'name' => 'Tompobulu',
            ),

            array (
                'code' => '73.09.12',
                'name' => 'Lau',
            ),

            array (
                'code' => '73.09.13',
                'name' => 'Moncong Loe',
            ),

            array (
                'code' => '73.09.14',
                'name' => 'Turikale',
            ),

            array (
                'code' => '73.10',
                'name' => 'KAB. PANGKAJENE KEPULAUAN',
            ),

            array (
                'code' => '73.10.01',
                'name' => 'Liukang Tangaya',
            ),

            array (
                'code' => '73.10.02',
                'name' => 'Liukang Kalmas',
            ),

            array (
                'code' => '73.10.03',
                'name' => 'Liukang Tupabbiring',
            ),

            array (
                'code' => '73.10.04',
                'name' => 'Pangkajene',
            ),

            array (
                'code' => '73.10.05',
                'name' => 'Balocci',
            ),

            array (
                'code' => '73.10.06',
                'name' => 'Bungoro',
            ),

            array (
                'code' => '73.10.07',
                'name' => 'Labakkang',
            ),

            array (
                'code' => '73.10.08',
                'name' => 'Marang',
            ),

            array (
                'code' => '73.10.09',
                'name' => 'Segeri',
            ),

            array (
                'code' => '73.10.10',
                'name' => 'Minasa Tene',
            ),

            array (
                'code' => '73.10.11',
                'name' => 'Mandalle',
            ),

            array (
                'code' => '73.10.12',
                'name' => 'Tondong Tallasa',
            ),

            array (
                'code' => '73.10.13',
                'name' => 'Liukang Tupabbiring Utara',
            ),

            array (
                'code' => '73.11',
                'name' => 'KAB. BARRU',
            ),

            array (
                'code' => '73.11.01',
                'name' => 'Tanete Riaja',
            ),

            array (
                'code' => '73.11.02',
                'name' => 'Tanete Rilau',
            ),

            array (
                'code' => '73.11.03',
                'name' => 'Barru',
            ),

            array (
                'code' => '73.11.04',
                'name' => 'Soppeng Riaja',
            ),

            array (
                'code' => '73.11.05',
                'name' => 'Mallusetasi',
            ),

            array (
                'code' => '73.11.06',
                'name' => 'Pujananting',
            ),

            array (
                'code' => '73.11.07',
                'name' => 'Balusu',
            ),

            array (
                'code' => '73.12',
                'name' => 'KAB. SOPPENG',
            ),

            array (
                'code' => '73.12.01',
                'name' => 'Marioriwawo',
            ),

            array (
                'code' => '73.12.02',
                'name' => 'Liliraja',
            ),

            array (
                'code' => '73.12.03',
                'name' => 'Lilirilau',
            ),

            array (
                'code' => '73.12.04',
                'name' => 'Lalabata',
            ),

            array (
                'code' => '73.12.05',
                'name' => 'Marioriawa',
            ),

            array (
                'code' => '73.12.06',
                'name' => 'Donri Donri',
            ),

            array (
                'code' => '73.12.07',
                'name' => 'Ganra',
            ),

            array (
                'code' => '73.12.08',
                'name' => 'Citta',
            ),

            array (
                'code' => '73.13',
                'name' => 'KAB. WAJO',
            ),

            array (
                'code' => '73.13.01',
                'name' => 'Sabangparu',
            ),

            array (
                'code' => '73.13.02',
                'name' => 'Pammana',
            ),

            array (
                'code' => '73.13.03',
                'name' => 'Takkalalla',
            ),

            array (
                'code' => '73.13.04',
                'name' => 'Sajoanging',
            ),

            array (
                'code' => '73.13.05',
                'name' => 'Majauleng',
            ),

            array (
                'code' => '73.13.06',
                'name' => 'Tempe',
            ),

            array (
                'code' => '73.13.07',
                'name' => 'Belawa',
            ),

            array (
                'code' => '73.13.08',
                'name' => 'Tanasitolo',
            ),

            array (
                'code' => '73.13.09',
                'name' => 'Maniangpajo',
            ),

            array (
                'code' => '73.13.10',
                'name' => 'Pitumpanua',
            ),

            array (
                'code' => '73.13.11',
                'name' => 'Bola',
            ),

            array (
                'code' => '73.13.12',
                'name' => 'Penrang',
            ),

            array (
                'code' => '73.13.13',
                'name' => 'Gilireng',
            ),

            array (
                'code' => '73.13.14',
                'name' => 'Keera',
            ),

            array (
                'code' => '73.14',
                'name' => 'KAB. SIDENRENG RAPPANG',
            ),

            array (
                'code' => '73.14.01',
                'name' => 'Panca Lautan',
            ),

            array (
                'code' => '73.14.02',
                'name' => 'Tellu Limpoe',
            ),

            array (
                'code' => '73.14.03',
                'name' => 'Watang Pulu',
            ),

            array (
                'code' => '73.14.04',
                'name' => 'Baranti',
            ),

            array (
                'code' => '73.14.05',
                'name' => 'Panca Rijang',
            ),

            array (
                'code' => '73.14.06',
                'name' => 'Kulo',
            ),

            array (
                'code' => '73.14.07',
                'name' => 'Maritengngae',
            ),

            array (
                'code' => '73.14.08',
                'name' => 'WT. Sidenreng  3  5',
            ),

            array (
                'code' => '73.14.09',
                'name' => 'Dua Pitue',
            ),

            array (
                'code' => '73.14.10',
                'name' => 'Pitu Riawa',
            ),

            array (
                'code' => '73.14.11',
                'name' => 'Pitu Raise',
            ),

            array (
                'code' => '73.15',
                'name' => 'KAB. PINRANG',
            ),

            array (
                'code' => '73.15.01',
                'name' => 'Matirro Sompe',
            ),

            array (
                'code' => '73.15.02',
                'name' => 'Suppa',
            ),

            array (
                'code' => '73.15.03',
                'name' => 'Mattiro Bulu',
            ),

            array (
                'code' => '73.15.04',
                'name' => 'Watang Sawito',
            ),

            array (
                'code' => '73.15.05',
                'name' => 'Patampanua',
            ),

            array (
                'code' => '73.15.06',
                'name' => 'Duampanua',
            ),

            array (
                'code' => '73.15.07',
                'name' => 'Lembang',
            ),

            array (
                'code' => '73.15.08',
                'name' => 'Cempa',
            ),

            array (
                'code' => '73.15.09',
                'name' => 'Tiroang',
            ),

            array (
                'code' => '73.15.10',
                'name' => 'Lansirang',
            ),

            array (
                'code' => '73.15.11',
                'name' => 'Paleteang',
            ),

            array (
                'code' => '73.15.12',
                'name' => 'Batu Lappa',
            ),

            array (
                'code' => '73.16',
                'name' => 'KAB. ENREKANG',
            ),

            array (
                'code' => '73.16.01',
                'name' => 'Maiwa',
            ),

            array (
                'code' => '73.16.02',
                'name' => 'Enrekang',
            ),

            array (
                'code' => '73.16.03',
                'name' => 'Baraka',
            ),

            array (
                'code' => '73.16.04',
                'name' => 'Anggeraja',
            ),

            array (
                'code' => '73.16.05',
                'name' => 'Alla',
            ),

            array (
                'code' => '73.16.06',
                'name' => 'Bungin',
            ),

            array (
                'code' => '73.16.07',
                'name' => 'Cendana',
            ),

            array (
                'code' => '73.16.08',
                'name' => 'Curio',
            ),

            array (
                'code' => '73.16.09',
                'name' => 'Malua',
            ),

            array (
                'code' => '73.16.10',
                'name' => 'Buntu Batu',
            ),

            array (
                'code' => '73.16.11',
                'name' => 'Masalle',
            ),

            array (
                'code' => '73.16.12',
                'name' => 'Baroko',
            ),

            array (
                'code' => '73.17',
                'name' => 'KAB. LUWU',
            ),

            array (
                'code' => '73.17.01',
                'name' => 'Basse Sangtempe',
            ),

            array (
                'code' => '73.17.02',
                'name' => 'Larompong',
            ),

            array (
                'code' => '73.17.03',
                'name' => 'Suli',
            ),

            array (
                'code' => '73.17.04',
                'name' => 'Bajo',
            ),

            array (
                'code' => '73.17.05',
                'name' => 'Bua Ponrang',
            ),

            array (
                'code' => '73.17.06',
                'name' => 'Walenrang',
            ),

            array (
                'code' => '73.17.07',
                'name' => 'Belopa',
            ),

            array (
                'code' => '73.17.08',
                'name' => 'Bua',
            ),

            array (
                'code' => '73.17.09',
                'name' => 'Lamasi',
            ),

            array (
                'code' => '73.17.10',
                'name' => 'Larompong Selatan',
            ),

            array (
                'code' => '73.17.11',
                'name' => 'Ponrang',
            ),

            array (
                'code' => '73.17.12',
                'name' => 'Latimojong',
            ),

            array (
                'code' => '73.17.13',
                'name' => 'Kamanre',
            ),

            array (
                'code' => '73.17.14',
                'name' => 'Belopa Utara',
            ),

            array (
                'code' => '73.17.15',
                'name' => 'Walenrang Barat',
            ),

            array (
                'code' => '73.17.16',
                'name' => 'Walenrang Utara',
            ),

            array (
                'code' => '73.17.17',
                'name' => 'Walenrang Timur',
            ),

            array (
                'code' => '73.17.18',
                'name' => 'Lamasi Timur',
            ),

            array (
                'code' => '73.17.19',
                'name' => 'Suli Barat',
            ),

            array (
                'code' => '73.17.20',
                'name' => 'Bajo Barat',
            ),

            array (
                'code' => '73.17.21',
                'name' => 'Ponrang Selatan',
            ),

            array (
                'code' => '73.17.22',
                'name' => 'Basse Sangtempe Utara',
            ),

            array (
                'code' => '73.18',
                'name' => 'KAB. TANA TORAJA',
            ),

            array (
                'code' => '73.18.01',
                'name' => 'Saluputi',
            ),

            array (
                'code' => '73.18.02',
                'name' => 'Bittuang',
            ),

            array (
                'code' => '73.18.03',
                'name' => 'Bonggakaradeng',
            ),

            array (
                'code' => '73.18.05',
                'name' => 'Makale',
            ),

            array (
                'code' => '73.18.09',
                'name' => 'Simbuang',
            ),

            array (
                'code' => '73.18.11',
                'name' => 'Rantetayo Iring',
            ),

            array (
                'code' => '73.18.12',
                'name' => 'Mengkendek',
            ),

            array (
                'code' => '73.18.13',
                'name' => 'Sangalla',
            ),

            array (
                'code' => '73.18.19',
                'name' => 'Gandangbatu Sillanan',
            ),

            array (
                'code' => '73.18.20',
                'name' => 'Rembon',
            ),

            array (
                'code' => '73.18.27',
                'name' => 'Makale Utara',
            ),

            array (
                'code' => '73.18.28',
                'name' => 'Mappak',
            ),

            array (
                'code' => '73.18.29',
                'name' => 'Makale Selatan',
            ),

            array (
                'code' => '73.18.31',
                'name' => 'Masanda',
            ),

            array (
                'code' => '73.18.33',
                'name' => 'Sangalla Selatan',
            ),

            array (
                'code' => '73.18.34',
                'name' => 'Sangalla Utara',
            ),

            array (
                'code' => '73.18.35',
                'name' => 'Malimbong Balepe',
            ),

            array (
                'code' => '73.18.37',
                'name' => 'Rano',
            ),

            array (
                'code' => '73.18.38',
                'name' => 'Kurra',
            ),

            array (
                'code' => '73.22',
                'name' => 'KAB. LUWU UTARA',
            ),

            array (
                'code' => '73.22.01',
                'name' => 'Malangke',
            ),

            array (
                'code' => '73.22.02',
                'name' => 'Bone Bone',
            ),

            array (
                'code' => '73.22.03',
                'name' => 'Masamba',
            ),

            array (
                'code' => '73.22.04',
                'name' => 'Sabbang',
            ),

            array (
                'code' => '73.22.05',
                'name' => 'Limbong',
            ),

            array (
                'code' => '73.22.06',
                'name' => 'Sukamaju',
            ),

            array (
                'code' => '73.22.07',
                'name' => 'Seko',
            ),

            array (
                'code' => '73.22.08',
                'name' => 'Malangke Barat',
            ),

            array (
                'code' => '73.22.09',
                'name' => 'Rampi',
            ),

            array (
                'code' => '73.22.10',
                'name' => 'Mappedeceng',
            ),

            array (
                'code' => '73.22.11',
                'name' => 'Baebunta',
            ),

            array (
                'code' => '73.22.12',
                'name' => 'Tana Lili',
            ),

            array (
                'code' => '73.24',
                'name' => 'KAB. LUWU TIMUR',
            ),

            array (
                'code' => '73.24.01',
                'name' => 'Mangkutana',
            ),

            array (
                'code' => '73.24.02',
                'name' => 'Nuha',
            ),

            array (
                'code' => '73.24.03',
                'name' => 'Towuti',
            ),

            array (
                'code' => '73.24.04',
                'name' => 'Malili',
            ),

            array (
                'code' => '73.24.05',
                'name' => 'Angkona',
            ),

            array (
                'code' => '73.24.06',
                'name' => 'Wotu',
            ),

            array (
                'code' => '73.24.07',
                'name' => 'Burau',
            ),

            array (
                'code' => '73.24.08',
                'name' => 'Tomoni',
            ),

            array (
                'code' => '73.24.09',
                'name' => 'Tomoni Timur',
            ),

            array (
                'code' => '73.24.10',
                'name' => 'Kalaena',
            ),

            array (
                'code' => '73.24.11',
                'name' => 'Wasuponda',
            ),

            array (
                'code' => '73.26',
                'name' => 'KAB. TORAJA UTARA',
            ),

            array (
                'code' => '73.26.01',
                'name' => 'Rantepao',
            ),

            array (
                'code' => '73.26.02',
                'name' => 'Sesean',
            ),

            array (
                'code' => '73.26.03',
                'name' => 'Nanggala',
            ),

            array (
                'code' => '73.26.04',
                'name' => 'Rindingallo',
            ),

            array (
                'code' => '73.26.05',
                'name' => 'Buntao',
            ),

            array (
                'code' => '73.26.06',
                'name' => 'Sa\'dan  3  7',
            ),

            array (
                'code' => '73.26.07',
                'name' => 'Sanggalangi',
            ),

            array (
                'code' => '73.26.08',
                'name' => 'Sopai',
            ),

            array (
                'code' => '73.26.09',
                'name' => 'Tikala',
            ),

            array (
                'code' => '73.26.10',
                'name' => 'Balusu',
            ),

            array (
                'code' => '73.26.11',
                'name' => 'Tallunglipu',
            ),

            array (
                'code' => '73.26.12',
                'name' => 'Dende\' Piongan Napo  1  7',
            ),

            array (
                'code' => '73.26.13',
                'name' => 'Buntu Pepasan',
            ),

            array (
                'code' => '73.26.14',
                'name' => 'Baruppu',
            ),

            array (
                'code' => '73.26.15',
                'name' => 'Kesu',
            ),

            array (
                'code' => '73.26.16',
                'name' => 'Tondon',
            ),

            array (
                'code' => '73.26.17',
                'name' => 'Bangkelekila',
            ),

            array (
                'code' => '73.26.18',
                'name' => 'Rantebua',
            ),

            array (
                'code' => '73.26.19',
                'name' => 'Sesean Suloara',
            ),

            array (
                'code' => '73.26.20',
                'name' => 'Kapala Pitu',
            ),

            array (
                'code' => '73.26.21',
                'name' => 'Awan Rante Karua',
            ),

            array (
                'code' => '73.71',
                'name' => 'KOTA MAKASSAR',
            ),

            array (
                'code' => '73.71.01',
                'name' => 'Mariso',
            ),

            array (
                'code' => '73.71.02',
                'name' => 'Mamajang',
            ),

            array (
                'code' => '73.71.03',
                'name' => 'Makasar',
            ),

            array (
                'code' => '73.71.04',
                'name' => 'Ujung Pandang',
            ),

            array (
                'code' => '73.71.05',
                'name' => 'Wajo',
            ),

            array (
                'code' => '73.71.06',
                'name' => 'Bontoala',
            ),

            array (
                'code' => '73.71.07',
                'name' => 'Tallo',
            ),

            array (
                'code' => '73.71.08',
                'name' => 'Ujung Tanah',
            ),

            array (
                'code' => '73.71.09',
                'name' => 'Panakukkang',
            ),

            array (
                'code' => '73.71.10',
                'name' => 'Tamalate',
            ),

            array (
                'code' => '73.71.11',
                'name' => 'Biringkanaya',
            ),

            array (
                'code' => '73.71.12',
                'name' => 'Manggala',
            ),

            array (
                'code' => '73.71.13',
                'name' => 'Rappocini',
            ),

            array (
                'code' => '73.71.14',
                'name' => 'Tamalanrea',
            ),

            array (
                'code' => '73.72',
                'name' => 'KOTA PARE PARE',
            ),

            array (
                'code' => '73.72.01',
                'name' => 'Bacukiki',
            ),

            array (
                'code' => '73.72.02',
                'name' => 'Ujung',
            ),

            array (
                'code' => '73.72.03',
                'name' => 'Soreang',
            ),

            array (
                'code' => '73.72.04',
                'name' => 'Bacukiki Barat',
            ),

            array (
                'code' => '73.73',
                'name' => 'KOTA PALOPO',
            ),

            array (
                'code' => '73.73.01',
                'name' => 'Wara',
            ),

            array (
                'code' => '73.73.02',
                'name' => 'Wara Utara',
            ),

            array (
                'code' => '73.73.03',
                'name' => 'Wara Selatan',
            ),

            array (
                'code' => '73.73.04',
                'name' => 'Telluwanua',
            ),

            array (
                'code' => '73.73.05',
                'name' => 'Wara Timur',
            ),

            array (
                'code' => '73.73.06',
                'name' => 'Wara Barat',
            ),

            array (
                'code' => '73.73.07',
                'name' => 'Sendana',
            ),

            array (
                'code' => '73.73.08',
                'name' => 'Mungkajang',
            ),

            array (
                'code' => '73.73.09',
                'name' => 'Bara',
            ),

            array (
                'code' => '74',
                'name' => 'SULAWESI TENGGARA',
            ),

            array (
                'code' => '74.01',
                'name' => 'KAB. KOLAKA',
            ),

            array (
                'code' => '74.01.01',
                'name' => 'Wundulako',
            ),

            array (
                'code' => '74.01.04',
                'name' => 'Kolaka',
            ),

            array (
                'code' => '74.01.07',
                'name' => 'Pomalaa',
            ),

            array (
                'code' => '74.01.08',
                'name' => 'Watubangga',
            ),

            array (
                'code' => '74.01.10',
                'name' => 'Wolo',
            ),

            array (
                'code' => '74.01.12',
                'name' => 'Baula',
            ),

            array (
                'code' => '74.01.14',
                'name' => 'Latambaga',
            ),

            array (
                'code' => '74.01.18',
                'name' => 'Tanggetada',
            ),

            array (
                'code' => '74.01.20',
                'name' => 'Samaturu',
            ),

            array (
                'code' => '74.01.24',
                'name' => 'Toari',
            ),

            array (
                'code' => '74.01.25',
                'name' => 'Polinggona',
            ),

            array (
                'code' => '74.01.27',
                'name' => 'Iwoimendaa',
            ),

            array (
                'code' => '74.02',
                'name' => 'KAB. KONAWE',
            ),

            array (
                'code' => '74.02.01',
                'name' => 'Lambuya',
            ),

            array (
                'code' => '74.02.02',
                'name' => 'Unaaha',
            ),

            array (
                'code' => '74.02.03',
                'name' => 'Wawotobi',
            ),

            array (
                'code' => '74.02.04',
                'name' => 'Pondidaha',
            ),

            array (
                'code' => '74.02.05',
                'name' => 'Sampara',
            ),

            array (
                'code' => '74.02.10',
                'name' => 'Abuki',
            ),

            array (
                'code' => '74.02.11',
                'name' => 'Soropia',
            ),

            array (
                'code' => '74.02.15',
                'name' => 'Tongauna',
            ),

            array (
                'code' => '74.02.16',
                'name' => 'Latoma',
            ),

            array (
                'code' => '74.02.17',
                'name' => 'Puriala',
            ),

            array (
                'code' => '74.02.18',
                'name' => 'Uepai',
            ),

            array (
                'code' => '74.02.19',
                'name' => 'Wonggeduku',
            ),

            array (
                'code' => '74.02.20',
                'name' => 'Besulutu',
            ),

            array (
                'code' => '74.02.21',
                'name' => 'Bondoala',
            ),

            array (
                'code' => '74.02.23',
                'name' => 'Routa',
            ),

            array (
                'code' => '74.02.24',
                'name' => 'Anggaberi',
            ),

            array (
                'code' => '74.02.25',
                'name' => 'Meluhu',
            ),

            array (
                'code' => '74.02.28',
                'name' => 'Amonggedo',
            ),

            array (
                'code' => '74.02.31',
                'name' => 'Asinua',
            ),

            array (
                'code' => '74.02.32',
                'name' => 'Konawe',
            ),

            array (
                'code' => '74.02.33',
                'name' => 'Kapoiala',
            ),

            array (
                'code' => '74.02.36',
                'name' => 'Lalonggasumeeto',
            ),

            array (
                'code' => '74.02.37',
                'name' => 'Onembute',
            ),

            array (
                'code' => '74.03',
                'name' => 'KAB. MUNA',
            ),

            array (
                'code' => '74.03.06',
                'name' => 'Napabalano',
            ),

            array (
                'code' => '74.03.07',
                'name' => 'Maligano',
            ),

            array (
                'code' => '74.03.13',
                'name' => 'Wakorumba Selatan',
            ),

            array (
                'code' => '74.03.14',
                'name' => 'Lasalepa',
            ),

            array (
                'code' => '74.03.15',
                'name' => 'Batalaiwaru',
            ),

            array (
                'code' => '74.03.16',
                'name' => 'Katobu',
            ),

            array (
                'code' => '74.03.17',
                'name' => 'Duruka',
            ),

            array (
                'code' => '74.03.18',
                'name' => 'Lohia',
            ),

            array (
                'code' => '74.03.19',
                'name' => 'Watopute',
            ),

            array (
                'code' => '74.03.20',
                'name' => 'Kontunaga',
            ),

            array (
                'code' => '74.03.23',
                'name' => 'Kabangka',
            ),

            array (
                'code' => '74.03.24',
                'name' => 'Kabawo',
            ),

            array (
                'code' => '74.03.25',
                'name' => 'Parigi',
            ),

            array (
                'code' => '74.03.26',
                'name' => 'Bone',
            ),

            array (
                'code' => '74.03.27',
                'name' => 'Tongkuno',
            ),

            array (
                'code' => '74.03.28',
                'name' => 'Pasir Putih',
            ),

            array (
                'code' => '74.03.30',
                'name' => 'Kontu Kowuna',
            ),

            array (
                'code' => '74.03.31',
                'name' => 'Marobo',
            ),

            array (
                'code' => '74.03.32',
                'name' => 'Tongkuno Selatan',
            ),

            array (
                'code' => '74.03.33',
                'name' => 'Pasi Kolaga',
            ),

            array (
                'code' => '74.03.34',
                'name' => 'Batukara',
            ),

            array (
                'code' => '74.03.37',
                'name' => 'Towea',
            ),

            array (
                'code' => '74.04',
                'name' => 'KAB. BUTON',
            ),

            array (
                'code' => '74.04.11',
                'name' => 'Pasarwajo',
            ),

            array (
                'code' => '74.04.22',
                'name' => 'Kapontori',
            ),

            array (
                'code' => '74.04.23',
                'name' => 'Lasalimu',
            ),

            array (
                'code' => '74.04.24',
                'name' => 'Lasalimu Selatan',
            ),

            array (
                'code' => '74.04.27',
                'name' => 'Siotapina',
            ),

            array (
                'code' => '74.04.28',
                'name' => 'Wolowa',
            ),

            array (
                'code' => '74.04.29',
                'name' => 'Wabula',
            ),

            array (
                'code' => '74.05',
                'name' => 'KAB. KONAWE SELATAN',
            ),

            array (
                'code' => '74.05.01',
                'name' => 'Tinanggea',
            ),

            array (
                'code' => '74.05.02',
                'name' => 'Angata',
            ),

            array (
                'code' => '74.05.03',
                'name' => 'Andoolo',
            ),

            array (
                'code' => '74.05.04',
                'name' => 'Palangga',
            ),

            array (
                'code' => '74.05.05',
                'name' => 'Landono',
            ),

            array (
                'code' => '74.05.06',
                'name' => 'Lainea',
            ),

            array (
                'code' => '74.05.07',
                'name' => 'Konda',
            ),

            array (
                'code' => '74.05.08',
                'name' => 'Ranomeeto',
            ),

            array (
                'code' => '74.05.09',
                'name' => 'Kolono',
            ),

            array (
                'code' => '74.05.10',
                'name' => 'Moramo',
            ),

            array (
                'code' => '74.05.11',
                'name' => 'Laonti',
            ),

            array (
                'code' => '74.05.12',
                'name' => 'Lalembuu',
            ),

            array (
                'code' => '74.05.13',
                'name' => 'Benua',
            ),

            array (
                'code' => '74.05.14',
                'name' => 'Palangga Selatan',
            ),

            array (
                'code' => '74.05.15',
                'name' => 'Mowila',
            ),

            array (
                'code' => '74.05.16',
                'name' => 'Moramo Utara',
            ),

            array (
                'code' => '74.05.17',
                'name' => 'Buke',
            ),

            array (
                'code' => '74.05.18',
                'name' => 'Wolasi',
            ),

            array (
                'code' => '74.05.19',
                'name' => 'Laeya',
            ),

            array (
                'code' => '74.05.20',
                'name' => 'Baito',
            ),

            array (
                'code' => '74.05.21',
                'name' => 'Basala',
            ),

            array (
                'code' => '74.05.22',
                'name' => 'Ranomeeto Barat',
            ),

            array (
                'code' => '74.06',
                'name' => 'KAB. BOMBANA',
            ),

            array (
                'code' => '74.06.01',
                'name' => 'Poleang',
            ),

            array (
                'code' => '74.06.02',
                'name' => 'Poleang Timur',
            ),

            array (
                'code' => '74.06.03',
                'name' => 'Rarowatu',
            ),

            array (
                'code' => '74.06.04',
                'name' => 'Rumbia',
            ),

            array (
                'code' => '74.06.05',
                'name' => 'Kabaena',
            ),

            array (
                'code' => '74.06.06',
                'name' => 'Kabaena Timur',
            ),

            array (
                'code' => '74.06.07',
                'name' => 'Poleang Barat',
            ),

            array (
                'code' => '74.06.08',
                'name' => 'Mata Oleo',
            ),

            array (
                'code' => '74.06.09',
                'name' => 'Rarowatu Utara',
            ),

            array (
                'code' => '74.06.10',
                'name' => 'Poleang Utara',
            ),

            array (
                'code' => '74.06.11',
                'name' => 'Poleang Selatan',
            ),

            array (
                'code' => '74.06.12',
                'name' => 'Poleang Tenggara',
            ),

            array (
                'code' => '74.06.13',
                'name' => 'Kabaena Selatan',
            ),

            array (
                'code' => '74.06.14',
                'name' => 'Kabaena Barat',
            ),

            array (
                'code' => '74.06.15',
                'name' => 'Kabaena Utara',
            ),

            array (
                'code' => '74.06.16',
                'name' => 'Kabaena Tengah',
            ),

            array (
                'code' => '74.06.17',
                'name' => 'Kep. Masaloka Raya  5',
            ),

            array (
                'code' => '74.06.18',
                'name' => 'Rumbia Tengah',
            ),

            array (
                'code' => '74.06.19',
                'name' => 'Poleang Tengah',
            ),

            array (
                'code' => '74.06.20',
                'name' => 'Tontonunu',
            ),

            array (
                'code' => '74.06.21',
                'name' => 'Lantari Jaya',
            ),

            array (
                'code' => '74.06.22',
                'name' => 'Mata Usu',
            ),

            array (
                'code' => '74.07',
                'name' => 'KAB. WAKATOBI',
            ),

            array (
                'code' => '74.07.01',
                'name' => 'Wangi-Wangi',
            ),

            array (
                'code' => '74.07.02',
                'name' => 'Kaledupa',
            ),

            array (
                'code' => '74.07.03',
                'name' => 'Tomia',
            ),

            array (
                'code' => '74.07.04',
                'name' => 'Binongko',
            ),

            array (
                'code' => '74.07.05',
                'name' => 'Wangi Wangi Selatan',
            ),

            array (
                'code' => '74.07.06',
                'name' => 'Kaledupa Selatan',
            ),

            array (
                'code' => '74.07.07',
                'name' => 'Tomia Timur',
            ),

            array (
                'code' => '74.07.08',
                'name' => 'Togo Binongko',
            ),

            array (
                'code' => '74.08',
                'name' => 'KAB. KOLAKA UTARA',
            ),

            array (
                'code' => '74.08.01',
                'name' => 'Lasusua',
            ),

            array (
                'code' => '74.08.02',
                'name' => 'Pakue',
            ),

            array (
                'code' => '74.08.03',
                'name' => 'Batu Putih',
            ),

            array (
                'code' => '74.08.04',
                'name' => 'Rante Angin',
            ),

            array (
                'code' => '74.08.05',
                'name' => 'Kodeoha',
            ),

            array (
                'code' => '74.08.06',
                'name' => 'Ngapa',
            ),

            array (
                'code' => '74.08.07',
                'name' => 'Wawo',
            ),

            array (
                'code' => '74.08.08',
                'name' => 'Lambai',
            ),

            array (
                'code' => '74.08.09',
                'name' => 'Watunohu',
            ),

            array (
                'code' => '74.08.10',
                'name' => 'Pakue Tengah',
            ),

            array (
                'code' => '74.08.11',
                'name' => 'Pakue Utara',
            ),

            array (
                'code' => '74.08.12',
                'name' => 'Porehu',
            ),

            array (
                'code' => '74.08.13',
                'name' => 'Tolala',
            ),

            array (
                'code' => '74.08.14',
                'name' => 'Tiwu',
            ),

            array (
                'code' => '74.08.15',
                'name' => 'Katoi',
            ),

            array (
                'code' => '74.09',
                'name' => 'KAB. KONAWE UTARA',
            ),

            array (
                'code' => '74.09.01',
                'name' => 'Asera',
            ),

            array (
                'code' => '74.09.02',
                'name' => 'Wiwirano',
            ),

            array (
                'code' => '74.09.03',
                'name' => 'Langgikima',
            ),

            array (
                'code' => '74.09.04',
                'name' => 'Molawe',
            ),

            array (
                'code' => '74.09.05',
                'name' => 'Lasolo',
            ),

            array (
                'code' => '74.09.06',
                'name' => 'Lembo',
            ),

            array (
                'code' => '74.09.07',
                'name' => 'Sawa',
            ),

            array (
                'code' => '74.09.08',
                'name' => 'Oheo',
            ),

            array (
                'code' => '74.09.09',
                'name' => 'Andowia',
            ),

            array (
                'code' => '74.09.10',
                'name' => 'Motui',
            ),

            array (
                'code' => '74.09.11',
                'name' => 'Wawolesea',
            ),

            array (
                'code' => '74.09.12',
                'name' => 'Lasolo Kepulauan',
            ),

            array (
                'code' => '74.09.13',
                'name' => 'Landawe',
            ),

            array (
                'code' => '74.10',
                'name' => 'KAB. BUTON UTARA',
            ),

            array (
                'code' => '74.10.01',
                'name' => 'Kulisusu',
            ),

            array (
                'code' => '74.10.02',
                'name' => 'Kambowa',
            ),

            array (
                'code' => '74.10.03',
                'name' => 'Bonegunu',
            ),

            array (
                'code' => '74.10.04',
                'name' => 'Kulisusu Barat',
            ),

            array (
                'code' => '74.10.05',
                'name' => 'Kulisusu Utara',
            ),

            array (
                'code' => '74.10.06',
                'name' => 'Wakorumba Utara',
            ),

            array (
                'code' => '74.11',
                'name' => 'KAB. KOLAKA TIMUR',
            ),

            array (
                'code' => '74.11.01',
                'name' => 'Tirawuta',
            ),

            array (
                'code' => '74.11.02',
                'name' => 'Loea',
            ),

            array (
                'code' => '74.11.03',
                'name' => 'Ladongi',
            ),

            array (
                'code' => '74.11.04',
                'name' => 'Poli Polia',
            ),

            array (
                'code' => '74.11.05',
                'name' => 'Lambandia',
            ),

            array (
                'code' => '74.11.06',
                'name' => 'Lalolae',
            ),

            array (
                'code' => '74.11.07',
                'name' => 'Mowewe',
            ),

            array (
                'code' => '74.11.08',
                'name' => 'Uluiwoi',
            ),

            array (
                'code' => '74.11.09',
                'name' => 'Tinondo',
            ),

            array (
                'code' => '74.11.10',
                'name' => 'Aere',
            ),

            array (
                'code' => '74.11.11',
                'name' => 'Ueesi',
            ),

            array (
                'code' => '74.11.12',
                'name' => 'Dangia',
            ),

            array (
                'code' => '74.12',
                'name' => 'KAB. KONAWE KEPULAUAN',
            ),

            array (
                'code' => '74.12.01',
                'name' => 'Wawonii Barat',
            ),

            array (
                'code' => '74.12.02',
                'name' => 'Wawonii Utara',
            ),

            array (
                'code' => '74.12.03',
                'name' => 'Wawonii Timur Laut',
            ),

            array (
                'code' => '74.12.04',
                'name' => 'Wawonii Timur',
            ),

            array (
                'code' => '74.12.05',
                'name' => 'Wawonii Tenggara',
            ),

            array (
                'code' => '74.12.06',
                'name' => 'Wawonii Selatan',
            ),

            array (
                'code' => '74.12.07',
                'name' => 'Wawonii Tengah',
            ),

            array (
                'code' => '74.13',
                'name' => 'KAB. MUNA BARAT',
            ),

            array (
                'code' => '74.13.01',
                'name' => 'Sawerigadi',
            ),

            array (
                'code' => '74.13.02',
                'name' => 'Barangka',
            ),

            array (
                'code' => '74.13.03',
                'name' => 'Lawa',
            ),

            array (
                'code' => '74.13.04',
                'name' => 'Wadaga',
            ),

            array (
                'code' => '74.13.05',
                'name' => 'Tiworo Selatan',
            ),

            array (
                'code' => '74.13.06',
                'name' => 'Maginti',
            ),

            array (
                'code' => '74.13.07',
                'name' => 'Tiworo Tengah',
            ),

            array (
                'code' => '74.13.08',
                'name' => 'Tiworo Utara',
            ),

            array (
                'code' => '74.13.09',
                'name' => 'Tiworo Kepulauan',
            ),

            array (
                'code' => '74.13.10',
                'name' => 'Kusambi',
            ),

            array (
                'code' => '74.13.11',
                'name' => 'Napano Kusambi',
            ),

            array (
                'code' => '74.14',
                'name' => 'KAB. BUTON TENGAH',
            ),

            array (
                'code' => '74.14.01',
                'name' => 'Lakudo',
            ),

            array (
                'code' => '74.14.02',
                'name' => 'Mawasangka Timur',
            ),

            array (
                'code' => '74.14.03',
                'name' => 'Mawasangka Tengah',
            ),

            array (
                'code' => '74.14.04',
                'name' => 'Mawasangka',
            ),

            array (
                'code' => '74.14.05',
                'name' => 'Talaga Raya',
            ),

            array (
                'code' => '74.14.06',
                'name' => 'Gu',
            ),

            array (
                'code' => '74.14.07',
                'name' => 'Sangia Wambulu',
            ),

            array (
                'code' => '74.15',
                'name' => 'KAB. BUTON SELATAN',
            ),

            array (
                'code' => '74.15.01',
                'name' => 'Batauga',
            ),

            array (
                'code' => '74.15.02',
                'name' => 'Sampolawa',
            ),

            array (
                'code' => '74.15.03',
                'name' => 'Lapandewa',
            ),

            array (
                'code' => '74.15.04',
                'name' => 'Batu Atas',
            ),

            array (
                'code' => '74.15.05',
                'name' => 'Siompu Barat',
            ),

            array (
                'code' => '74.15.06',
                'name' => 'Siompu',
            ),

            array (
                'code' => '74.15.07',
                'name' => 'Kadatua',
            ),

            array (
                'code' => '74.71',
                'name' => 'KOTA KENDARI',
            ),

            array (
                'code' => '74.71.01',
                'name' => 'Mandonga',
            ),

            array (
                'code' => '74.71.02',
                'name' => 'Kendari',
            ),

            array (
                'code' => '74.71.03',
                'name' => 'Baruga',
            ),

            array (
                'code' => '74.71.04',
                'name' => 'Poasia',
            ),

            array (
                'code' => '74.71.05',
                'name' => 'Kendari Barat',
            ),

            array (
                'code' => '74.71.06',
                'name' => 'Abeli',
            ),

            array (
                'code' => '74.71.07',
                'name' => 'Wua-Wua',
            ),

            array (
                'code' => '74.71.08',
                'name' => 'Kadia',
            ),

            array (
                'code' => '74.71.09',
                'name' => 'Puuwatu',
            ),

            array (
                'code' => '74.71.10',
                'name' => 'Kambu',
            ),

            array (
                'code' => '74.72',
                'name' => 'KOTA BAU BAU',
            ),

            array (
                'code' => '74.72.01',
                'name' => 'Betoambari',
            ),

            array (
                'code' => '74.72.02',
                'name' => 'Wolio',
            ),

            array (
                'code' => '74.72.03',
                'name' => 'Sorawolio',
            ),

            array (
                'code' => '74.72.04',
                'name' => 'Bungi',
            ),

            array (
                'code' => '74.72.05',
                'name' => 'Kokalukuna',
            ),

            array (
                'code' => '74.72.06',
                'name' => 'Murhum',
            ),

            array (
                'code' => '74.72.07',
                'name' => 'Lea-Lea',
            ),

            array (
                'code' => '74.72.08',
                'name' => 'Batupoaro',
            ),

            array (
                'code' => '75',
                'name' => 'GORONTALO',
            ),

            array (
                'code' => '75.01',
                'name' => 'KAB. GORONTALO',
            ),

            array (
                'code' => '75.01.01',
                'name' => 'Limboto',
            ),

            array (
                'code' => '75.01.02',
                'name' => 'Telaga',
            ),

            array (
                'code' => '75.01.03',
                'name' => 'Batudaa',
            ),

            array (
                'code' => '75.01.04',
                'name' => 'Tibawa',
            ),

            array (
                'code' => '75.01.05',
                'name' => 'Batudaa Pantai',
            ),

            array (
                'code' => '75.01.09',
                'name' => 'Boliyohuto',
            ),

            array (
                'code' => '75.01.10',
                'name' => 'Telaga Biru',
            ),

            array (
                'code' => '75.01.11',
                'name' => 'Bongomeme',
            ),

            array (
                'code' => '75.01.13',
                'name' => 'Tolangohula',
            ),

            array (
                'code' => '75.01.14',
                'name' => 'Mootilango',
            ),

            array (
                'code' => '75.01.16',
                'name' => 'Pulubala',
            ),

            array (
                'code' => '75.01.17',
                'name' => 'Limboto Barat',
            ),

            array (
                'code' => '75.01.18',
                'name' => 'Tilango',
            ),

            array (
                'code' => '75.01.19',
                'name' => 'Tabongo',
            ),

            array (
                'code' => '75.01.20',
                'name' => 'Biluhu',
            ),

            array (
                'code' => '75.01.21',
                'name' => 'Asparaga',
            ),

            array (
                'code' => '75.01.22',
                'name' => 'Talaga Jaya',
            ),

            array (
                'code' => '75.01.23',
                'name' => 'Bilato',
            ),

            array (
                'code' => '75.01.24',
                'name' => 'Dungaliyo',
            ),

            array (
                'code' => '75.02',
                'name' => 'KAB. BOALEMO',
            ),

            array (
                'code' => '75.02.01',
                'name' => 'Paguyaman',
            ),

            array (
                'code' => '75.02.02',
                'name' => 'Wonosari',
            ),

            array (
                'code' => '75.02.03',
                'name' => 'Dulupi',
            ),

            array (
                'code' => '75.02.04',
                'name' => 'Tilamuta',
            ),

            array (
                'code' => '75.02.05',
                'name' => 'Mananggu',
            ),

            array (
                'code' => '75.02.06',
                'name' => 'Botumoita',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '75.02.07',
                'name' => 'Paguyaman Pantai',
            ),

            array (
                'code' => '75.03',
                'name' => 'KAB. BONE BOLANGO',
            ),

            array (
                'code' => '75.03.01',
                'name' => 'Tapa',
            ),

            array (
                'code' => '75.03.02',
                'name' => 'Kabila',
            ),

            array (
                'code' => '75.03.03',
                'name' => 'Suwawa',
            ),

            array (
                'code' => '75.03.04',
                'name' => 'Bonepantai',
            ),

            array (
                'code' => '75.03.05',
                'name' => 'Bulango Utara',
            ),

            array (
                'code' => '75.03.06',
                'name' => 'Tilongkabila',
            ),

            array (
                'code' => '75.03.07',
                'name' => 'Botupingge',
            ),

            array (
                'code' => '75.03.08',
                'name' => 'Kabila Bone',
            ),

            array (
                'code' => '75.03.09',
                'name' => 'Bone',
            ),

            array (
                'code' => '75.03.10',
                'name' => 'Bone Raya',
            ),

            array (
                'code' => '75.03.11',
                'name' => 'Suwawa Timur',
            ),

            array (
                'code' => '75.03.12',
                'name' => 'Suwawa Selatan',
            ),

            array (
                'code' => '75.03.13',
                'name' => 'Suwawa Tengah',
            ),

            array (
                'code' => '75.03.14',
                'name' => 'Bulango Ulu',
            ),

            array (
                'code' => '75.03.15',
                'name' => 'Bulango Selatan',
            ),

            array (
                'code' => '75.03.16',
                'name' => 'Bulango Timur',
            ),

            array (
                'code' => '75.03.17',
                'name' => 'Bulawa',
            ),

            array (
                'code' => '75.03.18',
                'name' => 'Pinogu',
            ),

            array (
                'code' => '75.04',
                'name' => 'KAB. PAHUWATO',
            ),

            array (
                'code' => '75.04.01',
                'name' => 'Popayato',
            ),

            array (
                'code' => '75.04.02',
                'name' => 'Lemito',
            ),

            array (
                'code' => '75.04.03',
                'name' => 'Randangan',
            ),

            array (
                'code' => '75.04.04',
                'name' => 'Marisa',
            ),

            array (
                'code' => '75.04.05',
                'name' => 'Paguat',
            ),

            array (
                'code' => '75.04.06',
                'name' => 'Patilanggio',
            ),

            array (
                'code' => '75.04.07',
                'name' => 'Taluditi',
            ),

            array (
                'code' => '75.04.08',
                'name' => 'Dengilo',
            ),

            array (
                'code' => '75.04.09',
                'name' => 'Buntulia',
            ),

            array (
                'code' => '75.04.10',
                'name' => 'Duhiadaa',
            ),

            array (
                'code' => '75.04.11',
                'name' => 'Wanggarasi',
            ),

            array (
                'code' => '75.04.12',
                'name' => 'Popayato Timur',
            ),

            array (
                'code' => '75.04.13',
                'name' => 'Popayato Barat',
            ),

            array (
                'code' => '75.05',
                'name' => 'KAB. GORONTALO UTARA',
            ),

            array (
                'code' => '75.05.01',
                'name' => 'Atinggola',
            ),

            array (
                'code' => '75.05.02',
                'name' => 'Kwandang',
            ),

            array (
                'code' => '75.05.03',
                'name' => 'Anggrek',
            ),

            array (
                'code' => '75.05.04',
                'name' => 'Sumalata',
            ),

            array (
                'code' => '75.05.05',
                'name' => 'Tolinggula',
            ),

            array (
                'code' => '75.05.06',
                'name' => 'Gentuma Raya',
            ),

            array (
                'code' => '75.05.07',
                'name' => 'Tomolito',
            ),

            array (
                'code' => '75.05.08',
                'name' => 'Ponelo Kepulauan',
            ),

            array (
                'code' => '75.05.09',
                'name' => 'Monano',
            ),

            array (
                'code' => '75.05.10',
                'name' => 'Biau',
            ),

            array (
                'code' => '75.05.11',
                'name' => 'Sumalata Timur',
            ),

            array (
                'code' => '75.71',
                'name' => 'KOTA GORONTALO',
            ),

            array (
                'code' => '75.71.01',
                'name' => 'Kota Barat',
            ),

            array (
                'code' => '75.71.02',
                'name' => 'Kota Selatan',
            ),

            array (
                'code' => '75.71.03',
                'name' => 'Kota Utara',
            ),

            array (
                'code' => '75.71.04',
                'name' => 'Dungingi',
            ),

            array (
                'code' => '75.71.05',
                'name' => 'Kota Timur',
            ),

            array (
                'code' => '75.71.06',
                'name' => 'Kota Tengah',
            ),

            array (
                'code' => '75.71.07',
                'name' => 'Sipatana',
            ),

            array (
                'code' => '75.71.08',
                'name' => 'Dumbo Raya',
            ),

            array (
                'code' => '75.71.09',
                'name' => 'Hulonthalangi',
            ),

            array (
                'code' => '76',
                'name' => 'SULAWESI BARAT',
            ),

            array (
                'code' => '76.01',
                'name' => 'KAB. MAMUJU UTARA',
            ),

            array (
                'code' => '76.01.01',
                'name' => 'Bambalamotu',
            ),

            array (
                'code' => '76.01.02',
                'name' => 'Pasangkayu',
            ),

            array (
                'code' => '76.01.03',
                'name' => 'Baras',
            ),

            array (
                'code' => '76.01.04',
                'name' => 'Sarudu',
            ),

            array (
                'code' => '76.01.05',
                'name' => 'Dapurang',
            ),

            array (
                'code' => '76.01.06',
                'name' => 'Duripoku',
            ),

            array (
                'code' => '76.01.07',
                'name' => 'Bulu Taba',
            ),

            array (
                'code' => '76.01.08',
                'name' => 'Tikke Raya',
            ),

            array (
                'code' => '76.01.09',
                'name' => 'Pedongga',
            ),

            array (
                'code' => '76.01.10',
                'name' => 'Bambaira',
            ),

            array (
                'code' => '76.01.11',
                'name' => 'Sarjo',
            ),

            array (
                'code' => '76.01.12',
                'name' => 'Lariang',
            ),

            array (
                'code' => '76.02',
                'name' => 'KAB. MAMUJU',
            ),

            array (
                'code' => '76.02.01',
                'name' => 'Mamuju',
            ),

            array (
                'code' => '76.02.02',
                'name' => 'Tapalang',
            ),

            array (
                'code' => '76.02.03',
                'name' => 'Kalukku',
            ),

            array (
                'code' => '76.02.04',
                'name' => 'Kalumpang',
            ),

            array (
                'code' => '76.02.07',
                'name' => 'Papalang',
            ),

            array (
                'code' => '76.02.08',
                'name' => 'Sampaga',
            ),

            array (
                'code' => '76.02.11',
                'name' => 'Tommo',
            ),

            array (
                'code' => '76.02.12',
                'name' => 'Simboro dan Kepulauan',
            ),

            array (
                'code' => '76.02.13',
                'name' => 'Tapalang Barat',
            ),

            array (
                'code' => '76.02.15',
                'name' => 'Bonehau',
            ),

            array (
                'code' => '76.02.16',
                'name' => 'Kep. Bala Balakang  2',
            ),

            array (
                'code' => '76.03',
                'name' => 'KAB. MAMASA',
            ),

            array (
                'code' => '76.03.01',
                'name' => 'Mambi',
            ),

            array (
                'code' => '76.03.02',
                'name' => 'Aralle',
            ),

            array (
                'code' => '76.03.03',
                'name' => 'Mamasa',
            ),

            array (
                'code' => '76.03.04',
                'name' => 'Pana',
            ),

            array (
                'code' => '76.03.05',
                'name' => 'Tabulahan',
            ),

            array (
                'code' => '76.03.06',
                'name' => 'Sumarorong',
            ),

            array (
                'code' => '76.03.07',
                'name' => 'Messawa',
            ),

            array (
                'code' => '76.03.08',
                'name' => 'Sesenapadang',
            ),

            array (
                'code' => '76.03.09',
                'name' => 'Tanduk Kalua',
            ),

            array (
                'code' => '76.03.10',
                'name' => 'Tabang',
            ),

            array (
                'code' => '76.03.11',
                'name' => 'Bambang',
            ),

            array (
                'code' => '76.03.12',
                'name' => 'Balla',
            ),

            array (
                'code' => '76.03.13',
                'name' => 'Nosu',
            ),

            array (
                'code' => '76.03.14',
                'name' => 'Tawalian',
            ),

            array (
                'code' => '76.03.15',
                'name' => 'Rantebulahan Timur',
            ),

            array (
                'code' => '76.03.16',
                'name' => 'Buntumalangka',
            ),

            array (
                'code' => '76.03.17',
                'name' => 'Mehalaan',
            ),

            array (
                'code' => '76.04',
                'name' => 'KAB. POLEWALI MANDAR',
            ),

            array (
                'code' => '76.04.01',
                'name' => 'Tinambung',
            ),

            array (
                'code' => '76.04.02',
                'name' => 'Campalagian',
            ),

            array (
                'code' => '76.04.03',
                'name' => 'Wonomulyo',
            ),

            array (
                'code' => '76.04.04',
                'name' => 'Polewali',
            ),

            array (
                'code' => '76.04.05',
                'name' => 'Tutar',
            ),

            array (
                'code' => '76.04.06',
                'name' => 'Binuang',
            ),

            array (
                'code' => '76.04.07',
                'name' => 'Tapango',
            ),

            array (
                'code' => '76.04.08',
                'name' => 'Mapilli',
            ),

            array (
                'code' => '76.04.09',
                'name' => 'Matangnga',
            ),

            array (
                'code' => '76.04.10',
                'name' => 'Luyo',
            ),

            array (
                'code' => '76.04.11',
                'name' => 'Limboro',
            ),

            array (
                'code' => '76.04.12',
                'name' => 'Balanipa',
            ),

            array (
                'code' => '76.04.13',
                'name' => 'Anreapi',
            ),

            array (
                'code' => '76.04.14',
                'name' => 'Matakali',
            ),

            array (
                'code' => '76.04.15',
                'name' => 'Allu',
            ),

            array (
                'code' => '76.04.16',
                'name' => 'Bulo',
            ),

            array (
                'code' => '76.05',
                'name' => 'KAB. MAJENE',
            ),

            array (
                'code' => '76.05.01',
                'name' => 'Banggae',
            ),

            array (
                'code' => '76.05.02',
                'name' => 'Pamboang',
            ),

            array (
                'code' => '76.05.03',
                'name' => 'Sendana',
            ),

            array (
                'code' => '76.05.04',
                'name' => 'Malunda',
            ),

            array (
                'code' => '76.05.05',
                'name' => 'Ulumanda',
            ),

            array (
                'code' => '76.05.06',
                'name' => 'Tammerodo Sendana',
            ),

            array (
                'code' => '76.05.07',
                'name' => 'Tubo Sendana',
            ),

            array (
                'code' => '76.05.08',
                'name' => 'Banggae Timur',
            ),

            array (
                'code' => '76.06',
                'name' => 'KAB. MAMUJU TENGAH',
            ),

            array (
                'code' => '76.06.01',
                'name' => 'Tobadak',
            ),

            array (
                'code' => '76.06.02',
                'name' => 'Pangale',
            ),

            array (
                'code' => '76.06.03',
                'name' => 'Budong-Budong',
            ),

            array (
                'code' => '76.06.04',
                'name' => 'Topoyo',
            ),

            array (
                'code' => '76.06.05',
                'name' => 'Karossa',
            ),

            array (
                'code' => '81',
                'name' => 'MALUKU',
            ),

            array (
                'code' => '81.01',
                'name' => 'KAB. MALUKU TENGAH',
            ),

            array (
                'code' => '81.01.01',
                'name' => 'Amahai',
            ),

            array (
                'code' => '81.01.02',
                'name' => 'Teon Nila Serua',
            ),

            array (
                'code' => '81.01.06',
                'name' => 'Seram Utara',
            ),

            array (
                'code' => '81.01.09',
                'name' => 'Banda',
            ),

            array (
                'code' => '81.01.11',
                'name' => 'Tehoru',
            ),

            array (
                'code' => '81.01.12',
                'name' => 'Saparua',
            ),

            array (
                'code' => '81.01.13',
                'name' => 'Pulau Haruku',
            ),

            array (
                'code' => '81.01.14',
                'name' => 'Salahutu',
            ),

            array (
                'code' => '81.01.15',
                'name' => 'Leihitu',
            ),

            array (
                'code' => '81.01.16',
                'name' => 'Nusa Laut',
            ),

            array (
                'code' => '81.01.17',
                'name' => 'Kota Masohi',
            ),

            array (
                'code' => '81.01.20',
                'name' => 'Seram Utara Barat',
            ),

            array (
                'code' => '81.01.21',
                'name' => 'Teluk Elpaputih',
            ),

            array (
                'code' => '81.01.22',
                'name' => 'Leihitu Barat',
            ),

            array (
                'code' => '81.01.23',
                'name' => 'Telutih',
            ),

            array (
                'code' => '81.01.24',
                'name' => 'Seram Utara Timur Seti',
            ),

            array (
                'code' => '81.01.25',
                'name' => 'Seram Utara Timur Kobi',
            ),

            array (
                'code' => '81.01.26',
                'name' => 'Saparua Timur',
            ),

            array (
                'code' => '81.02',
                'name' => 'KAB. MALUKU TENGGARA',
            ),

            array (
                'code' => '81.02.01',
                'name' => 'Kei Kecil',
            ),

            array (
                'code' => '81.02.03',
                'name' => 'Kei Besar',
            ),

            array (
                'code' => '81.02.04',
                'name' => 'Kei Besar Selatan',
            ),

            array (
                'code' => '81.02.05',
                'name' => 'Kei Besar Utara Timur',
            ),

            array (
                'code' => '81.02.13',
                'name' => 'Kei Kecil Timur',
            ),

            array (
                'code' => '81.02.14',
                'name' => 'Kei Kecil Barat',
            ),

            array (
                'code' => '81.02.15',
                'name' => 'Manyeuw',
            ),

            array (
                'code' => '81.02.16',
                'name' => 'Hoat Sorbay',
            ),

            array (
                'code' => '81.02.17',
                'name' => 'Kei Besar Utara Barat',
            ),

            array (
                'code' => '81.02.18',
                'name' => 'Kei Besar Selatan Barat',
            ),

            array (
                'code' => '81.02.19',
                'name' => 'Kei Kecil Timur Selatan',
            ),

            array (
                'code' => '81.03',
                'name' => 'KAB MALUKU TENGGARA BARAT',
            ),

            array (
                'code' => '81.03.01',
                'name' => 'Tanimbar Selatan',
            ),

            array (
                'code' => '81.03.02',
                'name' => 'Selaru',
            ),

            array (
                'code' => '81.03.03',
                'name' => 'Wer Tamrian',
            ),

            array (
                'code' => '81.03.04',
                'name' => 'Wer Maktian',
            ),

            array (
                'code' => '81.03.05',
                'name' => 'Tanimbar Utara',
            ),

            array (
                'code' => '81.03.06',
                'name' => 'Yaru',
            ),

            array (
                'code' => '81.03.07',
                'name' => 'Wuar Labobar',
            ),

            array (
                'code' => '81.03.08',
                'name' => 'Kormomolin',
            ),

            array (
                'code' => '81.03.09',
                'name' => 'Nirunmas',
            ),

            array (
                'code' => '81.03.18',
                'name' => 'Molu Maru',
            ),

            array (
                'code' => '81.04',
                'name' => 'KAB. BURU',
            ),

            array (
                'code' => '81.04.01',
                'name' => 'Namlea',
            ),

            array (
                'code' => '81.04.02',
                'name' => 'Air Buaya',
            ),

            array (
                'code' => '81.04.03',
                'name' => 'Waeapo',
            ),

            array (
                'code' => '81.04.06',
                'name' => 'Waplau',
            ),

            array (
                'code' => '81.04.10',
                'name' => 'Batabual',
            ),

            array (
                'code' => '81.04.11',
                'name' => 'Lolong Guba',
            ),

            array (
                'code' => '81.04.12',
                'name' => 'Waelata',
            ),

            array (
                'code' => '81.04.13',
                'name' => 'Fena Leisela',
            ),

            array (
                'code' => '81.04.14',
                'name' => 'Teluk Kaiely',
            ),

            array (
                'code' => '81.04.15',
                'name' => 'Lilialy',
            ),

            array (
                'code' => '81.05',
                'name' => 'KAB. SERAM BAGIAN TIMUR',
            ),

            array (
                'code' => '81.05.01',
                'name' => 'Bula',
            ),

            array (
                'code' => '81.05.02',
                'name' => 'Seram Timur',
            ),

            array (
                'code' => '81.05.03',
                'name' => 'Werinama',
            ),

            array (
                'code' => '81.05.04',
                'name' => 'Pulau Gorom',
            ),

            array (
                'code' => '81.05.05',
                'name' => 'Wakate',
            ),

            array (
                'code' => '81.05.06',
                'name' => 'Tutuk Tolu',
            ),

            array (
                'code' => '81.05.07',
                'name' => 'Siwalalat',
            ),

            array (
                'code' => '81.05.08',
                'name' => 'Kilmury',
            ),

            array (
                'code' => '81.05.09',
                'name' => 'Pulau Panjang',
            ),

            array (
                'code' => '81.05.10',
                'name' => 'Teor',
            ),

            array (
                'code' => '81.05.11',
                'name' => 'Gorom Timur',
            ),

            array (
                'code' => '81.05.12',
                'name' => 'Bula Barat',
            ),

            array (
                'code' => '81.05.13',
                'name' => 'Kian Darat',
            ),

            array (
                'code' => '81.05.14',
                'name' => 'Siritaun Wida Timur',
            ),

            array (
                'code' => '81.05.15',
                'name' => 'Teluk Waru',
            ),

            array (
                'code' => '81.06',
                'name' => 'KAB. SERAM BAGIAN BARAT',
            ),

            array (
                'code' => '81.06.01',
                'name' => 'Kairatu',
            ),

            array (
                'code' => '81.06.02',
                'name' => 'Seram Barat',
            ),

            array (
                'code' => '81.06.03',
                'name' => 'Taniwel',
            ),

            array (
                'code' => '81.06.04',
                'name' => 'Huamual Belakang',
            ),

            array (
                'code' => '81.06.05',
                'name' => 'Amalatu',
            ),

            array (
                'code' => '81.06.06',
                'name' => 'Inamosol',
            ),

            array (
                'code' => '81.06.07',
                'name' => 'Kairatu Barat',
            ),

            array (
                'code' => '81.06.08',
                'name' => 'Huamual',
            ),

            array (
                'code' => '81.06.09',
                'name' => 'Kepulauan Manipa',
            ),

            array (
                'code' => '81.06.10',
                'name' => 'Taniwel Timur',
            ),

            array (
                'code' => '81.06.11',
                'name' => 'Elpaputih',
            ),

            array (
                'code' => '81.07',
                'name' => 'KAB. KEPULAUAN ARU',
            ),

            array (
                'code' => '81.07.01',
                'name' => 'Pulau-Pulau Aru',
            ),

            array (
                'code' => '81.07.02',
                'name' => 'Aru Selatan',
            ),

            array (
                'code' => '81.07.03',
                'name' => 'Aru Tengah',
            ),

            array (
                'code' => '81.07.04',
                'name' => 'Aru Utara',
            ),

            array (
                'code' => '81.07.05',
                'name' => 'Aru Utara Timur Batuley',
            ),

            array (
                'code' => '81.07.06',
                'name' => 'Sir-Sir',
            ),

            array (
                'code' => '81.07.07',
                'name' => 'Aru Tengah Timur',
            ),

            array (
                'code' => '81.07.08',
                'name' => 'Aru Tengah Selatan',
            ),

            array (
                'code' => '81.07.09',
                'name' => 'Aru Selatan Timur',
            ),

            array (
                'code' => '81.07.10',
                'name' => 'Aru Selatan Utara',
            ),

            array (
                'code' => '81.08',
                'name' => 'KAB. MALUKU BARAT DAYA',
            ),

            array (
                'code' => '81.08.01',
                'name' => 'Moa Lakor',
            ),

            array (
                'code' => '81.08.02',
                'name' => 'Damer',
            ),

            array (
                'code' => '81.08.03',
                'name' => 'Mndona Hiera',
            ),

            array (
                'code' => '81.08.04',
                'name' => 'Pulau-Pulau Babar',
            ),

            array (
                'code' => '81.08.05',
                'name' => 'Pulau-pulau Babar Timur',
            ),

            array (
                'code' => '81.08.06',
                'name' => 'Wetar',
            ),

            array (
                'code' => '81.08.07',
                'name' => 'Pulau-pulau Terselatan',
            ),

            array (
                'code' => '81.08.08',
                'name' => 'Pulau Leti',
            ),

            array (
                'code' => '81.08.09',
                'name' => 'Pulau Masela',
            ),

            array (
                'code' => '81.08.10',
                'name' => 'Dawelor Dawera',
            ),

            array (
                'code' => '81.08.11',
                'name' => 'Pulau Wetang',
            ),

            array (
                'code' => '81.08.12',
                'name' => 'Pulau Lakor',
            ),

            array (
                'code' => '81.08.13',
                'name' => 'Wetar Utara',
            ),

            array (
                'code' => '81.08.14',
                'name' => 'Wetar Barat',
            ),

            array (
                'code' => '81.08.15',
                'name' => 'Wetar Timur',
            ),

            array (
                'code' => '81.08.16',
                'name' => 'Kepulauan Romang',
            ),

            array (
                'code' => '81.08.17',
                'name' => 'Kisar Utara',
            ),

            array (
                'code' => '81.09',
                'name' => 'KAB. BURU SELATAN',
            ),

            array (
                'code' => '81.09.01',
                'name' => 'Namrole',
            ),

            array (
                'code' => '81.09.02',
                'name' => 'Waesama',
            ),

            array (
                'code' => '81.09.03',
                'name' => 'Ambalau',
            ),

            array (
                'code' => '81.09.04',
                'name' => 'Kepala Madan',
            ),

            array (
                'code' => '81.09.05',
                'name' => 'Leksula',
            ),

            array (
                'code' => '81.09.06',
                'name' => 'Fena Fafan',
            ),

            array (
                'code' => '81.71',
                'name' => 'KOTA AMBON',
            ),

            array (
                'code' => '81.71.01',
                'name' => 'Nusaniwe',
            ),

            array (
                'code' => '81.71.02',
                'name' => 'Sirimau',
            ),

            array (
                'code' => '81.71.03',
                'name' => 'Baguala',
            ),

            array (
                'code' => '81.71.04',
                'name' => 'Teluk Ambon',
            ),

            array (
                'code' => '81.71.05',
                'name' => 'Leitimur Selatan',
            ),

            array (
                'code' => '81.72',
                'name' => 'KOTA TUAL',
            ),

            array (
                'code' => '81.72.01',
                'name' => 'Pulau Dullah Utara',
            ),

            array (
                'code' => '81.72.02',
                'name' => 'Pulau Dullah Selatan',
            ),

            array (
                'code' => '81.72.03',
                'name' => 'Tayando Tam',
            ),

            array (
                'code' => '81.72.04',
                'name' => 'Pulau-Pulau Kur',
            ),

            array (
                'code' => '81.72.05',
                'name' => 'Kur Selatan',
            ),

            array (
                'code' => '82',
                'name' => 'MALUKU UTARA',
            ),

            array (
                'code' => '82.01',
                'name' => 'KAB. HALMAHERA BARAT',
            ),

            array (
                'code' => '82.01.01',
                'name' => 'Jailolo',
            ),

            array (
                'code' => '82.01.02',
                'name' => 'Loloda',
            ),

            array (
                'code' => '82.01.03',
                'name' => 'Ibu',
            ),

            array (
                'code' => '82.01.04',
                'name' => 'Sahu',
            ),

            array (
                'code' => '82.01.05',
                'name' => 'Jailolo Selatan',
            ),

            array (
                'code' => '82.01.07',
                'name' => 'Ibu Utara',
            ),

            array (
                'code' => '82.01.08',
                'name' => 'Ibu Selatan',
            ),

            array (
                'code' => '82.01.09',
                'name' => 'Sahu Timur',
            ),

            array (
                'code' => '82.02',
                'name' => 'KAB. HALMAHERA TENGAH',
            ),

            array (
                'code' => '82.02.01',
                'name' => 'Weda',
            ),

            array (
                'code' => '82.02.02',
                'name' => 'Patani',
            ),

            array (
                'code' => '82.02.03',
                'name' => 'Pulau Gebe',
            ),

            array (
                'code' => '82.02.04',
                'name' => 'Weda Utara',
            ),

            array (
                'code' => '82.02.05',
                'name' => 'Weda Selatan',
            ),

            array (
                'code' => '82.02.06',
                'name' => 'Patani Utara',
            ),

            array (
                'code' => '82.02.07',
                'name' => 'Weda Tengah',
            ),

            array (
                'code' => '82.02.08',
                'name' => 'Patani Barat',
            ),

            array (
                'code' => '82.02.09',
                'name' => 'Weda Timur',
            ),

            array (
                'code' => '82.02.10',
                'name' => 'Patani Timur',
            ),

            array (
                'code' => '82.03',
                'name' => 'KAB. HALMAHERA UTARA',
            ),

            array (
                'code' => '82.03.04',
                'name' => 'Galela',
            ),

            array (
                'code' => '82.03.05',
                'name' => 'Tobelo',
            ),

            array (
                'code' => '82.03.06',
                'name' => 'Tobelo Selatan',
            ),

            array (
                'code' => '82.03.07',
                'name' => 'Kao',
            ),

            array (
                'code' => '82.03.08',
                'name' => 'Malifut',
            ),

            array (
                'code' => '82.03.09',
                'name' => 'Loloda Utara',
            ),

            array (
                'code' => '82.03.10',
                'name' => 'Tobelo Utara',
            ),

            array (
                'code' => '82.03.11',
                'name' => 'Tobelo Tengah',
            ),

            array (
                'code' => '82.03.12',
                'name' => 'Tobelo Timur',
            ),

            array (
                'code' => '82.03.13',
                'name' => 'Tobelo Barat',
            ),

            array (
                'code' => '82.03.14',
                'name' => 'Galela Barat',
            ),

            array (
                'code' => '82.03.15',
                'name' => 'Galela Utara',
            ),

            array (
                'code' => '82.03.16',
                'name' => 'Galela Selatan',
            ),

            array (
                'code' => '82.03.19',
                'name' => 'Loloda Kepulauan',
            ),

            array (
                'code' => '82.03.20',
                'name' => 'Kao Utara',
            ),

            array (
                'code' => '82.03.21',
                'name' => 'Kao Barat',
            ),

            array (
                'code' => '82.03.22',
                'name' => 'Kao Teluk',
            ),

            array (
                'code' => '82.04',
                'name' => 'KAB. HALMAHERA SELATAN',
            ),

            array (
                'code' => '82.04.01',
                'name' => 'Pulau Makian',
            ),

            array (
                'code' => '82.04.02',
                'name' => 'Kayoa',
            ),

            array (
                'code' => '82.04.03',
                'name' => 'Gane Timur',
            ),

            array (
                'code' => '82.04.04',
                'name' => 'Gane Barat',
            ),

            array (
                'code' => '82.04.05',
                'name' => 'Obi Selatan',
            ),

            array (
                'code' => '82.04.06',
                'name' => 'Obi',
            ),

            array (
                'code' => '82.04.07',
                'name' => 'Bacan Timur',
            ),

            array (
                'code' => '82.04.08',
                'name' => 'Bacan',
            ),

            array (
                'code' => '82.04.09',
                'name' => 'Bacan Barat',
            ),

            array (
                'code' => '82.04.10',
                'name' => 'Makian Barat',
            ),

            array (
                'code' => '82.04.11',
                'name' => 'Kayoa Barat',
            ),

            array (
                'code' => '82.04.12',
                'name' => 'Kayoa Selatan',
            ),

            array (
                'code' => '82.04.13',
                'name' => 'Kayoa Utara',
            ),

            array (
                'code' => '82.04.14',
                'name' => 'Bacan Barat Utara',
            ),

            array (
                'code' => '82.04.15',
                'name' => 'Kasiruta Barat',
            ),

            array (
                'code' => '82.04.16',
                'name' => 'Kasiruta Timur',
            ),

            array (
                'code' => '82.04.17',
                'name' => 'Bacan Selatan',
            ),

            array (
                'code' => '82.04.18',
                'name' => 'Kepulauan Botanglomang',
            ),

            array (
                'code' => '82.04.19',
                'name' => 'Mandioli Selatan',
            ),

            array (
                'code' => '82.04.20',
                'name' => 'Mandioli Utara',
            ),

            array (
                'code' => '82.04.21',
                'name' => 'Bacan Timur Selatan',
            ),

            array (
                'code' => '82.04.22',
                'name' => 'Bacan Timur Tengah',
            ),

            array (
                'code' => '82.04.23',
                'name' => 'Gane Barat Selatan',
            ),

            array (
                'code' => '82.04.24',
                'name' => 'Gane Barat Utara',
            ),

            array (
                'code' => '82.04.25',
                'name' => 'Kepulauan Joronga',
            ),

            array (
                'code' => '82.04.26',
                'name' => 'Gane Timur Selatan',
            ),

            array (
                'code' => '82.04.27',
                'name' => 'Gane Timur Tengah',
            ),

            array (
                'code' => '82.04.28',
                'name' => 'Obi Barat',
            ),

            array (
                'code' => '82.04.29',
                'name' => 'Obi Timur',
            ),

            array (
                'code' => '82.04.30',
                'name' => 'Obi Utara',
            ),

            array (
                'code' => '82.05',
                'name' => 'KAB. KEPULAUAN SULA',
            ),

            array (
                'code' => '82.05.01',
                'name' => 'Mangoli Timur',
            ),

            array (
                'code' => '82.05.02',
                'name' => 'Sanana',
            ),

            array (
                'code' => '82.05.03',
                'name' => 'Sulabesi Barat',
            ),

            array (
                'code' => '82.05.06',
                'name' => 'Mangoli Barat',
            ),

            array (
                'code' => '82.05.07',
                'name' => 'Sulabesi Tengah',
            ),

            array (
                'code' => '82.05.08',
                'name' => 'Sulabesi Timur',
            ),

            array (
                'code' => '82.05.09',
                'name' => 'Sulabesi Selatan',
            ),

            array (
                'code' => '82.05.10',
                'name' => 'Mangoli Utara Timur',
            ),

            array (
                'code' => '82.05.11',
                'name' => 'Mangoli Tengah',
            ),

            array (
                'code' => '82.05.12',
                'name' => 'Mangoli Selatan',
            ),

            array (
                'code' => '82.05.13',
                'name' => 'Mangoli Utara',
            ),

            array (
                'code' => '82.05.18',
                'name' => 'Sanana Utara',
            ),

            array (
                'code' => '82.06',
                'name' => 'KAB. HALMAHERA TIMUR',
            ),

            array (
                'code' => '82.06.01',
                'name' => 'Wasile',
            ),

            array (
                'code' => '82.06.02',
                'name' => 'Maba',
            ),

            array (
                'code' => '82.06.03',
                'name' => 'Maba Selatan',
            ),

            array (
                'code' => '82.06.04',
                'name' => 'Wasile Selatan',
            ),

            array (
                'code' => '82.06.05',
                'name' => 'Wasile Tengah',
            ),

            array (
                'code' => '82.06.06',
                'name' => 'Wasile Utara',
            ),

            array (
                'code' => '82.06.07',
                'name' => 'Wasile Timur',
            ),

            array (
                'code' => '82.06.08',
                'name' => 'Maba Tengah',
            ),

            array (
                'code' => '82.06.09',
                'name' => 'Maba Utara',
            ),

            array (
                'code' => '82.06.10',
                'name' => 'Kota Maba',
            ),

            array (
                'code' => '82.07',
                'name' => 'KAB. PULAU MOROTAI',
            ),

            array (
                'code' => '82.07.01',
                'name' => 'Morotai Selatan',
            ),

            array (
                'code' => '82.07.02',
                'name' => 'Morotai Selatan Barat',
            ),

            array (
                'code' => '82.07.03',
                'name' => 'Morotai Jaya',
            ),

            array (
                'code' => '82.07.04',
                'name' => 'Morotai Utara',
            ),

            array (
                'code' => '82.07.05',
                'name' => 'Morotai Timur',
            ),

            array (
                'code' => '82.08',
                'name' => 'KAB. PULAU TALIABU',
            ),

            array (
                'code' => '82.08.01',
                'name' => 'Taliabu Barat',
            ),

            array (
                'code' => '82.08.02',
                'name' => 'Taliabu Barat Laut',
            ),

            array (
                'code' => '82.08.03',
                'name' => 'Lede',
            ),

            array (
                'code' => '82.08.04',
                'name' => 'Taliabu Utara',
            ),

            array (
                'code' => '82.08.05',
                'name' => 'Taliabu Timur',
            ),

            array (
                'code' => '82.08.06',
                'name' => 'Taliabu Timur Selatan',
            ),

            array (
                'code' => '82.08.07',
                'name' => 'Taliabu Selatan',
            ),

            array (
                'code' => '82.08.08',
                'name' => 'Tabona',
            ),

            array (
                'code' => '82.71',
                'name' => 'KOTA TERNATE',
            ),

            array (
                'code' => '82.71.01',
                'name' => 'Pulau Ternate',
            ),

            array (
                'code' => '82.71.02',
                'name' => 'Kota Ternate Selatan',
            ),

            array (
                'code' => '82.71.03',
                'name' => 'Kota Ternate Utara',
            ),

            array (
                'code' => '82.71.04',
                'name' => 'Pulau Moti',
            ),

            array (
                'code' => '82.71.05',
                'name' => 'Pulau Batang Dua',
            ),

            array (
                'code' => '82.71.06',
                'name' => 'Kota Ternate Tengah',
            ),

            array (
                'code' => '82.71.07',
                'name' => 'Pulau Hiri',
            ),

            array (
                'code' => '82.72',
                'name' => 'KOTA TIDORE KEPULAUAN',
            ),

            array (
                'code' => '82.72.01',
                'name' => 'Tidore',
            ),

            array (
                'code' => '82.72.02',
                'name' => 'Oba Utara',
            ),

            array (
                'code' => '82.72.03',
                'name' => 'Oba',
            ),

            array (
                'code' => '82.72.04',
                'name' => 'Tidore Selatan',
            ),

            array (
                'code' => '82.72.05',
                'name' => 'Tidore Utara',
            ),

            array (
                'code' => '82.72.06',
                'name' => 'Oba Tengah',
            ),

            array (
                'code' => '82.72.07',
                'name' => 'Oba Selatan',
            ),

            array (
                'code' => '82.72.08',
                'name' => 'Tidore Timur',
            ),

            array (
                'code' => '91',
                'name' => 'P A P U A',
            ),

            array (
                'code' => '91.01',
                'name' => 'KAB. MERAUKE',
            ),

            array (
                'code' => '91.01.01',
                'name' => 'Merauke',
            ),

            array (
                'code' => '91.01.02',
                'name' => 'Muting',
            ),

            array (
                'code' => '91.01.03',
                'name' => 'Okaba',
            ),

            array (
                'code' => '91.01.04',
                'name' => 'Kimaam',
            ),

            array (
                'code' => '91.01.05',
                'name' => 'Semangga',
            ),

            array (
                'code' => '91.01.06',
                'name' => 'Tanah Miring',
            ),

            array (
                'code' => '91.01.07',
                'name' => 'Jagebob',
            ),

            array (
                'code' => '91.01.08',
                'name' => 'Sota',
            ),

            array (
                'code' => '91.01.09',
                'name' => 'Ulilin',
            ),

            array (
                'code' => '91.01.10',
                'name' => 'Elikobal',
            ),

            array (
                'code' => '91.01.11',
                'name' => 'Kurik',
            ),

            array (
                'code' => '91.01.12',
                'name' => 'Naukenjerai',
            ),

            array (
                'code' => '91.01.13',
                'name' => 'Animha',
            ),

            array (
                'code' => '91.01.14',
                'name' => 'Malind',
            ),

            array (
                'code' => '91.01.15',
                'name' => 'Tubang',
            ),

            array (
                'code' => '91.01.16',
                'name' => 'Ngguti',
            ),

            array (
                'code' => '91.01.17',
                'name' => 'Kaptel',
            ),

            array (
                'code' => '91.01.18',
                'name' => 'Tabonji',
            ),

            array (
                'code' => '91.01.19',
                'name' => 'Waan',
            ),

            array (
                'code' => '91.01.20',
                'name' => 'Ilwayab',
            ),

            array (
                'code' => '91.02',
                'name' => 'KAB. JAYAWIJAYA',
            ),

            array (
                'code' => '91.02.01',
                'name' => 'Wamena',
            ),

            array (
                'code' => '91.02.03',
                'name' => 'Kurulu',
            ),

            array (
                'code' => '91.02.04',
                'name' => 'Asologaima',
            ),

            array (
                'code' => '91.02.12',
                'name' => 'Hubikosi',
            ),

            array (
                'code' => '91.02.15',
                'name' => 'Bolakme',
            ),

            array (
                'code' => '91.02.25',
                'name' => 'Walelagama',
            ),

            array (
                'code' => '91.02.27',
                'name' => 'Musatfak',
            ),

            array (
                'code' => '91.02.28',
                'name' => 'Wolo',
            ),

            array (
                'code' => '91.02.29',
                'name' => 'Asolokobal',
            ),

            array (
                'code' => '91.02.34',
                'name' => 'Pelebaga',
            ),

            array (
                'code' => '91.02.35',
                'name' => 'Yalengga',
            ),

            array (
                'code' => '91.02.40',
                'name' => 'Trikora',
            ),

            array (
                'code' => '91.02.41',
                'name' => 'Napua',
            ),

            array (
                'code' => '91.02.42',
                'name' => 'Walaik',
            ),

            array (
                'code' => '91.02.43',
                'name' => 'Wouma',
            ),

            array (
                'code' => '91.02.44',
                'name' => 'Hubikiak',
            ),

            array (
                'code' => '91.02.45',
                'name' => 'Ibele',
            ),

            array (
                'code' => '91.02.46',
                'name' => 'Taelarek',
            ),

            array (
                'code' => '91.02.47',
                'name' => 'Itlay Hisage',
            ),

            array (
                'code' => '91.02.48',
                'name' => 'Siepkosi',
            ),

            array (
                'code' => '91.02.49',
                'name' => 'Usilimo',
            ),

            array (
                'code' => '91.02.50',
                'name' => 'Wita Waya',
            ),

            array (
                'code' => '91.02.51',
                'name' => 'Libarek',
            ),

            array (
                'code' => '91.02.52',
                'name' => 'Wadangku',
            ),

            array (
                'code' => '91.02.53',
                'name' => 'Pisugi',
            ),

            array (
                'code' => '91.02.54',
                'name' => 'Koragi',
            ),

            array (
                'code' => '91.02.55',
                'name' => 'Tagime',
            ),

            array (
                'code' => '91.02.56',
                'name' => 'Molagalome',
            ),

            array (
                'code' => '91.02.57',
                'name' => 'Tagineri',
            ),

            array (
                'code' => '91.02.58',
                'name' => 'Silo Karno Doga',
            ),

            array (
                'code' => '91.02.59',
                'name' => 'Piramid',
            ),

            array (
                'code' => '91.02.60',
                'name' => 'Muliama',
            ),

            array (
                'code' => '91.02.61',
                'name' => 'Bugi',
            ),

            array (
                'code' => '91.02.62',
                'name' => 'Bpiri',
            ),

            array (
                'code' => '91.02.63',
                'name' => 'Welesi',
            ),

            array (
                'code' => '91.02.64',
                'name' => 'Asotipo',
            ),

            array (
                'code' => '91.02.65',
                'name' => 'Maima',
            ),

            array (
                'code' => '91.02.66',
                'name' => 'Popugoba',
            ),

            array (
                'code' => '91.02.67',
                'name' => 'Wame',
            ),

            array (
                'code' => '91.02.68',
                'name' => 'Wesaput',
            ),

            array (
                'code' => '91.03',
                'name' => 'KAB. JAYAPURA',
            ),

            array (
                'code' => '91.03.01',
                'name' => 'Sentani',
            ),

            array (
                'code' => '91.03.02',
                'name' => 'Sentani Timur',
            ),

            array (
                'code' => '91.03.03',
                'name' => 'Depapre',
            ),

            array (
                'code' => '91.03.04',
                'name' => 'Sentani Barat',
            ),

            array (
                'code' => '91.03.05',
                'name' => 'Kemtuk',
            ),

            array (
                'code' => '91.03.06',
                'name' => 'Kemtuk Gresi',
            ),

            array (
                'code' => '91.03.07',
                'name' => 'Nimboran',
            ),

            array (
                'code' => '91.03.08',
                'name' => 'Nimbokrang',
            ),

            array (
                'code' => '91.03.09',
                'name' => 'Unurum Guay',
            ),

            array (
                'code' => '91.03.10',
                'name' => 'Demta',
            ),

            array (
                'code' => '91.03.11',
                'name' => 'Kaureh',
            ),

            array (
                'code' => '91.03.12',
                'name' => 'Ebungfa',
            ),

            array (
                'code' => '91.03.13',
                'name' => 'Waibu',
            ),

            array (
                'code' => '91.03.14',
                'name' => 'Nambluong',
            ),

            array (
                'code' => '91.03.15',
                'name' => 'Yapsi',
            ),

            array (
                'code' => '91.03.16',
                'name' => 'Airu',
            ),

            array (
                'code' => '91.03.17',
                'name' => 'Raveni Rara',
            ),

            array (
                'code' => '91.03.18',
                'name' => 'Gresi Selatan',
            ),

            array (
                'code' => '91.03.19',
                'name' => 'Yokari',
            ),

            array (
                'code' => '91.04',
                'name' => 'KAB. NABIRE',
            ),

            array (
                'code' => '91.04.01',
                'name' => 'Nabire',
            ),

            array (
                'code' => '91.04.02',
                'name' => 'Napan',
            ),

            array (
                'code' => '91.04.03',
                'name' => 'Yaur',
            ),

            array (
                'code' => '91.04.06',
                'name' => 'Uwapa',
            ),

            array (
                'code' => '91.04.07',
                'name' => 'Wanggar',
            ),

            array (
                'code' => '91.04.10',
                'name' => 'Siriwo',
            ),

            array (
                'code' => '91.04.11',
                'name' => 'Makimi',
            ),

            array (
                'code' => '91.04.12',
                'name' => 'Teluk Umar',
            ),

            array (
                'code' => '91.04.16',
                'name' => 'Teluk Kimi',
            ),

            array (
                'code' => '91.04.17',
                'name' => 'Yaro',
            ),

            array (
                'code' => '91.04.21',
                'name' => 'Wapoga',
            ),

            array (
                'code' => '91.04.22',
                'name' => 'Nabire Barat',
            ),

            array (
                'code' => '91.04.23',
                'name' => 'Moora',
            ),

            array (
                'code' => '91.04.24',
                'name' => 'Dipa',
            ),

            array (
                'code' => '91.04.25',
                'name' => 'Menou',
            ),

            array (
                'code' => '91.05',
                'name' => 'KAB. KEPULAUAN YAPEN',
            ),

            array (
                'code' => '91.05.01',
                'name' => 'Yapen Selatan',
            ),

            array (
                'code' => '91.05.02',
                'name' => 'Yapen Barat',
            ),

            array (
                'code' => '91.05.03',
                'name' => 'Yapen Timur',
            ),

            array (
                'code' => '91.05.04',
                'name' => 'Angkaisera',
            ),

            array (
                'code' => '91.05.05',
                'name' => 'Poom',
            ),

            array (
                'code' => '91.05.06',
                'name' => 'Kosiwo',
            ),

            array (
                'code' => '91.05.07',
                'name' => 'Yapen Utara',
            ),

            array (
                'code' => '91.05.08',
                'name' => 'Raimbawi',
            ),

            array (
                'code' => '91.05.09',
                'name' => 'Teluk Ampimoi',
            ),

            array (
                'code' => '91.05.10',
                'name' => 'Kepulauan Ambai',
            ),

            array (
                'code' => '91.05.11',
                'name' => 'Wonawa',
            ),

            array (
                'code' => '91.05.12',
                'name' => 'Windesi',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '91.05.13',
                'name' => 'Pulau Kurudu',
            ),

            array (
                'code' => '91.05.14',
                'name' => 'Pulau Yerui',
            ),

            array (
                'code' => '91.06',
                'name' => 'KAB. BIAK NUMFOR',
            ),

            array (
                'code' => '91.06.01',
                'name' => 'Biak Kota',
            ),

            array (
                'code' => '91.06.02',
                'name' => 'Biak Utara',
            ),

            array (
                'code' => '91.06.03',
                'name' => 'Biak Timur',
            ),

            array (
                'code' => '91.06.04',
                'name' => 'Numfor Barat',
            ),

            array (
                'code' => '91.06.05',
                'name' => 'Numfor Timur',
            ),

            array (
                'code' => '91.06.08',
                'name' => 'Biak Barat',
            ),

            array (
                'code' => '91.06.09',
                'name' => 'Warsa',
            ),

            array (
                'code' => '91.06.10',
                'name' => 'Padaido',
            ),

            array (
                'code' => '91.06.11',
                'name' => 'Yendidori',
            ),

            array (
                'code' => '91.06.12',
                'name' => 'Samofa',
            ),

            array (
                'code' => '91.06.13',
                'name' => 'Yawosi',
            ),

            array (
                'code' => '91.06.14',
                'name' => 'Andey',
            ),

            array (
                'code' => '91.06.15',
                'name' => 'Swandiwe',
            ),

            array (
                'code' => '91.06.16',
                'name' => 'Bruyadori',
            ),

            array (
                'code' => '91.06.17',
                'name' => 'Orkeri',
            ),

            array (
                'code' => '91.06.18',
                'name' => 'Poiru',
            ),

            array (
                'code' => '91.06.19',
                'name' => 'Aimando Padaido',
            ),

            array (
                'code' => '91.06.20',
                'name' => 'Oridek',
            ),

            array (
                'code' => '91.06.21',
                'name' => 'Bondifuar',
            ),

            array (
                'code' => '91.07',
                'name' => 'KAB. PUNCAK JAYA',
            ),

            array (
                'code' => '91.07.01',
                'name' => 'Mulia',
            ),

            array (
                'code' => '91.07.03',
                'name' => 'Ilu',
            ),

            array (
                'code' => '91.07.06',
                'name' => 'Fawi',
            ),

            array (
                'code' => '91.07.07',
                'name' => 'Mewoluk',
            ),

            array (
                'code' => '91.07.08',
                'name' => 'Yamo',
            ),

            array (
                'code' => '91.07.10',
                'name' => 'Nume',
            ),

            array (
                'code' => '91.07.11',
                'name' => 'Torere',
            ),

            array (
                'code' => '91.07.12',
                'name' => 'Tingginambut',
            ),

            array (
                'code' => '91.07.17',
                'name' => 'Pagaleme',
            ),

            array (
                'code' => '91.07.18',
                'name' => 'Gurage',
            ),

            array (
                'code' => '91.07.19',
                'name' => 'Irimuli',
            ),

            array (
                'code' => '91.07.20',
                'name' => 'Muara',
            ),

            array (
                'code' => '91.07.21',
                'name' => 'Ilamburawi',
            ),

            array (
                'code' => '91.07.22',
                'name' => 'Yambi',
            ),

            array (
                'code' => '91.07.23',
                'name' => 'Lumo',
            ),

            array (
                'code' => '91.07.24',
                'name' => 'Molanikime',
            ),

            array (
                'code' => '91.07.25',
                'name' => 'Dokome',
            ),

            array (
                'code' => '91.07.26',
                'name' => 'Kalome',
            ),

            array (
                'code' => '91.07.27',
                'name' => 'Wanwi',
            ),

            array (
                'code' => '91.07.28',
                'name' => 'Yamoneri',
            ),

            array (
                'code' => '91.07.29',
                'name' => 'Waegi',
            ),

            array (
                'code' => '91.07.30',
                'name' => 'Nioga',
            ),

            array (
                'code' => '91.07.31',
                'name' => 'Gubume',
            ),

            array (
                'code' => '91.07.32',
                'name' => 'Taganombak',
            ),

            array (
                'code' => '91.07.33',
                'name' => 'Dagai',
            ),

            array (
                'code' => '91.07.34',
                'name' => 'Kiyage',
            ),

            array (
                'code' => '91.08',
                'name' => 'KAB. PANIAI',
            ),

            array (
                'code' => '91.08.01',
                'name' => 'Paniai Timur',
            ),

            array (
                'code' => '91.08.02',
                'name' => 'Paniai Barat',
            ),

            array (
                'code' => '91.08.04',
                'name' => 'Aradide',
            ),

            array (
                'code' => '91.08.07',
                'name' => 'Bogabaida',
            ),

            array (
                'code' => '91.08.09',
                'name' => 'Bibida',
            ),

            array (
                'code' => '91.08.12',
                'name' => 'Dumadama',
            ),

            array (
                'code' => '91.08.13',
                'name' => 'Siriwo',
            ),

            array (
                'code' => '91.08.19',
                'name' => 'Kebo',
            ),

            array (
                'code' => '91.08.20',
                'name' => 'Yatamo',
            ),

            array (
                'code' => '91.08.21',
                'name' => 'Ekadide',
            ),

            array (
                'code' => '91.08.22',
                'name' => 'Wegee Muka',
            ),

            array (
                'code' => '91.08.23',
                'name' => 'Wegee Bino',
            ),

            array (
                'code' => '91.08.24',
                'name' => 'Pugo Dagi',
            ),

            array (
                'code' => '91.08.25',
                'name' => 'Muye',
            ),

            array (
                'code' => '91.08.26',
                'name' => 'Nakama',
            ),

            array (
                'code' => '91.08.27',
                'name' => 'Teluk Deya',
            ),

            array (
                'code' => '91.08.28',
                'name' => 'Yagai',
            ),

            array (
                'code' => '91.08.29',
                'name' => 'Youtadi',
            ),

            array (
                'code' => '91.08.30',
                'name' => 'Baya Biru',
            ),

            array (
                'code' => '91.08.31',
                'name' => 'Deiyai  Miyo',
            ),

            array (
                'code' => '91.08.32',
                'name' => 'Dogomo',
            ),

            array (
                'code' => '91.08.33',
                'name' => 'Aweida',
            ),

            array (
                'code' => '91.08.34',
                'name' => 'Topiyai',
            ),

            array (
                'code' => '91.09',
                'name' => 'KAB. MIMIKA',
            ),

            array (
                'code' => '91.09.01',
                'name' => 'Mimika Baru',
            ),

            array (
                'code' => '91.09.02',
                'name' => 'Agimuga',
            ),

            array (
                'code' => '91.09.03',
                'name' => 'Mimika Timur',
            ),

            array (
                'code' => '91.09.04',
                'name' => 'Mimika Barat',
            ),

            array (
                'code' => '91.09.05',
                'name' => 'Jita',
            ),

            array (
                'code' => '91.09.06',
                'name' => 'Jila',
            ),

            array (
                'code' => '91.09.07',
                'name' => 'Mimika Timur Jauh',
            ),

            array (
                'code' => '91.09.08',
                'name' => 'Mimika Tengah',
            ),

            array (
                'code' => '91.09.09',
                'name' => 'Kuala Kencana',
            ),

            array (
                'code' => '91.09.10',
                'name' => 'Tembagapura',
            ),

            array (
                'code' => '91.09.11',
                'name' => 'Mimika Barat Jauh',
            ),

            array (
                'code' => '91.09.12',
                'name' => 'Mimika Barat Tengah',
            ),

            array (
                'code' => '91.09.13',
                'name' => 'Kwamki Narama',
            ),

            array (
                'code' => '91.09.14',
                'name' => 'Hoya',
            ),

            array (
                'code' => '91.09.15',
                'name' => 'Iwaka',
            ),

            array (
                'code' => '91.09.16',
                'name' => 'Wania',
            ),

            array (
                'code' => '91.09.17',
                'name' => 'Amar',
            ),

            array (
                'code' => '91.09.18',
                'name' => 'Alama',
            ),

            array (
                'code' => '91.10',
                'name' => 'KAB. SARMI',
            ),

            array (
                'code' => '91.10.01',
                'name' => 'Sarmi',
            ),

            array (
                'code' => '91.10.02',
                'name' => 'Tor Atas',
            ),

            array (
                'code' => '91.10.03',
                'name' => 'Pantai Barat',
            ),

            array (
                'code' => '91.10.04',
                'name' => 'Pantai Timur',
            ),

            array (
                'code' => '91.10.05',
                'name' => 'Bonggo',
            ),

            array (
                'code' => '91.10.09',
                'name' => 'Apawer Hulu',
            ),

            array (
                'code' => '91.10.12',
                'name' => 'Sarmi Selatan',
            ),

            array (
                'code' => '91.10.13',
                'name' => 'Sarmi Timur',
            ),

            array (
                'code' => '91.10.14',
                'name' => 'Pantai Timur Bagian Barat',
            ),

            array (
                'code' => '91.10.15',
                'name' => 'Bonggo Timur',
            ),

            array (
                'code' => '91.11',
                'name' => 'KAB. KEEROM',
            ),

            array (
                'code' => '91.11.01',
                'name' => 'Waris',
            ),

            array (
                'code' => '91.11.02',
                'name' => 'Arso',
            ),

            array (
                'code' => '91.11.03',
                'name' => 'Senggi',
            ),

            array (
                'code' => '91.11.04',
                'name' => 'Web',
            ),

            array (
                'code' => '91.11.05',
                'name' => 'Skanto',
            ),

            array (
                'code' => '91.11.06',
                'name' => 'Arso Timur',
            ),

            array (
                'code' => '91.11.07',
                'name' => 'Towe',
            ),

            array (
                'code' => '91.11.08',
                'name' => 'Arso Barat',
            ),

            array (
                'code' => '91.11.09',
                'name' => 'Mannem',
            ),

            array (
                'code' => '91.11.10',
                'name' => 'Yaffi',
            ),

            array (
                'code' => '91.11.11',
                'name' => 'Kaisenar',
            ),

            array (
                'code' => '91.12',
                'name' => 'KAB PEGUNUNGAN BINTANG',
            ),

            array (
                'code' => '91.12.01',
                'name' => 'Oksibil',
            ),

            array (
                'code' => '91.12.02',
                'name' => 'Kiwirok',
            ),

            array (
                'code' => '91.12.03',
                'name' => 'Okbibab',
            ),

            array (
                'code' => '91.12.04',
                'name' => 'Iwur',
            ),

            array (
                'code' => '91.12.05',
                'name' => 'Batom',
            ),

            array (
                'code' => '91.12.06',
                'name' => 'Borme',
            ),

            array (
                'code' => '91.12.07',
                'name' => 'Kiwirok Timur',
            ),

            array (
                'code' => '91.12.08',
                'name' => 'Aboy',
            ),

            array (
                'code' => '91.12.09',
                'name' => 'Pepera',
            ),

            array (
                'code' => '91.12.10',
                'name' => 'Bime',
            ),

            array (
                'code' => '91.12.11',
                'name' => 'Alemsom',
            ),

            array (
                'code' => '91.12.12',
                'name' => 'Okbape',
            ),

            array (
                'code' => '91.12.13',
                'name' => 'Kalomdol',
            ),

            array (
                'code' => '91.12.14',
                'name' => 'Oksop',
            ),

            array (
                'code' => '91.12.15',
                'name' => 'Serambakon',
            ),

            array (
                'code' => '91.12.16',
                'name' => 'Ok Aom',
            ),

            array (
                'code' => '91.12.17',
                'name' => 'Kawor',
            ),

            array (
                'code' => '91.12.18',
                'name' => 'Awinbon',
            ),

            array (
                'code' => '91.12.19',
                'name' => 'Tarup',
            ),

            array (
                'code' => '91.12.20',
                'name' => 'Okhika',
            ),

            array (
                'code' => '91.12.21',
                'name' => 'Oksamol',
            ),

            array (
                'code' => '91.12.22',
                'name' => 'Oklip',
            ),

            array (
                'code' => '91.12.23',
                'name' => 'Okbemtau',
            ),

            array (
                'code' => '91.12.24',
                'name' => 'Oksebang',
            ),

            array (
                'code' => '91.12.25',
                'name' => 'Okbab',
            ),

            array (
                'code' => '91.12.26',
                'name' => 'Batani',
            ),

            array (
                'code' => '91.12.27',
                'name' => 'Weime',
            ),

            array (
                'code' => '91.12.28',
                'name' => 'Murkim',
            ),

            array (
                'code' => '91.12.29',
                'name' => 'Mofinop',
            ),

            array (
                'code' => '91.12.30',
                'name' => 'Jetfa',
            ),

            array (
                'code' => '91.12.31',
                'name' => 'Teiraplu',
            ),

            array (
                'code' => '91.12.32',
                'name' => 'Eipumek',
            ),

            array (
                'code' => '91.12.33',
                'name' => 'Pamek',
            ),

            array (
                'code' => '91.12.34',
                'name' => 'Nongme',
            ),

            array (
                'code' => '91.13',
                'name' => 'KAB. YAHUKIMO',
            ),

            array (
                'code' => '91.13.01',
                'name' => 'Kurima',
            ),

            array (
                'code' => '91.13.02',
                'name' => 'Anggruk',
            ),

            array (
                'code' => '91.13.03',
                'name' => 'Ninia',
            ),

            array (
                'code' => '91.13.06',
                'name' => 'Silimo',
            ),

            array (
                'code' => '91.13.07',
                'name' => 'Samenage',
            ),

            array (
                'code' => '91.13.08',
                'name' => 'Nalca',
            ),

            array (
                'code' => '91.13.09',
                'name' => 'Dekai',
            ),

            array (
                'code' => '91.13.10',
                'name' => 'Obio',
            ),

            array (
                'code' => '91.13.11',
                'name' => 'Suru Suru',
            ),

            array (
                'code' => '91.13.12',
                'name' => 'Wusama',
            ),

            array (
                'code' => '91.13.13',
                'name' => 'Amuma',
            ),

            array (
                'code' => '91.13.14',
                'name' => 'Musaik',
            ),

            array (
                'code' => '91.13.15',
                'name' => 'Pasema',
            ),

            array (
                'code' => '91.13.16',
                'name' => 'Hogio',
            ),

            array (
                'code' => '91.13.17',
                'name' => 'Mugi',
            ),

            array (
                'code' => '91.13.18',
                'name' => 'Soba',
            ),

            array (
                'code' => '91.13.19',
                'name' => 'Werima',
            ),

            array (
                'code' => '91.13.20',
                'name' => 'Tangma',
            ),

            array (
                'code' => '91.13.21',
                'name' => 'Ukha',
            ),

            array (
                'code' => '91.13.22',
                'name' => 'Panggema',
            ),

            array (
                'code' => '91.13.23',
                'name' => 'Kosarek',
            ),

            array (
                'code' => '91.13.24',
                'name' => 'Nipsan',
            ),

            array (
                'code' => '91.13.25',
                'name' => 'Ubahak',
            ),

            array (
                'code' => '91.13.26',
                'name' => 'Pronggoli',
            ),

            array (
                'code' => '91.13.27',
                'name' => 'Walma',
            ),

            array (
                'code' => '91.13.28',
                'name' => 'Yahuliambut',
            ),

            array (
                'code' => '91.13.29',
                'name' => 'Hereapini',
            ),

            array (
                'code' => '91.13.30',
                'name' => 'Ubalihi',
            ),

            array (
                'code' => '91.13.31',
                'name' => 'Talambo',
            ),

            array (
                'code' => '91.13.32',
                'name' => 'Puldama',
            ),

            array (
                'code' => '91.13.33',
                'name' => 'Endomen',
            ),

            array (
                'code' => '91.13.34',
                'name' => 'Kona',
            ),

            array (
                'code' => '91.13.35',
                'name' => 'Dirwemna',
            ),

            array (
                'code' => '91.13.36',
                'name' => 'Holuon',
            ),

            array (
                'code' => '91.13.37',
                'name' => 'Lolat',
            ),

            array (
                'code' => '91.13.38',
                'name' => 'Soloikma',
            ),

            array (
                'code' => '91.13.39',
                'name' => 'Sela',
            ),

            array (
                'code' => '91.13.40',
                'name' => 'Korupun',
            ),

            array (
                'code' => '91.13.41',
                'name' => 'Langda',
            ),

            array (
                'code' => '91.13.42',
                'name' => 'Bomela',
            ),

            array (
                'code' => '91.13.43',
                'name' => 'Suntamon',
            ),

            array (
                'code' => '91.13.44',
                'name' => 'Seredela',
            ),

            array (
                'code' => '91.13.45',
                'name' => 'Sobaham',
            ),

            array (
                'code' => '91.13.46',
                'name' => 'Kabianggama',
            ),

            array (
                'code' => '91.13.47',
                'name' => 'Kwelemdua',
            ),

            array (
                'code' => '91.13.48',
                'name' => 'Kwikma',
            ),

            array (
                'code' => '91.13.49',
                'name' => 'Hilipuk',
            ),

            array (
                'code' => '91.13.50',
                'name' => 'Duram',
            ),

            array (
                'code' => '91.13.51',
                'name' => 'Yogosem',
            ),

            array (
                'code' => '91.13.52',
                'name' => 'Kayo',
            ),

            array (
                'code' => '91.13.53',
                'name' => 'Sumo',
            ),

            array (
                'code' => '91.14',
                'name' => 'KAB. TOLIKARA',
            ),

            array (
                'code' => '91.14.01',
                'name' => 'Karubaga',
            ),

            array (
                'code' => '91.14.02',
                'name' => 'Bokondini',
            ),

            array (
                'code' => '91.14.03',
                'name' => 'Kanggime',
            ),

            array (
                'code' => '91.14.04',
                'name' => 'Kembu',
            ),

            array (
                'code' => '91.14.05',
                'name' => 'Goyage',
            ),

            array (
                'code' => '91.14.06',
                'name' => 'Wunim',
            ),

            array (
                'code' => '91.14.07',
                'name' => 'Wina',
            ),

            array (
                'code' => '91.14.08',
                'name' => 'Umagi',
            ),

            array (
                'code' => '91.14.09',
                'name' => 'Panaga',
            ),

            array (
                'code' => '91.14.10',
                'name' => 'Woniki',
            ),

            array (
                'code' => '91.14.11',
                'name' => 'Kubu',
            ),

            array (
                'code' => '91.14.12',
                'name' => 'Konda/ Kondaga',
            ),

            array (
                'code' => '91.14.13',
                'name' => 'Nelawi',
            ),

            array (
                'code' => '91.14.14',
                'name' => 'Kuari',
            ),

            array (
                'code' => '91.14.15',
                'name' => 'Bokoneri',
            ),

            array (
                'code' => '91.14.16',
                'name' => 'Bewani',
            ),

            array (
                'code' => '91.14.18',
                'name' => 'Nabunage',
            ),

            array (
                'code' => '91.14.19',
                'name' => 'Gilubandu',
            ),

            array (
                'code' => '91.14.20',
                'name' => 'Nunggawi',
            ),

            array (
                'code' => '91.14.21',
                'name' => 'Gundagi',
            ),

            array (
                'code' => '91.14.22',
                'name' => 'Numba',
            ),

            array (
                'code' => '91.14.23',
                'name' => 'Timori',
            ),

            array (
                'code' => '91.14.24',
                'name' => 'Dundu',
            ),

            array (
                'code' => '91.14.25',
                'name' => 'Geya',
            ),

            array (
                'code' => '91.14.26',
                'name' => 'Egiam',
            ),

            array (
                'code' => '91.14.27',
                'name' => 'Poganeri',
            ),

            array (
                'code' => '91.14.28',
                'name' => 'Kamboneri',
            ),

            array (
                'code' => '91.14.29',
                'name' => 'Airgaram',
            ),

            array (
                'code' => '91.14.30',
                'name' => 'Wari/Taiyeve II',
            ),

            array (
                'code' => '91.14.31',
                'name' => 'Dow',
            ),

            array (
                'code' => '91.14.32',
                'name' => 'Tagineri',
            ),

            array (
                'code' => '91.14.33',
                'name' => 'Yuneri',
            ),

            array (
                'code' => '91.14.34',
                'name' => 'Wakuwo',
            ),

            array (
                'code' => '91.14.35',
                'name' => 'Gika',
            ),

            array (
                'code' => '91.14.36',
                'name' => 'Telenggeme',
            ),

            array (
                'code' => '91.14.37',
                'name' => 'Anawi',
            ),

            array (
                'code' => '91.14.38',
                'name' => 'Wenam',
            ),

            array (
                'code' => '91.14.39',
                'name' => 'Wugi',
            ),

            array (
                'code' => '91.14.40',
                'name' => 'Danime',
            ),

            array (
                'code' => '91.14.41',
                'name' => 'Tagime',
            ),

            array (
                'code' => '91.14.42',
                'name' => 'Kai',
            ),

            array (
                'code' => '91.14.43',
                'name' => 'Aweku',
            ),

            array (
                'code' => '91.14.44',
                'name' => 'Bogonuk',
            ),

            array (
                'code' => '91.14.45',
                'name' => 'Li Anogomma',
            ),

            array (
                'code' => '91.14.46',
                'name' => 'Biuk',
            ),

            array (
                'code' => '91.14.47',
                'name' => 'Yuko',
            ),

            array (
                'code' => '91.15',
                'name' => 'KAB. WAROPEN',
            ),

            array (
                'code' => '91.15.01',
                'name' => 'Waropen Bawah',
            ),

            array (
                'code' => '91.15.03',
                'name' => 'Masirei',
            ),

            array (
                'code' => '91.15.07',
                'name' => 'Risei Sayati',
            ),

            array (
                'code' => '91.15.08',
                'name' => 'Urei Faisei',
            ),

            array (
                'code' => '91.15.09',
                'name' => 'Inggerus',
            ),

            array (
                'code' => '91.15.10',
                'name' => 'Kirihi',
            ),

            array (
                'code' => '91.15.11',
                'name' => 'Oudate',
            ),

            array (
                'code' => '91.15.12',
                'name' => 'Wapoga',
            ),

            array (
                'code' => '91.15.13',
                'name' => 'Demba',
            ),

            array (
                'code' => '91.15.14',
                'name' => 'Wonti',
            ),

            array (
                'code' => '91.15.15',
                'name' => 'Soyoi Mambai',
            ),

            array (
                'code' => '91.16',
                'name' => 'KAB. BOVEN DIGOEL',
            ),

            array (
                'code' => '91.16.01',
                'name' => 'Mandobo',
            ),

            array (
                'code' => '91.16.02',
                'name' => 'Mindiptana',
            ),

            array (
                'code' => '91.16.03',
                'name' => 'Waropko',
            ),

            array (
                'code' => '91.16.04',
                'name' => 'Kouh',
            ),

            array (
                'code' => '91.16.05',
                'name' => 'Jair',
            ),

            array (
                'code' => '91.16.06',
                'name' => 'Bomakia',
            ),

            array (
                'code' => '91.16.07',
                'name' => 'Kombut',
            ),

            array (
                'code' => '91.16.08',
                'name' => 'Iniyandit',
            ),

            array (
                'code' => '91.16.09',
                'name' => 'Arimop',
            ),

            array (
                'code' => '91.16.10',
                'name' => 'Fofi',
            ),

            array (
                'code' => '91.16.11',
                'name' => 'Ambatkwi',
            ),

            array (
                'code' => '91.16.12',
                'name' => 'Manggelum',
            ),

            array (
                'code' => '91.16.13',
                'name' => 'Firiwage',
            ),

            array (
                'code' => '91.16.14',
                'name' => 'Yaniruma',
            ),

            array (
                'code' => '91.16.15',
                'name' => 'Subur',
            ),

            array (
                'code' => '91.16.16',
                'name' => 'Kombay',
            ),

            array (
                'code' => '91.16.17',
                'name' => 'Ninati',
            ),

            array (
                'code' => '91.16.18',
                'name' => 'Sesnuk',
            ),

            array (
                'code' => '91.16.19',
                'name' => 'Ki',
            ),

            array (
                'code' => '91.16.20',
                'name' => 'Kawagit',
            ),

            array (
                'code' => '91.17',
                'name' => 'KAB. MAPPI',
            ),

            array (
                'code' => '91.17.01',
                'name' => 'Obaa',
            ),

            array (
                'code' => '91.17.02',
                'name' => 'Mambioman Bapai',
            ),

            array (
                'code' => '91.17.03',
                'name' => 'Citak-Mitak',
            ),

            array (
                'code' => '91.17.04',
                'name' => 'Edera',
            ),

            array (
                'code' => '91.17.05',
                'name' => 'Haju',
            ),

            array (
                'code' => '91.17.06',
                'name' => 'Assue',
            ),

            array (
                'code' => '91.17.07',
                'name' => 'Kaibar',
            ),

            array (
                'code' => '91.17.08',
                'name' => 'Passue',
            ),

            array (
                'code' => '91.17.09',
                'name' => 'Minyamur',
            ),

            array (
                'code' => '91.17.10',
                'name' => 'Venaha',
            ),

            array (
                'code' => '91.17.11',
                'name' => 'Syahcame',
            ),

            array (
                'code' => '91.17.12',
                'name' => 'Yakomi',
            ),

            array (
                'code' => '91.17.13',
                'name' => 'Bamgi',
            ),

            array (
                'code' => '91.17.14',
                'name' => 'Passue Bawah',
            ),

            array (
                'code' => '91.17.15',
                'name' => 'Ti  Zain',
            ),

            array (
                'code' => '91.18',
                'name' => 'KAB. ASMAT',
            ),

            array (
                'code' => '91.18.01',
                'name' => 'Agats',
            ),

            array (
                'code' => '91.18.02',
                'name' => 'Atsj',
            ),

            array (
                'code' => '91.18.03',
                'name' => 'Sawa Erma',
            ),

            array (
                'code' => '91.18.04',
                'name' => 'Akat',
            ),

            array (
                'code' => '91.18.05',
                'name' => 'Fayit',
            ),

            array (
                'code' => '91.18.06',
                'name' => 'Pantai Kasuari',
            ),

            array (
                'code' => '91.18.07',
                'name' => 'Suator',
            ),

            array (
                'code' => '91.18.08',
                'name' => 'Suru-suru',
            ),

            array (
                'code' => '91.18.09',
                'name' => 'Kolf Braza',
            ),

            array (
                'code' => '91.18.10',
                'name' => 'Unir Sirau',
            ),

            array (
                'code' => '91.18.11',
                'name' => 'Joerat',
            ),

            array (
                'code' => '91.18.12',
                'name' => 'Pulau Tiga',
            ),

            array (
                'code' => '91.18.13',
                'name' => 'Jetsy',
            ),

            array (
                'code' => '91.18.14',
                'name' => 'Der Koumur',
            ),

            array (
                'code' => '91.18.15',
                'name' => 'Kopay',
            ),

            array (
                'code' => '91.18.16',
                'name' => 'Safan',
            ),

            array (
                'code' => '91.18.17',
                'name' => 'Sirets',
            ),

            array (
                'code' => '91.18.18',
                'name' => 'Ayip',
            ),

            array (
                'code' => '91.18.19',
                'name' => 'Betcbamu',
            ),

            array (
                'code' => '91.19',
                'name' => 'KAB. SUPIORI',
            ),

            array (
                'code' => '91.19.01',
                'name' => 'Supiori Selatan',
            ),

            array (
                'code' => '91.19.02',
                'name' => 'Supiori Utara',
            ),

            array (
                'code' => '91.19.03',
                'name' => 'Supiori Timur',
            ),

            array (
                'code' => '91.19.04',
                'name' => 'Kepulauan Aruri',
            ),

            array (
                'code' => '91.19.05',
                'name' => 'Supiori Barat',
            ),

            array (
                'code' => '91.20',
                'name' => 'KAB. MAMBERAMO RAYA',
            ),

            array (
                'code' => '91.20.01',
                'name' => 'Mamberamo Tengah',
            ),

            array (
                'code' => '91.20.02',
                'name' => 'Mamberamo Hulu',
            ),

            array (
                'code' => '91.20.03',
                'name' => 'Rufaer',
            ),

            array (
                'code' => '91.20.04',
                'name' => 'Mamberamo Tengah Timur',
            ),

            array (
                'code' => '91.20.05',
                'name' => 'Mamberamo Hilir',
            ),

            array (
                'code' => '91.20.06',
                'name' => 'Waropen Atas',
            ),

            array (
                'code' => '91.20.07',
                'name' => 'Benuki',
            ),

            array (
                'code' => '91.20.08',
                'name' => 'Sawai',
            ),

            array (
                'code' => '91.21',
                'name' => 'KAB. MAMBERAMO TENGAH',
            ),

            array (
                'code' => '91.21.01',
                'name' => 'Kobagma',
            ),

            array (
                'code' => '91.21.02',
                'name' => 'Kelila',
            ),

            array (
                'code' => '91.21.03',
                'name' => 'Eragayam',
            ),

            array (
                'code' => '91.21.04',
                'name' => 'Megambilis',
            ),

            array (
                'code' => '91.21.05',
                'name' => 'Ilugwa',
            ),

            array (
                'code' => '91.22',
                'name' => 'KAB. YALIMO',
            ),

            array (
                'code' => '91.22.01',
                'name' => 'Elelim',
            ),

            array (
                'code' => '91.22.02',
                'name' => 'Apalapsili',
            ),

            array (
                'code' => '91.22.03',
                'name' => 'Abenaho',
            ),

            array (
                'code' => '91.22.04',
                'name' => 'Benawa',
            ),

            array (
                'code' => '91.22.05',
                'name' => 'Welarek',
            ),

            array (
                'code' => '91.23',
                'name' => 'KAB. LANNY JAYA',
            ),

            array (
                'code' => '91.23.01',
                'name' => 'Tiom',
            ),

            array (
                'code' => '91.23.02',
                'name' => 'Pirime',
            ),

            array (
                'code' => '91.23.03',
                'name' => 'Makki',
            ),

            array (
                'code' => '91.23.04',
                'name' => 'Gamelia',
            ),

            array (
                'code' => '91.23.05',
                'name' => 'Dimba',
            ),

            array (
                'code' => '91.23.06',
                'name' => 'Melagineri',
            ),

            array (
                'code' => '91.23.07',
                'name' => 'Balingga',
            ),

            array (
                'code' => '91.23.08',
                'name' => 'Tiomneri',
            ),

            array (
                'code' => '91.23.09',
                'name' => 'Kuyawage',
            ),

            array (
                'code' => '91.23.10',
                'name' => 'Poga',
            ),

            array (
                'code' => '91.23.11',
                'name' => 'Niname',
            ),

            array (
                'code' => '91.23.12',
                'name' => 'Nogi',
            ),

            array (
                'code' => '91.23.13',
                'name' => 'Yiginua',
            ),

            array (
                'code' => '91.23.14',
                'name' => 'Tiom Ollo',
            ),

            array (
                'code' => '91.23.15',
                'name' => 'Yugungwi',
            ),

            array (
                'code' => '91.23.16',
                'name' => 'Mokoni',
            ),

            array (
                'code' => '91.23.17',
                'name' => 'Wereka',
            ),

            array (
                'code' => '91.23.18',
                'name' => 'Milimbo',
            ),

            array (
                'code' => '91.23.19',
                'name' => 'Wiringgambut',
            ),

            array (
                'code' => '91.23.20',
                'name' => 'Gollo',
            ),

            array (
                'code' => '91.23.21',
                'name' => 'Awina',
            ),

            array (
                'code' => '91.23.22',
                'name' => 'Ayumnati',
            ),

            array (
                'code' => '91.23.23',
                'name' => 'Wano Barat',
            ),

            array (
                'code' => '91.23.24',
                'name' => 'Goa Balim',
            ),

            array (
                'code' => '91.23.25',
                'name' => 'Bruwa',
            ),

            array (
                'code' => '91.23.26',
                'name' => 'Balingga Barat',
            ),

            array (
                'code' => '91.23.27',
                'name' => 'Gupura',
            ),

            array (
                'code' => '91.23.28',
                'name' => 'Kolawa',
            ),

            array (
                'code' => '91.23.29',
                'name' => 'Gelok Beam',
            ),

            array (
                'code' => '91.23.30',
                'name' => 'Kuly Lanny',
            ),

            array (
                'code' => '91.23.31',
                'name' => 'Lannyna',
            ),

            array (
                'code' => '91.23.32',
                'name' => 'Karu',
            ),

            array (
                'code' => '91.23.33',
                'name' => 'Yiluk',
            ),

            array (
                'code' => '91.23.34',
                'name' => 'Guna',
            ),

            array (
                'code' => '91.23.35',
                'name' => 'Kelulome',
            ),

            array (
                'code' => '91.23.36',
                'name' => 'Nikogwe',
            ),

            array (
                'code' => '91.23.37',
                'name' => 'Muara',
            ),

            array (
                'code' => '91.23.38',
                'name' => 'Buguk Gona',
            ),

            array (
                'code' => '91.23.39',
                'name' => 'Melagi',
            ),

            array (
                'code' => '91.24',
                'name' => 'KAB. NDUGA',
            ),

            array (
                'code' => '91.24.01',
                'name' => 'Kenyam',
            ),

            array (
                'code' => '91.24.02',
                'name' => 'Mapenduma',
            ),

            array (
                'code' => '91.24.03',
                'name' => 'Yigi',
            ),

            array (
                'code' => '91.24.04',
                'name' => 'Wosak',
            ),

            array (
                'code' => '91.24.05',
                'name' => 'Geselma',
            ),

            array (
                'code' => '91.24.06',
                'name' => 'Mugi',
            ),

            array (
                'code' => '91.24.07',
                'name' => 'Mbuwa',
            ),

            array (
                'code' => '91.24.08',
                'name' => 'Gearek',
            ),

            array (
                'code' => '91.24.09',
                'name' => 'Koroptak',
            ),

            array (
                'code' => '91.24.10',
                'name' => 'Kegayem',
            ),

            array (
                'code' => '91.24.11',
                'name' => 'Paro',
            ),

            array (
                'code' => '91.24.12',
                'name' => 'Mebarok',
            ),

            array (
                'code' => '91.24.13',
                'name' => 'Yenggelo',
            ),

            array (
                'code' => '91.24.14',
                'name' => 'Kilmid',
            ),

            array (
                'code' => '91.24.15',
                'name' => 'Alama',
            ),

            array (
                'code' => '91.24.16',
                'name' => 'Yal',
            ),

            array (
                'code' => '91.24.17',
                'name' => 'Mam',
            ),

            array (
                'code' => '91.24.18',
                'name' => 'Dal',
            ),

            array (
                'code' => '91.24.19',
                'name' => 'Nirkuri',
            ),

            array (
                'code' => '91.24.20',
                'name' => 'Inikgal',
            ),

            array (
                'code' => '91.24.21',
                'name' => 'Iniye',
            ),

            array (
                'code' => '91.24.22',
                'name' => 'Mbulmu Yalma',
            ),

            array (
                'code' => '91.24.23',
                'name' => 'Mbua Tengah',
            ),

            array (
                'code' => '91.24.24',
                'name' => 'Embetpen',
            ),

            array (
                'code' => '91.24.25',
                'name' => 'Kora',
            ),

            array (
                'code' => '91.24.26',
                'name' => 'Wusi',
            ),

            array (
                'code' => '91.24.27',
                'name' => 'Pija',
            ),

            array (
                'code' => '91.24.28',
                'name' => 'Moba',
            ),

            array (
                'code' => '91.24.29',
                'name' => 'Wutpaga',
            ),

            array (
                'code' => '91.24.30',
                'name' => 'Nenggeagin',
            ),

            array (
                'code' => '91.24.31',
                'name' => 'Krepkuri',
            ),

            array (
                'code' => '91.24.32',
                'name' => 'Pasir Putih',
            ),

            array (
                'code' => '91.25',
                'name' => 'KAB. PUNCAK',
            ),

            array (
                'code' => '91.25.01',
                'name' => 'Ilaga',
            ),

            array (
                'code' => '91.25.02',
                'name' => 'Wangbe',
            ),

            array (
                'code' => '91.25.03',
                'name' => 'Beoga',
            ),

            array (
                'code' => '91.25.04',
                'name' => 'Doufo',
            ),

            array (
                'code' => '91.25.05',
                'name' => 'Pogoma',
            ),

            array (
                'code' => '91.25.06',
                'name' => 'Sinak',
            ),

            array (
                'code' => '91.25.07',
                'name' => 'Agandugume',
            ),

            array (
                'code' => '91.25.08',
                'name' => 'Gome',
            ),

            array (
                'code' => '91.25.09',
                'name' => 'Dervos',
            ),

            array (
                'code' => '91.25.10',
                'name' => 'Beoga Barat',
            ),

            array (
                'code' => '91.25.11',
                'name' => 'Beoga Timur',
            ),

            array (
                'code' => '91.25.12',
                'name' => 'Ogamanim',
            ),

            array (
                'code' => '91.25.13',
                'name' => 'Kembru',
            ),

            array (
                'code' => '91.25.14',
                'name' => 'Bina',
            ),

            array (
                'code' => '91.25.15',
                'name' => 'Sinak Barat',
            ),

            array (
                'code' => '91.25.16',
                'name' => 'Mage\'abume  11',
            ),

            array (
                'code' => '91.25.17',
                'name' => 'Yugumuak',
            ),

            array (
                'code' => '91.25.18',
                'name' => 'Ilaga Utara',
            ),

            array (
                'code' => '91.25.19',
                'name' => 'Mabugi',
            ),

            array (
                'code' => '91.25.20',
                'name' => 'Omukia',
            ),

            array (
                'code' => '91.25.21',
                'name' => 'Lambewi',
            ),

            array (
                'code' => '91.25.22',
                'name' => 'Oneri',
            ),

            array (
                'code' => '91.25.23',
                'name' => 'Amungkalpia',
            ),

            array (
                'code' => '91.25.24',
                'name' => 'Gome Utara',
            ),

            array (
                'code' => '91.25.25',
                'name' => 'Erelmakawia',
            ),

            array (
                'code' => '91.26',
                'name' => 'KAB. DOGIYAI',
            ),

            array (
                'code' => '91.26.01',
                'name' => 'Kamu',
            ),

            array (
                'code' => '91.26.02',
                'name' => 'Mapia',
            ),

            array (
                'code' => '91.26.03',
                'name' => 'Piyaiye',
            ),

            array (
                'code' => '91.26.04',
                'name' => 'Kamu Utara',
            ),

            array (
                'code' => '91.26.05',
                'name' => 'Sukikai Selatan',
            ),

            array (
                'code' => '91.26.06',
                'name' => 'Mapia Barat',
            ),

            array (
                'code' => '91.26.07',
                'name' => 'Kamu Selatan',
            ),

            array (
                'code' => '91.26.08',
                'name' => 'Kamu Timur',
            ),

            array (
                'code' => '91.26.09',
                'name' => 'Mapia Tengah',
            ),

            array (
                'code' => '91.26.10',
                'name' => 'Dogiyai',
            ),

            array (
                'code' => '91.27',
                'name' => 'KAB. INTAN JAYA',
            ),

            array (
                'code' => '91.27.01',
                'name' => 'Sugapa',
            ),

            array (
                'code' => '91.27.02',
                'name' => 'Homeyo',
            ),

            array (
                'code' => '91.27.03',
                'name' => 'Wandai',
            ),

            array (
                'code' => '91.27.04',
                'name' => 'Biandoga',
            ),

            array (
                'code' => '91.27.05',
                'name' => 'Agisiga',
            ),

            array (
                'code' => '91.27.06',
                'name' => 'Hitadipa',
            ),

            array (
                'code' => '91.27.07',
                'name' => 'Ugimba',
            ),

            array (
                'code' => '91.27.08',
                'name' => 'Tomosiga',
            ),

            array (
                'code' => '91.28',
                'name' => 'KAB. DEIYAI',
            ),

            array (
                'code' => '91.28.01',
                'name' => 'Tigi',
            ),

            array (
                'code' => '91.28.02',
                'name' => 'Tigi Timur',
            ),

            array (
                'code' => '91.28.03',
                'name' => 'Bowobado',
            ),

            array (
                'code' => '91.28.04',
                'name' => 'Tigi Barat',
            ),

            array (
                'code' => '91.28.05',
                'name' => 'Kapiraya',
            ),

            array (
                'code' => '91.71',
                'name' => 'KOTA JAYAPURA',
            ),

            array (
                'code' => '91.71.01',
                'name' => 'Jayapura Utara',
            ),

            array (
                'code' => '91.71.02',
                'name' => 'Jayapura Selatan',
            ),

            array (
                'code' => '91.71.03',
                'name' => 'Abepura',
            ),

            array (
                'code' => '91.71.04',
                'name' => 'Muara Tami',
            ),

            array (
                'code' => '91.71.05',
                'name' => 'Heram',
            ),

            array (
                'code' => '92',
                'name' => 'PAPUA BARAT',
            ),

            array (
                'code' => '92.01',
                'name' => 'KAB. SORONG',
            ),

            array (
                'code' => '92.01.01',
                'name' => 'Makbon',
            ),

            array (
                'code' => '92.01.04',
                'name' => 'Beraur',
            ),

            array (
                'code' => '92.01.05',
                'name' => 'Salawati',
            ),

            array (
                'code' => '92.01.06',
                'name' => 'Seget',
            ),

            array (
                'code' => '92.01.07',
                'name' => 'Aimas',
            ),

            array (
                'code' => '92.01.08',
                'name' => 'Klamono',
            ),

            array (
                'code' => '92.01.10',
                'name' => 'Sayosa',
            ),

            array (
                'code' => '92.01.12',
                'name' => 'Segun',
            ),

            array (
                'code' => '92.01.13',
                'name' => 'Mayamuk',
            ),

            array (
                'code' => '92.01.14',
                'name' => 'Salawati Selatan',
            ),

            array (
                'code' => '92.01.17',
                'name' => 'Klabot',
            ),

            array (
                'code' => '92.01.18',
                'name' => 'Klawak',
            ),

            array (
                'code' => '92.01.20',
                'name' => 'Maudus',
            ),

            array (
                'code' => '92.01.39',
                'name' => 'Mariat',
            ),

            array (
                'code' => '92.01.40',
                'name' => 'Klayili',
            ),

            array (
                'code' => '92.01.41',
                'name' => 'Klaso',
            ),

            array (
                'code' => '92.01.42',
                'name' => 'Moisegen',
            ),

            array (
                'code' => '92.01.43',
                'name' => 'Sorong',
            ),

            array (
                'code' => '92.01.44',
                'name' => 'Bagun',
            ),

            array (
                'code' => '92.01.45',
                'name' => 'Wemak',
            ),

            array (
                'code' => '92.01.46',
                'name' => 'Sunook',
            ),

            array (
                'code' => '92.01.47',
                'name' => 'Buk',
            ),
        ));
        \DB::table('regions')->insert(array (

            array (
                'code' => '92.01.48',
                'name' => 'Saengkeduk',
            ),

            array (
                'code' => '92.01.49',
                'name' => 'Malabotom',
            ),

            array (
                'code' => '92.01.50',
                'name' => 'Konhir',
            ),

            array (
                'code' => '92.01.51',
                'name' => 'Klasafet',
            ),

            array (
                'code' => '92.01.52',
                'name' => 'Hobard',
            ),

            array (
                'code' => '92.01.53',
                'name' => 'Salawati Tengah',
            ),

            array (
                'code' => '92.01.54',
                'name' => 'Botain',
            ),

            array (
                'code' => '92.01.55',
                'name' => 'Sayosa Timur',
            ),

            array (
                'code' => '92.02',
                'name' => 'KAB. MANOKWARI',
            ),

            array (
                'code' => '92.02.03',
                'name' => 'Warmare',
            ),

            array (
                'code' => '92.02.04',
                'name' => 'Prafi',
            ),

            array (
                'code' => '92.02.05',
                'name' => 'Masni',
            ),

            array (
                'code' => '92.02.12',
                'name' => 'Manokwari Barat',
            ),

            array (
                'code' => '92.02.13',
                'name' => 'Manokwari Timur',
            ),

            array (
                'code' => '92.02.14',
                'name' => 'Manokwari Utara',
            ),

            array (
                'code' => '92.02.15',
                'name' => 'Manokwari Selatan',
            ),

            array (
                'code' => '92.02.17',
                'name' => 'Tanah Rubuh',
            ),

            array (
                'code' => '92.02.21',
                'name' => 'Sidey',
            ),

            array (
                'code' => '92.03',
                'name' => 'KAB. FAK FAK',
            ),

            array (
                'code' => '92.03.01',
                'name' => 'Fak-Fak',
            ),

            array (
                'code' => '92.03.02',
                'name' => 'Fak-Fak Barat',
            ),

            array (
                'code' => '92.03.03',
                'name' => 'Fak-Fak Timur',
            ),

            array (
                'code' => '92.03.04',
                'name' => 'Kokas',
            ),

            array (
                'code' => '92.03.05',
                'name' => 'Fak-Fak Tengah',
            ),

            array (
                'code' => '92.03.06',
                'name' => 'Karas',
            ),

            array (
                'code' => '92.03.07',
                'name' => 'Bomberay',
            ),

            array (
                'code' => '92.03.08',
                'name' => 'Kramongmongga',
            ),

            array (
                'code' => '92.03.09',
                'name' => 'Teluk Patipi',
            ),

            array (
                'code' => '92.03.10',
                'name' => 'Pariwari',
            ),

            array (
                'code' => '92.03.11',
                'name' => 'Wartutin',
            ),

            array (
                'code' => '92.03.12',
                'name' => 'Fakfak Timur Tengah',
            ),

            array (
                'code' => '92.03.13',
                'name' => 'Arguni',
            ),

            array (
                'code' => '92.03.14',
                'name' => 'Mbahamdandara',
            ),

            array (
                'code' => '92.03.15',
                'name' => 'Kayauni',
            ),

            array (
                'code' => '92.03.16',
                'name' => 'Furwagi',
            ),

            array (
                'code' => '92.03.17',
                'name' => 'Tomage',
            ),

            array (
                'code' => '92.04',
                'name' => 'KAB. SORONG SELATAN',
            ),

            array (
                'code' => '92.04.01',
                'name' => 'Teminabuan',
            ),

            array (
                'code' => '92.04.04',
                'name' => 'Inanwatan',
            ),

            array (
                'code' => '92.04.06',
                'name' => 'Sawiat',
            ),

            array (
                'code' => '92.04.09',
                'name' => 'Kokoda',
            ),

            array (
                'code' => '92.04.10',
                'name' => 'Moswaren',
            ),

            array (
                'code' => '92.04.11',
                'name' => 'Seremuk',
            ),

            array (
                'code' => '92.04.12',
                'name' => 'Wayer',
            ),

            array (
                'code' => '92.04.14',
                'name' => 'Kais',
            ),

            array (
                'code' => '92.04.15',
                'name' => 'Konda',
            ),

            array (
                'code' => '92.04.20',
                'name' => 'Matemani',
            ),

            array (
                'code' => '92.04.21',
                'name' => 'Kokoda Utara',
            ),

            array (
                'code' => '92.04.22',
                'name' => 'Saifi',
            ),

            array (
                'code' => '92.04.24',
                'name' => 'Fokour',
            ),

            array (
                'code' => '92.04.25',
                'name' => 'Salkma',
            ),

            array (
                'code' => '92.04.26',
                'name' => 'Kais Darat',
            ),

            array (
                'code' => '92.05',
                'name' => 'KAB. RAJA AMPAT',
            ),

            array (
                'code' => '92.05.01',
            'name' => 'Misool (Misool Utara)  5',
            ),

            array (
                'code' => '92.05.02',
                'name' => 'Waigeo Utara',
            ),

            array (
                'code' => '92.05.03',
                'name' => 'Waigeo Selatan',
            ),

            array (
                'code' => '92.05.04',
                'name' => 'Salawati Utara',
            ),

            array (
                'code' => '92.05.05',
                'name' => 'Kepulauan Ayau',
            ),

            array (
                'code' => '92.05.06',
                'name' => 'Misool Timur',
            ),

            array (
                'code' => '92.05.07',
                'name' => 'Waigeo Barat',
            ),

            array (
                'code' => '92.05.08',
                'name' => 'Waigeo Timur',
            ),

            array (
                'code' => '92.05.09',
                'name' => 'Teluk Mayalibit',
            ),

            array (
                'code' => '92.05.10',
                'name' => 'Kofiau',
            ),

            array (
                'code' => '92.05.11',
                'name' => 'Meos Mansar',
            ),

            array (
                'code' => '92.05.13',
                'name' => 'Misool Selatan',
            ),

            array (
                'code' => '92.05.14',
                'name' => 'Warwarbomi',
            ),

            array (
                'code' => '92.05.15',
                'name' => 'Waigeo Barat Kepulauan',
            ),

            array (
                'code' => '92.05.16',
                'name' => 'Misool Barat',
            ),

            array (
                'code' => '92.05.17',
                'name' => 'Kepulauan Sembilan',
            ),

            array (
                'code' => '92.05.18',
                'name' => 'Kota Waisai',
            ),

            array (
                'code' => '92.05.19',
                'name' => 'Tiplol Mayalibit',
            ),

            array (
                'code' => '92.05.20',
                'name' => 'Batanta Utara',
            ),

            array (
                'code' => '92.05.21',
                'name' => 'Salawati Barat',
            ),

            array (
                'code' => '92.05.22',
                'name' => 'Salawati Tengah',
            ),

            array (
                'code' => '92.05.23',
                'name' => 'Supnin',
            ),

            array (
                'code' => '92.05.24',
                'name' => 'Ayau',
            ),

            array (
                'code' => '92.05.25',
                'name' => 'Batanta Selatan',
            ),

            array (
                'code' => '92.06',
                'name' => 'KAB. TELUK BINTUNI',
            ),

            array (
                'code' => '92.06.01',
                'name' => 'Bintuni',
            ),

            array (
                'code' => '92.06.02',
                'name' => 'Merdey',
            ),

            array (
                'code' => '92.06.03',
                'name' => 'Babo',
            ),

            array (
                'code' => '92.06.04',
                'name' => 'Aranday',
            ),

            array (
                'code' => '92.06.05',
                'name' => 'Moskona Selatan',
            ),

            array (
                'code' => '92.06.06',
                'name' => 'Moskona Utara',
            ),

            array (
                'code' => '92.06.07',
                'name' => 'Wamesa',
            ),

            array (
                'code' => '92.06.08',
                'name' => 'Fafurwar',
            ),

            array (
                'code' => '92.06.09',
                'name' => 'Tembuni',
            ),

            array (
                'code' => '92.06.10',
                'name' => 'Kuri',
            ),

            array (
                'code' => '92.06.11',
                'name' => 'Manimeri',
            ),

            array (
                'code' => '92.06.12',
                'name' => 'Tuhiba',
            ),

            array (
                'code' => '92.06.13',
                'name' => 'Dataran Beimes',
            ),

            array (
                'code' => '92.06.14',
                'name' => 'Sumuri',
            ),

            array (
                'code' => '92.06.15',
                'name' => 'Kaitaro',
            ),

            array (
                'code' => '92.06.16',
                'name' => 'Aroba',
            ),

            array (
                'code' => '92.06.17',
                'name' => 'Masyeta',
            ),

            array (
                'code' => '92.06.18',
                'name' => 'Biscoop',
            ),

            array (
                'code' => '92.06.19',
                'name' => 'Tomu',
            ),

            array (
                'code' => '92.06.20',
                'name' => 'Kamundan',
            ),

            array (
                'code' => '92.06.21',
                'name' => 'Weriagar',
            ),

            array (
                'code' => '92.06.22',
                'name' => 'Moskona Barat',
            ),

            array (
                'code' => '92.06.23',
                'name' => 'Meyado',
            ),

            array (
                'code' => '92.06.24',
                'name' => 'Moskona Timur',
            ),

            array (
                'code' => '92.07',
                'name' => 'KAB. TELUK WONDAMA',
            ),

            array (
                'code' => '92.07.01',
                'name' => 'Wasior',
            ),

            array (
                'code' => '92.07.02',
                'name' => 'Windesi',
            ),

            array (
                'code' => '92.07.03',
                'name' => 'Teluk Duairi',
            ),

            array (
                'code' => '92.07.04',
                'name' => 'Wondiboy',
            ),

            array (
                'code' => '92.07.05',
                'name' => 'Wamesa',
            ),

            array (
                'code' => '92.07.06',
                'name' => 'Rumberpon',
            ),

            array (
                'code' => '92.07.07',
                'name' => 'Naikere',
            ),

            array (
                'code' => '92.07.08',
                'name' => 'Rasiei',
            ),

            array (
                'code' => '92.07.09',
                'name' => 'Kuri Wamesa',
            ),

            array (
                'code' => '92.07.10',
                'name' => 'Roon',
            ),

            array (
                'code' => '92.07.11',
                'name' => 'Roswar',
            ),

            array (
                'code' => '92.07.12',
                'name' => 'Nikiwar',
            ),

            array (
                'code' => '92.07.13',
                'name' => 'Soug Jaya',
            ),

            array (
                'code' => '92.08',
                'name' => 'KAB. KAIMANA',
            ),

            array (
                'code' => '92.08.01',
                'name' => 'Kaimana',
            ),

            array (
                'code' => '92.08.02',
                'name' => 'Buruway',
            ),

            array (
                'code' => '92.08.03',
                'name' => 'Teluk Arguni Atas',
            ),

            array (
                'code' => '92.08.04',
                'name' => 'Teluk Etna',
            ),

            array (
                'code' => '92.08.05',
                'name' => 'Kambrau',
            ),

            array (
                'code' => '92.08.06',
                'name' => 'Teluk Arguni Bawah',
            ),

            array (
                'code' => '92.08.07',
                'name' => 'Yamor',
            ),

            array (
                'code' => '92.09',
                'name' => 'KAB. TAMBRAUW',
            ),

            array (
                'code' => '92.09.01',
                'name' => 'Fef',
            ),

            array (
                'code' => '92.09.02',
                'name' => 'Miyah',
            ),

            array (
                'code' => '92.09.03',
                'name' => 'Yembun',
            ),

            array (
                'code' => '92.09.04',
                'name' => 'Kwoor',
            ),

            array (
                'code' => '92.09.05',
                'name' => 'Sausapor',
            ),

            array (
                'code' => '92.09.06',
                'name' => 'Abun',
            ),

            array (
                'code' => '92.09.07',
                'name' => 'Syujak',
            ),

            array (
                'code' => '92.09.08',
                'name' => 'Moraid',
            ),

            array (
                'code' => '92.09.09',
                'name' => 'Kebar',
            ),

            array (
                'code' => '92.09.10',
                'name' => 'Amberbaken',
            ),

            array (
                'code' => '92.09.11',
                'name' => 'Senopi',
            ),

            array (
                'code' => '92.09.12',
                'name' => 'Mubrani',
            ),

            array (
                'code' => '92.09.13',
                'name' => 'Bikar',
            ),

            array (
                'code' => '92.09.14',
                'name' => 'Bamusbama',
            ),

            array (
                'code' => '92.09.15',
                'name' => 'Ases',
            ),

            array (
                'code' => '92.09.16',
                'name' => 'Miyah Selatan',
            ),

            array (
                'code' => '92.09.17',
                'name' => 'Ireres',
            ),

            array (
                'code' => '92.09.18',
                'name' => 'Tobouw',
            ),

            array (
                'code' => '92.09.19',
                'name' => 'Wilhem Roumbouts',
            ),

            array (
                'code' => '92.09.20',
                'name' => 'Tinggouw',
            ),

            array (
                'code' => '92.09.21',
                'name' => 'Kwesefo',
            ),

            array (
                'code' => '92.09.22',
                'name' => 'Mawabuan',
            ),

            array (
                'code' => '92.09.23',
                'name' => 'Kebar Timur',
            ),

            array (
                'code' => '92.09.24',
                'name' => 'Kebar Selatan',
            ),

            array (
                'code' => '92.09.25',
                'name' => 'Manekar',
            ),

            array (
                'code' => '92.09.26',
                'name' => 'Mpur',
            ),

            array (
                'code' => '92.09.27',
                'name' => 'Amberbaken Barat',
            ),

            array (
                'code' => '92.09.28',
                'name' => 'Kasi',
            ),

            array (
                'code' => '92.09.29',
                'name' => 'Selemkai',
            ),

            array (
                'code' => '92.10',
                'name' => 'KAB. MAYBRAT',
            ),

            array (
                'code' => '92.10.01',
                'name' => 'Aifat',
            ),

            array (
                'code' => '92.10.02',
                'name' => 'Aifat Utara',
            ),

            array (
                'code' => '92.10.03',
                'name' => 'Aifat Timur',
            ),

            array (
                'code' => '92.10.04',
                'name' => 'Aifat Selatan',
            ),

            array (
                'code' => '92.10.05',
                'name' => 'Aitinyo Barat',
            ),

            array (
                'code' => '92.10.06',
                'name' => 'Aitinyo',
            ),

            array (
                'code' => '92.10.07',
                'name' => 'Aitinyo Utara',
            ),

            array (
                'code' => '92.10.08',
                'name' => 'Ayamaru',
            ),

            array (
                'code' => '92.10.09',
                'name' => 'Ayamaru Utara',
            ),

            array (
                'code' => '92.10.10',
                'name' => 'Ayamaru Timur',
            ),

            array (
                'code' => '92.10.11',
                'name' => 'Mare',
            ),

            array (
                'code' => '92.10.12',
                'name' => 'Aifat Timur Tengah',
            ),

            array (
                'code' => '92.10.13',
                'name' => 'Aifat Timur Jauh',
            ),

            array (
                'code' => '92.10.14',
                'name' => 'Aifat Timur Selatan',
            ),

            array (
                'code' => '92.10.15',
                'name' => 'Ayamaru Selatan',
            ),

            array (
                'code' => '92.10.16',
                'name' => 'Ayamaru Jaya',
            ),

            array (
                'code' => '92.10.17',
                'name' => 'Ayamaru Selatan Jaya',
            ),

            array (
                'code' => '92.10.18',
                'name' => 'Ayamaru Timur Selatan',
            ),

            array (
                'code' => '92.10.19',
                'name' => 'Ayamaru Utara Timur',
            ),

            array (
                'code' => '92.10.20',
                'name' => 'Ayamaru Tengah',
            ),

            array (
                'code' => '92.10.21',
                'name' => 'Ayamaru Barat',
            ),

            array (
                'code' => '92.10.22',
                'name' => 'Aitinyo Tengah',
            ),

            array (
                'code' => '92.10.23',
                'name' => 'Aitinyo Raya',
            ),

            array (
                'code' => '92.10.24',
                'name' => 'Mare Selatan',
            ),

            array (
                'code' => '92.11',
                'name' => 'KAB. MANOKWARI SELATAN',
            ),

            array (
                'code' => '92.11.01',
                'name' => 'Ransiki',
            ),

            array (
                'code' => '92.11.02',
                'name' => 'Oransbari',
            ),

            array (
                'code' => '92.11.03',
                'name' => 'Neney',
            ),

            array (
                'code' => '92.11.04',
                'name' => 'Dataran Isim',
            ),

            array (
                'code' => '92.11.05',
                'name' => 'Momi Waren',
            ),

            array (
                'code' => '92.11.06',
                'name' => 'Tahota',
            ),

            array (
                'code' => '92.12',
                'name' => 'KAB. PEGUNUNGAN ARFAK',
            ),

            array (
                'code' => '92.12.01',
                'name' => 'Anggi',
            ),

            array (
                'code' => '92.12.02',
                'name' => 'Anggi Gida',
            ),

            array (
                'code' => '92.12.03',
                'name' => 'Membey',
            ),

            array (
                'code' => '92.12.04',
                'name' => 'Sururey',
            ),

            array (
                'code' => '92.12.05',
                'name' => 'Didohu',
            ),

            array (
                'code' => '92.12.06',
                'name' => 'Taige',
            ),

            array (
                'code' => '92.12.07',
                'name' => 'Catubouw',
            ),

            array (
                'code' => '92.12.08',
                'name' => 'Testega',
            ),

            array (
                'code' => '92.12.09',
                'name' => 'Minyambaouw',
            ),

            array (
                'code' => '92.12.10',
                'name' => 'Hingk',
            ),

            array (
                'code' => '92.71',
                'name' => 'KOTA SORONG',
            ),

            array (
                'code' => '92.71.01',
                'name' => 'Sorong',
            ),

            array (
                'code' => '92.71.02',
                'name' => 'Sorong Timur',
            ),

            array (
                'code' => '92.71.03',
                'name' => 'Sorong Barat',
            ),

            array (
                'code' => '92.71.04',
                'name' => 'Sorong Kepulauan',
            ),

            array (
                'code' => '92.71.05',
                'name' => 'Sorong Utara',
            ),

            array (
                'code' => '92.71.06',
                'name' => 'Sorong Manoi',
            ),

            array (
                'code' => '92.71.07',
                'name' => 'Sorong Kota',
            ),

            array (
                'code' => '92.71.08',
                'name' => 'Klaurung',
            ),

            array (
                'code' => '92.71.09',
                'name' => 'Malaimsimsa',
            ),

            array (
                'code' => '92.71.10',
                'name' => 'Maladum Mes',
            ),
        ));


    }

}
