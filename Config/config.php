<?php

return [
    'name' => 'Common',
    'menu' => [
        'admin' => [
            'setting' => [
                'position' => 5,
                'route' => 'admin.setting.index',
                'icon' => 'setting',
                'label' => 'Pengaturan',
                'children' => [
                    [
                        'route' => 'admin.setting.index',
                        'label' => 'Pengaturan',
                    ],
                    [
                        'route' => 'admin.setting.modules',
                        'label' => 'Modul',
                    ],
                    [
                        'route' => 'admin.setting.choices',
                        'label' => 'Data Pilihan',
                    ],
                ],
            ],
        ],
    ],
    'settings' => [
        'general' => [
            'items' => [
                [
                    'name' => 'common_teks',
                    'label' => 'Teks',
                    'type' => 'text',
                    'default' => 'Text setting',
                    'group' => 'test'
                ],
                [
                    'name' => 'common_check',
                    'label' => 'Multiple Choice',
                    'type' => 'check',
                    'default' => [],
                    'options' => [
                        'check_1' => 'Cek 1',
                        'check_2' => 'Cek 2',
                        'check_3' => 'Cek 3',
                    ],
                    'group' => 'test'
                ],
                [
                    'name' => 'common_option',
                    'label' => 'Pilihan',
                    'type' => 'option',
                    'default' => 'option_1',
                    'options' => [
                        'option_1' => 'Pilihan 1',
                        'option_2' => 'Pilihan 2',
                        'option_3' => 'Pilihan 3',
                    ],
                    'group' => 'test'
                ],
                [
                    'name' => 'common_toggle',
                    'label' => 'Toggle',
                    'type' => 'toggle',
                    'default' => false,
                    'group' => 'test_2'
                ],
            ]
        ]
    ]
];
