@section('page-title', 'Modul')

<div x-data="{
        input: {},
        selectedModule: null,
    }"
>
    <x-ui::layout.list-with-detail
        title="Modul"
        selected-var="selectedModule"
        detail-title="Detil Modul"
        actions-visible="!input.is_core"
    >
        <x-slot name="list">
            <x-ui::select-list.generic
                data-hub="Common::GetAllModules"
                data-key="name"
                x-model="selectedModule"
                x-on:change="input = $event.detail ?? {}"
                height="calc(100vh - 140px)"
            >
                <div class="flex">
                    <div class="flex items-center mr-2">
                        <div class="w-3 h-3 bg-gray-200 rounded-full" aria-hidden="true"
                            x-bind:class="{
                                '!bg-blue-500': item.data.is_core,
                                '!bg-green-500': item.data.enabled && !item.data.is_core,
                            }"
                        ></div>
                    </div>
                    <div x-text="item.data.label" class="px-1"></div>
                </div>
            </x-ui::select-list.generic>
        </x-slot>

        <x-slot name="detail">
            <dl>
                <div class="px-4 py-5 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        Nama Modul
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <span x-text="input.name"></span>
                    </dd>
                </div>
                <div class="px-4 py-5 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        Deskripsi
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <span x-text="input.description"></span>
                    </dd>
                </div>
                <div class="px-4 py-5 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        Developer
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <template x-for="author in input.authors" hidden>
                            <p x-text="`${author.name} <${author.email}>`"></p>
                        </template>
                    </dd>
                </div>
            </dl>

            <pre x-json="input"></pre>

        </x-slot>

        <x-slot name="detailActions">
            <x-ui::button
                x-text="(input.enabled ? 'Non-aktifkan' : 'Aktifkan') + ' Modul'"
                x-bind:class="{'text-red-600': input.enabled, 'text-green-600': !input.enabled}"
                x-on:click="async () => {
                    const actionLabel = input.enabled ? 'Non-aktifkan' : 'Aktifkan'

                    if ( ! await openConfirm(actionLabel + ' modul ini?') ) return

                    $wire.toggle({
                        action: input.enabled ? 'disable' : 'enable',
                        module: input.name
                    }).then((resp) => {
                        if (resp.success) location.reload()
                    })
                }"
            ></x-ui::button>
        </x-slot>
    </x-ui::layout.list-with-detail>

</div>
