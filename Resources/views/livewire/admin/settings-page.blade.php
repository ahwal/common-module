@section('page-title', 'Pengaturan')

<x-ui::layout.multi-list-with-detail
    selected-var="currentTab"
    x-data="dataPage"
    wire:ignore
>
    <x-slot name="title">
        <h1 x-text="pageTitle"></h1>
    </x-slot>

    <x-slot name="afterTitle"></x-slot>

    <x-slot name="detailTitle">
        <h1 x-text="pageTitle"></h1>
    </x-slot>

    <x-slot name="group">
        <div class="md:border">
            <x-ui::list.vertical-tab-nav
                x-model="currentTab"
                x-bind:data-items="JSON.stringify(settingsTabs)"
                x-on:change="
                    input = settingsData[currentTab] ? settingsData[currentTab].values : {};
                    currentGroup = 'all';
                "
            ></x-ui::list.vertical-tab-nav>
        </div>
    </x-slot>

    <x-slot name="list" border="1">
        <x-ui::list.vertical-tab-nav
            x-model="currentGroup"
            x-bind:data-items="JSON.stringify(settingsGroups)"
            x-init="$watch('currentTab', () => reloadItems($root))"
        ></x-ui::list.vertical-tab-nav>
    </x-slot>

    <x-slot name="detail">
        <template x-if="currentTab != null" hidden>
            <div class="px-4 bg-white">

                <h1 class="mb-4 text-lg font-medium leading-6 text-gray-900" x-text="settingsGroups[currentGroup]"></h1>

                {{-- <pre x-json="input"></pre> --}}

                <form x-on:submit.prevent="$wire.store(input).then(resp => settingsData = resp)" class="space-y-5">
                    <button type="submit" x-ref="submit" hidden></button>

                    <template x-for="item in settingsData[currentTab].items" :key="item.name">
                        <div class="text-sm sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5" x-show="currentGroup == 'all' || currentGroup == item.group">
                            <div class="font-medium text-gray-700 sm:mt-px sm:pt-1">
                                <span x-text="item.label || item.name"></span>
                            </div>
                            <div class="mt-1 sm:mt-0 sm:col-span-2">
                                <x-ui::x-field data-var="item" data-model="input[item.name]" />
                            </div>
                            {{-- <pre x-json="item"></pre> --}}
                        </div>
                    </template>

                </form>

            </div>
        </template>
    </x-slot> <!-- name=detail -->

    <x-slot name="detailActions">
        <x-ui::button.primary x-on:click="$refs.submit.click()">Simpan</x-ui::button.primary>
    </x-slot>
</x-ui::layout.multi-list-with-detail>

@push('scripts')
<script>
    function dataPage() {
        return {
            selectedData: null,
            input: null,
            currentTab: null,
            currentGroup: 'all',
            init() {
                this.currentTab = Object.keys(this.settingsData)[0];
                this.input = this.settingsData[this.currentTab].values
            },

            settingsData: @json($settings),

            get settingsTabs() {
                return _.mapValues(this.settingsData, (s) => s.label);
            },
            get settingsGroups() {
                if (!this.currentTab) return;

                return this.settingsData[this.currentTab].groups;
            },

            get pageTitle() {
                if (!this.currentTab) return 'Pengaturan';

                return 'Pengaturan ' + this.settingsData[this.currentTab].label
            }
        }
    }
</script>
@endpush
