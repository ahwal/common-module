@section('page-title', 'Data Pilihan')

<x-ui::layout.multi-list-with-detail
    selected-var="selectedData"
    x-data="dataPage"
>
    <x-slot name="title">
        <h1 x-text="currentTab"></h1>
    </x-slot>

    <x-slot name="afterTitle">
        <x-ui::button.secondary
            x-on:click="newData"
            class="!ml-auto sm:!ml-4"
        >Baru</x-ui::button.secondary>
    </x-slot>

    <x-slot name="detailTitle">
        <span x-text="selectedData == 'new' ? currentTab + ' Baru' : 'Detil ' + currentTab"></span>
    </x-slot>

    <x-slot name="group">
        <div class="md:border">
            <x-ui::list.vertical-tab-nav
                x-model="currentTab"
                x-on:change="selectedData = null; refreshData()"
                x-init="async () => {
                    const data = (await dataHub('Common::GetChoiceGroups'))
                        .reduce((obj, item) => ({...obj, [item]: { label: item }}), {})
                    setItems(data)
                }"
            />
        </div>
    </x-slot>

    <x-slot name="list">
        <x-ui::select-list.generic
            id="select-data"
            data-hub="Common::GetPaginatedChoices"
            data-filter="{group: 'staff_status'}"
            x-model="selectedData"
            x-on:change="onChangeData($event.detail)"
            height="calc(100vh - 140px)"
        >
            <div x-text="item.data.name"></div>
        </x-ui::select-list.generic>
    </x-slot>

    <x-slot name="detail">
        <div class="px-4 bg-white">

            <form x-on:submit.prevent="storeData">
                <button type="submit" x-ref="submit" hidden></button>

                <div class="space-y-6 sm:space-y-5">
                    <x-ui::field-group label="Label">
                        <x-ui::field-group.input
                            class="w-full max-w-lg sm:max-w-xs"
                            x-model="input.name"
                            x-ref="firstInput"
                            required
                        />
                    </x-ui::field-group>
                    <x-ui::field-group label="Kode">
                        <x-ui::field-group.input
                            class="w-full max-w-lg sm:max-w-xs"
                            x-model="input.key"
                            x-on:keyup="manualKeyInput = input.key != ''"
                            required
                        />
                    </x-ui::field-group>
                </div>

            </form>

        </div>
    </x-slot> <!-- name=detail -->

    <x-slot name="detailActions">
        <x-ui::button.primary x-on:click="$refs.submit.click()">Simpan</x-ui::button.primary>

        <x-ui::button.secondary
            x-show="input.id"
            x-on:click="deleteData(input.id)"
            class="!border-red-500 !text-red-500 hover:bg-red-100 ml-auto"
        >Hapus</x-ui::button.secondary>
    </x-slot>
</x-ui::layout.multi-list-with-detail>

@push('scripts')
<script>
    function dataPage() {
        return {
            selectedData: null,
            selectedYear: null,
            selectedStage: null,

            defaultInput: {},
            input: {},
            errors: {},
            currentTab: 'staff_status',

            manualKeyInput: false,

            async init() {
                this.input = _.clone(this.defaultInput)

                // auto generate key if not changed manually
                this.$watch('input.name', val => {
                    if (this.manualKeyInput || !val) return
                    this.input.key = val.toLowerCase().replace(/ /g, '-')
                })
            },

            refreshData() {
                xData('#select-data')
                    .applyFilters({group: this.currentTab})
                    .refreshData()
            },

            newData() {
                this.selectedData = 'new'
                this.input = _.clone(this.defaultInput)
                this.input.group = this.currentTab

                this.manualKeyInput = false

                setTimeout(() => this.$nextTick(() => this.$refs.firstInput.focus()), 100)
            },

            onChangeData(data) {
                this.input = data ? this.prepareForEdit(data) : _.clone(this.defaultInput)
            },

            prepareForEdit(input) {
                if (typeof input != 'object') return input

                errors = {}

                // {{-- delete uneditable attributes --}}
                delete input.created_at
                delete input.updated_at

                return input
            },
            async storeData() {
                const resp = await this.$wire.store(this.input);
                if (resp && resp.error) {
                    console.log('error', resp);
                }
                xData('#select-data').refreshData();

                this.selectedData = null;
                this.input = {};
            },
            async deleteData(id) {
                const confirmed = await openConfirm('Mau menghapus data pilihan: ' + this.input.name + '?');

                if (!confirmed) return;

                await this.$wire.delete(id);
                xData('#select-data').refreshData();
            },
            getError(fieldName) {
                if (this.errors[fieldName] && Array.isArray(this.errors[fieldName])) {
                    return this.errors[fieldName][0]
                }

                return null
            },
        }
    }
</script>
@endpush
