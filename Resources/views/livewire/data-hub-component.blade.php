<div class="hidden">
    <script>
    (function(){
        const useCache = false;

        // make sure getData is called one by one
        window.dataHubReady = true;

        window.dataHub = async function(...args) {
            if (window.livewire) {
                return new Promise((resolve, reject) => {
                    waitFor('dataHubReady', async function() {
                        window.dataHubReady = false;
                        resolve( await cachedOrRequest(...args) )
                        window.dataHubReady = true
                    })
                })
            } else {
                console.log('livewire not ready')
                // wait for livewire to be ready
                return new Promise((resolve, reject) => {
                    waitFor('dataHubReady', async function() {
                        window.dataHubReady = false;
                        waitFor('livewire', async function() {
                            resolve( await cachedOrRequest(...args) )
                            window.dataHubReady = true;
                        })
                    })
                })
            }
        }

        window.dataHubDeleteCache = function(prefix) {
            Object.keys(localStorage).filter(k => k.match(new RegExp('^' + prefix))).map(k => delete localStorage[k])
        }

        async function cachedOrRequest(...args) {
            const key = args.shift()
            const hash = hashCode(key + JSON.stringify(args))

            const localData = useCache ? localStorage.getItem(key + hash) : null

            if (localData) {
                return JSON.parse(localData)
            } else {
                const data = await window.livewire.components.componentsById['{{ $this->id }}'].$wire.getData(key,...args)

                if (!data) {
                    return data;
                }

                if (useCache) {
                    // remove existing cache
                    dataHubDeleteCache(key)

                    localStorage.setItem(key + hash, JSON.stringify(data))
                }

                return data
            }
        }

        function hashCode(str) {
           return str.split('').reduce((prevHash, currVal) =>
                (((prevHash << 5) - prevHash) + currVal.charCodeAt(0))|0, 0);
        }

        function waitFor(variable, callback) {
            let interval = setInterval(function() {
                if (window[variable]) {
                    clearInterval(interval);
                    callback();
                }
            }, 50);
        }
    })()
    </script>
</div>
