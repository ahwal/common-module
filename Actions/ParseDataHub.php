<?php

namespace Modules\Common\Actions;

class ParseDataHub
{
    public static function run($key, ...$params) {
        try {
            [$module, $action] = explode('::', $key);
        } catch (\Throwable $th) {
            [$module, $action] = [null, $key];
        }

        $className = "Modules\\$module\\Actions\\DataHub\\$action";

        if (class_exists($className)) {
            $data = $className::run(...$params);

            if (is_array($data)) {
                return $data;
            }

            if (is_callable([$data, 'toArray'])) {
                return $data->toArray();
            }

            return ['error' => 'DataHub response must be an array or arrayable object.'];
        }

        return ['error' => "DataHub $key not exists."];
    }
}
