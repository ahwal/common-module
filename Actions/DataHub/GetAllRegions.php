<?php
namespace Modules\Common\Actions\DataHub;

use Modules\Common\Models\Region;

class GetAllRegions
{
    public static function run()
    {
        return Region::all()->toArray();
    }
}
