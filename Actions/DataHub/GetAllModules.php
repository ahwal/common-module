<?php
namespace Modules\Common\Actions\DataHub;

use Illuminate\Pagination\LengthAwarePaginator;

class GetAllModules
{
    public static function run()
    {
        $modules = collect(app('modules')->all())
            ->transform(function ($module) {
                /** @var \Nwidart\Modules\Module $module */
                return [
                    'name' => $module->getName(),
                    'label' => $module->get('label') ?? $module->getName(),
                    'description' => $module->getDescription(),
                    'authors' => $module->getComposerAttr('authors', []),
                    'enabled' => $module->isEnabled(),
                    'priority' => $module->get('priority'),
                    'path' => $module->getPath(),
                    'is_core' => in_array($module->getName(), config('app.core_modules', ['Common', 'UI', 'User'])),
                    'requires' => $module->getRequires(),
                ];
            })->sortBy([
                ['is_core', 'desc'],
                ['name', 'asc'],
            ]);

        return (new LengthAwarePaginator($modules, $modules->count(), -1))
            ->toArray();
    }
}
