<?php
namespace Modules\Common\Actions\DataHub;

use Modules\Common\Models\Choice;

class GetChoiceGroups
{
    public static function run($params = []) {
        return Choice::select('group')->distinct()->get()->pluck('group')->toArray();
    }
}
