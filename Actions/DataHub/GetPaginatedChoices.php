<?php
namespace Modules\Common\Actions\DataHub;

use Modules\Common\Models\Choice;

class GetPaginatedChoices
{
    public static function run($params = []) {
        $page = $params['page'] ?? 1;

        $query = Choice::query();

        if (isset($params['filter'])) {
            $query->filter($params['filter']);
        }

        return $query->orderByDesc('created_at')
            ->paginate(30, ['*'], 'page', $page)->toArray();
    }
}
