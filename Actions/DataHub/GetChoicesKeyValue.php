<?php
namespace Modules\Common\Actions\DataHub;

use Modules\Common\Models\Choice;

class GetChoicesKeyValue
{
    public static function run($params = []) {
        if (!isset($params['for'])) {
            return [];
        }

        return Choice::for($params['for'])->get(['key', 'name'])
            ->map(function ($item) {
                return [
                    'id' => $item['key'],
                    'name' => $item['name'],
                ];
            })->toArray();
    }
}
