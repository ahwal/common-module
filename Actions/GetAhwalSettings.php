<?php

namespace Modules\Common\Actions;

use Modules\Common\Models\Setting;

class GetAhwalSettings
{
    public static function run()
    {
        $values = Setting::allValues();

        $settings = collect(GetAhwalConfigs::run('settings', []))
            ->map(function ($value, $key) use ($values) {
                $items = collect($value['items']);

                return [
                    'label' => $value['label'] ?? str($key)->headline()->toString(),
                    'items' => $value['items'],
                    'values' => $items
                        ->pluck('default', 'name')
                        ->map(fn ($v, $k) => $values[$k] ?? $v)
                        ->toArray(),
                    'groups' => ['all' => 'Semua'] + $items
                        ->unique('group')
                        ->mapWithKeys(fn ($v) => isset($v['group']) ? [
                            $v['group'] => str($v['group'])->headline()->toString(),
                        ] : [])
                        ->toArray(),
                ];
            })
            ->toArray();

        return $settings;
    }
}
