<?php

namespace Modules\Common\Actions;

class GetAhwalConfigs
{
    public static function run($key, $default = null) {
        $configs = config('ahwal.' . $key, $default);

        collect(app('modules')->getByStatus(1))
            ->each(function($module) use (&$configs, $key) {
                /** @var \Nwidart\Modules\Module $module */
                if ($module_items = config($module->getLowerName() . '.' . $key)) {
                    $configs = array_merge_recursive($configs, $module_items);
                }
            });

        return $configs;
    }
}
