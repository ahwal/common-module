<?php

namespace Modules\Common\Http\Livewire;

use Livewire\Component;
use Modules\Common\Actions\ParseDataHub;

class DataHubComponent extends Component
{
    // used for validation
    public $input;

    public function render()
    {
        return view('common::livewire.data-hub-component');
    }

    public function getData($key, ...$params)
    {
        return ParseDataHub::run($key, ...$params);
    }
}
