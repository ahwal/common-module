<?php

namespace Modules\Common\Http\Livewire\Admin;

use Livewire\Component;
use Modules\Common\Models\Choice;

class ChoicesPage extends Component
{
    public function render()
    {
        return view('common::livewire.admin.choices-page');
    }

    public function store($data)
    {
        if (isset($data['id']) && Choice::find($data['id'])) {
            return $this->update($data);
        }

        $dataNew = Choice::create($data);

        return $dataNew->toArray();
    }

    private function update($data)
    {
        return Choice::find($data['id'])->update($data);
    }

    public function delete(Choice $data)
    {
        return $data->delete();
    }
}
