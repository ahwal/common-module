<?php

namespace Modules\Common\Http\Livewire\Admin;

use Livewire\Component;
use Modules\Common\Actions\GetAhwalSettings;
use Modules\Common\Models\Setting;

class SettingsPage extends Component
{
    public function render()
    {
        return view('common::livewire.admin.settings-page', [
            'settings' => GetAhwalSettings::run(),
        ]);
    }

    public function store($data)
    {
        // dd($data);
        collect($data)->each(function ($value, $name) {
            Setting::put($name, $value);
        });
        $this->notify('Pengaturan berhasil disimpan');

        return GetAhwalSettings::run();
    }
}
