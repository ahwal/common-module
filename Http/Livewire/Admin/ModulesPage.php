<?php

namespace Modules\Common\Http\Livewire\Admin;

use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Livewire\Component;

class ModulesPage extends Component
{
    public $input = null;

    public function render()
    {
        return view('common::livewire.admin.modules-page');
    }

    public function toggle($request)
    {
        $this->input = $request;

        try {
            $this->validate([
                'input.action' => ['required', 'in:enable,disable'],
                'input.module' => [
                    'required',
                    'string',
                    Rule::notIn(config('app.core_modules', ['Common', 'UI', 'User']))
                ],
            ]);
        } catch (ValidationException $e) {
            $this->notify('Modul tidak bisa di-nonaktifkan');
            return ['success' => false];
        }

        $this->input = null;

        $name = $request['module'];

        /** @var \Nwidart\Modules\Module $module */
        $module = app('modules')->findOrFail($name);

        switch ($request['action']) {
            case 'enable':
                // check dependencies
                $deps = $module->getRequires();
                foreach ($deps as $dep) {
                    /** @var \Nwidart\Modules\Module $m */
                    $m = app('modules')->find($dep);
                    if (!$m || $m->isDisabled()) {
                        $this->notify(sprintf('Unable to enable <strong>%s</strong> module. <strong>%s</strong> module is required.', $name, $dep));
                        return ['success' => false];
                    }
                }

                if ($module->isDisabled()) $module->enable();
                break;
            case 'disable':
                // check dependencies
                $modules = app('modules')->getByStatus(1);
                /** @var \Nwidart\Modules\Module $m */
                foreach ($modules as $m) {
                    $deps = $m->getRequires();
                    if (in_array($name, $deps)) {
                        $this->notify(sprintf('Unable to disable <strong>%s</strong> module. Required by <strong>%s</strong> module.', $name, $m->getName()));
                        return ['success' => false];
                    }
                }

                if ($module->isEnabled()) $module->disable();
                break;
        }

        return ['success' => true];
    }
}
