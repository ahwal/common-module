<?php

namespace Modules\Common\Traits;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;

trait HasMetaColumnTrait
{
    /**
     * Initialize the trait for an instance.
     *
     * @return void
     */
    public function initializeHasMetaColumnTrait()
    {
        if (!isset($this->casts['meta'])) {
            $this->casts['meta'] = AsArrayObject::class;
        }

        if (!in_array('meta', $this->fillable)) {
            $this->fillable[] = 'meta';
        }

        // set default value
        if (!$this->meta) {
            $this->meta = [];
        }
    }

    public function getMeta($key)
    {
        return $this->meta[$key] ?? null;
    }

    public function updateMeta($key, $value)
    {
        return tap($this, function () use ($key, $value) {
            $this->meta[$key] = $value;
            $this->save();
        });
    }

    public function deleteMeta($key)
    {
        return tap($this, function () use ($key) {
            if (!isset($this->meta[$key])) {
                return;
            }

            unset($this->meta[$key]);

            $this->save();
        });
    }
}
