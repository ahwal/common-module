<?php

namespace Modules\Common\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    use HasFactory;

    protected $fillable = ['group', 'key', 'name', 'sort', 'is_active'];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    public function scopeFor(Builder $query, $group)
    {
        $query->where('group', $group);
    }

    public function scopeFilter(Builder $query, $filter)
    {
        $query->when($filter['group'] ?? null, function($query, $group) {
            $query->where('group', $group);
        });
    }
}
