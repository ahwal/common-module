<?php

namespace Modules\Common\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Common\Casts\Json;

class Setting extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'value'];

    protected $casts = [
        'value' => Json::class,
    ];

    public static function allValues()
    {
        return self::all()->pluck('value', 'name')->toArray();
    }

    public static function get($name, $default = null)
    {
        return optional(self::where('name', $name))->value('value') ?? $default;
    }

    public static function put($name, $value)
    {
        return self::updateOrCreate(
            ['name' => $name],
            ['value' => $value],
        );
    }
}
