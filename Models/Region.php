<?php

namespace Modules\Common\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public function getLongNameAttribute()
    {
        $long_name = $this->name;

        // kecamatan
        if (strlen($this->code) > 5) {
            $codes = explode('.', $this->code);
            $regions = self::whereIn('code', [$codes[0], "{$codes[0]}.{$codes[1]}"])
                ->orderBy('code', 'desc')->get();

            $long_name = 'Kec. ' . $long_name;

            foreach ($regions as $region) {
                $long_name .= ', ' . $region->name;
            }
        }

        return $long_name;
    }
}
