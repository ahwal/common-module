<?php

use Modules\Common\Actions\ParseDataHub;
use Modules\Common\Models\Setting;

function dataHub($key, ...$params) {
    return ParseDataHub::run($key, ...$params);
}

function ahwal_setting($name, $default = null) {
    return Setting::get($name, $default);
}

function ahwal_setting_update($name, $value) {
    return Setting::put($name, $value);
}
